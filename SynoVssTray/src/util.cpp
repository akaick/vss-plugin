#include "mainwindow.h"
#include "util.h"

void showMsgBox(const QString& text, const QString& title)
{
    QMessageBox msgBox;
    
    msgBox.setWindowTitle(title);
    msgBox.setWindowIcon(QIcon(TRAY_ICON_IMG));
    msgBox.setText(text);
    msgBox.exec();
}