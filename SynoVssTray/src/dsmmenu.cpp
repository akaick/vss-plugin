#include "dsmmenu.h"
#include "socketipc.h"

DsmMenu::DsmMenu(const QString& title, QWidget* parent) :
    QMenu(title, parent)
{
}

void DsmMenu::onEdit()
{
    emit showEditDialog(dsmInfo);
}

// Should still remove the registered DSM even when it is offline
void DsmMenu::onRemove()
{
    int timePast = 0;

    if (!removeDsm(dsmInfo.dsmSn)) {
        // Error condition
        return;
    }
    // Add this part back after the command is completely implemented
    while (DSM_ST_REMOVED != isDsmConnected(dsmInfo.dsmSn, "")) {
        Sleep(DEFAULT_SLEEP_TIME);
        timePast += DEFAULT_SLEEP_TIME;
        if (DEFAULT_TIMEOUT <= timePast) {
            break;
        }
    }

    // Plugin service will remove regisry key with selected DSM SN, so we should reload registries here
    emit refreshDsmList();
}

void DsmMenu::setDsmInfo(const DsmInfo& info)
{
    dsmInfo.dsmSn = info.dsmSn;
    dsmInfo.dsmIp = info.dsmIp;
    dsmInfo.user = info.user;
    dsmInfo.key = info.key;
}