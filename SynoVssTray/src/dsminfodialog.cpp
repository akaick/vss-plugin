#include "dsminfodialog.h"
#include "ui_dsminfodialog.h"
#include "util.h"
#include "sockcmdstructs.h"

DsmInfoDialog::DsmInfoDialog(QWidget* parent) :
    QDialog(parent),
    ui(new Ui::DsmInfoDialog)
{
    QFont f(DSM_FONT);
    setFont(f);
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
    ui->setupUi(this);
    // Set max length of IPv6
    ui->ipEdit->setMaxLength(MAX_IP_LEN);
    // Set max length of user name according to SYNO_USERNAME_UTF32_MAX defined in synosdk
    ui->userEdit->setMaxLength(MAX_USERNAME_LEN);
    // Set max length of password according to synosdk SYNO_USER_PLAIN_PWD_UTF32_MAX defined in synosdk
    ui->pwdEdit->setMaxLength(MAX_PWD_LEN);
    ui->pwdEdit->setEchoMode(QLineEdit::Password);
}

DsmInfoDialog::~DsmInfoDialog()
{
    delete ui;
}

void DsmInfoDialog::closeEvent(QCloseEvent* event)
{
    hide();
    event->ignore();
}

void DsmInfoDialog::setDesc(const QString dialogDesc)
{
    ui->descLabel->setText(dialogDesc);
    ui->descLabel->setWordWrap(true);
    ui->descLabel->setAlignment(Qt::AlignTop);
}

void DsmInfoDialog::setDsmInfo(DsmInfo info)
{
    ui->ipEdit->setText(info.dsmIp);
    ui->userEdit->setText(info.user);
    ui->pwdEdit->setText("****");
}

void DsmInfoDialog::getDialogData(QString& ip, QString& user, QString& password)
{
    ip = ui->ipEdit->text();
    user = ui->userEdit->text();
    password = ui->pwdEdit->text();
}

bool DsmInfoDialog::isModified()
{
    return ui->ipEdit->isModified() || ui->userEdit->isModified() || ui->pwdEdit->isModified();
}

bool DsmInfoDialog::isValidInput()
{
    bool ret = true;
    QString text(tr("invalid_dsm_dialog_fields"));
    QString ip = ui->ipEdit->text();
    QString user = ui->userEdit->text();
    QString password = ui->pwdEdit->text();
    QStringList invalidFields;

    // Check user account, check DSM vtype: username
    if (user.isEmpty()) {
        invalidFields.append(tr("user"));
        ret = false;
    }

    // Check password, check DSM vtype: password
    
    for (int i = 0; i < invalidFields.size(); i++) {
        text += "\n";
        text += invalidFields.at(i);
    }
    if (!ret) {
        showMsgBox(text, this->windowTitle());
    }

    return ret;
}

void DsmInfoDialog::on_applyBtn_clicked()
{
    if (!isValidInput()) {
        return;
    }
    setResult(Accepted);
    hide();
}

void DsmInfoDialog::on_cancelBtn_clicked()
{
    hide();
}
