#include "socketipc.h"
#include "util.h"

SocketTask::SocketTask()
{
    connectSocket = INVALID_SOCKET;
    status = NOT_INITED;
}

SocketTask::~SocketTask()
{
    // Release the Winsock DLL resources
    if (WSADATA_INITED <= status) {
        WSACleanup();
    }

    if (SOCKET_INITED <= status) {
        closesocket(connectSocket);
    }
}

void SocketTask::fillAddrInfo(addrinfo& hints)
{
    ZeroMemory(&hints, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;
}

bool SocketTask::connectWithService()
{
    bool ret = false;
    int iResult = 1;
    WSADATA wsaData;
    struct addrinfo *result = NULL, *ptr = NULL, hints;

    if (NOT_INITED != status) {
        // connectWithService and WSAStartup already have been called
        goto END;
    }

    // http://msdn.microsoft.com/en-us/library/windows/desktop/ms742213(v=vs.85).aspx
    // Make sure WSAStartup is only called once
    if (0 != (iResult = WSAStartup(MAKEWORD(2,2), &wsaData))) {
        // WSAStartup failed with error: iResult
        goto END;
    }

    // Update status, WSACleanup required after this stage
    status = WSADATA_INITED;

    fillAddrInfo(hints);
    if (0 != (iResult = getaddrinfo(NULL, DEFAULT_PORT, &hints, &result))) {
        // getaddrinfo() failed with error: iResult
        goto END;
    }

    // Attempt to connect to an address until one succeeds
    for(ptr = result; ptr != NULL; ptr=ptr->ai_next) {
        // Create a SOCKET for connecting to server
        if (INVALID_SOCKET == (connectSocket = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol))) {
            // Socket failed with error: %ld, WSAGetLastError();
            goto END;
        }

        // Connect to server.
        if (SOCKET_ERROR == (iResult = connect(connectSocket, ptr->ai_addr, (int)ptr->ai_addrlen))) {
            closesocket(connectSocket);
            connectSocket = INVALID_SOCKET;
            continue;
        }
        break;
    }

    if (connectSocket == INVALID_SOCKET) {
        // Unable to connect to server!
        goto END;
    }

    // Update status, closesocket required after this stage
    status = SOCKET_INITED;
    ret = true;

END:
    if (result) {
        // http://msdn.microsoft.com/en-us/library/windows/desktop/ms737931(v=vs.85).aspx
        // All dynamic pointer stored in result will be also freed, including ptr
        freeaddrinfo(result);
    }
    return ret;
}

bool SocketTask::sendData(const char* sendBuf, const size_t bufSize)
{
    bool ret = false;
    int iResult = SOCKET_ERROR;

    if (SOCKET_INITED < status) {
        // Invalid socket status: status
        goto END;
    }

    //iResult = send(connectSocket, sendBuf, (int)strlen(sendBuf) + 1, 0);
    iResult = send(connectSocket, sendBuf, (int)bufSize, 0);
    if (iResult == SOCKET_ERROR) {
        // Send failed with error: %d, WSAGetLastError()
        goto END;
    }

    // shutdown the connection since no more data will be sent
    iResult = shutdown(connectSocket, SD_SEND);
    if (iResult == SOCKET_ERROR) {
        // Shutdown failed with error: %d WSAGetLastError()
        goto END;
    }

    status = DATA_SENT;
    ret = true;

END:
    return ret;
}

bool SocketTask::receiveData(char* resp, const size_t respSize)
{
    bool ret = false;
    int iResult = 0;
    char recvBuf[DEFAULT_BUFLEN];

    if (DATA_SENT > status) {
        // Invalid socket status: status
        goto END;
    }

    do {
        iResult = recv(connectSocket, recvBuf, DEFAULT_BUFLEN, 0);
        if (0 < iResult) {
            // Append received data
            // TODO: handle respSize v.s. DEFAULT_BUFLEN
            memcpy(resp, recvBuf, respSize);
		} else if (0 == iResult) {
            // Connection closed
        } else {
            // Recv failed with error: %d, WSAGetLastError()
        }
    } while(0 < iResult);

    // There may be no data received
    status = SOCKET_CMD_FINISHED;
    ret = true;

END:
    return ret;
}

bool sockCmd(const char* cmd, const size_t cmdSize, char* resp, const size_t respSize)
{
    bool ret = false;
    SocketTask socket;

    if (!socket.connectWithService()) {
        // Error, or service is not enabled
        goto END;
    }

    if (!socket.sendData(cmd, cmdSize)) {
        // Error, or timeout
        goto END;
    }

    if (!socket.receiveData(resp, respSize)) {
        // Error, or timeout
        goto END;
    }

    ret = true;

END:
    return ret;
}

// Allow using either SN or IP as input, if dsmSn = "" then the receiver will use dsmIp to determine which DSM it is
DSM_CONNECTION_STATUS isDsmConnected(const QString& dsmSn, const QString& dsmIp)
{
    DSM_CONNECTION_STATUS ret = DSM_ST_UNKNOWN;
    char sendBuf[DEFAULT_BUFLEN] = {0};
    char respBuf[DEFAULT_BUFLEN] = {0};
    size_t cmdSize = sizeof(DWORD) * 2 + sizeof(DATA_IS_DSM_CONNECTED);
    DATA_IS_DSM_CONNECTED dataIsDsmConnected;
    
    // Assign socket command content
    PSOCKET_CMD cmd = (PSOCKET_CMD)malloc(cmdSize);
    cmd->opCode = CMD_DSM_IS_CONNECTED;
    cmd->dataSize = sizeof(DATA_IS_DSM_CONNECTED);
    _snprintf_s(dataIsDsmConnected.dsmSn, MAX_SN_LEN, _TRUNCATE, dsmSn.toUtf8().constData());
    _snprintf_s(dataIsDsmConnected.dsmIp, MAX_IP_LEN, _TRUNCATE, dsmIp.toUtf8().constData());
    memcpy(cmd->data, &dataIsDsmConnected, sizeof(DATA_IS_DSM_CONNECTED));
    memcpy(sendBuf, cmd, cmdSize);

    if (!sockCmd(sendBuf, DEFAULT_BUFLEN, respBuf, DEFAULT_BUFLEN)) {
        // Socket error, the service probably not running, return false in this case
        goto END;
    }

    PSOCKET_RESP resp = (PSOCKET_RESP)respBuf;
    PDATA_SOCKET_RESP respData = (PDATA_SOCKET_RESP)resp->data;
    
    if (SOCKET_CMD_SUCCESS != (respData->errCode)) {
        goto END;
    }

    switch(respData->resultCode) {
        case SOCKET_RESULT_DSM_IS_CONNECTED:
            ret = DSM_ST_CONNECTED;
            break;
        case SOCKET_RESULT_DSM_IS_DISCONNECTED:
            ret = DSM_ST_DISCONNECTED;
            break;
        case SOCKET_RESULT_DSM_NOT_IN_LIST:
            ret = DSM_ST_REMOVED;
            break;
        default:
            // Unknwon status
            break;
    }

END:
    if (cmd) {
        free(cmd);
    }
    return ret;
}

SOCKET_CMD_ERR registerDsm(const QString& ip, const QString& user, const QString& password)
{
    SOCKET_CMD_ERR ret = SOCKET_CMD_ERR_UNKNOWN;
    char sendBuf[DEFAULT_BUFLEN] = {0};
    char respBuf[DEFAULT_BUFLEN] = {0};
    size_t cmdSize = sizeof(DWORD) * 2 + sizeof(DATA_REGISTER_DSM);
    DATA_REGISTER_DSM dataRegisterDsm;
    
    // Assign socket command content
    PSOCKET_CMD cmd = (PSOCKET_CMD)malloc(cmdSize);
    cmd->opCode = CMD_DSM_REGISTER;
    cmd->dataSize = sizeof(DATA_REGISTER_DSM);
    _snprintf_s(dataRegisterDsm.ip, MAX_IP_LEN, _TRUNCATE, ip.toUtf8().constData());
    _snprintf_s(dataRegisterDsm.user, MAX_USERNAME_LEN, _TRUNCATE, user.toUtf8().constData());
    _snprintf_s(dataRegisterDsm.password, MAX_PWD_LEN, _TRUNCATE, password.toUtf8().constData());

    memcpy(cmd->data, &dataRegisterDsm, sizeof(DATA_REGISTER_DSM));
    memcpy(sendBuf, cmd, cmdSize);

    if (!sockCmd(sendBuf, DEFAULT_BUFLEN, respBuf, DEFAULT_BUFLEN)) {
        // Socket error, service may not be running
        goto END;
    }

    PSOCKET_RESP resp = (PSOCKET_RESP)respBuf;
    PDATA_SOCKET_RESP respData = (PDATA_SOCKET_RESP)resp->data;
    
    if (SOCKET_CMD_SUCCESS != (respData->errCode)) {
        goto END;
    }

    // TODO: refine return type?
    ret = SOCKET_CMD_SUCCESS;

END:
    if (cmd) {
        free(cmd);
    }
    return ret;
}

bool removeDsm(const QString& dsmSn)
{
    bool ret = false;
    char sendBuf[DEFAULT_BUFLEN] = {0};
    char respBuf[DEFAULT_BUFLEN] = {0};
    size_t cmdSize = sizeof(DWORD) * 2 + sizeof(DATA_REMOVE_DSM);
    DATA_REMOVE_DSM dataRemoveDsm;
    
    // Assign socket command content
    PSOCKET_CMD cmd = (PSOCKET_CMD)malloc(cmdSize);
    cmd->opCode = CMD_DSM_REMOVE;
    cmd->dataSize = sizeof(DATA_REMOVE_DSM);
    _snprintf_s(dataRemoveDsm.dsmSn, MAX_SN_LEN, _TRUNCATE, dsmSn.toUtf8().constData());

    memcpy(cmd->data, &dataRemoveDsm, sizeof(DATA_REMOVE_DSM));
    memcpy(sendBuf, cmd, cmdSize);

    if (!sockCmd(sendBuf, DEFAULT_BUFLEN, respBuf, DEFAULT_BUFLEN)) {
        // Socket error, service may not be running
        goto END;
    }

    PSOCKET_RESP resp = (PSOCKET_RESP)respBuf;
    PDATA_SOCKET_RESP respData = (PDATA_SOCKET_RESP)resp->data;
    
    if (SOCKET_CMD_SUCCESS != (respData->errCode)) {
        goto END;
    }

    ret = true;

END:
    if (cmd) {
        free(cmd);
    }
    return ret;
}