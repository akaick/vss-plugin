#include "systray.h"

SysTray::SysTray(const QIcon& icon, QObject* parent) :
    QSystemTrayIcon(icon, parent),
    menu(NULL)
{
    connect(this, SIGNAL(activated(QSystemTrayIcon::ActivationReason)), this, SLOT(iconActivated(QSystemTrayIcon::ActivationReason)));
}

void SysTray::setContextMenu(QMenu* menuSetting)
{
    QSystemTrayIcon::setContextMenu(menuSetting);
    menu = menuSetting;
}

void SysTray::iconActivated(QSystemTrayIcon::ActivationReason reason)
{
    switch (reason) {
        case QSystemTrayIcon::Trigger:
            menu->popup(QCursor::pos());
            break;
        default:
            break;
    }
}
