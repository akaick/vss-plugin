
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "dsminfodialog.h"
#include "socketipc.h"
#include "util.h"

MainWindow::MainWindow(QWidget* parent) :
    QMainWindow(parent),
    font(DSM_FONT),
    aboutPage(NULL),
    helpPage(NULL),
    registerDialog(NULL),
    editDialog(NULL),
    ui(new Ui::MainWindow)
{
    createActions();
    createTrayIcon();
    trayIcon->show();

    QTimer* timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(loadRegistryKeys()));
    timer->start(UI_TIMER);

    // Use the same font with DSM style
    setFont(font);

    ui->setupUi(this);
    setWindowTitle(tr("app_name"));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::createActions()
{
    // FIXME: mark the DSM list window first, in case we might want to display it in the future
    addDsmAction = new QAction(tr("register_dsm_action"), this);
    helpAction = new QAction(tr("help_action"), this);
    aboutAction = new QAction(tr("about_page_action"), this);
    quitAction = new QAction(tr("quit_action"), this);
    connect(addDsmAction, SIGNAL(triggered()), this, SLOT(onAddDsm()));
    connect(helpAction, SIGNAL(triggered()), this, SLOT(onShowHelpPage()));
    connect(aboutAction, SIGNAL(triggered()), this, SLOT(onShowAboutPage()));
    connect(quitAction, SIGNAL(triggered()), qApp, SLOT(quit()));
    // TODO: add online document for help page
}

void MainWindow::createTrayIcon()
{
    dsmListMenu = new QMenu(tr("dsm_list_menu"), this);

    loadRegistryKeys();

    trayIconMenu = new QMenu(this);
    trayIconMenu->addMenu(dsmListMenu);
    trayIconMenu->addSeparator();
    trayIconMenu->setFont(font);

    trayIconMenu->addAction(addDsmAction);
    trayIconMenu->addAction(helpAction);
    trayIconMenu->addAction(aboutAction);
    trayIconMenu->addSeparator();
    trayIconMenu->addAction(quitAction);

    trayIcon = new SysTray(QIcon(TRAY_ICON_IMG), this);
    // setContextMenu does not take ownership of this QMenu, must set it manually
    trayIcon->setContextMenu(trayIconMenu);
}

bool MainWindow::validateDsmRegistry(const QSettings& settings)
{
    bool ret = true;
    QString requiredRegKeys[3] = {DSM_ADDR, DSM_ACCOUNT, DSM_KEY};

    for (int i = 0; i < 3; i++) {
        if (!settings.contains(requiredRegKeys[i])) {
            // TODO: add logs for invalid registry key entries
            ret = false;
        }
    }

    return ret;
}

// Read content from registry and then set tableWidget values
// http://qt-project.org/doc/qt-5.0/qtcore/qsettings.html
void MainWindow::loadRegistryKeys()
{
    QSettings settings(PLUGIN_REG_DSM_GROUPS, QSettings::NativeFormat);
    QStringList dsmList = settings.childGroups();

    // Clear the previous query results, also delete old DSM list menu and actions
    dsmListMenu->clear();
    for (int i = 0; i < dsmList.size(); i++) {
        settings.beginGroup(dsmList.at(i));
        if (!validateDsmRegistry(settings)) {
            settings.endGroup();
            continue;
        }
        DsmInfo info;
        DSM_CONNECTION_STATUS dsmStatus = DSM_ST_UNKNOWN;

        info.dsmSn = dsmList.at(i);
        info.dsmIp = settings.value(DSM_ADDR).toString();
        info.user = settings.value(DSM_ACCOUNT).toString();
        info.key = settings.value(DSM_KEY).toString();
        // Get DSM connection status from service
        dsmStatus = isDsmConnected(info.dsmSn, "");
        // TODO: handle unknown status conditions
        info.status = (DSM_ST_CONNECTED == dsmStatus) ? tr("connected") : tr("disconnected");

        manageDsmMenu = new DsmMenu(info.dsmIp + ":  " + info.status, this);
        manageDsmMenu->setDsmInfo(info);

        QAction* edit = new QAction(tr("edit_action"), this);
        QAction* remove = new QAction(tr("remove_action"), this);
        manageDsmMenu->addAction(edit);
        manageDsmMenu->addAction(remove);
        connect(edit, SIGNAL(triggered()), manageDsmMenu, SLOT(onEdit()));
        connect(manageDsmMenu, SIGNAL(showEditDialog(DsmInfo)), this, SLOT(onShowEditDialog(DsmInfo)));
        connect(remove, SIGNAL(triggered()), manageDsmMenu, SLOT(onRemove()));
        connect(manageDsmMenu, SIGNAL(refreshDsmList()), this, SLOT(loadRegistryKeys()));
        dsmListMenu->addMenu(manageDsmMenu);

        settings.endGroup();
    }
}

void MainWindow::onShowHelpPage()
{
    if (!helpPage) {
        QSettings settings(PLUGIN_REGKEY, QSettings::NativeFormat);

        helpPage = new HelpPage(this);
        helpPage->exec();
        return;
    }

    if (!helpPage->isVisible()) {
        helpPage->show();
    }
}

void MainWindow::onShowAboutPage()
{
    if (!aboutPage) {
        QSettings settings(PLUGIN_REGKEY, QSettings::NativeFormat);

        aboutPage = new AboutPage(this);
        aboutPage->setWindowTitle(tr("about_page_title"));
        aboutPage->setVersionDesc(tr("version_desc").arg(settings.value(PLUGIN_VERSION_FIELD).toString()));
        aboutPage->exec();
        return;
    }

    if (!aboutPage->isVisible()) {
        aboutPage->show();
    }
}

void MainWindow::onQuit()
{
    // TODO: reserved interface for future use, can trigger service stop here
    // TODO: check document about quit flows
    qApp->quit();
}

void MainWindow::onAddDsm()
{
    int timePast = 0;
    QString ip, user, password, text;
    SOCKET_CMD_ERR result = SOCKET_CMD_ERR_UNKNOWN;

    if (!registerDialog) {
        registerDialog = new DsmInfoDialog(this);
    }

    if (registerDialog->isVisible()) {
        // Do not delete the dialog content as the user hasn't done with the previous opened dialog
        return;
    }

    // Ask DSM to perform register DSM action
    registerDialog->setWindowTitle(tr("register_dsm_action"));
    registerDialog->setDesc(tr("register_dsm_desc"));
    if (QDialog::Accepted != registerDialog->exec() || !registerDialog->isModified()) {
        goto END;
    }

    // TODO: the return codes have been changed, the real result is now returned by following isDsmConnected()
    registerDialog->getDialogData(ip, user, password);
    if (SOCKET_CMD_SUCCESS != (result = registerDsm(ip, user, password))) {
        if (SOCKET_CMD_ERR_UNKNOWN == result) {
            text = tr("err_service_not_running");
        } else if (SOCKET_CMD_INVALID_USER == result || SOCKET_CMD_WRONG_PWD == result) {
            text = tr("err_register_failures");
        } else if (SOCKET_CMD_DSM_NO_RESPONSE == result) {
            text = tr("err_connection_failure");
        } else if (SOCKET_CMD_DSM_OUTDATED == result) {
            text = tr("err_DSM_need_upgrade");
        } else if (SOCKET_CMD_PLUGIN_OUTDATED == result) {
            text = tr("err_plugin_need_upgrade");
        }
        goto SHOWBOX;
    }

    // TODO: how to get detail results like account or password errors?
    while (DSM_ST_CONNECTED != isDsmConnected("", ip)) {
        Sleep(DEFAULT_SLEEP_TIME);
        timePast += DEFAULT_SLEEP_TIME;
        if (DEFAULT_TIMEOUT <= timePast) {
            text = tr("err_register_failures");
            goto SHOWBOX;
        }
    }

    text = tr("register_success");

    // Reload registry keys of registered DSM
    loadRegistryKeys();

SHOWBOX:
    showMsgBox(text, tr("register_dsm_action"));
END:
    delete registerDialog;
    registerDialog = NULL;
}

// Looks like can be achieved by "remove" + "register" cmds?
void MainWindow::onShowEditDialog(DsmInfo dsmInfo)
{
    int timePast = 0;
    QString ip, user, password, text;
    SOCKET_CMD_ERR result = SOCKET_CMD_ERR_UNKNOWN;

    if (!editDialog) {
        editDialog = new DsmInfoDialog(this);
    }

    if (editDialog->isVisible()) {
        // Do not delete the dialog content as the user hasn't done with the previous opened dialog
        return;
    }

    editDialog->setWindowTitle(tr("edit_dsm_title"));
    editDialog->setDesc(tr("edit_dsm_desc"));
    editDialog->setDsmInfo(dsmInfo);
    if (QDialog::Accepted != editDialog->exec() || !editDialog->isModified()) {
        return;
    }
    editDialog->getDialogData(ip, user, password);

    if (!removeDsm(dsmInfo.dsmSn)) {
        // Error condition
        return;
    }

    // TODO: should use timer instead of blocking main UI thread?
    while (DSM_ST_REMOVED != isDsmConnected(dsmInfo.dsmSn, "")) {
        Sleep(DEFAULT_SLEEP_TIME);
        timePast += DEFAULT_SLEEP_TIME;
        if (DEFAULT_TIMEOUT <= timePast) {
            break;
        }
    }

    if (SOCKET_CMD_SUCCESS != (result = registerDsm(ip, user, password))) {
        text = tr("err_service_not_running");
        showMsgBox(text, tr("edit_dsm_title"));
        return;
    }

    timePast = 0;
    // TODO: how to get detail results?
    while (DSM_ST_CONNECTED != isDsmConnected("", ip)) {
        Sleep(DEFAULT_SLEEP_TIME);
        timePast += DEFAULT_SLEEP_TIME;
        if (DEFAULT_TIMEOUT <= timePast) {
            break;
        }
    }

    text = tr("register_success");
    showMsgBox(text, tr("edit_dsm_title"));

    // Reload registry keys of registered DSM
    loadRegistryKeys();
}