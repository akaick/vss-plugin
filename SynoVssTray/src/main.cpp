
#include "common.h"
#include "qtsingleapplication.h"
#include "mainwindow.h"
#include "util.h"

int main(int argc, char* argv[])
{
    QtSingleApplication app(argc, argv);
    QTranslator translator;

    // A language setting must be loaded before MainWindow and QApplication start
    translator.load(DEFAULT_LANG);
    app.installTranslator(&translator);
    app.setQuitOnLastWindowClosed(false);

    if (app.isRunning()) {
        showMsgBox(QObject::tr("app_already_running"), NULL);
        // TODO: detect zombie status
        return 0;
    }

    MainWindow w;

    return app.exec();
}