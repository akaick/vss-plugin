#include "helppage.h"

HelpPage::HelpPage(QWidget* parent) :
    QDialog(parent)
{
    txtBrowser = new QTextBrowser;
    setWindowTitle(tr("app_name"));
    setWindowIcon(QIcon(TRAY_ICON_IMG));
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
    resize(HELP_PAGE_WIDTH, HELP_PAGE_HEIGHT);

    QFile file(DEFAULT_HELP_LANG_FILE);
    if(file.open(QIODevice::ReadOnly)) {
        QTextStream stream(&file);
        txtBrowser->setOpenExternalLinks(true);
        stream.setCodec("UTF-8");
        txtBrowser->setText(stream.readAll());
    }
    file.close();
    
    layout = new QVBoxLayout;
    layout->addWidget(txtBrowser);
    setLayout(layout);
}

HelpPage::~HelpPage(){
    delete txtBrowser;
    delete layout;
}