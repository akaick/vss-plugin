#include "aboutpage.h"
#include "ui_aboutpage.h"

AboutPage::AboutPage(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AboutPage)
{
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
    ui->setupUi(this);
}

AboutPage::~AboutPage()
{
    delete ui;
}

void AboutPage::setVersionDesc(const QString version)
{
    ui->versionDescLabel->setText(version);
    ui->versionDescLabel->setWordWrap(true);
    ui->versionDescLabel->setAlignment(Qt::AlignTop);
    ui->copyrightLabel->setWordWrap(true);
    ui->copyrightLabel->setAlignment(Qt::AlignTop);
}

void AboutPage::on_pushButton_clicked()
{
    accept();
}
