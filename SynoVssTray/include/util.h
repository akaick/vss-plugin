#ifndef UTIL_H
#define UTIL_H

#include "common.h"

void showMsgBox(const QString& text, const QString& title = QObject::tr("app_name"));

#endif // UTIL_H