#ifndef SOCKETIPC_H
#define SOCKETIPC_H

#include "common.h"
#include <sockcmdstructs.h>

enum SOCKET_STATUS {
    NOT_INITED = 0,
    WSADATA_INITED,
    SOCKET_INITED,
    DATA_SENT,
    SOCKET_CMD_FINISHED
};

enum DSM_CONNECTION_STATUS {
    DSM_ST_UNKNOWN = 0,
    DSM_ST_CONNECTED = 1,
    DSM_ST_DISCONNECTED = 2,
    DSM_ST_REMOVED = 3
};

// TODO: define resp struct
bool sockCmd(const char* cmd, const size_t cmdSize, char* resp, const size_t respSize);
DSM_CONNECTION_STATUS isDsmConnected(const QString& dsmSn, const QString& dsmIp);
SOCKET_CMD_ERR registerDsm(const QString& ip, const QString& user, const QString& password);
bool removeDsm(const QString& dsmSn);

class SocketTask
{
public:
    SocketTask();
    ~SocketTask();
    bool connectWithService();
    bool sendData(const char* sendBuf, const size_t bufSize);
    bool receiveData(char* resp, const size_t respSize);

private:
    void fillAddrInfo(addrinfo& hints);

    SOCKET connectSocket;
    SOCKET_STATUS status;
};

#endif // SOCKETIPC_H