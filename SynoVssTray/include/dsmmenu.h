#ifndef DSMMENU_H
#define DSMMENU_H

#include "common.h"
#include "dsminfodialog.h"

class DsmMenu : public QMenu
{
    Q_OBJECT
public:
    explicit DsmMenu(const QString& title, QWidget* parent = 0);
    void setDsmInfo(const DsmInfo& info);

signals:
    void refreshDsmList();
    void showEditDialog(DsmInfo dsmInfo);

public slots:
    void onEdit();
    void onRemove();
private:
    DsmInfo dsmInfo;
};

#endif // DSMMENU_H