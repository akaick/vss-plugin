#ifndef STDAFX_H
#define STDAFX_H

#define WIN32_LEAN_AND_MEAN

// Windows headers
#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>

// Qt classes
#include <QFont>
#include <QString>
#include <QDialog>
#include <QCloseEvent>
#include <QApplication>
#include <QMainWindow>
#include <QSystemTrayIcon>
#include <QSettings>
#include <QTranslator>
#include <QMessageBox>
#include <QHostAddress>
#include <QMenu>
#include <QTextBrowser>
#include <QVBoxLayout>
#include <QTimer>

#define PLUGIN_REGKEY           "HKEY_LOCAL_MACHINE\\Software\\Synology\\Snapshot Manager for Windows"
#define PLUGIN_REG_DSM_GROUPS   PLUGIN_REGKEY"\\Groups"
#define PLUGIN_VERSION_FIELD    "PluginVersion"
// TODO: load language settings from registry
#define LANG_REGKEY         "HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\Synology Snapshot Manager for Windows"
#define TRAY_ICON_IMG       ":/images/app_icon.png"
#define DSM_FONT            "Verdana"
#define LANG_PATH           ":/languages/"
#define DEFAULT_LANG        LANG_PATH"synovsstray_enu.qm"
#define DEFAULT_TIMEOUT     20000
#define DEFAULT_SLEEP_TIME  500
#define UI_TIMER            15000

// LANG_TYPE reserved for future use
// TODO: add more supporting languages
// http://msdn.microsoft.com/zh-tw/library/bb165625(v=vs.90).aspx
enum LANG_TYPE {
    CHINESE_TRADITIONAL = 1028,
    ENGLISH = 1033
};

class DsmInfo
{
public:
    QString dsmSn;
    QString dsmIp;
    QString user;
    QString key;
    // Indicate the DSM is connected with host or not
    QString status;
};

#endif // STDAFX_H
