#ifndef HELPPAGE_H
#define HELPPAGE_H

#include "common.h"

#define HELP_PAGE_WIDTH 700
#define HELP_PAGE_HEIGHT 500
#define DEFAULT_HELP_LANG_FILE ":/help/help_enu.html"

class HelpPage : public QDialog
{
    Q_OBJECT

public:
    explicit HelpPage(QWidget* parent = 0);
    ~HelpPage();

private:
    QTextBrowser* txtBrowser;
    QVBoxLayout* layout;
};


#endif // HELPWINDOW_H