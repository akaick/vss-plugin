#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "common.h"
#include "dsmmenu.h"
#include "aboutpage.h"
#include "helppage.h"
#include "systray.h"

#define DSM_ADDR "Address"
#define DSM_ACCOUNT "Account"
#define DSM_KEY "Key"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public slots:
    void loadRegistryKeys();

public:
    explicit MainWindow(QWidget* parent = 0);
    ~MainWindow();
    void createActions();
    void createTrayIcon();

private slots:
    void onQuit();
    void onShowHelpPage();
    void onShowAboutPage();
    void onAddDsm();
    void onShowEditDialog(DsmInfo dsmInfo);

private:
    Ui::MainWindow* ui;
    QFont font;
    QAction* showListAction;
    QAction* helpAction;
    QAction* aboutAction;
    QAction* quitAction;

    QAction* addDsmAction;
    QAction* editDsmAction;
    QAction* removeDsmAction;

    QMenu* trayIconMenu;
    QMenu* dsmListMenu;
    DsmMenu* manageDsmMenu;
    SysTray* trayIcon;
    AboutPage* aboutPage;
    HelpPage* helpPage;
    DsmInfoDialog* registerDialog;
    DsmInfoDialog* editDialog;

    bool validateDsmRegistry(const QSettings& settings);
};

#endif // MAINWINDOW_H
