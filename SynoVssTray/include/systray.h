#ifndef SYSTRAY_H
#define SYSTRAY_H

#include "common.h"

class SysTray : public QSystemTrayIcon
{
    Q_OBJECT

public:
    SysTray(const QIcon& icon, QObject* parent);
    void setContextMenu(QMenu* menuSetting);

private slots:
    void iconActivated(QSystemTrayIcon::ActivationReason reason);

private:
    QMenu* menu;
};

#endif // SYSTRAY_H