#ifndef ABOUTPAGE_H
#define ABOUTPAGE_H

#include "common.h"

namespace Ui {
class AboutPage;
}

class AboutPage : public QDialog
{
    Q_OBJECT

public:
    explicit AboutPage(QWidget* parent = 0);
    ~AboutPage();
    void setVersionDesc(const QString version);

private slots:
    void on_pushButton_clicked();

private:
    Ui::AboutPage* ui;
};

#endif // ABOUTPAGE_H
