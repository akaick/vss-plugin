#ifndef DSMINFODIALOG_H
#define DSMINFODIALOG_H

#include "common.h"

namespace Ui {
class DsmInfoDialog;
}

class DsmInfoDialog : public QDialog
{
    Q_OBJECT

public:
    explicit DsmInfoDialog(QWidget* parent = 0);
    ~DsmInfoDialog();
    void setDesc(const QString dialogDesc);
    void setDsmInfo(DsmInfo info);
    void getDialogData(QString& ip, QString& user, QString& password);
    bool isModified();

protected:
    void closeEvent(QCloseEvent* event);

private slots:
    void on_applyBtn_clicked();

    void on_cancelBtn_clicked();

private:
    Ui::DsmInfoDialog* ui;

    bool isValidInput();
};

#endif // DSMINFODIALOG_H