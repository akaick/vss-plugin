<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="zh_CN">
<context>
    <name>AboutPage</name>
    <message>
        <location filename="../aboutpage.ui" line="37"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutpage.ui" line="63"/>
        <source>ok</source>
        <translation type="unfinished">OK</translation>
    </message>
    <message>
        <location filename="../aboutpage.ui" line="76"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;img src=&quot;:/images/about_icon.png&quot;/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutpage.ui" line="97"/>
        <source>copyright_desc</source>
        <translation type="unfinished">版權所有 (c) 2014 群暉科技 所有權利均保留</translation>
    </message>
    <message>
        <location filename="../aboutpage.ui" line="118"/>
        <source>Version Description</source>
        <oldsource>version_desc</oldsource>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DsmInfoDialog</name>
    <message>
        <location filename="../dsminfodialog.ui" line="39"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dsminfodialog.ui" line="192"/>
        <source>apply</source>
        <translation type="unfinished">套用</translation>
    </message>
    <message>
        <location filename="../dsminfodialog.ui" line="204"/>
        <source>cancel</source>
        <translation type="unfinished">取消</translation>
    </message>
    <message>
        <location filename="../dsminfodialog.ui" line="124"/>
        <location filename="../src/dsminfodialog.cpp" line="72"/>
        <source>ip</source>
        <translation type="unfinished">IP</translation>
    </message>
    <message>
        <location filename="../dsminfodialog.ui" line="134"/>
        <location filename="../src/dsminfodialog.cpp" line="78"/>
        <source>user</source>
        <translation type="unfinished">使用者</translation>
    </message>
    <message>
        <location filename="../dsminfodialog.ui" line="144"/>
        <source>password</source>
        <translation type="unfinished">密碼</translation>
    </message>
    <message>
        <location filename="../dsminfodialog.ui" line="99"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dsminfodialog.cpp" line="63"/>
        <source>invalid_dsm_dialog_fields</source>
        <translation type="unfinished">下列欄位並未填入合法的輸入:</translation>
    </message>
</context>
<context>
    <name>DsmMenu</name>
    <message>
        <source>edit_dsm_title</source>
        <translation type="obsolete">編輯 DSM 資訊</translation>
    </message>
    <message>
        <source>edit_dsm_desc</source>
        <translation type="obsolete">編輯此 DSM 的註冊資訊。</translation>
    </message>
</context>
<context>
    <name>HelpPage</name>
    <message>
        <location filename="../src/helppage.cpp" line="7"/>
        <source>app_name</source>
        <translation type="unfinished">Synology Snapshot Manager for Windows</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.ui" line="31"/>
        <source>MainWindow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="53"/>
        <source>Add DSM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="70"/>
        <source>IP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="85"/>
        <source>Status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="105"/>
        <source>Edit DSM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="121"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="25"/>
        <source>app_name</source>
        <translation type="unfinished">Synology Snapshot Manager for Windows</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="40"/>
        <location filename="../src/mainwindow.cpp" line="187"/>
        <source>register_dsm_action</source>
        <translation type="unfinished">註冊 DSM</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="41"/>
        <source>help_action</source>
        <translation type="unfinished">說明</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="42"/>
        <source>about_page_action</source>
        <translation type="unfinished">關於</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="43"/>
        <source>quit_action</source>
        <translation type="unfinished">離開</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="53"/>
        <source>dsm_list_menu</source>
        <translation type="unfinished">DSM 清單</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="113"/>
        <source>connected</source>
        <translation type="unfinished">已連線</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="113"/>
        <source>disconnected</source>
        <translation type="unfinished">未連線</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="118"/>
        <source>edit_action</source>
        <translation type="unfinished">編輯</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="119"/>
        <source>remove_action</source>
        <translation type="unfinished">移除</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="153"/>
        <source>about_page_title</source>
        <translation type="unfinished">關於 Synology Snapshot Manager</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="154"/>
        <source>version_desc</source>
        <translation type="unfinished">Synology Snapshot Manager for Windows 版本 %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="188"/>
        <source>register_dsm_desc</source>
        <translation type="unfinished">新增 DSM 可啟用 data consistent snapshot 功能於此 DSM 上的 LUN。</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="197"/>
        <location filename="../src/mainwindow.cpp" line="270"/>
        <source>err_service_not_running</source>
        <translation type="unfinished">請確認 “SynoVssPlugin” 服務有被正常啟動</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="199"/>
        <source>err_register_failures</source>
        <translation type="unfinished">註冊 DSM 失敗, 請確認帳號資訊是否正確</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="201"/>
        <source>err_connection_failure</source>
        <translation type="unfinished">與 DSM 連線失敗，請確認 Windows 與 DSM 的網路狀態皆正常運作。</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="203"/>
        <source>err_DSM_need_upgrade</source>
        <translation type="unfinished">DSM 版本過舊，請升級 DSM 至版本 %1。</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="205"/>
        <source>err_plugin_need_upgrade</source>
        <translation type="unfinished">plugin 版本過舊，請升級 plugin 版本至 %1。</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="220"/>
        <location filename="../src/mainwindow.cpp" line="285"/>
        <source>register_success</source>
        <translation type="unfinished">註冊 DSM 成功。</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="247"/>
        <source>edit_dsm_title</source>
        <translation type="unfinished">編輯 DSM 資訊</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="248"/>
        <source>edit_dsm_desc</source>
        <translation type="unfinished">編輯此 DSM 的註冊資訊。</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/main.cpp" line="18"/>
        <source>app_already_running</source>
        <translation type="unfinished">此應用程式已經正在被執行中。</translation>
    </message>
    <message>
        <location filename="../src/util.cpp" line="11"/>
        <source>app_name</source>
        <translation type="unfinished">Synology Snapshot Manager for Windows</translation>
    </message>
</context>
</TS>
