<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name>AboutPage</name>
    <message>
        <location filename="../aboutpage.ui" line="37"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutpage.ui" line="63"/>
        <source>ok</source>
        <oldsource>OK</oldsource>
        <translation type="unfinished">OK</translation>
    </message>
    <message>
        <location filename="../aboutpage.ui" line="76"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;img src=&quot;:/images/about_icon.png&quot;/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutpage.ui" line="97"/>
        <source>copyright_desc</source>
        <translation type="unfinished">Copyright (c) 2014 Synology Inc. All rights reserved.</translation>
    </message>
    <message>
        <location filename="../aboutpage.ui" line="118"/>
        <source>Version Description</source>
        <oldsource>version_desc</oldsource>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DsmInfoDialog</name>
    <message>
        <location filename="../dsminfodialog.ui" line="39"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dsminfodialog.ui" line="192"/>
        <source>apply</source>
        <translation type="unfinished">Apply</translation>
    </message>
    <message>
        <location filename="../dsminfodialog.ui" line="204"/>
        <source>cancel</source>
        <translation type="unfinished">Cancel</translation>
    </message>
    <message>
        <location filename="../dsminfodialog.ui" line="124"/>
        <location filename="../src/dsminfodialog.cpp" line="72"/>
        <source>ip</source>
        <translation type="unfinished">IP</translation>
    </message>
    <message>
        <location filename="../dsminfodialog.ui" line="134"/>
        <location filename="../src/dsminfodialog.cpp" line="78"/>
        <source>user</source>
        <translation type="unfinished">User</translation>
    </message>
    <message>
        <location filename="../dsminfodialog.ui" line="144"/>
        <source>password</source>
        <translation type="unfinished">Password</translation>
    </message>
    <message>
        <location filename="../dsminfodialog.ui" line="99"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dsminfodialog.cpp" line="63"/>
        <source>invalid_dsm_dialog_fields</source>
        <translation type="unfinished">The following field(s) is not filled with valid value:</translation>
    </message>
</context>
<context>
    <name>DsmMenu</name>
    <message>
        <source>edit_dsm_title</source>
        <translation type="obsolete">Edit DSM</translation>
    </message>
    <message>
        <source>edit_dsm_desc</source>
        <translation type="obsolete">Edit the information of this registered DSM.</translation>
    </message>
</context>
<context>
    <name>HelpPage</name>
    <message>
        <location filename="../src/helppage.cpp" line="7"/>
        <source>app_name</source>
        <translation type="unfinished">Synology Snapshot Manager for Windows</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.ui" line="31"/>
        <source>MainWindow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="53"/>
        <source>Add DSM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="70"/>
        <source>IP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="85"/>
        <source>Status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="105"/>
        <source>Edit DSM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="121"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="25"/>
        <source>app_name</source>
        <translation type="unfinished">Synology Snapshot Manager for Windows</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="40"/>
        <location filename="../src/mainwindow.cpp" line="187"/>
        <source>register_dsm_action</source>
        <translation type="unfinished">Register DSM</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="41"/>
        <source>help_action</source>
        <translation type="unfinished">Help</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="42"/>
        <source>about_page_action</source>
        <translation type="unfinished">About</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="43"/>
        <source>quit_action</source>
        <translation type="unfinished">Quit</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="53"/>
        <source>dsm_list_menu</source>
        <translation type="unfinished">DSM list</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="113"/>
        <source>connected</source>
        <translation type="unfinished">Connected</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="113"/>
        <source>disconnected</source>
        <translation type="unfinished">Disconnected</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="118"/>
        <source>edit_action</source>
        <translation type="unfinished">Edit</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="119"/>
        <source>remove_action</source>
        <translation type="unfinished">Remove</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="153"/>
        <source>about_page_title</source>
        <translation type="unfinished">About Synology Snapshot Manager</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="154"/>
        <source>version_desc</source>
        <translation type="unfinished">Synology Snapshot Manager for Windows version %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="188"/>
        <source>register_dsm_desc</source>
        <translation type="unfinished">Add a DSM to enable taking data consistent snapshot of LUNs on this DSM.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="197"/>
        <location filename="../src/mainwindow.cpp" line="270"/>
        <source>err_service_not_running</source>
        <translation type="unfinished">Please confirm the &quot;SynoVssPlugin&quot; service is running.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="199"/>
        <source>err_register_failures</source>
        <translation type="unfinished">Failed to register with DSM. Please make sure the information of account is correct.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="201"/>
        <source>err_connection_failure</source>
        <translation type="unfinished">Failed to connect to DSM. Please check the network status of both Windows and DSM are working properly.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="203"/>
        <source>err_DSM_need_upgrade</source>
        <translation type="unfinished">The DSM version is outdated. Please upgrade the DSM to version %1.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="205"/>
        <source>err_plugin_need_upgrade</source>
        <translation type="unfinished">The plugin version is outdated. Please upgrade the plugin to version %1.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="220"/>
        <location filename="../src/mainwindow.cpp" line="285"/>
        <source>register_success</source>
        <translation type="unfinished">Successfully registered a DSM.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="247"/>
        <source>edit_dsm_title</source>
        <translation type="unfinished">Edit DSM</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="248"/>
        <source>edit_dsm_desc</source>
        <translation type="unfinished">Edit the information of this registered DSM.</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/main.cpp" line="18"/>
        <source>app_already_running</source>
        <translation type="unfinished">Another instance of this application is already running.</translation>
    </message>
    <message>
        <location filename="../src/util.cpp" line="11"/>
        <source>app_name</source>
        <translation type="unfinished">Synology Snapshot Manager for Windows</translation>
    </message>
</context>
</TS>
