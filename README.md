This project contains a plugin running on client machines. It allows users to take data snapshot through VSS (Volume Shadow Copy) protocol, a technology to ensure data consistency while taking snapshots. It has reference on Microsoft's Hardware Provider sample project and implement more detailed interfaces and functions. The project contains 2 parts: (1) client plugin (2) server side (front-end web interface and back-end handling data snapshot). Where the second part is not open source, so it's not revealed here.

P.S. A snapshot is the concept of data backup, but take less required space and usually only recording the data difference between each backup.

Environment Settings:

Client: Windows Server 2008, Windows Server 2008 R2 or Windows Server 2012

Frontend: Ext JS

Backend: Linux server

Dependencies:
Windows SDK 8

Any suggestions or feedback. Please contact akai.wmlab@gmail.com