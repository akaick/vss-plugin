#ifndef _SYNO_COMM_ENG_CH_MANAGE_H_
#define _SYNO_COMM_ENG_CH_MANAGE_H_

#ifndef WIN32
#include <synocomm/synocomm_base.h>
#else
#include "stdafx.h"
#include "synocomm_base.h"
#endif

#ifdef __cplusplus
__SYNO_BEGIN_DECLS
#endif

extern SYNOCOMM_CHANNEL *DupSocketRequestRxChannel(char *ip, char *hostname, void *pRq);

//Engine Control Handle Function
extern int HandleAppRegister(void *pMsg);
extern int HandleAppURegister(void *pMsg);
extern int HandleRConnectV4(void *pMsg);
extern int HandleConnectV4(void *pMsg);
extern int HandleSendMsg(void *pMsg);
extern int HandleRecvMsg(void *pMsg);
extern int HandleBindMsg(void *pMsg);
extern int HandleBondMsg(void *pMsg);
extern int HandleBandMsg(void *pMsg);
extern int HandleUBindMsg(void *pMsg);
extern int HandleUBondMsg(void *pMsg);
extern int HandleUBandMsg(void *pMsg);
extern int HandlePingMsg(void *pMsg);
extern int HandlePongMsg(void *pMsg);
extern int HandlePungMsg(void *pMsg);

#ifdef __cplusplus
__SYNO_END_DECLS
#endif

#endif
