#ifndef WIN32
#include <synocomm/synocomm_base.h>
#else
#include "synocomm_base.h"
#endif

#ifndef _SYNO_COMM_TYPES_H_
#define _SYNO_COMM_TYPES_H_

#ifdef __cplusplus
__SYNO_BEGIN_DECLS
#endif

#ifndef WIN32
typedef pthread_mutex_t SYNOCOMM_MUTEX;
#define SYNOCOMM_MUTEX_INIT(m)   pthread_mutex_init(&m, NULL)
#define SYNOCOMM_MUTEX_LOCK(m)   pthread_mutex_lock(&m)
#define SYNOCOMM_MUTEX_UNLOCK(m) pthread_mutex_unlock(&m)
#define SYNOCOMM_MUTEX_FREE(m)
#else
typedef HANDLE SYNOCOMM_MUTEX;
#define SYNOCOMM_MUTEX_INIT(m)   m = CreateMutex(NULL, FALSE, NULL)
#define SYNOCOMM_MUTEX_LOCK(m)   WaitForSingleObject(m, INFINITE)
#define SYNOCOMM_MUTEX_UNLOCK(m) ReleaseMutex(m)
#define SYNOCOMM_MUTEX_FREE(m)   CloseHandle(m)
#endif

#define MAX_UUID_LEN		(32)
#define MAX_IPV4_LEN		(4)
#define MAX_IPV4_STR_LEN	(16)
#define	MAX_SERVICE_NAME_LEN	(32)
#define	MAX_HOSTNAME_LEN		(32)

typedef struct _field_ipv4_ {
	char bits[MAX_IPV4_LEN];
}FIELD_IPV4;

typedef struct _field_uuid_ {
	char bits[MAX_UUID_LEN];
}FIELD_UUID;

typedef enum _channel_type_ {
	CH_SOCKET 	 = 1,
	CH_NAMEPIPE  = 2,
	CH_BASE      = 3,
} CH_TYPE;

typedef enum _host_type_ {
	HOST_LOCAL	= 1,
	HOST_REMOTE	= 2,
} HOST_TYPE;

typedef enum _app_type_ {
	APP_LOCAL	= 1,
	APP_REMOTE	= 2,
} APP_TYPE;

typedef enum _app_role_ {
	APP_SERVER	= 1,
	APP_CLIENT	= 2,
} APP_ROLE;

typedef struct _service_identity_ {
	/*
	 * the service name
	 */
	char service_name[MAX_SERVICE_NAME_LEN];
	/*
	 * indicate the channel belong to which process
	 * it's a pid or something meaningful
	 */
	int owner;
	/*
	 * indicate the unique identity
	 * service_name + pid
	 */
	FIELD_UUID	uuid_filed;
#define			serviceuuid	uuid_filed.bits
} SERVICE_ID, *P_SERVICE_ID;

#ifdef WIN32
static void *___zalloc(int size)
{
	char *__zalloc_tmp = (char *)malloc(size);
	if (__zalloc_tmp) {
		memset(__zalloc_tmp, 0, size);
	}
	return __zalloc_tmp;
}

#define ZALLOC(t) ___zalloc(sizeof(t))
#else
#define ZALLOC(t) ({                        \
	t *__zalloc_tmp = NULL;                 \
	__zalloc_tmp = (t *)malloc(sizeof(t));  \
	if (NULL != __zalloc_tmp) {             \
		memset(__zalloc_tmp, 0, sizeof(t)); \
	}                                       \
	__zalloc_tmp;                           \
})
#endif

extern int Str2Ipv4(const char *str, FIELD_IPV4 *ipv4);
extern int Ipv42Str(FIELD_IPV4 *ipv4, char **str);
extern int ServiceUUIDGenerate(char *name, int id, char *uuid, int len);
extern int ServiceIDGenerateUUID(SERVICE_ID *psid);
extern int CompareUUID(FIELD_UUID *pUUID1, FIELD_UUID *pUUID2);
extern int CompareSID(SERVICE_ID *pID1, SERVICE_ID *pID2);
extern P_SERVICE_ID GenServiceID(const char *service_name, int owner, const FIELD_UUID *pUUID);
extern int GetLocalIPV4Str(char *szIPstr, const int cbIPstr);

#ifdef __cplusplus
__SYNO_END_DECLS
#endif

#endif
