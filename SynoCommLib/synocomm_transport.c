#ifndef WIN32
#include <stdlib.h>
#include <synocomm/synocomm_base.h>
#include <synocomm/synocomm_transport.h>
#else
#include "synocomm_base.h"
#include "synocomm_transport.h"
#endif

struct _synocomm_channel_ * synocomm_base_construct(struct _synocomm_channel_ *ch, int ctype, int role, int pid, const char *service_name)
{
	int err = -1;

	struct _synocomm_channel_ *pch = ch;
	SYNOCOMM_CHANNEL_DESC *pdesc = NULL;

	if (NULL == pch) {
			pch = (SYNOCOMM_CHANNEL *)malloc(sizeof(SYNOCOMM_CHANNEL));
			if (NULL == pch) {
				goto ERR;
			}
			memset(pch, 0, sizeof(SYNOCOMM_CHANNEL));
	}

	pdesc = (SYNOCOMM_CHANNEL_DESC *)malloc(sizeof(SYNOCOMM_CHANNEL_DESC));
	if (NULL == pdesc) {
		goto ERR;
	}
	memset(pdesc, 0, sizeof(SYNOCOMM_CHANNEL_DESC));

	pch->channel_desc = pdesc;

	pdesc->serviceid.owner = pid;
	snprintf(pdesc->serviceid.service_name, sizeof(pdesc->serviceid.service_name), "%s", service_name);

	pdesc->role = role;
	pdesc->channel = pch;

	err = 0;
ERR:
	if (err && pch) {
		if (pch->channel_desc) {
			free(pch->channel_desc);
		}
		free(pch);
		pch = NULL;
	}
	return pch;
}

void synocomm_base_destruct(struct _synocomm_channel_ *ch)
{
	SYNOCOMM_TRANSPORT *t = NULL;

	if (NULL != ch) {
		t = ch->transport;
		t->destruct(ch);
		free(ch->channel_desc);
		free(ch);
	}
}

int synocomm_base_send_msg(struct _synocomm_channel_ *ch, char *data, int length)
{
	int err = -1;
	int ret = -1;
	P_SYNOCOMM_CHANNEL_DESC pdesc = (P_SYNOCOMM_CHANNEL_DESC)ch->channel_desc;

	if (SERVER_END == pdesc->role) {
		if (ch->external_response) {
			ret = SENDMSG(ch->external_response_channel, data, length);
		} else {
			ret = TRANSPORT(ch)->write_op(ch->response_channel, data, length);
		}
	} else if (CLIENT_END == pdesc->role) {
		if (ch->external_request) {
			ret = SENDMSG(ch->external_request_channel, data, length);
		} else {
			ret = TRANSPORT(ch)->write_op(ch->request_channel, data, length);
		}
	} else {
		goto ERR;
	}

	err = ret;
ERR:
	return err;
}

int synocomm_base_recv_msg(struct _synocomm_channel_ *ch, char *data, int length)
{
	int err = -1;
	int ret = -1;
	P_SYNOCOMM_CHANNEL_DESC pdesc = (P_SYNOCOMM_CHANNEL_DESC)ch->channel_desc;

	if (SERVER_END == pdesc->role) {
		if (ch->external_request) {
			ret = RECVMSG(ch->external_request_channel, data, length);
		} else {
			ret = TRANSPORT(ch)->read_op(ch->request_channel, data, length);
		}
	} else if (CLIENT_END == pdesc->role) {
		if (ch->external_response) {
			ret = RECVMSG(ch->external_response_channel, data, length);
		} else {
			ret = TRANSPORT(ch)->read_op(ch->response_channel, data, length);
		}
	} else {
		goto ERR;
	}

	err = ret;
ERR:
	return err;
}

int synocomm_base_external_request(struct _synocomm_channel_ *ch, struct _synocomm_channel_ *ech)
{
	int err = -1;

	if (NULL == ech) {
		goto ERR;
	}

	ch->external_request = 1;
	ch->external_request_channel = ech;
	err = 0;
ERR:
	return err;
}

int synocomm_base_external_response(struct _synocomm_channel_ *ch, struct _synocomm_channel_ *ech)
{
	int err = -1;

	if (NULL == ech) {
		goto ERR;
	}

	ch->external_response = 1;
	ch->external_response_channel = ech;
	err = 0;
ERR:
	return err;
}

SYNOCOMM_TRANSPORT base_channel_transport = {
	SFINIT(.media_type, CH_BASE),
	SFINIT(.construct, NULL),
	SFINIT(.destruct, NULL),
	SFINIT(.start_service, NULL),
	SFINIT(.stop_service, NULL),
	SFINIT(.create_internal_request, NULL),
	SFINIT(.create_internal_response, NULL),
	SFINIT(.setup_internal_request, NULL),
	SFINIT(.setup_internal_response, NULL),
	SFINIT(.setup_external_request, synocomm_base_external_request),
	SFINIT(.setup_external_response, synocomm_base_external_response),
	SFINIT(.write_op, NULL),
	SFINIT(.read_op, NULL),
	SFINIT(.send_msg, synocomm_base_send_msg),
	SFINIT(.recv_msg, synocomm_base_recv_msg),
};

