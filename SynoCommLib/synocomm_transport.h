#ifndef _SYNO_COMM_TRANSPORT_H_
#define _SYNO_COMM_TRANSPORT_H_

#ifndef WIN32
#include <synocomm/synocomm_base.h>
#else
#include "synocomm_base.h"
#endif

#ifdef __cplusplus
__SYNO_BEGIN_DECLS
#endif

typedef struct _synocomm_channel_transport_ {
	/*
	 * transport media type
	 */
	unsigned long media_type;
	struct _synocomm_channel_ * (*construct)(struct _synocomm_channel_ *, int, int, int, const char *);
	void (*destruct)(struct _synocomm_channel_ *);
	int (*start_service)(struct _synocomm_channel_ *);
	int (*stop_service)(struct _synocomm_channel_ *);
	int (*create_internal_request) (struct _synocomm_channel_ *, SYNOCOMM_HOST *);
	int (*create_internal_response) (struct _synocomm_channel_ *, SYNOCOMM_HOST *);
	int (*setup_internal_request) (struct _synocomm_channel_ *, void*);
	int (*setup_internal_response) (struct _synocomm_channel_ *, void*);
	int (*setup_external_request) (struct _synocomm_channel_ *, struct _synocomm_channel_ *);
	int (*setup_external_response) (struct _synocomm_channel_ *, struct _synocomm_channel_ *);
	int (*write_op)(void *, char *, int);
	int (*read_op)(void *, char *, int);
	int (*send_msg)(struct _synocomm_channel_ *, char *, int);
	int (*recv_msg)(struct _synocomm_channel_ *, char *, int);
} SYNOCOMM_TRANSPORT, *P_SYNOCOMM_TRANSPORT;

#define TRANSPORT(ch) (ch->transport)
#define SENDMSG(ch, msg, len) TRANSPORT(ch)->send_msg(ch, (char *)msg, len)
#define RECVMSG(ch, msg, len) TRANSPORT(ch)->recv_msg(ch, (char *)msg, len)

extern SYNOCOMM_TRANSPORT base_channel_transport;
extern struct _synocomm_channel_ * synocomm_base_construct(struct _synocomm_channel_ *, int, int, int, const char *);
extern void synocomm_base_destruct(struct _synocomm_channel_ *);
extern int synocomm_base_external_request(struct _synocomm_channel_ *, struct _synocomm_channel_ *);
extern int synocomm_base_external_response(struct _synocomm_channel_ *, struct _synocomm_channel_ *);
extern int synocomm_base_send_msg(struct _synocomm_channel_ *, char *, int );
extern int synocomm_base_recv_msg(struct _synocomm_channel_ *, char *, int );

#ifdef __cplusplus
__SYNO_END_DECLS
#endif

#endif
