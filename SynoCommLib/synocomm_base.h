#ifndef _SYNO_COMM_BASE_H_
#define _SYNO_COMM_BASE_H_

#ifndef WIN32
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <pthread.h>
#include <synotype.h>
#include <synoglobal.h>
#include <linux/syno.h>

#include <synocomm/synocomm_macro.h>
#include <synocomm/synocomm_list.h>
#include <synocomm/synocomm_types.h>
#include <synocomm/synocomm_channel_desc.h>

#else
#include <stdlib.h>
#include <stdio.h>
#include <malloc.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <windows.h>
#include <process.h>

#include "synocomm_macro.h"
#include "synocomm_list.h"
#include "synocomm_types.h"
#include "synocomm_channel_desc.h"

#endif

#ifdef __cplusplus
__SYNO_BEGIN_DECLS
#endif

typedef unsigned char u8;

#define	INT_REQUEST		0x10000000
#define	INT_RESPONSE	0x20000000
#define	EXT_REQUEST		0x40000000
#define	EXT_RESPONSE	0x80000000

#define	INTERNAL_REQUEST(ext)	(INT_REQUEST & ext)
#define	INTERNAL_RESPONSE(ext)	(INT_RESPONSE & ext)
#define	EXTERNAL_REQUEST(ext)	(EXT_REQUEST & ext)
#define	EXTERNAL_RESPONSE(ext)	(EXT_RESPONSE & ext)

/*
 * if ENG applied, fixed pipe used
 * ENG_CH has fixed communication msg/control path
 * APP_CH has it's own communication msg path, named by it's id
 */
#define	ENG_CH	0x00000010
#define	APP_CH	0x00000020

/*
 * request/response direction
 * server: read  request /write response
 * client: write request /read  response
 */
#define	SERVER_END	0x00000001
#define	CLIENT_END	0x00000002

/* user can use this define */
#define	ENG_CH_SERVER_END	(ENG_CH | SERVER_END)
#define	ENG_CH_CLIENT_END	(ENG_CH | CLIENT_END)
#define	APP_CH_SERVER_END	(APP_CH | SERVER_END)
#define	APP_CH_CLIENT_END	(APP_CH | CLIENT_END)

#define	CHTYPE(t)	 (t & 0x000000f0)
#define	CHROLE(t)	 (t & 0x0000000f)
#define	CHEXT(t)     (t & 0xf0000000)

struct _synocomm_channel_;
typedef struct _synocomm_channel_ {
	/*
	 * channel description
	 */
	void *channel_desc;
	/*
	 * media for transport, such as pipe, socket or mq
	 */
	int external_request;
	void *request_channel;
	struct _synocomm_channel_ *external_request_channel;

	int external_response;
	void *response_channel;
	struct _synocomm_channel_ *external_response_channel;
	/*
	 * interface for processing the media I/O
	 */
	struct _synocomm_channel_transport_ *transport;
	/*
	 * mutex for protection
	 */
	volatile int to_be_deleted;
	volatile int ref_count;
	SYNOCOMM_MUTEX mutex;

} SYNOCOMM_CHANNEL, *P_SYNOCOMM_CHANNEL;

#define ATOMIC_REF_COUNT_INC(p)            \
	do {                                   \
		SYNOCOMM_MUTEX_LOCK(p->mutex);     \
		p->ref_count++;                    \
		SYNOCOMM_MUTEX_UNLOCK(p->mutex);   \
	} while (0)

#define ATOMIC_REF_COUNT_DEC(p)            \
	do {                                   \
		SYNOCOMM_MUTEX_LOCK(p->mutex);     \
		p->ref_count--;                    \
		SYNOCOMM_MUTEX_UNLOCK(p->mutex);   \
	} while (0)

#define ISNULL(p) (NULL == p)

#define SZF_VAR_RUN_SYNOCOMM_ISCCORE "/var/run/isccore.pid"
#define SZF_VAR_RUN_SYNOCOMM_ISS "/var/run/iss.pid"
#define SZF_ISS_SERVER_NAME "ISS-SERVER"
#define SZF_DSM_CLIENT_NAME "DSM-CLIENT"

#ifdef __cplusplus
__SYNO_END_DECLS
#endif


#endif // _SYNO_COMM_BASE_H_
