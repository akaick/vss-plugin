#ifndef _SYNO_COMM_MSG_H_
#define _SYNO_COMM_MSG_H_

#ifndef WIN32
#include <synocomm/synocomm_types.h>
#else
#include "synocomm_types.h"
#endif

#ifdef __cplusplus
__SYNO_BEGIN_DECLS
#endif

typedef enum _synocomm_command_opcode_ {
	OPCODE_CONNECTV4     = (char)0x10,
	OPCODE_CONNACKV4     = (char)0x11,
	OPCODE_RCONNECTV4    = (char)0x12,
	OPCODE_RCONNACKV4    = (char)0x13,
	OPCODE_REGISTRY      = (char)0x14,
	OPCODE_REGISACK      = (char)0x15,
	OPCODE_UREGISTRY     = (char)0x16,
	OPCODE_UREGISACK     = (char)0x17,
	OPCODE_SENDMSG       = (char)0x20,
	OPCODE_RECVMSG       = (char)0x21,
	OPCODE_BINDSERVICE   = (char)0x30,
	OPCODE_BONDSERVICE   = (char)0x31,
	OPCODE_BANDSERVICE   = (char)0x32,
	OPCODE_UBINDSERVICE  = (char)0x33,
	OPCODE_UBONDSERVICE  = (char)0x34,
	OPCODE_UBANDSERVICE  = (char)0x35,
	OPCODE_PINGSERVICE   = (char)0x40,
	OPCODE_PONGSERVICE   = (char)0x41,
	OPCODE_PUNGSERVICE   = (char)0x42,
	OPCODE_RAPP_BROKEN   = (char)0x01,
	OPCODE_SERVICESTOP   = (char)0x50,
} SYNOCOMM_CMD_OPCODE;

typedef struct _synocomm_command_fmt_ {
	SYNOCOMM_CMD_OPCODE opcode;
	const char *fmt;
} SYNOCOMM_CMD_FORMAT, *P_SYNOCOMM_CMD_FORMAT;

typedef struct _cmd_connectv4_ {
	char	   opcode;
	char	   reserve[3];
	FIELD_IPV4 cip;
#define client_ip	cip.bits
	FIELD_IPV4 sip;
#define	server_ip	sip.bits
} CONNECTV4, *P_CONNECTV4, RCONNECTV4, *P_RCONNECTV4;

#define	CONNECT_OK		(1)
#define	CONNECT_FAIL	(-1)
typedef struct _cmd_connackv4_ {
	char	   opcode;
	char	   reserve[3];
	FIELD_IPV4 cip;
	FIELD_IPV4 sip;
	int		   result;
} CONNACKV4, *P_CONNACKV4, RCONNACKV4, *P_RCONNACKV4;

typedef struct _cmd_registry_ {
	char	opcode;
	char	reserve[3];
	char	service_name[MAX_SERVICE_NAME_LEN];
	long	pid;
} REGISTRY, *P_REGISTRY;

typedef struct _cmd_regisack_ {
	char	opcode;
	char	reserve[3];
	char	service_name[MAX_SERVICE_NAME_LEN];
	long	pid;
	FIELD_UUID	uuid_filed;
#define		regsackuuid	uuid_filed.bits
} REGISACK, *P_REGISACK, UREGISTRY, *P_UREGISTRY, UREGISACK, *P_UREGISACK;

#define MAX_COMM_CMD_SIZE	(4096)
typedef struct _synocomm_command_ {
	char		opcode;
	char		param[MAX_COMM_CMD_SIZE - 1];
} SYNOCOMM_CMD, *P_SYNOCOMM_CMD;

#define MAX_COMM_DATA_SIZE	(4096)
typedef struct _synocomm_data_packet_ {
	char    opcode;//0
	char    reserve[3];
	FIELD_UUID	from_uuid;//4
	FIELD_UUID	to_uuid;//20
	int		cmdid;//36
	int		seq;//40
	int		len;//44
	int		cksum;//48
#define		MAX_DATA_PACKETS	(MAX_COMM_DATA_SIZE - 52)
	char	data[MAX_DATA_PACKETS];//52
} SYNOCOMM_DATA, *P_SYNOCOMM_DATA;

#ifndef WIN32
#define COMM_DATA_PACKETS_SIZE(p)	\
	({\
		int ___datas = 0;\
		___datas = (p->len + ((int)p->data - (int)p));\
		___datas;\
	})
#else
static int comm_data_packets_size(P_SYNOCOMM_DATA p)
{
	return (p->len + ((int)p->data - (int)p));
}
#define COMM_DATA_PACKETS_SIZE(p) comm_data_packets_size(p)
#endif

typedef struct _synocomm_bind_ {
	char    opcode;
	char    reserve[3];
	FIELD_IPV4 cip;
	SERVICE_ID cid;
	FIELD_IPV4 sip;
	SERVICE_ID sid;
} SYNOCOMM_BIND, *P_SYNOCOMM_BIND, SYNOCOMM_BOND, *P_SYNOCOMM_BOND, SYNOCOMM_UBIND, *P_SYNOCOMM_UBIND, SYNOCOMM_UBOND, *P_SYNOCOMM_UBOND, SYNOCOMM_BAND, *P_SYNOCOMM_BAND, SYNOCOMM_UBAND, *P_SYNOCOMM_UBAND;

#define PING_LOCAL_TYPE  (0x00001000)
#define PING_REMOTE_TYPE (0x00002000)
#define PING_CLIENT_ENG  (0x00000010)
#define PING_CLIENT_APP  (0x00000001)
#define PING_SERVER_ENG  (0x00000020)
#define PING_SERVER_APP  (0x00000002)

typedef struct _synocomm_ping_ {
	char    opcode;
	char    reserve[3];
	FIELD_IPV4 cip;
	SERVICE_ID cid;
	FIELD_IPV4 sip;
	SERVICE_ID sid;
	int     status;
} SYNOCOMM_PING, *P_SYNOCOMM_PING, SYNOCOMM_PONG, *P_SYNOCOMM_PONG, SYNOCOMM_PUNG, *P_SYNOCOMM_PUNG;

typedef struct _synocomm_appnotify_ {
	char    opcode;
	char    reserve[3];
	FIELD_IPV4 sip;
	SERVICE_ID sid;
} SYNOCOMM_APPNOTIFY, *P_SYNOCOMM_APPNOTIFY;

typedef struct _synocomm_service_notify_ {
	char    opcode;
	char    reserve[3];
	FIELD_IPV4 sip;
	SERVICE_ID sid;
} SYNOCOMM_SERVICE_NOTIFY, *P_SYNOCOMM_SERVICE_NOTIFY;

extern P_SYNOCOMM_DATA CreateSendDataPacket(P_SYNOCOMM_DATA _p, FIELD_UUID *from, FIELD_UUID *to, const char *s, int l);
extern P_SYNOCOMM_DATA CreateRecvDataPacket(P_SYNOCOMM_DATA _p, FIELD_UUID *from, FIELD_UUID *to, const char *s, int l);
extern int ExtractFromUUIDByDataPacket(P_SYNOCOMM_DATA p, FIELD_UUID *from);
extern int ExtractToUUIDByDataPacket(P_SYNOCOMM_DATA p, FIELD_UUID *to);
extern int FillInFromUUID(P_SYNOCOMM_DATA p, FIELD_UUID *from);
extern int FillInToUUID(P_SYNOCOMM_DATA p, FIELD_UUID *to);

extern SYNOCOMM_CMD_FORMAT cmdset[];

#define FORMAT_CMD(opcode) ({ \
	int ___idx = 0;\
	P_SYNOCOMM_CMD_FORMAT ___fmt = NULL;\
	___fmt = &cmdset[___idx];	\
	___fmt;\
})
/*
#define FORMAT_CMD(opcode) ({ \
	int ___idx = 0;\
	P_SYNOCOMM_CMD_FORMAT ___fmt = NULL;\
	while(NULL != cmdset[___idx].fmt) {\
		if (cmdset[___idx].opcode == (opcode)) { \
			___fmt = &cmdset[___idx];	\
			break;	\
		}\
		___idx++;\
	}\
	___fmt;\
})
*/

#ifdef __cplusplus
__SYNO_END_DECLS
#endif

#endif
