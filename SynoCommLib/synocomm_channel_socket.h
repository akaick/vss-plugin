#ifndef _SYNO_COMM_CH_SOCKET_H_
#define _SYNO_COMM_CH_SOCKET_H_

#ifndef WIN32
#include <synocomm/synocomm_base.h>
#include <synocomm/synocomm_transport.h>
typedef int SYNOCOMM_SOCKET_FD_TYPE;
#else
#include "synocomm_base.h"
#include "synocomm_transport.h"
typedef SOCKET SYNOCOMM_SOCKET_FD_TYPE;
#endif

#ifdef __cplusplus
__SYNO_BEGIN_DECLS
#endif

#define SYNOCOMM_CHAN_SOCKET_SVR_PORT 3262
#define SYNOCOMM_CHAN_SOCKET_CLT_PORT 3262

extern SYNOCOMM_TRANSPORT socket_channel_transport;
extern BOOL IsSocketAlive(struct _synocomm_channel_ *ch);
extern void __synocomm_stop_server(struct _synocomm_channel_ *ch);
#ifdef WIN32
extern int InitWinWSA(void);
#endif

#ifdef __cplusplus
__SYNO_END_DECLS
#endif

#endif // _SYNO_COMM_CH_SOCKET_H_
