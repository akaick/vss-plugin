#ifndef WIN32
#include <synocomm/synocomm_base.h>
#include <synocomm/synocomm_channel.h>
#include <synocomm/synocomm_transport.h>
#include <stdlib.h>
#include <search.h>
#else
#include "synocomm_base.h"
#include "synocomm_channel.h"
#include "synocomm_transport.h"
#include "stdlib.h"
#include "search.h"
#endif

SYNOCOMM_CHANNEL_SET *pChannelSet = NULL;

static P_SYNOCOMM_CHANNEL_DESC _get_channel_desc(SYNOCOMM_CHANNEL *pch)
{
	return (P_SYNOCOMM_CHANNEL_DESC)(pch->channel_desc);
}

static int _add_channel_tail(SYNOCOMM_CHANNEL_SET *pCHSet, SYNOCOMM_CHANNEL *pch, int listtype)
{
	int err = -1;
	P_SYNOCOMM_CHANNEL_DESC pdesc = _get_channel_desc(pch);

	pdesc->listtype = listtype;

	SYNOCOMM_MUTEX_LOCK(pCHSet->mutex);
	switch(listtype) {
		case INT_CH:
			COMM_LIST_ADD_TAIL(&pdesc->chlist, &pCHSet->internals);
			pCHSet->icount++;
			break;
		case EXTI_CH:
			COMM_LIST_ADD_TAIL(&pdesc->chlist, &pCHSet->extins);
			pCHSet->eicount++;
			break;
		case EXTO_CH:
			COMM_LIST_ADD_TAIL(&pdesc->chlist, &pCHSet->extouts);
			pCHSet->eocount++;
			break;
		case PROC_CH:
			COMM_LIST_ADD_TAIL(&pdesc->chlist, &pCHSet->processes);
			pCHSet->pcount++;
			break;
		default:
			pdesc->listtype = UNKNOW_CH;
			goto ERR;
	}

	err = 0;
ERR:
	SYNOCOMM_MUTEX_UNLOCK(pCHSet->mutex);
	return err;
}

static int _remove_channel(SYNOCOMM_CHANNEL_SET *pCHSet, SYNOCOMM_CHANNEL *pch)
{
	int err = -1;
	P_SYNOCOMM_CHANNEL_DESC pDesc = _get_channel_desc(pch);

	SYNOCOMM_MUTEX_LOCK(pCHSet->mutex);
	switch(pDesc->listtype) {
		case INT_CH:
			COMM_LIST_DEL(&pDesc->chlist);
			pCHSet->icount--;
			break;
		case EXTI_CH:
			COMM_LIST_DEL(&pDesc->chlist);
			pCHSet->eicount--;
			break;
		case EXTO_CH:
			COMM_LIST_DEL(&pDesc->chlist);
			pCHSet->eocount--;
			break;
		case PROC_CH:
			COMM_LIST_DEL(&pDesc->chlist);
			pCHSet->pcount--;
			break;
		default:
			goto ERR;
	}

	err = 0;
ERR:
	SYNOCOMM_MUTEX_UNLOCK(pCHSet->mutex);
	return err;
}

static SYNOCOMM_CHANNEL *_get_channel_by_uuid(SYNOCOMM_CHANNEL_SET *pCHSet, FIELD_UUID *ch_uuid, int listtype)
{
	SYNOCOMM_CHANNEL *pCh = NULL;
	P_SYNOCOMM_LINKEDLIST plist = NULL;
	P_SYNOCOMM_CHANNEL_DESC pdesc = NULL;

	SYNOCOMM_MUTEX_LOCK(pCHSet->mutex);
	switch (listtype) {
		case INT_CH:
			plist = &pCHSet->internals;
			break;
		case EXTI_CH:
			plist = &pCHSet->extins;
			break;
		case EXTO_CH:
			plist = &pCHSet->extouts;
			break;
		case PROC_CH:
			plist = &pCHSet->processes;
			break;
		default:
			goto ERR;
	}

	COMM_LIST_FOR_EACH_ENTRY_BEGIN(pdesc, SYNOCOMM_CHANNEL_DESC, plist, chlist)
	COMM_LIST_FOR_EACH_ENTRY(pdesc, SYNOCOMM_CHANNEL_DESC, plist, chlist) {
		if (0 == memcmp(pdesc->serviceid.serviceuuid, ch_uuid, sizeof(FIELD_UUID))) {
			pCh = (SYNOCOMM_CHANNEL *)pdesc->channel;
			ATOMIC_REF_COUNT_INC(pCh);
			goto ERR;
		}
	}
	COMM_LIST_FOR_EACH_ENTRY_END

	pCh = NULL;
ERR:
	SYNOCOMM_MUTEX_UNLOCK(pCHSet->mutex);
	return pCh;
}

static SYNOCOMM_CHANNEL *_get_channel_by_name(SYNOCOMM_CHANNEL_SET *pCHSet, char *service_name, int listtype)
{
	SYNOCOMM_CHANNEL *pCh = NULL;
	P_SYNOCOMM_LINKEDLIST plist = NULL;
	P_SYNOCOMM_CHANNEL_DESC pdesc = NULL;

	SYNOCOMM_MUTEX_LOCK(pCHSet->mutex);
	switch (listtype) {
		case INT_CH:
			plist = &pCHSet->internals;
			break;
		case EXTI_CH:
			plist = &pCHSet->extins;
			break;
		case EXTO_CH:
			plist = &pCHSet->extouts;
			break;
		case PROC_CH:
			plist = &pCHSet->processes;
			break;
		default:
			goto ERR;
	}

	COMM_LIST_FOR_EACH_ENTRY_BEGIN(pdesc, SYNOCOMM_CHANNEL_DESC, plist, chlist)
	COMM_LIST_FOR_EACH_ENTRY(pdesc, SYNOCOMM_CHANNEL_DESC, plist, chlist) {
		if (0 == strncmp(pdesc->serviceid.service_name, service_name, sizeof(pdesc->serviceid.service_name))) {
			pCh = (SYNOCOMM_CHANNEL *)pdesc->channel;
			ATOMIC_REF_COUNT_INC(pCh);
			goto ERR;
		}
	}
	COMM_LIST_FOR_EACH_ENTRY_END

	pCh = NULL;
ERR:
	SYNOCOMM_MUTEX_UNLOCK(pCHSet->mutex);
	return pCh;
}

/*
 * external interfaces
 */
int AddInternalChannel(SYNOCOMM_CHANNEL_SET *pCHSet, SYNOCOMM_CHANNEL *pch)
{
	return _add_channel_tail(pCHSet, pch, INT_CH);
}

int AddExternalInChannel(SYNOCOMM_CHANNEL_SET *pCHSet, SYNOCOMM_CHANNEL *pch)
{
	return _add_channel_tail(pCHSet, pch, EXTI_CH);
}

int AddExternalOutChannel(SYNOCOMM_CHANNEL_SET *pCHSet, SYNOCOMM_CHANNEL *pch)
{
	return _add_channel_tail(pCHSet, pch, EXTO_CH);
}

int AddProcessChannel(SYNOCOMM_CHANNEL_SET *pCHSet, SYNOCOMM_CHANNEL *pch)
{
	return _add_channel_tail(pCHSet, pch, PROC_CH);
}

int RemoveChannel(SYNOCOMM_CHANNEL_SET *pCHSet, SYNOCOMM_CHANNEL *pch)
{
	return _remove_channel(pCHSet, pch);
}

SYNOCOMM_CHANNEL *GetInternalChannel(SYNOCOMM_CHANNEL_SET *pCHSet, char *service_name)
{
	return _get_channel_by_name(pCHSet, service_name, INT_CH);
}

SYNOCOMM_CHANNEL *GetExternalInChannel(SYNOCOMM_CHANNEL_SET *pCHSet, char *service_name)
{
	return _get_channel_by_name(pCHSet, service_name, EXTI_CH);
}

SYNOCOMM_CHANNEL *GetExternalOutChannel(SYNOCOMM_CHANNEL_SET *pCHSet, char *service_name)
{
	return _get_channel_by_name(pCHSet, service_name, EXTO_CH);
}

SYNOCOMM_CHANNEL *GetProcessChannel(SYNOCOMM_CHANNEL_SET *pCHSet, char *service_name)
{
	return _get_channel_by_name(pCHSet, service_name, PROC_CH);
}

SYNOCOMM_CHANNEL *GetExternalOutChannelByUUID(SYNOCOMM_CHANNEL_SET *pCHSet, FIELD_UUID *ch_uuid)
{
	return _get_channel_by_uuid(pCHSet, ch_uuid, EXTO_CH);
}

SYNOCOMM_CHANNEL *GetProcessChannelByUUID(SYNOCOMM_CHANNEL_SET *pCHSet, FIELD_UUID *ch_uuid)
{
	return _get_channel_by_uuid(pCHSet, ch_uuid, PROC_CH);
}

static SYNOCOMM_CHANNEL_SET *CreateChannelSet()
{
	SYNOCOMM_CHANNEL_SET *pCHSet = (SYNOCOMM_CHANNEL_SET *)malloc(sizeof(SYNOCOMM_CHANNEL_SET));
	memset(pCHSet, 0, sizeof(SYNOCOMM_CHANNEL_SET));

	MSG("Create ChannelSet as %p\n", pCHSet);
	pCHSet->icount = 0;
	COMM_LIST_HEAD_INIT(&pCHSet->internals);
	pCHSet->eocount = 0;
	COMM_LIST_HEAD_INIT(&pCHSet->extouts);
	pCHSet->eicount = 0;
	COMM_LIST_HEAD_INIT(&pCHSet->extins);
	pCHSet->pcount = 0;
	COMM_LIST_HEAD_INIT(&pCHSet->processes);
	SYNOCOMM_MUTEX_INIT(pCHSet->mutex);

	return pCHSet;
}

void DestroyChannelSet(SYNOCOMM_CHANNEL_SET *pCHSet)
{
	if (NULL == pCHSet) {
		return;
	}

	SYNOCOMM_MUTEX_LOCK(pCHSet->mutex);
	if (pCHSet->icount) {
		;
	}
	if (pCHSet->eocount) {
		;
	}
	if (pCHSet->eicount) {
		;
	}
	if (pCHSet->pcount) {
		;
	}
	SYNOCOMM_MUTEX_UNLOCK(pCHSet->mutex);

}

int InitChannelSet()
{
	int err = -1;
	if (NULL == pChannelSet) {
		pChannelSet = CreateChannelSet();
	} else {
		MSG("pChannelSet is NOT empty\n")
		goto ERR;
	}

	err = 0;
ERR:
	return err;
}

static void _list_all_channel(SYNOCOMM_CHANNEL_SET *pCHSet, int listtype)
{
	SYNOCOMM_CHANNEL *pch = NULL;
	P_SYNOCOMM_LINKEDLIST plist = NULL;
	P_SYNOCOMM_CHANNEL_DESC pdesc = NULL;

	MSG("List all channel, listtype %d\n", listtype);
	SYNOCOMM_MUTEX_LOCK(pCHSet->mutex);
	switch (listtype) {
		case INT_CH:
			plist = &pCHSet->internals;
			break;
		case EXTI_CH:
			plist = &pCHSet->extins;
			break;
		case EXTO_CH:
			plist = &pCHSet->extouts;
			break;
		case PROC_CH:
			plist = &pCHSet->processes;
			break;
		default:
			;
	}

	COMM_LIST_FOR_EACH_ENTRY_BEGIN(pdesc, SYNOCOMM_CHANNEL_DESC, plist, chlist)
	COMM_LIST_FOR_EACH_ENTRY(pdesc, SYNOCOMM_CHANNEL_DESC, plist, chlist) {
		pch = (SYNOCOMM_CHANNEL *)pdesc->channel;
		MSG("Found, pch %p, %s\n", pch, pdesc->serviceid.service_name);
	}
	COMM_LIST_FOR_EACH_ENTRY_END
	SYNOCOMM_MUTEX_UNLOCK(pCHSet->mutex);
}

void ListExternalInChannel(SYNOCOMM_CHANNEL_SET *pCHSet)
{
#ifndef WIN32
	return _list_all_channel(pCHSet, EXTI_CH);
#else
	_list_all_channel(pCHSet, EXTI_CH);
#endif
}

void ListExternalOutChannel(SYNOCOMM_CHANNEL_SET *pCHSet)
{
#ifndef WIN32
	return _list_all_channel(pCHSet, EXTO_CH);
#else
	_list_all_channel(pCHSet, EXTO_CH);
#endif
}

void ListInternalChannel(SYNOCOMM_CHANNEL_SET *pCHSet)
{
#ifndef WIN32
	return _list_all_channel(pCHSet, INT_CH);
#else
	_list_all_channel(pCHSet, INT_CH);
#endif
}

void ListProcessChannel(SYNOCOMM_CHANNEL_SET *pCHSet)
{
#ifndef WIN32
	return _list_all_channel(pCHSet, PROC_CH);
#else
	_list_all_channel(pCHSet, PROC_CH);
#endif
}

void ChannelConnectionCheck(SYNOCOMM_CHANNEL_SET *pCHSet)
{
	//check each host connection every 10 seconds
	while (1) {
		MSG("*\n");
		ListExternalInChannel(pChannelSet);
		MSG("**\n");
		ListExternalOutChannel(pChannelSet);
		MSG("***\n");
		ListInternalChannel(pChannelSet);
		MSG("****\n");
		ListProcessChannel(pChannelSet);
		break;
	}
}

static void _broadcast_all_channel(SYNOCOMM_CHANNEL_SET *pCHSet, int listtype, const char *pMsg, int length)
{
	SYNOCOMM_CHANNEL *pCh = NULL;
	P_SYNOCOMM_LINKEDLIST plist = NULL;
	P_SYNOCOMM_CHANNEL_DESC pdesc = NULL;

	switch (listtype) {
		case INT_CH:
			plist = &pCHSet->internals;
			break;
		case EXTI_CH:
			plist = &pCHSet->extins;
			break;
		case EXTO_CH:
			plist = &pCHSet->extouts;
			break;
		case PROC_CH:
			plist = &pCHSet->processes;
			break;
		default:
			;
	}

	COMM_LIST_FOR_EACH_ENTRY_BEGIN(pdesc, SYNOCOMM_CHANNEL_DESC, plist, chlist)
	COMM_LIST_FOR_EACH_ENTRY(pdesc, SYNOCOMM_CHANNEL_DESC, plist, chlist) {
		pCh = (SYNOCOMM_CHANNEL *)pdesc->channel;
		MSG("Broacast opcode [0x%2x], pCh %p, %s\n", pMsg[0], pCh, pdesc->serviceid.service_name);
		SENDMSG(pCh, pMsg, length);
	}
	COMM_LIST_FOR_EACH_ENTRY_END
}

void ChannelBroadCast(SYNOCOMM_CHANNEL_SET *pCHSet, const char *pMsg, int length)
{
	_broadcast_all_channel(pCHSet, PROC_CH, pMsg, length);
}

void ChannelPut(SYNOCOMM_CHANNEL *pCh)
{
	if (pCh) {
		SYNOCOMM_MUTEX_LOCK(pCh->mutex);
		pCh->ref_count--;
		SYNOCOMM_MUTEX_UNLOCK(pCh->mutex);
	}

	if (0 == pCh->ref_count &&
		0 != pCh->to_be_deleted) {
		//ReleaseApp(pApp);
		//TODO add Release Channel(pCh);
	}
}

void ChannelFree(SYNOCOMM_CHANNEL *pCh)
{
	if (pCh) {
		SYNOCOMM_MUTEX_LOCK(pCh->mutex);
		pCh->to_be_deleted = 1;
		SYNOCOMM_MUTEX_UNLOCK(pCh->mutex);
	}

	ChannelPut(pCh);
}
