#ifndef WIN32
#include <stdlib.h>
#include <search.h>
#include <synocomm/synocomm_base.h>
#include <synocomm/synocomm_app.h>
#include <synocomm/synocomm_channel.h>
#include <synocomm/synocomm_channel_manage.h>
#include <synocomm/synocomm_channel_control.h>
#include <synocomm/synocomm_channel_socket.h>
#else
#include "stdafx.h"
#include "stdlib.h"
#include "stddef.h"
#include "synocomm_base.h"
#include "synocomm_app.h"
#include "synocomm_channel.h"
#include "synocomm_channel_manage.h"
#include "synocomm_channel_control.h"
#include "synocomm_channel_socket.h"
#endif

extern SYNOCOMM_CHANNEL_SET *pChannelSet;

//pRq is a media that will used to setup internal request
SYNOCOMM_CHANNEL *DupSocketRequestRxChannel(char *ip, char *hostname, void *pRq)
{
	char service_name[MAX_SERVICE_NAME_LEN];
	SYNOCOMM_CHANNEL *pSRqRx_ = NULL;
	SYNOCOMM_CHANNEL *pPRqSn = NULL;

	SYNOCOMM_HOST *pRH = SearchRemoteHost(ip);
	
	if (NULL == pRH) {
		if (0 != AddRemoteHost(ip, hostname)) {
			goto ERR;
		}

		pRH = SearchRemoteHost(ip);
	}

	pPRqSn = GetInternalChannel(pChannelSet, "PRqSn");
	if (NULL == pPRqSn) {
		MSG("No Local Request Sender (PRqSn) exist\n");
		goto ERR;
	}
		
	snprintf(service_name, MAX_SERVICE_NAME_LEN, "SRqRx_%s", ip);

	pSRqRx_ = GetExternalInChannel(pChannelSet, service_name);

	if (pSRqRx_) {
		MSG("Channel %s found, pSRqRx_ %p\n", service_name, pSRqRx_);
		ChannelPut(pSRqRx_);
		goto ERR;
	}

	pSRqRx_ = EngineSocketRequestReceiver(pRH, service_name, pPRqSn);

	if (NULL == pSRqRx_) {
		MSG("Create SRqRx fail\n");
		goto ERR;
	}

	MSG("Create Channel %s, %p\n", service_name, pSRqRx_);
	SetupEngineSocketRequestReceiver(pSRqRx_, pRq);

	MSG("Add ExternalIn Channel %s, %p\n", service_name, pSRqRx_);
	AddExternalInChannel(pChannelSet, pSRqRx_);
ERR:
	return pSRqRx_;
}

int HandleAppRegister(void *pMsg)
{
	int err = -1;

	SYNOCOMM_APP *pApp = NULL;
	SYNOCOMM_CHANNEL *pAppCh = NULL;
	P_REGISACK pRegAck = NULL;
	SYNOCOMM_HOST *pH = NULL;

	P_REGISTRY pReg = (P_REGISTRY)pMsg;

	int pid = pReg->pid;
	char *service_name = pReg->service_name;

	MSG("service_name %s, pid %d\n", service_name, pid);
	pH = SearchLocalHost(NULL);
	if (NULL == pH) {
		goto ERR;
	}

	pApp = CreateLocalAppServer(service_name, pid);
	LocalHostAdoptApp(pH, pApp);

	pAppCh = Engine2AppPipeResponseSender(pH, service_name, pid);
	MSG("Add Process Channel %s, %p\n", service_name, pAppCh);
	AddProcessChannel(pChannelSet, pAppCh);

	BindAppChannel(pApp, pAppCh);

	pRegAck = CreateRegisAckMsg(pApp);
	if (NULL == pRegAck) {
		goto ERR;
	}

	AppSendControl(pApp, (char *)pRegAck, sizeof(REGISACK));
	AppPut(pApp);

	err = 0;
ERR:
	if (err) {
		if (pAppCh) {
			free(pAppCh);
		}
		if (pApp) {
			free(pApp);
		}
	}
	if (pRegAck) {
		free(pRegAck);
	}
	return err;
}

int HandleAppURegister(void *pMsg)
{
	int err = -1;
	P_SERVICE_ID pSID = NULL;
	SYNOCOMM_APP *pApp = NULL;
	P_UREGISACK pURegAck = NULL;
	SYNOCOMM_HOST *pH = NULL;

	P_UREGISTRY pUReg = (P_UREGISTRY)pMsg;

	int pid = pUReg->pid;
	char *service_name = pUReg->service_name;

	MSG("service_name %s, pid %d\n", service_name, pid);
	pH = SearchLocalHost(NULL);
	if (NULL == pH) {
		goto ERR;
	}

	if (NULL == (pSID = GenServiceID(service_name, pid, &pUReg->uuid_filed))) {
		goto ERR;
	}

	if (NULL == (pApp = SearchAppInHostByIDExt(pH, pSID))) {
		goto ERR;
	}

	if (NULL == (pURegAck = CreateURegisAckMsg(pApp))) {
		goto ERR;
	}

	AppSendControl(pApp, (char *)pURegAck, sizeof(UREGISACK));

	//1. Remove app from host if necessary
	//2. Remove app channel from channel set
	//3. Remove all routing info of app
	//and call CloseAppChannel to invoke channel destructor
	//at last free the app structure
#if 1
	AppFree(pApp);
#else
	ReleaseApp(pApp);
#endif
	//release app
	//FIXME
	//Need to remove host if there's no app associated with it ?

	err = 0;
ERR:
	if (pSID) {
		free(pSID);
	}
	if (pURegAck) {
		free(pURegAck);
	}
	return err;
}

int HandleRConnectV4(void *pMsg)
{
	int err = -1;

	char *pCliIpStr = NULL;
	char *pSrvIpStr = NULL;
	char szSRqSn[MAX_SERVICE_NAME_LEN] = {0};

	P_CONNECTV4 pRConV4 = (P_RCONNECTV4)pMsg;
	P_CONNECTV4 pConV4 = NULL;
	SYNOCOMM_HOST *pRH = NULL;
	SYNOCOMM_HOST *pH = NULL;
	SYNOCOMM_CHANNEL *pSRqSn_ = NULL;


	Ipv42Str(&pRConV4->cip, &pCliIpStr);
	Ipv42Str(&pRConV4->sip, &pSrvIpStr);

	pH = SearchLocalHost(pCliIpStr);

	//Client is one localhost
	MSG("################pH %p, pCliIpStr %s\n", pH, pCliIpStr);
	if (0 == memcmp(&pH->hip, &pRConV4->cip, sizeof(pH->hip))) {
		MSG("Client Ip %s Match Local Host, And Want to Connect Server Ip %s\n", pCliIpStr, pSrvIpStr);

		pRH = SearchRemoteHost(pSrvIpStr);
		if (NULL == pRH) {
			AddRemoteHost(pSrvIpStr, pSrvIpStr);
			MSG("########################Add %s As RemoteHost\n", pSrvIpStr);
		}
		pRH = SearchRemoteHost(pSrvIpStr);

		snprintf(szSRqSn, MAX_SERVICE_NAME_LEN, "SRqSn_%s", pSrvIpStr);//"SRqSn_192.168.16.191"
		pSRqSn_ = GetExternalOutChannel(pChannelSet, szSRqSn);

		if (pSRqSn_) {
			struct _synocomm_ping_ *pPing = (struct _synocomm_ping_ *)malloc(sizeof(struct _synocomm_ping_));
			memset(pPing, 0, sizeof(struct _synocomm_ping_));
			pPing->opcode = OPCODE_PINGSERVICE;

			MSG("***************************** Found %s\n", szSRqSn);

			if (0 > SENDMSG(pSRqSn_, (char *)pPing, sizeof(struct _synocomm_ping_))) {
				MSG("***************************** Fail Send msg to %s\n", szSRqSn);
				TRANSPORT(pSRqSn_)->stop_service(pSRqSn_);
				ChannelFree(pSRqSn_);
				if (NULL == (pSRqSn_ = EngineSocketRequestSender(pRH, szSRqSn))) {
					MSG("Fail to re-create %s\n", szSRqSn);
					goto ERR;
				}
				AddExternalOutChannel(pChannelSet, pSRqSn_);
			} else {
				MSG("********************************** pSRqSn_ %p found\n", pSRqSn_);
				ChannelPut(pSRqSn_);
			}

		} else {
			if (NULL == (pSRqSn_ = EngineSocketRequestSender(pRH, szSRqSn))) {
				MSG("Fail to create %s\n", szSRqSn);
				goto ERR;
			}
			MSG("Create SRqSn %p\n", pSRqSn_);
			AddExternalOutChannel(pChannelSet, pSRqSn_);
		}

		sleep(1);
		pSRqSn_ = GetExternalOutChannel(pChannelSet, szSRqSn);
		if (pSRqSn_) {
			//Send Connect control to remote
			pConV4 = CreateConnectV4Msg(pRH, pH);
			if (0 > SENDMSG(pSRqSn_, (char *)pConV4, sizeof(CONNECTV4))) {
				MSG("Fail to send pConV4 to %s", szSRqSn);
				TRANSPORT(pSRqSn_)->stop_service(pSRqSn_);
				ChannelFree(pSRqSn_);
			} else {
				MSG("Send pConV4 to %s successfully", szSRqSn);
				ChannelPut(pSRqSn_);
				err = 0;
			}
		} else {
			MSG("%s does NOT exist\n", szSRqSn);
		}

	} else {
		MSG("Client Ip %s NOT Match Local Host\n", pCliIpStr);
	}

ERR:
	return err;
}

int HandleConnectV4(void *pMsg)
{
	int err = -1;

	char *pCliIpStr = NULL;
	char *pSrvIpStr = NULL;
	SYNOCOMM_HOST *pH = NULL;

	P_CONNECTV4 pConV4 = (P_CONNECTV4)pMsg;
	Ipv42Str(&pConV4->cip, &pCliIpStr);
	Ipv42Str(&pConV4->sip, &pSrvIpStr);
	pH = SearchLocalHost(pCliIpStr);
	if (NULL == pH) {
		MSG("Client Ip %s NOT Match Local Host\n", pCliIpStr);
		goto ERR;
	}

	//Client is one localhost
	if (0 == memcmp(&pH->hip, &pConV4->cip, sizeof(pH->hip))) {

		SYNOCOMM_HOST *pRH = NULL;
		char szSRqSn[MAX_SERVICE_NAME_LEN] = {0};
		SYNOCOMM_CHANNEL *pSRqSn = NULL;

		pRH = SearchRemoteHost(pSrvIpStr);
		if (NULL == pRH) {
			AddRemoteHost(pSrvIpStr, pSrvIpStr);
			pRH = SearchRemoteHost(pSrvIpStr);
		}

		snprintf(szSRqSn, MAX_SERVICE_NAME_LEN, "SRqSn_%s", pSrvIpStr);//"SRqSn_192.168.16.191"
		pSRqSn = GetExternalOutChannel(pChannelSet, szSRqSn);

		if (pSRqSn) {
			struct _synocomm_ping_ *pPing = (struct _synocomm_ping_ *)malloc(sizeof(struct _synocomm_ping_));
			memset(pPing, 0, sizeof(struct _synocomm_ping_));
			pPing->opcode = OPCODE_PINGSERVICE;

			MSG("***************************** Found %s\n", szSRqSn);

			if (0 > SENDMSG(pSRqSn, (char *)pPing, sizeof(struct _synocomm_ping_))) {
				MSG("***************************** Fail Send msg to %s\n", szSRqSn);
				TRANSPORT(pSRqSn)->stop_service(pSRqSn);
				ChannelFree(pSRqSn);
				if (NULL == (pSRqSn = EngineSocketRequestSender(pRH, szSRqSn))) {
					MSG("Fail to re-create %s\n", szSRqSn);
					goto ERR;
				}
				AddExternalOutChannel(pChannelSet, pSRqSn);
			} else {
				ChannelPut(pSRqSn);
				MSG("********************************** pSRqSn %p found\n", pSRqSn);
			}

		} else {
			if (NULL == (pSRqSn = EngineSocketRequestSender(pRH, szSRqSn))) {
				MSG("Fail to create %s\n", szSRqSn);
				goto ERR;
			}
			MSG("Create pSRqSn %p, %s\n", pSRqSn, szSRqSn);
			AddExternalOutChannel(pChannelSet, pSRqSn);
		}
	}

	err = 0;

ERR:
	return err;
}

static int NotifyHost(SYNOCOMM_HOST *pH, SYNOCOMM_APPNOTIFY *pAppNty)
{
	int err = 0;
	SYNOCOMM_APP *pApp = NULL;
	COMM_LIST_FOR_EACH_ENTRY_BEGIN(pApp, SYNOCOMM_APP, &pH->alist, alist)
	COMM_LIST_FOR_EACH_ENTRY(pApp, SYNOCOMM_APP, &pH->alist, alist) {
		if (0 > AppSendControl(pApp, (char *)pAppNty, sizeof(SYNOCOMM_APPNOTIFY))) {
			MSG("Fail to notify App %s\n", pApp->serviceid.serviceuuid);
			err = -1;
		} else {
			MSG("Notify App %s successfully\n", pApp->serviceid.serviceuuid);
		}
	}
	COMM_LIST_FOR_EACH_ENTRY_END

	return err;
}

static int NotifyHostSet(int host_type, SYNOCOMM_APPNOTIFY *pAppNty)
{
	int err = -1;
	extern SYNOCOMM_HOST_SET *pLocalHostSet;
	extern SYNOCOMM_HOST_SET *pRemoteHostSet;
	SYNOCOMM_HOST_SET *pHS = NULL;
	SYNOCOMM_HOST *pH = NULL;

	if (HOST_LOCAL != host_type &&
		HOST_REMOTE != host_type) {
		goto ERR;
	}

	pHS = (HOST_LOCAL == host_type) ? pLocalHostSet : pRemoteHostSet;

	COMM_LIST_FOR_EACH_ENTRY_BEGIN(pH, SYNOCOMM_HOST, &pHS->hlist, hlist)
	COMM_LIST_FOR_EACH_ENTRY(pH, SYNOCOMM_HOST, &pHS->hlist, hlist) {
		MSG("***** LOCAL ***** Host pH %p, ip %s found\n", pH, pH->hostname);
		NotifyHost(pH, pAppNty);
	}
	COMM_LIST_FOR_EACH_ENTRY_END

	err = 0;
ERR:
	return err;
}

static int RemoteAppSendErrorHandle(SYNOCOMM_APP *pApp)
{
	extern SYNOCOMM_CHANNEL_SET *pChannelSet;
	SYNOCOMM_APP *pRApp = NULL, *pTmpApp = NULL;
	SYNOCOMM_HOST *pRH = pApp->host;

	NotifyHostSet(HOST_LOCAL, CreateAppBroken(&pRH->hip, &pApp->serviceid));

	COMM_LIST_FOR_EACH_ENTRY_BEGIN(pRApp, SYNOCOMM_APP, &pRH->alist, alist)
		COMM_LIST_FOR_EACH_ENTRY(pRApp, SYNOCOMM_APP, &pRH->alist, alist) {
			MSG("pRH %s has %d apps, pRApp %s\n", pRH->hostname, pRH->acount, pRApp->serviceid.serviceuuid);
		}
	COMM_LIST_FOR_EACH_ENTRY_END

	COMM_LIST_FOR_EACH_ENTRY_BEGIN(pRApp, SYNOCOMM_APP, &pRH->alist, alist)
		COMM_LIST_FOR_EACH_ENTRY_SAFE(pRApp, pTmpApp, SYNOCOMM_APP, &pRH->alist, alist) {
			MSG("Release pRApp %s from host %s\n", pRApp->serviceid.serviceuuid, pRH->hostname);
			UPDATE_DEL(&pRApp->alist, pRH->acount);
			pRApp->host = NULL;
			//FIXME Workaround, just notify not destroy the socket
			pRApp->channel = NULL;
			ReleaseApp(pRApp);
		}
	COMM_LIST_FOR_EACH_ENTRY_END
	MSG("Release pApp %s from host %s\n", pApp->serviceid.serviceuuid, pRH->hostname);
	RemoveHost(pRH);

	return 0;
}

int HandleSendMsg(void *pMsg)
{
	int err = -1;
	BOOL blRemote = FALSE;

	int plen = 0;
	SYNOCOMM_APP *pApp = NULL;

	P_SYNOCOMM_DATA pData = (P_SYNOCOMM_DATA)pMsg;

	MSG("From uuid %s to uuid %s\n\t-%s-\n", pData->from_uuid.bits, pData->to_uuid.bits, pData->data);

	if ((pApp = (SYNOCOMM_APP *)SearchAppInRemoteHostSetByUUID(&pData->to_uuid))) {
		MSG("FOUND Rmote App %p\n", pApp);
		UpdateRemoteChannelInRemoteApp(pApp);
		blRemote = TRUE;
	} else if ((pApp = (SYNOCOMM_APP *)SearchAppInLocalHostSetByUUID(&pData->to_uuid))) {
		MSG("FOUND Local App %p\n", pApp);
	} else {
		MSG("No Routing Rule\n");
		goto ERR;
	}

	plen = COMM_DATA_PACKETS_SIZE(pData);

	pData->opcode = OPCODE_RECVMSG;

	if (0 < AppSendControl(pApp, (char *)pData, plen)) {
		err = 0;
	} else {
		if (blRemote) {
			RemoteAppSendErrorHandle(pApp);
		} else {
			MSG("LocalAppSendErrorHandle Not implement yet\n");
		}
	}

ERR:
	AppPut(pApp);
	return err;
}

int HandleRecvMsg(void *pMsg)
{
	int err = -1;
	int plen = 0;

	SYNOCOMM_APP *pApp = NULL;

	P_SYNOCOMM_DATA pData = (P_SYNOCOMM_DATA)pMsg;

	MSG("From uuid %s to uuid %s\n", pData->from_uuid.bits, pData->to_uuid.bits);

	pApp = (SYNOCOMM_APP *)SearchAppInLocalHostSetByUUID(&pData->to_uuid);

	if (NULL == pApp) {
		MSG("No Routing Rule\n");
		goto ERR;
	}
	MSG("FOUND Local pApp %p\n", pApp);

	plen = COMM_DATA_PACKETS_SIZE(pData);

	if (0 < AppSendControl(pApp, (char *)pData, plen)) {
		err = 0;
	}

ERR:
	AppPut(pApp);
	return err;
}

static int IsLocalService(const char *fIpStr, const char *tIpStr)
{
	return (0 == strcmp(fIpStr, tIpStr));
}

static int HandleLocalBind(const char *fIpStr, SERVICE_ID *cid, const char *tIpStr, SERVICE_ID *sid)
{
	int err = -1;
	SYNOCOMM_HOST *pH = NULL;
	SYNOCOMM_HOST *pRH = NULL;
	SYNOCOMM_APP *pAppServer = NULL;
	SYNOCOMM_APP *pAppClient = NULL;
	P_SYNOCOMM_BOND pBond = NULL;

	if (NULL == (pH = SearchLocalHost(fIpStr)) ||
		NULL == (pRH = SearchLocalHost(tIpStr))) {
		MSG("No Such Local Host with IP %s or %s\n", fIpStr, tIpStr);
		goto ERR;
	}
	pAppServer = (SYNOCOMM_APP *)SearchAppInLocalHostSetByName(sid->service_name);
	pAppClient = (SYNOCOMM_APP *)SearchAppInLocalHostSetByID(cid);

	if (NULL == pAppServer ||
		NULL == pAppClient) {
		MSG("No such AppServer %p, AppClient %p\n", pAppServer, pAppClient);
		goto ERR;
	}

	AddAppRouteRule(pAppServer, pAppClient);
	AddAppRouteRule(pAppClient, pAppServer);

	//SEND BONDSERVICE
	pBond = CreateBondServiceMsg(pAppClient, pAppServer);
	MSG("############### Send Bond Service\n");
	AppSendControl(pAppClient, (char *)pBond, sizeof(SYNOCOMM_BOND));

	err = 0;
ERR:
	AppPut(pAppServer);
	AppPut(pAppClient);
	return err;
}

int HandleBindMsg(void *pMsg)
{
	int err = -1;
	int ret = -1;
	BOOL blCreateRApp = FALSE;
	BOOL blLocalBand = FALSE;
	BOOL blRemoteBand = FALSE;
	char szSRqSn[MAX_SERVICE_NAME_LEN] = {0};
	SYNOCOMM_CHANNEL *pSRqSn = NULL;

	P_SYNOCOMM_BIND pBind = NULL;
	P_SYNOCOMM_BOND pBond = NULL;
	P_SYNOCOMM_BAND pBand = NULL;

	SYNOCOMM_APP *pApp = NULL;
	SYNOCOMM_APP *pRApp = NULL;
	SYNOCOMM_HOST *pH = NULL;
	SYNOCOMM_HOST *pRH = NULL;

	SERVICE_ID *cid = NULL;
	char *fIpStr = NULL;
	SERVICE_ID *sid = NULL;
	char *tIpStr = NULL;

	pBind = (P_SYNOCOMM_BIND)pMsg;

	cid = &pBind->cid;
	Ipv42Str(&pBind->cip, &fIpStr);
	sid = &pBind->sid;
	Ipv42Str(&pBind->sip, &tIpStr);

	MSG("Bind Msg should be %d bytes\n", sizeof(SYNOCOMM_BIND));
	MSG("Bind ip %s, service_name %s, owner %d, uuid %s\n", fIpStr, cid->service_name, cid->owner, cid->serviceuuid);
	MSG("To   ip %s, service_name %s, owner %d, uuid %s\n", tIpStr, sid->service_name, sid->owner, sid->serviceuuid);

	if (IsLocalService(fIpStr, tIpStr)) {
		if (0 > (ret = HandleLocalBind(fIpStr, cid, tIpStr, sid))) {
			blLocalBand = TRUE;
			goto ERR;
		}

	} else if ((pRH = SearchRemoteHost(tIpStr))) {
		//MSG("ip %s is Remote Host\n", tIpStr);
		//check if remote app exist
		//search the remote channel and bind to remote app

		snprintf(szSRqSn, MAX_SERVICE_NAME_LEN, "SRqSn_%s", tIpStr);//"SRqSn_192.168.16.191"
		pSRqSn = GetExternalOutChannel(pChannelSet, szSRqSn);

		MSG("******** pSRqSn %p, szSRqSn %s\n", pSRqSn, szSRqSn);
		if (NULL != pSRqSn) {
			MSG("Found Remote Channel %s, %p\n", szSRqSn, pSRqSn);

			if (0 > (ret = SENDMSG(pSRqSn, pBind, sizeof(SYNOCOMM_BIND)))) {
				TRANSPORT(pSRqSn)->stop_service(pSRqSn);
				ChannelFree(pSRqSn);
				pSRqSn = NULL;
			}
		}

		if (0 > ret) {
			blLocalBand = TRUE;
			goto ERR;
		}

		ChannelPut(pSRqSn);
	} else if ((pH = SearchLocalHost(tIpStr))) {

		MSG("ip %s is Local Host\n", tIpStr);

		//search the remote channel and bind to remote app
		snprintf(szSRqSn, MAX_SERVICE_NAME_LEN, "SRqSn_%s", fIpStr);//"SRqSn_192.168.16.191"
		pSRqSn = GetExternalOutChannel(pChannelSet, szSRqSn);
		MSG("%s is %p\n", szSRqSn, pSRqSn);

		if (NULL == (pRH = SearchRemoteHost(fIpStr)) ||
			NULL == pSRqSn ||
			//search local app and add routing rule to it and remote app
			NULL == (pApp = (SYNOCOMM_APP *)SearchAppInLocalHostSetByName(sid->service_name))) {
			blRemoteBand = TRUE;
			MSG("Fail to Remote Bind for remote host %s\n", fIpStr);
			goto ERR;
		}

		if (NULL == (pRApp = SearchAppInHostByIDExt(pRH, cid))) {
			pRApp = CreateRemoteAppClient(cid->service_name, cid->owner);
			if (NULL == pRApp) {
				blRemoteBand = TRUE;
				MSG("Fail to Create pRApp %s\n", cid->serviceuuid);
				goto ERR;
			}
			MSG("Create pRApp %s, %p\n", cid->service_name, pRApp);
			blCreateRApp = TRUE;
		}

		RemoteHostAdoptApp(pRH, pRApp);
		BindAppChannel(pRApp, pSRqSn);
		MSG("%s Bind to pRApp %s\n", szSRqSn, cid->service_name);

		MSG("Found pApp service_name %s, pid %d, uuid %s\n",
				pApp->serviceid.service_name, pApp->serviceid.owner, pApp->serviceid.serviceuuid);
		MSG("AppServer END, bind service_name %s, pid %d, uuid %s To service_name %s, pid %d, uuid %s\n",
				pApp->serviceid.service_name, pApp->serviceid.owner, pApp->serviceid.serviceuuid,
				pRApp->serviceid.service_name, pRApp->serviceid.owner, pRApp->serviceid.serviceuuid);
		AddAppRouteRule(pApp, pRApp);
		AddAppRouteRule(pRApp, pApp);

		//SEND BONDSERVICE
		pBond = CreateBondServiceMsg(pRApp, pApp);
		MSG("############### Send Bond Service\n");

		if (NULL != pSRqSn) {
			if (0 > (ret = SENDMSG(pSRqSn, pBond, sizeof(SYNOCOMM_BOND)))) {
				//FIXME will double stop_service and leak of channel free
				TRANSPORT(pSRqSn)->stop_service(pSRqSn);
				ChannelFree(pSRqSn);
				pSRqSn = NULL;
			}
		}
		free(pBond);

		if (0 > ret) {
			MSG("Fail to send Bond to remote app %s, host %s\n", pRApp->serviceid.serviceuuid, fIpStr);
			blRemoteBand = TRUE;
			goto ERR;
		}

	} else {
		goto ERR;
	}

	err = 0;

ERR:
	if (fIpStr) {
		free(fIpStr);
	}
	if (tIpStr) {
		free(tIpStr);
	}

	AppPut(pApp);

	if (blLocalBand &&
		NULL != (pApp = (SYNOCOMM_APP *)SearchAppInLocalHostSetByID(cid))) {
		if (NULL != (pBand = CreateBandServiceMsgEx(&pBind->cip, cid, &pBind->sip, sid))) {
			AppSendControl(pApp, (char *)pBand, sizeof(SYNOCOMM_BAND));
			free(pBand);
		}
		AppPut(pApp);
	}
	if (blRemoteBand) {
		if (NULL != pSRqSn &&
			NULL != (pBand = CreateBandServiceMsgEx(&pBind->cip, cid, &pBind->sip, sid))) {
			if (0 > (ret = SENDMSG(pSRqSn, (char *)pBand, sizeof(SYNOCOMM_BAND)))) {
				TRANSPORT(pSRqSn)->stop_service(pSRqSn);
				ChannelFree(pSRqSn);
			}
			free(pBand);
		}
		if (blCreateRApp) {
			//ReleaseApp(pRApp);
			AppFree(pRApp);
		} else {
			AppPut(pRApp);
		}
	} else {
		AppPut(pRApp);
	}
	return err;
}

int HandleBondMsg(void *pMsg)
{
	int err = -1;
	SERVICE_ID *cid = NULL;
	char *fIpStr = NULL;
	SERVICE_ID *sid = NULL;
	char *tIpStr = NULL;

	char szSRqSn[MAX_SERVICE_NAME_LEN] = {0};
	SYNOCOMM_CHANNEL *pSRqSn = NULL;

	SYNOCOMM_APP *pApp = NULL;
	SYNOCOMM_APP *pRApp = NULL;
	SYNOCOMM_HOST *pH = NULL;
	SYNOCOMM_HOST *pRH = NULL;

	P_SYNOCOMM_BOND pBond = NULL;
	pBond = (P_SYNOCOMM_BOND)pMsg;

	cid = &pBond->cid;
	Ipv42Str(&pBond->cip, &fIpStr);
	sid = &pBond->sid;
	Ipv42Str(&pBond->sip, &tIpStr);

	//SERVICE_ID *tid = &pBond->tid;
	//MSG("Bond ip %s, service_name %s, owner %d, uuid %s (update to owner %d, uuid %s)\n",
	//		fIpStr, cid->service_name, cid->owner, cid->serviceuuid, tid->owner, tid->serviceuuid);
	MSG("Bond ip %s, service_name %s, owner %d, uuid %s\n",
			fIpStr, cid->service_name, cid->owner, cid->serviceuuid);
	MSG("To   ip %s, service_name %s, owner %d, uuid %s\n", tIpStr, sid->service_name, sid->owner, sid->serviceuuid);

	if (NULL != (pH = SearchLocalHost(fIpStr))) {

		MSG("ip %s is Local Host\n", fIpStr);
		pRApp = CreateRemoteAppClient(sid->service_name, sid->owner);
		MSG("Create pRApp service_name %s, pid %d, uuid %s\n",
				pRApp->serviceid.service_name, pRApp->serviceid.owner, pRApp->serviceid.serviceuuid);

		snprintf(szSRqSn, MAX_SERVICE_NAME_LEN, "SRqSn_%s", tIpStr);//"SRqSn_192.168.16.191"
		pSRqSn = GetExternalOutChannel(pChannelSet, szSRqSn);

		MSG("%s is %p\n", szSRqSn, pSRqSn);
		BindAppChannel(pRApp, pSRqSn);
		ChannelPut(pSRqSn);

		pRH = SearchRemoteHost(tIpStr);

		MSG("Search ip %s pRH %p\n", tIpStr, pRH);
		RemoteHostAdoptApp(pRH, pRApp);

		MSG("xxxxxxxxxxxxxxxxxxxx 1, cid %p\n", cid);
		pApp = (SYNOCOMM_APP *)SearchAppInLocalHostSetByID(cid);
		MSG("xxxxxxxxxxxxxxxxxxxx 2 pApp %p\n", pApp);
		MSG("Found pApp service_name %s, pid %d, uuid %s\n",
				pApp->serviceid.service_name, pApp->serviceid.owner, pApp->serviceid.serviceuuid);

		MSG("xxxxxxxxxxxxxxxxxxxx 3\n");
		MSG("AppClient END, bond service_name %s, pid %d, uuid %s To service_name %s, pid %d, uuid %s\n",
				pApp->serviceid.service_name, pApp->serviceid.owner, pApp->serviceid.serviceuuid,
				pRApp->serviceid.service_name, pRApp->serviceid.owner, pRApp->serviceid.serviceuuid);
		MSG("xxxxxxxxxxxxxxxxxxxx 4\n");
		AddAppRouteRule(pApp, pRApp);
		MSG("xxxxxxxxxxxxxxxxxxxx 5\n");
		AddAppRouteRule(pRApp, pApp);
		MSG("xxxxxxxxxxxxxxxxxxxx 6\n");

		//notify the app
		MSG("Send BOND to notify App\n");
		MSG("xxxxxxxxxxxxxxxxxxxx 7\n");
		AppSendControl(pApp, (char *)pBond, sizeof(SYNOCOMM_BOND));

		MSG("xxxxxxxxxxxxxxxxxxxx 8\n");
	} else {
		goto ERR;
	}

	err = 0;
ERR:

	AppPut(pApp);
	AppPut(pRApp);

	if (fIpStr) {
		free(fIpStr);
	}
	if (tIpStr) {
		free(tIpStr);
	}

	return err;
}

int HandleBandMsg(void *pMsg)
{
	int err = -1;
	P_SYNOCOMM_BAND pBand = NULL;
	SYNOCOMM_APP *pApp = NULL;

	if (NULL == (pBand = (P_SYNOCOMM_BAND)pMsg) ||
		NULL == (pApp = (SYNOCOMM_APP *)SearchAppInLocalHostSetByID(&pBand->cid))) {
		goto ERR;
	}

	MSG("Found pApp service_name %s, pid %d, uuid %s\n",
			pApp->serviceid.service_name, pApp->serviceid.owner, pApp->serviceid.serviceuuid);

	//notify the app
	MSG("Send BOND to notify App\n");
	AppSendControl(pApp, (char *)pBand, sizeof(SYNOCOMM_BAND));

	err = 0;
ERR:
	AppPut(pApp);

	return err;
}

static int HandleLocalUBind(const char *fIpStr, SERVICE_ID *cid, const char *tIpStr, SERVICE_ID *sid)
{
	int err = -1;
	SYNOCOMM_HOST *pH = NULL;
	SYNOCOMM_HOST *pRH = NULL;
	SYNOCOMM_APP *pAppServer = NULL;
	SYNOCOMM_APP *pAppClient = NULL;
	P_SYNOCOMM_UBOND pUBond = NULL;

	if (NULL == (pH = SearchLocalHost(fIpStr)) ||
		NULL == (pRH = SearchLocalHost(tIpStr))) {
		MSG("No Such Local Host with IP %s or %s\n", fIpStr, tIpStr);
		goto ERR;
	}
	pAppServer = (SYNOCOMM_APP *)SearchAppInHostByIDExt(pRH, sid);
	pAppClient = (SYNOCOMM_APP *)SearchAppInHostByIDExt(pH, cid);

	if (NULL == pAppServer ||
		NULL == pAppClient) {
		MSG("No such AppServer %p, AppClient %p\n", pAppServer, pAppClient);
		goto ERR;
	}

	//SEND BONDSERVICE
	pUBond = CreateUBondServiceMsg(pAppClient, pAppServer);
	MSG("############### Send UBond Service\n");
	AppSendControl(pAppClient, (char *)pUBond, sizeof(SYNOCOMM_UBOND));

	RemoveAppRouteRule(pAppServer, pAppClient);
	RemoveAppRouteRule(pAppClient, pAppServer);

	err = 0;
ERR:
	AppPut(pAppServer);
	AppPut(pAppClient);

	return err;
}

int HandleUBindMsg(void *pMsg)
{
	int err = -1;
	int ret = -1;
	char szSRqSn[MAX_SERVICE_NAME_LEN] = {0};

	BOOL blLocalUBand = FALSE;
	BOOL blRemoteUBand = FALSE;
	SYNOCOMM_CHANNEL *pSRqSn = NULL;

	P_SYNOCOMM_UBIND pUBind = NULL;
	P_SYNOCOMM_UBOND pUBond = NULL;
	P_SYNOCOMM_UBAND pUBand = NULL;

	SYNOCOMM_APP *pApp = NULL;
	SYNOCOMM_APP *pRApp = NULL;
	SYNOCOMM_HOST *pH = NULL;
	SYNOCOMM_HOST *pRH = NULL;

	SERVICE_ID *cid = NULL;
	char *fIpStr = NULL;
	SERVICE_ID *sid = NULL;
	char *tIpStr = NULL;

	pUBind = (P_SYNOCOMM_UBIND)pMsg;

	cid = &pUBind->cid;
	Ipv42Str(&pUBind->cip, &fIpStr);
	sid = &pUBind->sid;
	Ipv42Str(&pUBind->sip, &tIpStr);

	MSG("UBind Msg should be %d bytes\n", sizeof(SYNOCOMM_BIND));
	MSG("UBind ip %s, service_name %s, owner %d, uuid %s\n", fIpStr, cid->service_name, cid->owner, cid->serviceuuid);
	MSG("To    ip %s, service_name %s, owner %d, uuid %s\n", tIpStr, sid->service_name, sid->owner, sid->serviceuuid);

	if (IsLocalService(fIpStr, tIpStr)) {
		if (0 > (ret = HandleLocalUBind(fIpStr, cid, tIpStr, sid))) {
			MSG("++++++++++ LOCAL BAND from %s@%s to %s@%s ++++++++++\n", cid->serviceuuid, fIpStr, sid->serviceuuid, tIpStr);
			blLocalUBand = TRUE;
			goto ERR;
		}

	} else if ((pRH = SearchRemoteHost(tIpStr))) {
		//MSG("ip %s is Remote Host\n", tIpStr);
		//check if remote app exist
		//search the remote channel and bind to remote app
		snprintf(szSRqSn, MAX_SERVICE_NAME_LEN, "SRqSn_%s", tIpStr);//"SRqSn_192.168.16.191"
		if (NULL != (pSRqSn = GetExternalOutChannel(pChannelSet, szSRqSn))) {
			MSG("Found Remote Channel %s, %p\n", szSRqSn, pSRqSn);
			if (0 > (ret = SENDMSG(pSRqSn, pUBind, sizeof(SYNOCOMM_UBIND)))) {
				TRANSPORT(pSRqSn)->stop_service(pSRqSn);
				ChannelFree(pSRqSn);
				pSRqSn = NULL;
			}
		}

		if (0 > ret) {
			MSG("++++++++++ LOCAL BAND from %s@%s to %s@%s ++++++++++\n", cid->serviceuuid, fIpStr, sid->serviceuuid, tIpStr);
			blLocalUBand = TRUE;
			goto ERR;
		}
		ChannelPut(pSRqSn);

	} else if ((pH = SearchLocalHost(tIpStr))) {

		MSG("ip %s is Local Host\n", tIpStr);

		//search the remote channel and bind to remote app
		snprintf(szSRqSn, MAX_SERVICE_NAME_LEN, "SRqSn_%s", fIpStr);//"SRqSn_192.168.16.191"

		if (NULL == (pRH = SearchRemoteHost(fIpStr)) ||
			NULL == (pSRqSn = GetExternalOutChannel(pChannelSet, szSRqSn))) {
			MSG("++++++++++ REMOTE BAND from %s@%s to %s@%s ++++++++++\n", cid->serviceuuid, fIpStr, sid->serviceuuid, tIpStr);
			blRemoteUBand = TRUE;
			MSG("Fail to Remote UBind for remote host %s\n", fIpStr);
			goto ERR;
		}
		MSG("%s is %p\n", szSRqSn, pSRqSn);

		if (NULL == (pRApp = (SYNOCOMM_APP *)SearchAppInHostByIDExt(pRH, cid)) ||
			//search real local app and add routing rule to it and remote app
			NULL == (pApp = (SYNOCOMM_APP *)SearchAppInHostByIDExt(pH, sid))) {
			MSG("Fail to Remote UBind for remote app %s, host %s\n", cid->serviceuuid, fIpStr);
			MSG("++++++++++ REMOTE UBAND from %s@%s to %s@%s ++++++++++\n", cid->serviceuuid, fIpStr, sid->serviceuuid, tIpStr);
			blRemoteUBand = TRUE;
			goto ERR;
		}

		MSG("Search pRApp service_name %s, pid %d, uuid %s, cid->service_name %s\n",
				pRApp->serviceid.service_name, pRApp->serviceid.owner, pRApp->serviceid.serviceuuid, cid->service_name);

		MSG("Found pApp service_name %s, pid %d, uuid %s\n",
				pApp->serviceid.service_name, pApp->serviceid.owner, pApp->serviceid.serviceuuid);
		MSG("AppServer END, ubind service_name %s, pid %d, uuid %s To service_name %s, pid %d, uuid %s\n",
				pApp->serviceid.service_name, pApp->serviceid.owner, pApp->serviceid.serviceuuid,
				pRApp->serviceid.service_name, pRApp->serviceid.owner, pRApp->serviceid.serviceuuid);

		pUBond = CreateUBondServiceMsg(pRApp, pApp);

		//SEND UBONDSERVICE
		if (NULL != pSRqSn) {
			if (0 > (ret = SENDMSG(pSRqSn, pUBond, sizeof(SYNOCOMM_UBOND)))) {
				TRANSPORT(pSRqSn)->stop_service(pSRqSn);
				ChannelFree(pSRqSn);
				pSRqSn = NULL;
			}
		}

		if (0 < ret) {
			MSG("############### Send UBond Service to %s successfully\n", cid->serviceuuid);
		} else {
			MSG("############### Fail to send UBond Service to %s\n", cid->serviceuuid);
		}
		RemoveAppRouteRule(pApp, pRApp);
		RemoveAppRouteRule(pRApp, pApp);

		free(pUBond);
	} else {
		goto ERR;
	}

	err = 0;

ERR:
	AppPut(pApp);
	AppPut(pRApp);

	if (fIpStr) {
		free(fIpStr);
	}
	if (tIpStr) {
		free(tIpStr);
	}

	if (blLocalUBand &&
		NULL != (pApp = (SYNOCOMM_APP *)SearchAppInLocalHostSetByID(cid))) {
		if (NULL != (pUBand = CreateUBandServiceMsgEx(&pUBind->cip, cid, &pUBind->sip, sid))) {
			MSG("++++++++++ LOCAL UBAND from %s@%s to %s@%s ++++++++++\n", cid->serviceuuid, fIpStr, sid->serviceuuid, tIpStr);
			AppSendControl(pApp, (char *)pUBand, sizeof(SYNOCOMM_UBAND));
			free(pUBand);
		}
		AppPut(pApp);
	}
	if (blRemoteUBand) {
		if (NULL != pSRqSn &&
			NULL != (pUBand = CreateUBandServiceMsgEx(&pUBind->cip, cid, &pUBind->sip, sid))) {
			MSG("++++++++++ REMOTE UBAND from %s@%s to %s@%s ++++++++++\n", cid->serviceuuid, fIpStr, sid->serviceuuid, tIpStr);
			if (0 > (ret = SENDMSG(pSRqSn, (char *)pUBand, sizeof(SYNOCOMM_UBAND)))) {
				TRANSPORT(pSRqSn)->stop_service(pSRqSn);
				ChannelFree(pSRqSn);
				pSRqSn = NULL;
			}
			free(pUBand);
		}
	}
	return err;
}

int HandleUBondMsg(void *pMsg)
{
	int err = -1;
	SERVICE_ID *cid = NULL;
	char *fIpStr = NULL;
	SERVICE_ID *sid = NULL;
	char *tIpStr = NULL;

	SYNOCOMM_APP *pApp = NULL;
	SYNOCOMM_APP *pRApp = NULL;
	SYNOCOMM_HOST *pH = NULL;
	SYNOCOMM_HOST *pRH = NULL;

	P_SYNOCOMM_UBOND pUBond = NULL;
	pUBond = (P_SYNOCOMM_UBOND)pMsg;

	cid = &pUBond->cid;
	Ipv42Str(&pUBond->cip, &fIpStr);
	sid = &pUBond->sid;
	Ipv42Str(&pUBond->sip, &tIpStr);

	MSG("UBond ip %s, service_name %s, owner %d, uuid %s\n",
			fIpStr, cid->service_name, cid->owner, cid->serviceuuid);
	MSG("To    ip %s, service_name %s, owner %d, uuid %s\n", tIpStr, sid->service_name, sid->owner, sid->serviceuuid);

	if ((pH = SearchLocalHost(fIpStr))) {
		MSG("ip %s is Local Host\n", fIpStr);

		pRH = SearchRemoteHost(tIpStr);
		MSG("Search ip %s pRH %p\n", tIpStr, pRH);
		pRApp = (SYNOCOMM_APP *)SearchAppInHostByIDExt(pRH, sid);
		MSG("Search pRApp service_name %s, pid %d, uuid %s, sid->service_name %s\n",
				pRApp->serviceid.service_name, pRApp->serviceid.owner, pRApp->serviceid.serviceuuid, sid->service_name);

		pApp = (SYNOCOMM_APP *)SearchAppInLocalHostSetByID(cid);
		MSG("Found pApp service_name %s, pid %d, uuid %s\n",
				pApp->serviceid.service_name, pApp->serviceid.owner, pApp->serviceid.serviceuuid);

		MSG("AppClient END, ubond service_name %s, pid %d, uuid %s To service_name %s, pid %d, uuid %s\n",
				pApp->serviceid.service_name, pApp->serviceid.owner, pApp->serviceid.serviceuuid,
				pRApp->serviceid.service_name, pRApp->serviceid.owner, pRApp->serviceid.serviceuuid);

		//notify the app
		MSG("Send BOND to notify App\n");
		AppSendControl(pApp, (char *)pUBond, sizeof(SYNOCOMM_UBOND));

		RemoveAppRouteRule(pApp, pRApp);
		RemoveAppRouteRule(pRApp, pApp);
	} else {
		goto ERR;
	}

	err = 0;
ERR:
	AppPut(pApp);
	AppPut(pRApp);

	if (fIpStr) {
		free(fIpStr);
	}
	if (tIpStr) {
		free(tIpStr);
	}

	return err;
}

int HandleUBandMsg(void *pMsg)
{
	//FIXME duplicated from HandleUBondMsg
	return HandleUBondMsg(pMsg);
}

static int HandleLocalPing(FIELD_IPV4 *pCIP, SERVICE_ID *pCID, FIELD_IPV4 *pSIP, SERVICE_ID *pSID)
{
	int err = -1;
	SYNOCOMM_APP *pAppServer = NULL;
	SYNOCOMM_APP *pAppClient = NULL;
	P_SYNOCOMM_PONG pPong = NULL;

	pAppServer = (SYNOCOMM_APP *)SearchAppInLocalHostSetByName(pSID->service_name);
	pAppClient = (SYNOCOMM_APP *)SearchAppInLocalHostSetByID(pCID);

	if (NULL == pAppClient) {
		MSG("No such AppServer %p (%s), AppClient %p (%s)\n", pAppServer, pSID->service_name, pAppClient, pCID->uuid_filed.bits);
		goto ERR;
	}

	if (NULL != pAppServer) {
		MSG("Found pApp service_name %s, pid %d, uuid %s\n",
				pAppServer->serviceid.service_name, pAppServer->serviceid.owner, pAppServer->serviceid.serviceuuid);
		pPong = CreatePongServiceMsg(pCIP, pCID, pSIP, &pAppServer->serviceid, (PING_LOCAL_TYPE | PING_SERVER_ENG | PING_SERVER_APP));
	} else {
		MSG("Not Found pApp service_name %s, pid %d, uuid %s\n",
				pSID->service_name, pSID->owner, pSID->serviceuuid);
		pPong = CreatePongServiceMsg(pCIP, pCID, pSIP, pSID, (PING_LOCAL_TYPE | PING_SERVER_ENG));
	}
	//SEND BONDSERVICE
	MSG("############### Send Pong Service, status 0x%x\n", pPong->status);
	AppSendControl(pAppClient, (char *)pPong, sizeof(SYNOCOMM_PONG));

	err = 0;
ERR:
	AppPut(pAppServer);
	AppPut(pAppClient);

	return err;
}

static int HandleLocalPung(FIELD_IPV4 *pCIP, SERVICE_ID *pCID, FIELD_IPV4 *pSIP, SERVICE_ID *pSID)
{
	int err = -1;
	SYNOCOMM_APP *pSrvApp = NULL;
	SYNOCOMM_APP *pCliApp = NULL;
	P_SYNOCOMM_PUNG pPung = NULL;

	pSrvApp = (SYNOCOMM_APP *)SearchAppInLocalHostSetByName(pSID->service_name);
	pCliApp = (SYNOCOMM_APP *)SearchAppInLocalHostSetByID(pCID);

	if (NULL == pCliApp) {
		MSG("No such AppServer %p, AppClient %p\n", pSrvApp, pCliApp);
		goto ERR;
	}

	if (NULL != pSrvApp) {
		MSG("Found pApp service_name %s, pid %d, uuid %s\n",
				pSrvApp->serviceid.service_name, pSrvApp->serviceid.owner, pSrvApp->serviceid.serviceuuid);
		pPung = CreatePungServiceMsg(pCIP, pCID, pSIP, &pSrvApp->serviceid, (PING_LOCAL_TYPE | PING_SERVER_ENG | PING_SERVER_APP));
	} else {
		MSG("Not Found pApp service_name %s, pid %d, uuid %s\n",
				pSID->service_name, pSID->owner, pSID->serviceuuid);
		pPung = CreatePungServiceMsg(pCIP, pCID, pSIP, pSID, (PING_LOCAL_TYPE | PING_SERVER_ENG));
	}
	//SEND PONGSERVICE
	MSG("############### Send Pung Service, status 0x%x\n", pPung->status);
	AppSendControl(pCliApp, (char *)pPung, sizeof(SYNOCOMM_PONG));

	err = 0;
ERR:
	AppPut(pSrvApp);
	AppPut(pCliApp);

	return err;
}

int HandlePingMsg(void *pMsg)
{
	int err = -1;
	int ret = -1;
	char szSRqSn[MAX_SERVICE_NAME_LEN] = {0};
	SYNOCOMM_CHANNEL *pSRqSn = NULL;
	P_SYNOCOMM_PING pPing = NULL;
	P_SYNOCOMM_PONG pPong = NULL;

	SYNOCOMM_APP *pApp = NULL;
	SYNOCOMM_HOST *pH = NULL;
	SYNOCOMM_HOST *pRH = NULL;

	SERVICE_ID *cid = NULL;
	char *fIpStr = NULL;
	SERVICE_ID *sid = NULL;
	char *tIpStr = NULL;

	pPing = (P_SYNOCOMM_PING)pMsg;

	cid = &pPing->cid;
	Ipv42Str(&pPing->cip, &fIpStr);
	sid = &pPing->sid;
	Ipv42Str(&pPing->sip, &tIpStr);

	MSG("Ping Msg should be %d bytes\n", sizeof(SYNOCOMM_PING));
	MSG("Ping ip %s, service_name %s, owner %d, uuid %s\n", fIpStr, cid->service_name, cid->owner, cid->serviceuuid);
	MSG("To   ip %s, service_name %s, owner %d, uuid %s\n", tIpStr, sid->service_name, sid->owner, sid->serviceuuid);

	if (IsLocalService(fIpStr, tIpStr)) {
		return HandleLocalPing(&pPing->cip, cid, &pPing->sip, sid);
	}

	if ((pRH = SearchRemoteHost(tIpStr))) {
		//MSG("ip %s is Remote Host\n", tIpStr);
		//check if remote app exist
		//search the remote channel and ping to remote app

		snprintf(szSRqSn, MAX_SERVICE_NAME_LEN, "SRqSn_%s", tIpStr);//"SRqSn_192.168.16.191"
		pSRqSn = GetExternalOutChannel(pChannelSet, szSRqSn);

		MSG("Found Remote Channel %s, %p\n", szSRqSn, pSRqSn);

		if (NULL != pSRqSn) {
			if (0 > (ret = SENDMSG(pSRqSn, pPing, sizeof(SYNOCOMM_PING)))) {
				TRANSPORT(pSRqSn)->stop_service(pSRqSn);
				ChannelFree(pSRqSn);
				pSRqSn = NULL;
			}
		}

		if (0 > ret) {
			pPong = CreatePongServiceMsg(&pPing->cip, &pPing->cid, &pPing->sip, &pPing->sid, PING_REMOTE_TYPE);
			if (NULL != pPong &&
				NULL != (pApp = (SYNOCOMM_APP *)SearchAppInLocalHostSetByID(&pPing->cid))) {
				AppSendControl(pApp, (char *)pPong, sizeof(SYNOCOMM_PONG));
				free(pPong);
			}
		}

	} else if ((pH = SearchLocalHost(tIpStr))) {

		//search the remote channel and ping to remote app
		snprintf(szSRqSn, MAX_SERVICE_NAME_LEN, "SRqSn_%s", fIpStr);//"SRqSn_192.168.16.191"
		pSRqSn = GetExternalOutChannel(pChannelSet, szSRqSn);
		MSG("%s is %p\n", szSRqSn, pSRqSn);

		if (NULL == pSRqSn) {
			MSG("No such %s\n", szSRqSn);
			goto ERR;
		}

		//search local app and add routing rule to it and remote app
		if (NULL != (pApp = (SYNOCOMM_APP *)SearchAppInLocalHostSetByName(sid->service_name))) {
			MSG("Found pApp service_name %s, pid %d, uuid %s\n",
					pApp->serviceid.service_name, pApp->serviceid.owner, pApp->serviceid.serviceuuid);
			pPong = CreatePongServiceMsg(&pPing->cip, &pPing->cid, &pPing->sip, &pApp->serviceid, (PING_REMOTE_TYPE | PING_SERVER_ENG | PING_SERVER_APP));
		} else {
			MSG("Not Found pApp service_name %s, pid %d, uuid %s\n",
					pPing->sid.service_name, pPing->sid.owner, pPing->sid.serviceuuid);
			pPong = CreatePongServiceMsg(&pPing->cip, &pPing->cid, &pPing->sip, &pPing->sid, (PING_REMOTE_TYPE | PING_SERVER_ENG));
		}

		//SEND PONGSERVICE
		if (0 > SENDMSG(pSRqSn, pPong, sizeof(SYNOCOMM_PONG))) {
			TRANSPORT(pSRqSn)->stop_service(pSRqSn);
			ChannelFree(pSRqSn);
			pSRqSn = NULL;
			MSG("############### Fail to Send Pong Service to %s\n", pPong->cid.serviceuuid);
		} else {
			MSG("############### Send Pong Service to %s successfully\n", pPong->cid.serviceuuid);
		}

		free(pPong);
	}

ERR:
	AppPut(pApp);

	if (fIpStr) {
		free(fIpStr);
	}
	if (tIpStr) {
		free(tIpStr);
	}
	return err;
}

int HandlePongMsg(void *pMsg)
{
	int err = -1;
	SERVICE_ID *cid = NULL;
	char *fIpStr = NULL;
	SERVICE_ID *sid = NULL;
	char *tIpStr = NULL;

	SYNOCOMM_APP *pApp = NULL;
	SYNOCOMM_HOST *pH = NULL;

	P_SYNOCOMM_PONG pPong = NULL;
	pPong = (P_SYNOCOMM_PONG)pMsg;

	cid = &pPong->cid;
	Ipv42Str(&pPong->cip, &fIpStr);
	sid = &pPong->sid;
	Ipv42Str(&pPong->sip, &tIpStr);

	MSG("Pong ip %s, service_name %s, owner %d, uuid %s\n",
			fIpStr, cid->service_name, cid->owner, cid->serviceuuid);
	MSG("To   ip %s, service_name %s, owner %d, uuid %s\n", tIpStr, sid->service_name, sid->owner, sid->serviceuuid);

	if ((pH = SearchLocalHost(fIpStr))) {

		pApp = (SYNOCOMM_APP *)SearchAppInLocalHostSetByID(cid);
		if (NULL != pApp) {
			MSG("Found pApp service_name %s, pid %d, uuid %s\n",
					pApp->serviceid.service_name, pApp->serviceid.owner, pApp->serviceid.serviceuuid);
			//notify the app
			MSG("Send PONG to notify App, status = 0x%x\n", pPong->status);
			AppSendControl(pApp, (char *)pPong, sizeof(SYNOCOMM_PONG));
		} else {
			MSG("Not Found pApp service_name %s, pid %d, uuid %s\n",
					pPong->cid.service_name, pPong->cid.owner, pPong->cid.serviceuuid);
		}
	}

	AppPut(pApp);

	if (fIpStr) {
		free(fIpStr);
	}
	if (tIpStr) {
		free(tIpStr);
	}
	return err;
}

int HandlePungMsg(void *pMsg)
{
	int err = -1;
	P_SYNOCOMM_PUNG pPung = NULL;

	SYNOCOMM_APP *pCliApp = NULL;
	SYNOCOMM_APP *pSrvApp = NULL;
	SYNOCOMM_HOST *pH = NULL;
	SYNOCOMM_HOST *pRH = NULL;

	SERVICE_ID *cid = NULL;
	char *fIpStr = NULL;
	SERVICE_ID *sid = NULL;
	char *tIpStr = NULL;
	SYNOCOMM_CHANNEL *pSRqRx = NULL;
	char zSRqRx[MAX_SERVICE_NAME_LEN] = {0};
	int status = 0;

	pPung = (P_SYNOCOMM_PUNG)pMsg;

	cid = &pPung->cid;
	Ipv42Str(&pPung->cip, &fIpStr);
	sid = &pPung->sid;
	Ipv42Str(&pPung->sip, &tIpStr);

	MSG("Pung Msg should be %d bytes\n", sizeof(SYNOCOMM_PUNG));
	MSG("Pung ip %s, service_name %s, owner %d, uuid %s\n", fIpStr, cid->service_name, cid->owner, cid->serviceuuid);
	MSG("To   ip %s, service_name %s, owner %d, uuid %s\n", tIpStr, sid->service_name, sid->owner, sid->serviceuuid);

	if (IsLocalService(fIpStr, tIpStr)) {
		return HandleLocalPung(&pPung->cip, cid, &pPung->sip, sid);
	}

	if ((pRH = SearchRemoteHost(tIpStr))) {
		//MSG("ip %s is Remote Host\n", tIpStr);
		//check if remote app exist
		//search the remote channel and ping to remote app

		snprintf(zSRqRx, MAX_SERVICE_NAME_LEN, "SRqRx_%s", tIpStr);//"SRqSn_192.168.16.191"
		pSRqRx = GetExternalInChannel(pChannelSet, zSRqRx);
		MSG("Found Remote Channel %s, %p\n", zSRqRx, pSRqRx);

		pSrvApp = SearchAppInHostByNameExt(pRH, sid->service_name);
	}

	if ((pH = SearchLocalHost(fIpStr))) {
		pCliApp = SearchAppInHostByIDExt(pH, cid);
	}

	if (pH && pCliApp) {

		status |= PING_REMOTE_TYPE;
		if (NULL != pSRqRx) {
			if (IsSocketAlive(pSRqRx)) {
				status |= PING_SERVER_ENG;
				if (NULL != pSrvApp) {
					status |= PING_SERVER_APP;
				}
			}
			ChannelPut(pSRqRx);
		}
	}

	pPung = CreatePungServiceMsg(&pPung->cip, &pPung->cid, &pPung->sip, &pPung->sid, status);

	MSG("############### Send Pung Service\n");

	if (NULL != pCliApp) {
		AppSendControl(pCliApp, (char *)pPung, sizeof(SYNOCOMM_PUNG));
	} else {
		MSG("No such ClientApp ???? %s\n", pPung->cid.service_name);
	}

	free(pPung);

	AppPut(pSrvApp);
	AppPut(pCliApp);

	if (fIpStr) {
		free(fIpStr);
	}
	if (tIpStr) {
		free(tIpStr);
	}
	return err;
}

