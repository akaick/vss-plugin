#ifndef _SYNO_COMM_CHANNEL_HOST_H_
#define _SYNO_COMM_CHANNEL_HOST_H_

#ifndef WIN32
#include <synocomm/synocomm_types.h>
#else
#include "synocomm_types.h"
#endif

#ifdef __cplusplus
__SYNO_BEGIN_DECLS
#endif

typedef struct _synocomm_host_ {
	/*
	 * indicate the channel is used to connect to remote host
	 * or belong to local host
	 */
	HOST_TYPE host_type;
	/*
	 * remote/local host ip
	 */
	FIELD_IPV4 hip;
#define		   hostip hip.bits
	/*
	 * the host name
	 */
	char hostname[MAX_HOSTNAME_LEN];
	/*
	 * host list, link to host_set
	 */
	SYNOCOMM_LINKEDLIST hlist;
	/*
	 * app count
	 */
	int acount;
	/*
	 * app list head
	 */
	SYNOCOMM_LINKEDLIST alist;
	/*
	 * mutex for protection
	 */
	SYNOCOMM_MUTEX mutex;
} SYNOCOMM_HOST, *P_SYNOCOMM_HOST;

typedef struct _synocomm_host_set_ {
	/*
	 * host count
	 */
	int hcount;
	/*
	 * host list;
	 */
	SYNOCOMM_LINKEDLIST hlist;
	/*
	 * mutex for protection
	 */
	SYNOCOMM_MUTEX	mutex;
} SYNOCOMM_HOST_SET, *P_SYNOCOMM_HOST_SET;

/*
 * Global variable
 */
extern SYNOCOMM_HOST_SET *pRemoteHostSet;
extern SYNOCOMM_HOST_SET *pLocalHostSet;

extern int InitHostSet(void);
extern int AddLocalHost(const char *ip, const char *hostname);
extern int AddRemoteHost(const char *ip, const char *hostname);
extern int RemoveHost(SYNOCOMM_HOST *pH);
extern SYNOCOMM_HOST *SearchLocalHost(const char *ip);
extern SYNOCOMM_HOST *SearchRemoteHost(const char *ip);
extern void *SearchAppInLocalHostSetByName(const char *service_name);
extern void *SearchAppInRemoteHostSetByName(const char *service_name);
extern void *SearchAppInLocalHostSetByID(SERVICE_ID *pID);
extern void *SearchAppInRemoteHostSetByID(SERVICE_ID *pID);
extern void *SearchAppInLocalHostSetByUUID(FIELD_UUID *pUUID);
extern void *SearchAppInRemoteHostSetByUUID(FIELD_UUID *pUUID);

extern void *SearchAppInHostByIDExt(SYNOCOMM_HOST *pH, SERVICE_ID *pID);
extern void *SearchAppInHostByNameExt(SYNOCOMM_HOST *pH, const char *service_name);

extern int HostAdoptApp(SYNOCOMM_HOST *pH, void *pApp);
extern int HostAbandonApp(SYNOCOMM_HOST *pH, void *pApp);
extern int LocalHostAdoptApp(SYNOCOMM_HOST *pH, void *pApp);
extern int RemoteHostAdoptApp(SYNOCOMM_HOST *pH, void *pApp);

#ifdef __cplusplus
__SYNO_END_DECLS
#endif

#endif
