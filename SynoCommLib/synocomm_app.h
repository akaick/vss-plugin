#ifndef _SYNO_COMM_APP_H_
#define _SYNO_COMM_APP_H_

#ifndef WIN32
#include <synocomm/synocomm_base.h>
#include <synocomm/synocomm_types.h>
#include <synocomm/synocomm_routing.h>
#include <synocomm/synocomm_message.h>
#else
#include "synocomm_base.h"
#include "synocomm_types.h"
#include "synocomm_routing.h"
#include "synocomm_message.h"
#endif

#ifdef __cplusplus
__SYNO_BEGIN_DECLS
#endif

enum {
	APP_SERVICE_STARTING = 1,
	APP_SERVICE_READY,
	APP_SERVICE_STOPPED
};

typedef struct _synocomm_app_ {
	/*
	 * sercie identity, same with desc
	 */
	SERVICE_ID	serviceid;
	/*
	 * pointer to in/out channel
	 */
	SYNOCOMM_CHANNEL *channel;
	/*
	 * app_type, LOCAL or REMOTE
	 */
	int app_type;
	/*
	 * a pointer to the host that have this app
	 */
	SYNOCOMM_HOST *host;
	/*
	 * a list to link to host
	 */
	SYNOCOMM_LINKEDLIST alist;
	/*
	 * a rule_items collection
	 */
	SYNOCOMM_RULE rule;
	/*
	 * app status:
	 * starting/ready/stopped
	 */
	int status;
	/*
	 * mutex for protection
	 */
	volatile int to_be_deleted;
	volatile int ref_count;
	SYNOCOMM_MUTEX mutex;
} SYNOCOMM_APP, *P_SYNOCOMM_APP;

extern SYNOCOMM_APP *CreateLocalAppServer(const char *name, int pid);
extern SYNOCOMM_APP *CreateLocalAppClient(const char *name, int pid);
extern SYNOCOMM_APP *CreateRemoteAppServer(const char *name, int pid);
extern SYNOCOMM_APP *CreateRemoteAppClient(const char *name, int pid);
extern SYNOCOMM_APP *UpdateAppID(SYNOCOMM_APP *pApp, SERVICE_ID *pID);
extern void BindAppChannel(SYNOCOMM_APP *app, SYNOCOMM_CHANNEL *pch);
extern void UnBindAppChannel(SYNOCOMM_APP *app);
extern void CloseAppChannel(SYNOCOMM_APP *app);
extern void UpdateRemoteChannelInRemoteApp(SYNOCOMM_APP *pRApp);
extern void ReleaseApp(SYNOCOMM_APP *pApp);
extern int AddAppRouteRule(SYNOCOMM_APP *appSrc, SYNOCOMM_APP *appDst);
extern int RemoveAppRouteRule(SYNOCOMM_APP *appSrc, SYNOCOMM_APP *appDst);
extern int RemoveAppRouteRuleByID(SYNOCOMM_APP *pApp, SERVICE_ID *pID);
extern int RemoveAppRouteRuleByFD(SYNOCOMM_APP *appSrc, int fd);
extern SYNOCOMM_APP *GetToAppByRule(SYNOCOMM_APP *app, int appFd);

extern int AppSendControl(SYNOCOMM_APP *pApp, char *data, int len);
extern int AppRecvControl(SYNOCOMM_APP *pApp, char *data, int len);
extern int AppSendMessage(SYNOCOMM_APP *pApp, int appFd, const char *data, int len);
extern int AppUSendMessage(SYNOCOMM_APP *pApp, FIELD_UUID *pToUUID, const char *data, int len);
extern int AppRecvMessage(SYNOCOMM_APP *pApp, char *data, int len);
extern int AppGetMessageFromFD(SYNOCOMM_APP *pApp, P_SYNOCOMM_DATA pData);
extern int AppRetMessage(SYNOCOMM_APP *pApp, P_SYNOCOMM_DATA pData, char *data, int len);

extern SYNOCOMM_APP *AppGet(SYNOCOMM_APP *pApp);
extern void AppPut(SYNOCOMM_APP *pApp);
extern void AppFree(SYNOCOMM_APP *pApp);

#ifdef __cplusplus
__SYNO_END_DECLS
#endif

#endif
