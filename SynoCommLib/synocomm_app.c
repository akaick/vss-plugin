#ifndef WIN32
#include <stdlib.h>
#include <search.h>
#include <synocomm/synocomm_app.h>
#include <synocomm/synocomm_types.h>
#include <synocomm/synocomm_message.h>
#include <synocomm/synocomm_channel_control.h>
#include <synocomm/synocomm_routing.h>
#include <synocomm/synocomm_channel.h>
#else
#include "stdafx.h"
#include "stddef.h"
#include "synocomm_app.h"
#include "synocomm_types.h"
#include "synocomm_message.h"
#include "synocomm_channel_control.h"
#include "synocomm_routing.h"
#include "synocomm_channel.h"
#endif

static SYNOCOMM_APP *create_an_app(const char *name, int pid, int type, int role)
{
	int err = -1;
	SYNOCOMM_APP *pApp = (SYNOCOMM_APP *)ZALLOC(SYNOCOMM_APP);

	snprintf(pApp->serviceid.service_name, sizeof(pApp->serviceid.service_name), "%s", name);
	pApp->serviceid.owner = pid;

	if (0 != ServiceIDGenerateUUID(&pApp->serviceid)) {
		goto ERR;
	}

	pApp->channel = NULL;
	pApp->app_type = type;

	COMM_LIST_HEAD_INIT(&pApp->rule.ilist);
	pApp->to_be_deleted = 0;
	pApp->ref_count = 1;
	SYNOCOMM_MUTEX_INIT(pApp->mutex);

	err = 0;
ERR:
	if (err && pApp) {
		free(pApp);
		pApp = NULL;
	}
	return pApp;
}

/*
 * exteranl interface
 */
SYNOCOMM_APP *CreateLocalAppServer(const char *name, int pid)
{
	MSG("Create App %s, pid %d\n", name, pid);
	return create_an_app(name, pid, APP_LOCAL, APP_SERVER);
}

SYNOCOMM_APP *CreateLocalAppClient(const char *name, int pid)
{
	MSG("Create App %s, pid %d\n", name, pid);
	return create_an_app(name, pid, APP_LOCAL, APP_CLIENT);
}

SYNOCOMM_APP *CreateRemoteAppServer(const char *name, int pid)
{
	MSG("Create App %s, pid %d\n", name, pid);
	return create_an_app(name, pid, APP_REMOTE, APP_SERVER);
}

SYNOCOMM_APP *CreateRemoteAppClient(const char *name, int pid)
{
	MSG("Create App %s, pid %d\n", name, pid);
	return create_an_app(name, pid, APP_REMOTE, APP_CLIENT);
}

SYNOCOMM_APP *UpdateAppID(SYNOCOMM_APP *pApp, SERVICE_ID *pID)
{
	if (NULL == pApp ||
		NULL == pID) {
		return NULL;
	}

	memcpy(&pApp->serviceid, pID, sizeof(SERVICE_ID));
	if (0 != ServiceIDGenerateUUID(&pApp->serviceid)) {
		pApp = NULL;
		goto ERR;
	
	}
ERR:
	return pApp;
}

void BindAppChannel(SYNOCOMM_APP *pApp, SYNOCOMM_CHANNEL *pCh)
{
	//Atomic increase the ref_count before enter BindAppChannel(
	ATOMIC_REF_COUNT_INC(pCh);
	pApp->channel = pCh;
}

void UnBindAppChannel(SYNOCOMM_APP *pApp)
{
	SYNOCOMM_CHANNEL *pCh = (SYNOCOMM_CHANNEL *)pApp->channel;
	ATOMIC_REF_COUNT_DEC(pCh);
	ChannelFree(pCh);
	pApp->channel = NULL;
}

void CloseAppChannel(SYNOCOMM_APP *pApp)
{
	extern SYNOCOMM_CHANNEL_SET *pChannelSet;

	if (pApp && pApp->channel) {
		RemoveChannel(pChannelSet, pApp->channel);
		synocomm_base_destruct(pApp->channel);
		pApp->channel = NULL;
	}
}

void UpdateRemoteChannelInRemoteApp(SYNOCOMM_APP *pRApp)
{
	char *szIpStr = NULL;
	char szSRqSn[MAX_SERVICE_NAME_LEN] = {0};
	SYNOCOMM_CHANNEL *pSRqSn = NULL;
	SYNOCOMM_HOST *pH = NULL;
	if (NULL != pRApp &&
		NULL != (pH = pRApp->host)) {
		
		Ipv42Str(&pH->hip, &szIpStr);
		snprintf(szSRqSn, MAX_SERVICE_NAME_LEN, "SRqSn_%s", szIpStr);//"SRqSn_192.168.16.191"
		pSRqSn = GetExternalOutChannel(pChannelSet, szSRqSn);

		if (NULL != pSRqSn &&
			pRApp->channel != pSRqSn) {
			MSG(">>>>>>>>>>>>>>>>>>>>>>> Update pRApp %s with new %s, %p\n", pRApp->serviceid.serviceuuid, szSRqSn, pSRqSn);
			UnBindAppChannel(pRApp);
			BindAppChannel(pRApp, pSRqSn);
		}

		free(szIpStr);
	}
}

void ReleaseApp(SYNOCOMM_APP *pApp)
{
	SYNOCOMM_HOST *pH = NULL;
	SYNOCOMM_RULE_ITEM *pItem = NULL, *pTmp = NULL;

	if (pApp) {
		if (NULL != (pH = pApp->host)) {
			MSG(">>>>>>>>>>>>>>>>> Release %s\n", pApp->serviceid.serviceuuid, pApp->ref_count, pApp->to_be_deleted);
			HostAbandonApp(pH, pApp);
		}

		COMM_LIST_FOR_EACH_ENTRY_SAFE(pItem, pTmp, SYNOCOMM_RULE_ITEM, &pApp->rule.ilist, ilist) {
			UPDATE_DEL(&pItem->ilist, pApp->rule.icount);
			free(pItem);
		}
		CloseAppChannel(pApp);
		free(pApp);
	}
}

extern P_SYNOCOMM_RULE_ITEM CreateRuleItem(SYNOCOMM_APP *appFrom, SYNOCOMM_APP *appTo);
int AddAppRouteRule(SYNOCOMM_APP *appSrc, SYNOCOMM_APP *appDst)
{
	int err = -1;

	P_SYNOCOMM_RULE_ITEM pItem = CreateRuleItem(appSrc, appDst);

	if (pItem) {
		UPDATE_ADD_TAIL(&pItem->ilist, &appSrc->rule.ilist, appSrc->rule.icount);
		err = pItem->rule_index;
	}

	return err;
}

static BOOL IsSameApp(SYNOCOMM_APP *app1, SYNOCOMM_APP *app2)
{
	return ((0 == CompareUUID(&(app1->serviceid.uuid_filed), &(app2->serviceid.uuid_filed))) ? TRUE:FALSE);
}

static BOOL IsSameAppID(SERVICE_ID *pSrcID, SERVICE_ID *pDstID)
{
	return (0 == CompareSID(pSrcID, pDstID)) ? TRUE:FALSE;
}

int RemoveAppRouteRule(SYNOCOMM_APP *appSrc, SYNOCOMM_APP *appDst)
{
	int fd = -1;
	SYNOCOMM_RULE_ITEM *pItem = NULL, *pTmp = NULL;

	COMM_LIST_FOR_EACH_ENTRY_SAFE(pItem, pTmp, SYNOCOMM_RULE_ITEM, &appSrc->rule.ilist, ilist) {
		if (IsSameApp(pItem->pdest, appDst)) {
			UPDATE_DEL(&pItem->ilist, appSrc->rule.icount);
			fd = pItem->rule_index;
		}
	}

	return fd;
}

int RemoveAppRouteRuleByID(SYNOCOMM_APP *pApp, SERVICE_ID *pID)
{
	int fd = -1;
	SYNOCOMM_RULE_ITEM *pItem = NULL, *pTmp = NULL;

	COMM_LIST_FOR_EACH_ENTRY_SAFE(pItem, pTmp, SYNOCOMM_RULE_ITEM, &pApp->rule.ilist, ilist) {
		if (IsSameAppID(&pItem->pdest->serviceid, pID)) {
			UPDATE_DEL(&pItem->ilist, pApp->rule.icount);
			fd = pItem->rule_index;
		}
	}

	return fd;
}

int RemoveAppRouteRuleByFD(SYNOCOMM_APP *appSrc, int fd)
{
	int ret = -1;
	SYNOCOMM_RULE_ITEM *pItem = NULL, *pTmp = NULL;

	COMM_LIST_FOR_EACH_ENTRY_SAFE(pItem, pTmp, SYNOCOMM_RULE_ITEM, &appSrc->rule.ilist, ilist) {
		if (fd == pItem->rule_index) {
			UPDATE_DEL(&pItem->ilist, appSrc->rule.icount);
			ret = fd;
		}
	}

	return ret;
}

SYNOCOMM_APP *GetFromAppByRule(SYNOCOMM_APP *app, int appFd)
{
	P_SYNOCOMM_RULE_ITEM pItem = NULL;

	COMM_LIST_FOR_EACH_ENTRY_BEGIN(pItem, SYNOCOMM_RULE_ITEM, &app->rule.ilist, ilist)
	COMM_LIST_FOR_EACH_ENTRY(pItem, SYNOCOMM_RULE_ITEM, &app->rule.ilist, ilist) {
		if (pItem->rule_index == appFd) {
			return pItem->psrc;
		}
	}
	COMM_LIST_FOR_EACH_ENTRY_END

	return NULL;
}

SYNOCOMM_APP *GetToAppByRule(SYNOCOMM_APP *app, int appFd)
{
	P_SYNOCOMM_RULE_ITEM pItem = NULL;

	COMM_LIST_FOR_EACH_ENTRY_BEGIN(pItem, SYNOCOMM_RULE_ITEM, &app->rule.ilist, ilist)
	COMM_LIST_FOR_EACH_ENTRY(pItem, SYNOCOMM_RULE_ITEM, &app->rule.ilist, ilist) {
		if (pItem->rule_index == appFd) {
			return pItem->pdest;
		}
	}
	COMM_LIST_FOR_EACH_ENTRY_END

	return NULL;
}

static int GetRuleIndexByDstAppUUID(SYNOCOMM_APP *app, FIELD_UUID *appUUID)
{
	//FIXME it's workaround that same destination in the same rule set.
	//Just return the latest one.
	int workaround_index = -1;
	P_SYNOCOMM_RULE_ITEM pItem = NULL;

	MSG("################### Search UUID %s\n", appUUID->bits);
	COMM_LIST_FOR_EACH_ENTRY_BEGIN(pItem, SYNOCOMM_RULE_ITEM, &app->rule.ilist, ilist)
	COMM_LIST_FOR_EACH_ENTRY(pItem, SYNOCOMM_RULE_ITEM, &app->rule.ilist, ilist) {

		MSG("################### Current UUID %s, rule_index %d\n", pItem->pdest->serviceid.uuid_filed.bits, pItem->rule_index);
		if (0 == CompareUUID(&(pItem->pdest->serviceid.uuid_filed), appUUID)) {
			workaround_index = pItem->rule_index;
		}
	}
	COMM_LIST_FOR_EACH_ENTRY_END

	return workaround_index;
}

int AppSendControl(SYNOCOMM_APP *pApp, char *data, int len)
{
	SYNOCOMM_CHANNEL *pCh = pApp->channel;

	if (pCh) {
		return SENDMSG(pCh, data, len);
	} else {
		return -1;
	}
}

static int IsAppOnService(SYNOCOMM_APP *pApp)
{
	if (pApp) {
		if (APP_SERVICE_READY == pApp->status)
			return 0;
		return -(pApp->status);
	}
	return -1;
}

int AppRecvControl(SYNOCOMM_APP *pApp, char *data, int len)
{
	int fd = -1;
	int ret = -1;
	SYNOCOMM_CHANNEL *pCh = NULL;
	P_SYNOCOMM_APPNOTIFY pAppNty = NULL;

	if (NULL != pApp &&
		NULL != (pCh = pApp->channel)) {

		if (0 > (ret = IsAppOnService(pApp))) {
			MSG("pApp is not ready [%d]\n", ret);
			goto ERR;
		}

		memset(data, 0, len);
		ret = RECVMSG(pCh, data, len);
		if (0 > ret) {
			goto ERR;
		}
		pAppNty = (P_SYNOCOMM_APPNOTIFY)data;
		if (OPCODE_RAPP_BROKEN == pAppNty->opcode) {
			if (0 > (fd = RemoveAppRouteRuleByID(pApp, &pAppNty->sid))) {
				MSG("Fail to remove %s route info from %s\n", pAppNty->sid.serviceuuid, pApp->serviceid.serviceuuid);
			} else {
				MSG("Remove %s route info from %s Successfully\n", pAppNty->sid.serviceuuid, pApp->serviceid.serviceuuid);
			}
			ret = -1;
		} else if (OPCODE_SERVICESTOP == pAppNty->opcode) {
			MSG(">>>>>>>>>>>>>>>>>>>>>>>>>> App RECV OPCODE_SERVICESTOP\n");
			pApp->status = APP_SERVICE_STOPPED;
			ret = -APP_SERVICE_STOPPED;
		}
	}

ERR:
	return ret;
}

int AppSendMessage(SYNOCOMM_APP *pApp, int appFd, const char *data, int len)
{
	int err = -1;
	int total;
	SYNOCOMM_APP *pFromApp = NULL;
	SYNOCOMM_APP *pToApp = NULL;
	FIELD_UUID *pFromUUID = NULL;
	FIELD_UUID *pToUUID = NULL;
	P_SYNOCOMM_DATA pData = NULL;

	if (0 > appFd ||
		NULL == pApp) {
		MSG("Invalid parameter\n");
		goto ERR;
	}

	pFromApp = GetFromAppByRule(pApp, appFd);
	pToApp = GetToAppByRule(pApp, appFd);

	if (NULL == pFromApp ||
		NULL == pToApp) {
		MSG("No such routing info [%d] in %s.\n", appFd, pApp->serviceid.serviceuuid);
		goto ERR;
	}

	pFromUUID = &pFromApp->serviceid.uuid_filed;
	pToUUID = &pToApp->serviceid.uuid_filed;

	pData = CreateSendDataPacket(NULL, pFromUUID, pToUUID, data, len);

	total = COMM_DATA_PACKETS_SIZE(pData);

	if (pData) {
		err = AppSendControl(pApp, (char *)pData, total);
		free(pData);
	}

ERR:
	return err;
}

int AppUSendMessage(SYNOCOMM_APP *pApp, FIELD_UUID *pToUUID, const char *data, int len)
{
	int err = -1;

	FIELD_UUID *pFromUUID = &pApp->serviceid.uuid_filed;

	P_SYNOCOMM_DATA pData = CreateSendDataPacket(NULL, pFromUUID, pToUUID, data, len);

	int total = COMM_DATA_PACKETS_SIZE(pData);

	if (pData) {
		err = AppSendControl(pApp, (char *)pData, total);
		free(pData);
	}

	return err;
}

int AppRecvMessage(SYNOCOMM_APP *pApp, char *data, int len)
{
	return AppRecvControl(pApp, data, len);
}

int AppGetMessageFromFD(SYNOCOMM_APP *pApp, P_SYNOCOMM_DATA pData)
{
	FIELD_UUID from;
	ExtractFromUUIDByDataPacket(pData, &from);

	return GetRuleIndexByDstAppUUID(pApp, &from);
}

/*
 * Return message
 */
int AppRetMessage(SYNOCOMM_APP *pApp, P_SYNOCOMM_DATA pData, char *data, int len)
{
	int err = -1;
	FIELD_UUID from, to;

	if (len > MAX_DATA_PACKETS) {
		MSG("Data length(%d) exceed MAX_DATA_PACKETS(%d)\n", len, MAX_DATA_PACKETS);
		goto ERR;
	}

	ExtractFromUUIDByDataPacket(pData, &to);
	ExtractToUUIDByDataPacket(pData, &from);

	pData = CreateSendDataPacket(pData, &from, &to, data, len);

	if (pData) {
		err = AppSendControl(pApp, (char *)pData, COMM_DATA_PACKETS_SIZE(pData));
	}

	if (0 > FillInFromUUID(pData, &to) ||
		0 > FillInToUUID(pData, &from)) {
		EMSG("Fail to restore From To UUID\n");
		goto ERR;
	}

	err = 0;
ERR:
	return err;
}

SYNOCOMM_APP *AppGet(SYNOCOMM_APP *pApp)
{
	SYNOCOMM_APP *pAppOut = NULL;

	if (pApp) {
		SYNOCOMM_MUTEX_LOCK(pApp->mutex);
		MSG(">>>>>>>>>>>>>>>>> LOCK %s, ref_count %d, to_be_deleted %d\n", pApp->serviceid.serviceuuid, pApp->ref_count, pApp->to_be_deleted);
		if (0 == pApp->to_be_deleted) {
			pApp->ref_count++;
			MSG(">>>>>>>>>>>>>>>>> %s: ref_count %d\n", pApp->serviceid.serviceuuid, pApp->ref_count);
			pAppOut = pApp;
		}
		SYNOCOMM_MUTEX_UNLOCK(pApp->mutex);
	}
	return pAppOut;
}

void AppPut(SYNOCOMM_APP *pApp)
{
	if (pApp) {
		SYNOCOMM_MUTEX_LOCK(pApp->mutex);
		pApp->ref_count--;
		SYNOCOMM_MUTEX_UNLOCK(pApp->mutex);

		MSG(">>>>>>>>>>>>>>>>> %s: ref_count %d, to_be_deleted %d\n", pApp->serviceid.serviceuuid, pApp->ref_count, pApp->to_be_deleted);
		if (0 == pApp->ref_count &&
			1 == pApp->to_be_deleted) {
			MSG(">>>>>>>>>>>>>>>>> Release %s: ref_count %d, to_be_deleted %d\n", pApp->serviceid.serviceuuid, pApp->ref_count, pApp->to_be_deleted);
			ReleaseApp(pApp);
			MSG(">>>>>>>>>>>>>>>>> Release Done\n");
		}
	}
}

void AppFree(SYNOCOMM_APP *pApp)
{
	if (pApp) {
		SYNOCOMM_MUTEX_LOCK(pApp->mutex);
		pApp->to_be_deleted = 1;
		MSG(">>>>>>>>>>>>>>>>> %s: ref_count %d, to_be_deleted %d\n", pApp->serviceid.serviceuuid, pApp->ref_count, pApp->to_be_deleted);
		SYNOCOMM_MUTEX_UNLOCK(pApp->mutex);
	}

	AppPut(pApp);
}
