#ifndef WIN32
#include <synocomm/synocomm_macro.h>
#else
#include "synocomm_macro.h"
#endif

#ifndef _SYNO_COMM_H_
#define _SYNO_COMM_H_

#ifdef __cplusplus
__SYNO_BEGIN_DECLS
#endif

typedef struct _syno_appcomm_ {
	void *comm_app;
} SYNOAPPCOMM, *P_SYNOAPPCOMM;

typedef struct _syno_engcomm_ {
	void *pPRqRx;
	void *pPRqSn;
	void *pSRqLn;
} SYNOENGCOMM, *P_SYNOENGCOMM;

typedef struct _syno_comm_envelope_ {
	void *pData;
} SYNOCOMM_ENVELOPE, *P_SYNOCOMM_ENVELOPE;

//#define SYNOCOMM_EXPORT
#ifdef SYNOCOMM_EXPORT
#define SYNOCOMM_DLL_EXP_API __declspec(dllexport)
SYNOCOMM_DLL_EXP_API int DestroySynoComm(P_SYNOAPPCOMM pComm);
SYNOCOMM_DLL_EXP_API int InitSynoComm(P_SYNOAPPCOMM pComm, const char *local_hostip, const char *hostname, const char *app_name);
SYNOCOMM_DLL_EXP_API P_SYNOAPPCOMM CreateSynoComm(const char *local_hostip, const char *hostname, const char *app_name);
SYNOCOMM_DLL_EXP_API int SynoCommConnect(P_SYNOAPPCOMM pComm, const char *remote_hostip, const char *app_name);
SYNOCOMM_DLL_EXP_API int SynoCommDisconnect(P_SYNOAPPCOMM pComm, int fd);
SYNOCOMM_DLL_EXP_API int SynoCommIsConnect(P_SYNOAPPCOMM pComm, const char *remote_hostip, const char *app_name);
SYNOCOMM_DLL_EXP_API int SynoCommConnectionStatus(P_SYNOAPPCOMM pComm, const char *remote_hostip, const char *app_name);
SYNOCOMM_DLL_EXP_API int CommSendMessage(P_SYNOAPPCOMM pComm, int fd, const char *data, int len);
SYNOCOMM_DLL_EXP_API int CommRecvMessage(P_SYNOAPPCOMM pComm, char *data, int len);
SYNOCOMM_DLL_EXP_API P_SYNOENGCOMM CreateEngComm(const char *local_hostip, const char *hostname);
SYNOCOMM_DLL_EXP_API int StartEngCommSockServer(P_SYNOENGCOMM pEngComm);
SYNOCOMM_DLL_EXP_API int StopEngCommSockServer(P_SYNOENGCOMM pEngComm);
SYNOCOMM_DLL_EXP_API int StartEngCommPipeServer(P_SYNOENGCOMM pEngComm);
SYNOCOMM_DLL_EXP_API int StopEngCommPipeServer(P_SYNOENGCOMM pEngComm);
SYNOCOMM_DLL_EXP_API int StartConnectionDetector(P_SYNOENGCOMM pEngComm);

SYNOCOMM_DLL_EXP_API int InitSynoCommEvlp(P_SYNOCOMM_ENVELOPE pEvlp);
SYNOCOMM_DLL_EXP_API P_SYNOCOMM_ENVELOPE CreateSynoCommEvlp(void);
SYNOCOMM_DLL_EXP_API int CommRecvEvlp(P_SYNOAPPCOMM pComm, P_SYNOCOMM_ENVELOPE pEvlp);
SYNOCOMM_DLL_EXP_API char *CommGetEvlpData(P_SYNOCOMM_ENVELOPE pEvlp);
SYNOCOMM_DLL_EXP_API int CommRetEvlp(P_SYNOAPPCOMM pComm, P_SYNOCOMM_ENVELOPE pEvlp, char *data, int len);
SYNOCOMM_DLL_EXP_API char *CommGetEvlpData(P_SYNOCOMM_ENVELOPE pEvlp);
SYNOCOMM_DLL_EXP_API int CommGetEvlpFD(P_SYNOAPPCOMM pComm, P_SYNOCOMM_ENVELOPE pEvlp);

#else
extern int DestroySynoComm(P_SYNOAPPCOMM pComm);
extern int InitSynoComm(P_SYNOAPPCOMM pComm, const char *local_hostip, const char *hostname, const char *app_name);
extern P_SYNOAPPCOMM CreateSynoComm(const char *local_hostip, const char *hostname, const char *app_name);
extern int SynoCommConnect(P_SYNOAPPCOMM pComm, const char *remote_hostip, const char *app_name);
extern int SynoCommDisconnect(P_SYNOAPPCOMM pComm, int fd);
extern int SynoCommIsConnect(P_SYNOAPPCOMM pComm, const char *remote_hostip, const char *app_name);
extern int SynoCommConnectionStatus(P_SYNOAPPCOMM pComm, const char *remote_hostip, const char *app_name);
extern int CommSendMessage(P_SYNOAPPCOMM pComm, int fd, const char *data, int len);
extern int CommUSendMessage(P_SYNOAPPCOMM pComm, void *pToUUID, const char *data, int len);
extern int CommRecvMessage(P_SYNOAPPCOMM pComm, char *data, int len);
extern P_SYNOENGCOMM CreateEngComm(const char *local_hostip, const char *hostname);
extern int StartEngCommSockServer(P_SYNOENGCOMM pEngComm);
extern int StopEngCommSockServer(P_SYNOENGCOMM pEngComm);
extern int StartEngCommPipeServer(P_SYNOENGCOMM pEngComm);
extern int StopEngCommPipeServer(P_SYNOENGCOMM pEngComm);
extern  int StartConnectionDetector(P_SYNOENGCOMM pEngComm);

extern int InitSynoCommEvlp(P_SYNOCOMM_ENVELOPE pEvlp);
extern P_SYNOCOMM_ENVELOPE CreateSynoCommEvlp(void);
extern int CommRecvEvlp(P_SYNOAPPCOMM pComm, P_SYNOCOMM_ENVELOPE pEvlp);
extern char *CommGetEvlpData(P_SYNOCOMM_ENVELOPE pEvlp);
extern int CommRetEvlp(P_SYNOAPPCOMM pComm, P_SYNOCOMM_ENVELOPE pEvlp, char *data, int len);
extern char *CommGetEvlpData(P_SYNOCOMM_ENVELOPE pEvlp);
extern int CommGetEvlpFD(P_SYNOAPPCOMM pComm, P_SYNOCOMM_ENVELOPE pEvlp);

#endif

#ifdef __cplusplus
__SYNO_END_DECLS
#endif

#endif
