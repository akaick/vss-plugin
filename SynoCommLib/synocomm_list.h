#ifndef _SYNO_COMM_LIST_H_
#define _SYNO_COMM_LIST_H_

#ifndef WIN32
#include <synocomm/synocomm_base.h>
#else
#include "stdafx.h"
#include "synocomm_base.h"
#endif

#ifdef __cplusplus
__SYNO_BEGIN_DECLS
#endif

/*
 * It's compatible with struct qelem in <search.h>
 * You may need to include <search.h> or implement your own insque/remque
 * when using it. Please refer to insque/remque manpages for more details.
 */
typedef struct _synocomm_linkedlist_ {
    struct _synocomm_linkedlist_* q_forw;
    struct _synocomm_linkedlist_* q_back;
} SYNOCOMM_LINKEDLIST, *P_SYNOCOMM_LINKEDLIST;

#ifdef WIN32
//#define offsetof(s,m)   (size_t)&reinterpret_cast<const volatile char&>((((s *)0)->m))
static void insque (struct _synocomm_linkedlist_ *elem, struct _synocomm_linkedlist_ *pred)
{
  elem->q_forw = pred->q_forw;
  pred->q_forw->q_back = elem;
  elem->q_back = pred;
  pred->q_forw = elem;
}
static void remque (struct _synocomm_linkedlist_ *elem)
{
  elem->q_forw->q_back = elem->q_back;
  elem->q_back->q_forw = elem->q_forw;
}
static void *getstruct(char *m, int offset)
{
	return (void *)(m - offset);
}
#endif

#define COMM_LIST_HEAD_INIT(ptr)                                                \
        do {                                                                    \
            (ptr)->q_forw = (ptr); (ptr)->q_back = (ptr);                       \
        } while(0)

#define COMM_LIST_EMPTY(ptr)                                                    \
        ( (ptr)->q_forw == (ptr) )

/**
 * insert a new entry after the specified head.
 */
#define COMM_LIST_ADD(ptr, head)                                                \
        insque(ptr, head)

/**
 * insert a new entry before the specified head.
 */
#define COMM_LIST_ADD_TAIL(ptr, head)                                           \
        insque(ptr, (head)->q_back)

/**
 * delete entry from list
 */
#define COMM_LIST_DEL(ptr)                                                      \
        remque(ptr)

/**
 * get the structruct for this entry
 */
#ifdef WIN32
#define COMM_LIST_ENTRY(ptr, type, member) ((type *)getstruct((char *)ptr, offsetof(type, member)))
#else
#define COMM_LIST_ENTRY(ptr, type, member) ({                                   \
        const typeof(((type*)0)->member)*__mptr = (ptr);                        \
        (type*)((char*)__mptr - OFFSET_OF(type, member));})
#endif

/**
 * iterate over a list of given type
 */
#ifdef WIN32
#define	COMM_LIST_FOR_EACH_ENTRY_BEGIN(pos, type, head, member)                         \
	{                                                                                   \
		SYNOCOMM_LINKEDLIST *___listptr = (head)->q_forw;
#define COMM_LIST_FOR_EACH_ENTRY(pos, type, head, member)                               \
		for ( pos = COMM_LIST_ENTRY(___listptr, type, member);                          \
		___listptr != head;                                                             \
		___listptr = ___listptr->q_forw, pos = COMM_LIST_ENTRY(___listptr, type, member)\
		)
#define	COMM_LIST_FOR_EACH_ENTRY_END                                                    \
	}
#else
#define	COMM_LIST_FOR_EACH_ENTRY_BEGIN(pos, type, head, member)
#define COMM_LIST_FOR_EACH_ENTRY(pos, type, head, member)                       \
        for( pos = COMM_LIST_ENTRY((head)->q_forw, typeof(*pos), member);       \
             &pos->member != (head);                                            \
             pos = COMM_LIST_ENTRY(pos->member.q_forw, typeof(*pos), member))
#define	COMM_LIST_FOR_EACH_ENTRY_END
#endif

/**
 * iterate over a list safe against removal of list entry
 */
#ifdef WIN32
#define COMM_LIST_FOR_EACH_ENTRY_SAFE(pos, n, type, head, member)              \
        for( pos = COMM_LIST_ENTRY((head)->q_forw, type, member),              \
             n = COMM_LIST_ENTRY(pos->member.q_forw, type, member);            \
             &pos->member != (head);                                           \
             pos = n, n = COMM_LIST_ENTRY(n->member.q_forw, type, member))
#else
#define COMM_LIST_FOR_EACH_ENTRY_SAFE(pos, n, type, head, member)               \
        for( pos = COMM_LIST_ENTRY((head)->q_forw, typeof(*pos), member),       \
             n = COMM_LIST_ENTRY(pos->member.q_forw, typeof(*pos), member);     \
             &pos->member != (head);                                            \
             pos = n, n = COMM_LIST_ENTRY(n->member.q_forw, typeof(*n), member))
#endif

#define COMM_LIST_FREE(head, type, member)                                      \
        while( !COMM_LIST_EMPTY(head) ) {                                       \
            type* __lptr = COMM_LIST_ENTRY((head)->q_forw, type, member);       \
            COMM_LIST_DEL((head)->q_forw);                                      \
            free(__lptr);                                                       \
        }

#ifdef WIN32
#define COMM_LIST_FIRST_ENTRY(pos, type, head, member)                          \
        (pos = ((COMM_LIST_EMPTY(head)) ? NULL :                                \
		COMM_LIST_ENTRY((head)->q_forw, type, member)))
#else
#define COMM_LIST_FIRST_ENTRY(pos, type, head, member)                          \
        (pos = ((COMM_LIST_EMPTY(head)) ? NULL :                                \
        COMM_LIST_ENTRY((head)->q_forw, typeof(*pos), member)))
#endif

#ifdef WIN32
#define COMM_LIST_LAST_ENTRY(pos, type, head, member)                           \
        (pos = ((COMM_LIST_EMPTY(head)) ? NULL :                                \
        COMM_LIST_ENTRY((head)->q_back, type, member)))
#else
#define COMM_LIST_LAST_ENTRY(pos, type, head, member)                           \
        (pos = ((COMM_LIST_EMPTY(head)) ? NULL :                                \
        COMM_LIST_ENTRY((head)->q_back, typeof(*pos), member)))
#endif

/**
 * add item to list tail and update the count
 */
#define UPDATE_ADD_TAIL(ptr, head, count)                                       \
		{                                                                       \
			COMM_LIST_ADD_TAIL(ptr, head);                                      \
			count++;                                                            \
		} while(0)

/**
 * add item to list and update the count
 */
#define UPDATE_ADD(ptr, head, count)                                       \
		{                                                                  \
			COMM_LIST_ADD(ptr, head);                                      \
			count++;                                                       \
		} while(0)

#define UPDATE_DEL(ptr, count)                                  \
		{                                                       \
			COMM_LIST_DEL(ptr);                                 \
			count--;                                            \
		} while(0)

#ifdef __cplusplus
__SYNO_END_DECLS
#endif

#endif

