#ifndef _SYNO_COMM_CHANNEL_MANAGE_H_
#define _SYNO_COMM_CHANNEL_MANAGE_H_

#ifndef WIN32
#include <synocomm/synocomm_base.h>
#else
#include "synocomm_base.h"
#endif

#ifdef __cplusplus
__SYNO_BEGIN_DECLS
#endif

extern SYNOCOMM_CHANNEL *EnginePipeRequestReceiver(SYNOCOMM_HOST *host, const char *service_name);
extern SYNOCOMM_CHANNEL *EnginePipeRequestSender(SYNOCOMM_HOST *host, const char *service_name);
extern SYNOCOMM_CHANNEL *EngineSocketRequestListener(SYNOCOMM_HOST *host, const char *service_name);
extern SYNOCOMM_CHANNEL *EngineSocketRequestReceiver(SYNOCOMM_HOST *host, const char *service_name, SYNOCOMM_CHANNEL *ext_response);
extern int SetupEngineSocketRequestReceiver(SYNOCOMM_CHANNEL *pch, void *media);
extern SYNOCOMM_CHANNEL *EngineSocketRequestSender(SYNOCOMM_HOST *host, const char *service_name);
extern SYNOCOMM_CHANNEL *Engine2AppPipeResponseSender(SYNOCOMM_HOST *host, const char *service_name, int pid);
extern SYNOCOMM_CHANNEL *AppRequestResponse(SYNOCOMM_HOST *host, const char *service_name, int pid);

#ifdef __cplusplus
__SYNO_END_DECLS
#endif

#endif // _SYNO_COMM_CHANNEL_MANAGE_H_
