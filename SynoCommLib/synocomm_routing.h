#ifndef _SYNO_COMM_CHANNEL_ROUTING_H_
#define _SYNO_COMM_CHANNEL_ROUTING_H_

#ifndef WIN32
#include <synocomm/synocomm_base.h>
#include <synocomm/synocomm_types.h>
#else
#include "synocomm_base.h"
#include "synocomm_types.h"
#endif

#ifdef __cplusplus
__SYNO_BEGIN_DECLS
#endif

struct _synocomm_app_;
/*
 * a rule time belongs to rule and rule_set
 */
typedef struct _synocomm_rule_item_ {
	/*
	 * pointer to source app description
	 */
	//P_SYNOCOMM_APP psrc;
	struct _synocomm_app_ *psrc;
	/*
	 * pointer to dest app description
	 */
	//P_SYNOCOMM_APP pdest;
	struct _synocomm_app_ *pdest;
	/*
	 *
	 */
	int rule_index;
	/*
	 * a list to link next rule item
	 */
	SYNOCOMM_LINKEDLIST ilist;
	/*
	 * a list to rule-set
	 */
	SYNOCOMM_LINKEDLIST slist;
} SYNOCOMM_RULE_ITEM, *P_SYNOCOMM_RULE_ITEM;

/*
 * used in app to describe the routing info.
 */
typedef struct _synocomm_rule_ {
	/*
	 * rule count
	 */
	int icount;
	/*
	 * a list to link next rule item
	 */
	SYNOCOMM_LINKEDLIST ilist;
} SYNOCOMM_RULE, *P_SYNOCOMM_RULE;

/*
 * all rule_items collection
 * it's a global view to rule_items
 */
typedef struct _synocomm_rule_set_ {
	/*
	 * rule count
	 */
	int scount;
	/*
	 * a list to link all rules
	 */
	SYNOCOMM_LINKEDLIST slist;
} SYNOCOMM_RULE_SET, *P_SYNOCOMM_RULE_SET;

#ifdef __cplusplus
__SYNO_END_DECLS
#endif

#endif
