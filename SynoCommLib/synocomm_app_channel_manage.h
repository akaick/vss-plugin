#ifndef _SYNO_COMM_APP_CH_MANAGE_H_
#define _SYNO_COMM_APP_CH_MANAGE_H_

#ifndef WIN32
#include <synocomm/synocomm_base.h>
#include <synocomm/synocomm_app.h>
#include <synocomm/synocomm_host.h>
#else
#include "stdafx.h"
#include "synocomm_base.h"
#include "synocomm_app.h"
#include "synocomm_host.h"
#endif

#ifdef __cplusplus
__SYNO_BEGIN_DECLS
#endif

extern int AppRegister2Engine(SYNOCOMM_APP *pApp);
extern int AppURegister2Engine(SYNOCOMM_APP *pApp);
extern int AppConnect(SYNOCOMM_APP *pCliApp, const char *remote_host_ip, const char *service_name);
extern int AppIsConnect(SYNOCOMM_APP *pCliApp, const char *remote_host_ip, const char *service_name);
extern int AppConnectionStatus(SYNOCOMM_APP *pCliApp, const char *remote_host_ip, const char *service_name);
extern int AppDisconnect(SYNOCOMM_APP *pApp, int fd);

#ifdef __cplusplus
__SYNO_END_DECLS
#endif

#endif
