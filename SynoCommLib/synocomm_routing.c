#ifndef WIN32
#include <stdlib.h>
#include <synocomm/synocomm_base.h>
#include <synocomm/synocomm_types.h>
#include <synocomm/synocomm_app.h>
#include <synocomm/synocomm_routing.h>
#else
#include "stdafx.h"
#include "stddef.h"
#include "synocomm_base.h"
#include "synocomm_types.h"
#include "synocomm_app.h"
#include "synocomm_routing.h"
#endif

#define MAX_ROUTE_INDEX (256)

static int GetUnusedRuleIndex(SYNOCOMM_APP *pApp)
{
	int index;
	unsigned char __map[MAX_ROUTE_INDEX] = {0};
	SYNOCOMM_RULE_ITEM *pItem = NULL;

	COMM_LIST_FOR_EACH_ENTRY_BEGIN(pItem, SYNOCOMM_RULE_ITEM, &pApp->rule.ilist, ilist)
	COMM_LIST_FOR_EACH_ENTRY(pItem, SYNOCOMM_RULE_ITEM, &pApp->rule.ilist, ilist) {
		__map[pItem->rule_index] = 1;
	}
	COMM_LIST_FOR_EACH_ENTRY_END

	for (index = 0; index < MAX_ROUTE_INDEX; index++) {
		if (0 == __map[index]) {
			return index;
		}
	}
	return -1;
}

P_SYNOCOMM_RULE_ITEM CreateRuleItem(SYNOCOMM_APP *appFrom, SYNOCOMM_APP *appTo)
{
	int index = GetUnusedRuleIndex(appFrom);
	P_SYNOCOMM_RULE_ITEM pItem = NULL;

	if (0 > index) {
		MSG("Fail to create routing index\n");
		goto ERR;
	}

	pItem = (P_SYNOCOMM_RULE_ITEM)malloc(sizeof(SYNOCOMM_RULE_ITEM));

	if (pItem) {
		memset(pItem, 0, sizeof(SYNOCOMM_RULE_ITEM));
		pItem->psrc = appFrom;
		pItem->pdest = appTo;
		pItem->rule_index = index;
	}

ERR:
	return pItem;
}
