#ifndef _SYNO_COMM_CHANNEL_DESC_H_
#define _SYNO_COMM_CHANNEL_DESC_H_

#ifndef WIN32
#include <synocomm/synocomm_types.h>
#include <synocomm/synocomm_list.h>
#include <synocomm/synocomm_host.h>
#else
#include "synocomm_types.h"
#include "synocomm_list.h"
#include "synocomm_host.h"
#endif

#ifdef __cplusplus
__SYNO_BEGIN_DECLS
#endif

typedef struct _synocomm_channel_desc_ {
	/*
	 * sercie identity
	 */
	SERVICE_ID	serviceid;
	/*
	 * indicate the channel used in server/client side
	 */
	int role;
	/*
	 * list type: internal/extins/extouts/processes
	 */
	int listtype;
	/*
	 * pointer to the channel
	 */
	void *channel;
	/*
	 * list head for channel list set
	 */
	SYNOCOMM_LINKEDLIST chlist;
} SYNOCOMM_CHANNEL_DESC, *P_SYNOCOMM_CHANNEL_DESC;

#ifdef __cplusplus
__SYNO_END_DECLS
#endif

#endif
