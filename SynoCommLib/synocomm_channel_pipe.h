#ifndef _SYNO_COMM_CH_PIPE_H_
#define _SYNO_COMM_CH_PIPE_H_

#ifndef WIN32
#include <synocomm/synocomm_base.h>
#include <synocomm/synocomm_transport.h>
#else
#include "synocomm_base.h"
#include "synocomm_transport.h"
#endif

#ifdef __cplusplus
__SYNO_BEGIN_DECLS
#endif

#ifndef WIN32
#define SYNOCOMM_CHAN_PIPE_ROOT		"/tmp/"
typedef int SYNOCOMM_PIPE_FD_TYPE;
#else
#define SYNOCOMM_CHAN_PIPE_ROOT		"\\\\.\\pipe\\"
typedef HANDLE SYNOCOMM_PIPE_FD_TYPE;
#define SYNOCOMM_CHAN_PIPE_BUFSIZE	4096
#define SYNOCOMM_CHAN_PIPE_TIMEOUT	1000000
#endif

#define SYNOCOMM_CHAN_PIPE_REQ_PATH SYNOCOMM_CHAN_PIPE_ROOT"synocomm_pipe_req_%d"
#define SYNOCOMM_CHAN_PIPE_REP_PATH SYNOCOMM_CHAN_PIPE_ROOT"synocomm_pipe_rep_%d"

#define	SYNOCOMM_CHAN_PIPE_MSG	(-1)
#define	SYNOCOMM_CHAN_PIPE_CTL	(-2)
#define SYNOCOMM_CHAN_PIPE_SVR_REQ		SYNOCOMM_CHAN_PIPE_ROOT"synocomm_pipe_svr_msg_req"
#define SYNOCOMM_CHAN_PIPE_SVR_REP		SYNOCOMM_CHAN_PIPE_ROOT"synocomm_pipe_svr_msg_rep"
#define SYNOCOMM_CHAN_PIPE_SVR_CTL_REQ	SYNOCOMM_CHAN_PIPE_ROOT"synocomm_pipe_svr_ctl_req"
#define SYNOCOMM_CHAN_PIPE_SVR_CTL_REP	SYNOCOMM_CHAN_PIPE_ROOT"synocomm_pipe_svr_ctl_rep"

extern SYNOCOMM_TRANSPORT pipe_channel_transport;

#ifdef WIN32
extern struct _synocomm_channel_ *TestServerPipe(char *name, DWORD count);
extern struct _synocomm_channel_ *TestClientPipe(char *name);
extern int TestServerRead(struct _synocomm_channel_ *pch, char *data, DWORD length, DWORD *cbRead);
extern int TestClientWrite(void *media, char *data, int length);
#endif

#ifdef __cplusplus
__SYNO_END_DECLS
#endif

#endif // _SYNO_COMM_CH_PIPE_H_

