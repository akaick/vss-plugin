#ifndef WIN32
#include <stdlib.h>
#include <synocomm/synocomm_base.h>
#include <synocomm/synocomm_message.h>

#else
#include "synocomm_base.h"
#include "synocomm_message.h"

#endif

static P_SYNOCOMM_DATA CreateDataPacket(P_SYNOCOMM_DATA _p, FIELD_UUID *from, FIELD_UUID *to, const char *s, int l)
{
	P_SYNOCOMM_DATA p = NULL;

	if (NULL == s) {
		MSG("Invalid parameter\n");
		goto ERR;
	}

	if (l < 0 || l > MAX_DATA_PACKETS) {
		MSG("Invalid parameter\n");
		goto ERR;
	}

	if (NULL == from ||
		NULL == to) {
		MSG("Invalid parameter\n");
		goto ERR;
	}

	if (NULL == _p) {
		p = (P_SYNOCOMM_DATA)malloc(sizeof(SYNOCOMM_DATA));
	} else {
		p = _p;
	}

	memset(p, 0, sizeof(SYNOCOMM_DATA));
	memcpy(p->data, s, l);
	p->len = l;

	memcpy(&p->from_uuid, from, sizeof(FIELD_UUID));
	memcpy(&p->to_uuid, to, sizeof(FIELD_UUID));
ERR:
	return p;
}

P_SYNOCOMM_DATA CreateSendDataPacket(P_SYNOCOMM_DATA _p, FIELD_UUID *from, FIELD_UUID *to, const char *s, int l)
{
	P_SYNOCOMM_DATA p = CreateDataPacket(_p, from, to, s, l);

	if (NULL  == p) {
		goto ERR;
	}

	p->opcode = OPCODE_SENDMSG;
ERR:
	return p;
}

P_SYNOCOMM_DATA CreateRecvDataPacket(P_SYNOCOMM_DATA _p, FIELD_UUID *from, FIELD_UUID *to, const char *s, int l)
{
	P_SYNOCOMM_DATA p = CreateDataPacket(_p, from, to, s, l);

	if (NULL  == p) {
		goto ERR;
	}

	p->opcode = OPCODE_RECVMSG;
ERR:
	return p;
}

int ExtractFromUUIDByDataPacket(P_SYNOCOMM_DATA p, FIELD_UUID *from)
{
	int err = -1;
	if (NULL == p ||
		NULL == from) {
		EMSG("Invalid parameter\n");
		goto ERR;
	}

	memcpy(from, &p->from_uuid, sizeof(FIELD_UUID));
	err = 0;
ERR:
	return err;
}

int ExtractToUUIDByDataPacket(P_SYNOCOMM_DATA p, FIELD_UUID *to)
{
	int err = -1;
	if (NULL == p ||
		NULL == to) {
		EMSG("Invalid parameter\n");
		goto ERR;
	}

	memcpy(to, &p->to_uuid, sizeof(FIELD_UUID));
	err = 0;
ERR:
	return err;
}

int FillInFromUUID(P_SYNOCOMM_DATA p, FIELD_UUID *from)
{
	int err = -1;
	if (NULL == p ||
		NULL == from) {
		EMSG("Invalid parameter\n");
		goto ERR;
	}

	memcpy(&p->from_uuid, from, sizeof(FIELD_UUID));
	err = 0;
ERR:
	return err;
}

int FillInToUUID(P_SYNOCOMM_DATA p, FIELD_UUID *to)
{
	int err = -1;
	if (NULL == p ||
		NULL == to) {
		EMSG("Invalid parameter\n");
		goto ERR;
	}

	memcpy(&p->to_uuid, to, sizeof(FIELD_UUID));
	err = 0;
ERR:
	return err;
}
