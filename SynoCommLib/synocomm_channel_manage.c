#ifndef WIN32
#include <synocomm/synocomm_base.h>
#include <synocomm/synocomm_transport.h>
#include <synocomm/synocomm_channel_pipe.h>
#include <synocomm/synocomm_channel_socket.h>
#else
#include "stdafx.h"
#include "synocomm_base.h"
#include "synocomm_transport.h"
#include "synocomm_channel_pipe.h"
#include "synocomm_channel_socket.h"
#endif

/*
 * Here's new transport interface
 */
SYNOCOMM_TRANSPORT *transports[] = {
	&pipe_channel_transport,
	&socket_channel_transport,
	NULL
};

const SYNOCOMM_TRANSPORT *__get_transport(const CH_TYPE t)
{
	int i = 0;
	while (transports[i] && (t != transports[i]->media_type))
		i++;
	return transports[i];
}

/*
 * ctype: ENG_CH/APP_CH
 *
 * This function is used to combine the host and channel to connect to remote
 */
static int __setup_channel_request_response(const SYNOCOMM_TRANSPORT *t,
		SYNOCOMM_CHANNEL *pch, SYNOCOMM_HOST *host,
		const int ext, SYNOCOMM_CHANNEL *ext_request, SYNOCOMM_CHANNEL *ext_response)
{
	int err = -1;

	if (pch) {

		if (INTERNAL_REQUEST(ext)) {
			MSG("ext 0x%x\n", ext);
			if (0 > t->create_internal_request(pch, host)) {
				MSG("Fail to create internal request\n");
				goto ERR;
			}
		} else if (EXTERNAL_REQUEST(ext)) {
			MSG("ext 0x%x, ext_request %p\n", ext, ext_request);
			if (ext_request) {
				if (0 > t->setup_external_request(pch, ext_request)) {
					MSG("Fail to setup external request\n");
					goto ERR;
				}
			} else {
				EMSG("setup_external_request fail\n");
			}
		}
		if (INTERNAL_RESPONSE(ext)) {
			MSG("ext 0x%x\n", ext);
			if (0 > t->create_internal_response(pch, host)) {
				MSG("Fail to create internal response\n");
				goto ERR;
			}
		} else if (EXTERNAL_RESPONSE(ext)) {
			MSG("ext 0x%x, ext_response %p\n", ext, ext_response);
			if (ext_response) {
				if (0 > t->setup_external_response(pch, ext_response)) {
					MSG("Fail to setup external response\n");
					goto ERR;
				}
			} else {
				EMSG("setup_external_response fail\n");
			}
		}

		err = 0;
	}

ERR:
	return err;
}

static int __setup_internal_media(const SYNOCOMM_TRANSPORT *t, SYNOCOMM_CHANNEL *pch,
		const int ext, void *int_request_media, void *int_response_media)
{
	int err = -1;

	if (pch) {
		if (INTERNAL_REQUEST(ext)) {
			t->setup_internal_request(pch, int_request_media);
		}

		if (INTERNAL_RESPONSE(ext)) {
			t->setup_internal_response(pch, int_response_media);
		}

		err = 0;
	}

	return err;
}

static SYNOCOMM_CHANNEL *__create_channel(const CH_TYPE ttype, const int ctype,
		const int role, const int pid, const char *service_name, SYNOCOMM_HOST *host,
		const int ext, SYNOCOMM_CHANNEL *ext_request, SYNOCOMM_CHANNEL *ext_response)
{
	SYNOCOMM_CHANNEL *ch = NULL;
	const SYNOCOMM_TRANSPORT *t = __get_transport(ttype);

	if (NULL == t) {
		MSG("No such type[%d] transport", ttype);
		goto ERR;
	}

	if (NULL == (ch = t->construct(NULL, ctype, role, pid, service_name))) {
		goto ERR;
	}

	ch->to_be_deleted = 0;
	ch->ref_count = 0;
	//pthread_mutex_init(&ch->mutex, NULL);
	SYNOCOMM_MUTEX_INIT(ch->mutex);

	if (ext) {
		MSG("ext 0x%x\n", ext);
		if (0 > __setup_channel_request_response(t, ch, host, ext, ext_request, ext_response)) {
			MSG("Fail to setup channel request/response\n");
			t->destruct(ch);
			ch = NULL;
			goto ERR;
		}
	}

ERR:
	return ch;
}

static SYNOCOMM_CHANNEL *__create_engine_server_channel(int ttype, int pid, const char *service_name, SYNOCOMM_HOST *host,
		const int ext, SYNOCOMM_CHANNEL *ext_request, SYNOCOMM_CHANNEL *ext_response)
{
	return __create_channel((CH_TYPE)ttype, ENG_CH, SERVER_END, pid, service_name, host, ext, ext_request, ext_response);
}

static SYNOCOMM_CHANNEL *__create_engine_client_channel(int ttype, int pid, const char *service_name, SYNOCOMM_HOST *host,
		const int ext, SYNOCOMM_CHANNEL *ext_request, SYNOCOMM_CHANNEL *ext_response)
{
	return __create_channel((CH_TYPE)ttype, ENG_CH, CLIENT_END, pid, service_name, host, ext, ext_request, ext_response);
}

static SYNOCOMM_CHANNEL *__create_app_server_channel(int ttype, int pid, const char *service_name, SYNOCOMM_HOST *host,
		const int ext, SYNOCOMM_CHANNEL *ext_request, SYNOCOMM_CHANNEL *ext_response)
{
	return __create_channel((CH_TYPE)ttype, APP_CH, SERVER_END, pid, service_name, host, ext, ext_request, ext_response);
}

static SYNOCOMM_CHANNEL *__create_app_client_channel(int ttype, int pid, const char *service_name, SYNOCOMM_HOST *host,
		const int ext, SYNOCOMM_CHANNEL *ext_request, SYNOCOMM_CHANNEL *ext_response)
{
	return __create_channel((CH_TYPE)ttype, APP_CH, CLIENT_END, pid, service_name, host, ext, ext_request, ext_response);
}


/*
 * for external interface
 */
SYNOCOMM_CHANNEL *EnginePipeRequestReceiver(SYNOCOMM_HOST *host, const char *service_name)
{
	return __create_engine_server_channel(CH_NAMEPIPE, SYNOCOMM_CHAN_PIPE_MSG, service_name, host, INT_REQUEST, NULL, NULL);
}

SYNOCOMM_CHANNEL *EnginePipeRequestSender(SYNOCOMM_HOST *host, const char *service_name)
{
	return __create_engine_client_channel(CH_NAMEPIPE, SYNOCOMM_CHAN_PIPE_MSG, service_name, host, INT_REQUEST, NULL, NULL);
}

SYNOCOMM_CHANNEL *EngineSocketRequestListener(SYNOCOMM_HOST *host, const char *service_name)
{
	return __create_engine_server_channel(CH_SOCKET, 0, service_name, host, INT_REQUEST, NULL, NULL);
}

SYNOCOMM_CHANNEL *EngineSocketRequestReceiver(SYNOCOMM_HOST *host, const char *service_name, SYNOCOMM_CHANNEL *ext_response)
{
	return __create_engine_server_channel(CH_SOCKET, 0, service_name, host, EXT_RESPONSE, NULL, ext_response);
}

int SetupEngineSocketRequestReceiver(SYNOCOMM_CHANNEL *pch, void *media)
{
	const SYNOCOMM_TRANSPORT *t = __get_transport(CH_SOCKET);

	return __setup_internal_media(t, pch, INT_REQUEST, media, NULL);
}

SYNOCOMM_CHANNEL *EngineSocketRequestSender(SYNOCOMM_HOST *host, const char *service_name)
{
	return __create_engine_client_channel(CH_SOCKET, 0, service_name, host, INT_REQUEST, NULL, NULL);
}

SYNOCOMM_CHANNEL *Engine2AppPipeResponseSender(SYNOCOMM_HOST *host, const char *service_name, int pid)
{
	return __create_app_server_channel(CH_NAMEPIPE, pid, service_name, host, INT_RESPONSE, NULL, NULL);
}

/*
 * This is for Creating App Channel
 */
SYNOCOMM_CHANNEL *AppRequestResponse(SYNOCOMM_HOST *host, const char *service_name, int pid)
{
	SYNOCOMM_CHANNEL *pPRqSn = EnginePipeRequestSender(host, "PRqSn");
	return  __create_app_client_channel(CH_NAMEPIPE, pid, service_name, host, INT_RESPONSE|EXT_REQUEST, pPRqSn, NULL);
}

