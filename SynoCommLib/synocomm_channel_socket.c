#ifndef WIN32
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <errno.h>

#include <synocomm/synocomm_base.h>
#include <synocomm/synocomm_channel_socket.h>
#include <synocomm/synocomm_eng_channel_manage.h>
#include <synocomm/synocomm_channel.h>

#else
#include "stdafx.h"
#include "io.h"
#include "synocomm_base.h"
#include "synocomm_channel_socket.h"
#include "synocomm_eng_channel_manage.h"
#include "synocomm_channel.h"

// link with Ws2_32.lib
#pragma comment(lib, "Ws2_32.lib")

#endif

typedef struct _synocomm_socket_media_ {
	SYNOCOMM_SOCKET_FD_TYPE listen_fd;
	SYNOCOMM_SOCKET_FD_TYPE request_fd;
	/*
	 * server_ip
	 */
    struct sockaddr_in serv_addr; 
	/*
	 * sigaction
	 */
#ifndef WIN32
    struct sigaction sa;
#endif
	/*
	 * pipe name
	 */
	char name[MAX_PATH_LEN];
	/*
	 * The last time socket received message
	 */
	time_t  lrecv;
	/*
	 * The last time socket send message
	 */
	time_t  lsend;
	/*
	 * indicate if the socket alive ?
	 */
	int           alive;
} SYNOCOMM_SOCKET, *P_SYNOCOMM_SOCKET;

struct _synocomm_channel_ *synocomm_socket_construct(struct _synocomm_channel_ *ch, int ctype, int role, int pid, const char *service_name)
{
	int err = -1;
	SYNOCOMM_CHANNEL_DESC *pdesc = NULL;
	struct _synocomm_channel_ *pch = synocomm_base_construct(ch, ctype, role, pid, service_name);

	if (NULL == pch) {
		goto ERR;
	}

	pch->transport = &socket_channel_transport;
	pdesc = (SYNOCOMM_CHANNEL_DESC *)pch->channel_desc;

	if (0 != ServiceIDGenerateUUID(&pdesc->serviceid)) {
		goto ERR;
	}

	err = 0;
ERR:
	if (err && pch) {
		if (pch->channel_desc) {
			free(pch->channel_desc);
		}
		free(pch);
		pch = NULL;
	}
	return pch;
}

static void __synocomm_socket_shutdown_fd(P_SYNOCOMM_SOCKET p)
{
#ifndef WIN32
	if (p->request_fd) {
		shutdown(p->request_fd, SHUT_RDWR);
		close(p->request_fd);
	}
	if (p->listen_fd) {
		shutdown(p->listen_fd, SHUT_RDWR);
		close(p->listen_fd);
	}
#else
	if (p->request_fd) {
		shutdown(p->request_fd, SD_BOTH);
		closesocket(p->request_fd);
	}
	if (p->listen_fd) {
		shutdown(p->listen_fd, SD_BOTH);
		closesocket(p->listen_fd);
	}
#endif
}

static void __synocomm_socket_destruct(P_SYNOCOMM_SOCKET p)
{

	if (!p)
		return;
	__synocomm_socket_shutdown_fd(p);
	free(p);
}

void synocomm_socket_destruct(struct _synocomm_channel_ *ch)
{
	SYNOCOMM_CHANNEL_DESC *pDesc = NULL;
	if (!ch)
		return;

	pDesc = ch->channel_desc;
	MSG(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Free Channel %p, %s\n", ch, pDesc->serviceid.serviceuuid);

	if (ch->external_request) {
		synocomm_base_destruct(ch->external_request_channel);
		ch->external_request_channel = NULL;
		ch->external_request = 0;
	}
	//FIXME Should add reference count to represent if only one referece it
	//If it's ture, we can destruct it.
#if 0
	if (ch->external_response) {
		synocomm_base_destruct(ch->external_response_channel);
		ch->external_response_channel = NULL;
		ch->external_response = 0;
	}
#endif

	__synocomm_socket_destruct((P_SYNOCOMM_SOCKET)ch->request_channel);
	__synocomm_socket_destruct((P_SYNOCOMM_SOCKET)ch->response_channel);

	free(ch);
	return;
}

static
int __socket_stop_client(SYNOCOMM_CHANNEL *pSRqSn)
{
	int err = -1;
	extern SYNOCOMM_CHANNEL_SET *pChannelSet;
	if (NULL == pSRqSn ||
		NULL == pChannelSet) {
		goto ERR;
	}

	RemoveChannel(pChannelSet, pSRqSn);

	TRANSPORT(pSRqSn)->destruct(pSRqSn);
	err = 0;
ERR:
	return err;
}

BOOL __is_general_channel(SYNOCOMM_CHANNEL *pCh)
{
	SYNOCOMM_CHANNEL_DESC *pDesc = NULL;

	if (NULL == pCh ||
		NULL == (pDesc = (SYNOCOMM_CHANNEL_DESC *)pCh->channel_desc)) {
		return FALSE;
	}

	//FIXME should add an attribute for identify
	if (5 < strlen(pDesc->serviceid.service_name)) {
		return TRUE;
	}

	return FALSE;
}

static
int __socket_stop_server(SYNOCOMM_CHANNEL *pSRqRx_)
{
	int err = -1;
	char szSRqSn[MAX_SERVICE_NAME_LEN] = {0};
	SYNOCOMM_CHANNEL *pSRqSn = NULL;

	extern SYNOCOMM_CHANNEL_SET *pChannelSet;
	SYNOCOMM_CHANNEL_DESC *pDesc = NULL;

	if (NULL == pSRqRx_ ||
		NULL == pChannelSet) {
		goto ERR;
	}

	pDesc = (SYNOCOMM_CHANNEL_DESC *)pSRqRx_->channel_desc;
	RemoveChannel(pChannelSet, pSRqRx_);

	//FIXME Remove SRqSn_192.168.x.x
	if (TRUE == __is_general_channel(pSRqRx_)) {
		memcpy(szSRqSn, pDesc->serviceid.service_name, strlen(pDesc->serviceid.service_name));
		memcpy(szSRqSn, "SRqSn_", 6);
		MSG("Removeing .................. %s\n", szSRqSn);

		pSRqSn = GetExternalOutChannel(pChannelSet, szSRqSn);
		if (pSRqSn) {
			__synocomm_socket_shutdown_fd((P_SYNOCOMM_SOCKET)pSRqSn->request_channel);
			RemoveChannel(pChannelSet, pSRqSn);
		}
		ChannelFree(pSRqSn);
	}

	//SRqRx will thread will exit automatically
	//we need to destruct it.
	if (0 == strcmp(pDesc->serviceid.service_name, "SRqLn")) {
		MSG("Just close the SRqLn fd first ................ \n");
		__synocomm_socket_shutdown_fd((P_SYNOCOMM_SOCKET)pSRqRx_->request_channel);
	} else {
		TRANSPORT(pSRqRx_)->destruct(pSRqRx_);
	}
	err = 0;
ERR:
	return err;
}

#ifndef WIN32
static void *__socket_server_routine(void *param)
#else
static void __socket_server_routine(void *param)
#endif
{
	int err = -1;
	int len = 0;
	char *szCmd = (char *)malloc(1024);

	SYNOCOMM_CHANNEL *pSRqRx_ = (SYNOCOMM_CHANNEL *)param;

	SYNOCOMM_CHANNEL_DESC *pDesc = (SYNOCOMM_CHANNEL_DESC *)pSRqRx_->channel_desc;
	MSG("START SERVER %s\n", pDesc->serviceid.service_name);

	while (1) {
		len = RECVMSG(pSRqRx_, szCmd, 1024);

		if (len > 0) {
			if (szCmd[0]) {
				SENDMSG(pSRqRx_, szCmd, len);
			}
			memset(szCmd, 0, 1024);
			len = 0;
		} else {
			break;
		}
	}

	__socket_stop_server(pSRqRx_);
	if (szCmd) {
		free(szCmd);
	}
	err = 0;
#ifndef WIN32
	return NULL;
#endif
}

static int __start_server_service(struct _synocomm_channel_ *ch)
{
	int err = -1;
	P_SYNOCOMM_SOCKET p = NULL;
	SYNOCOMM_CHANNEL_DESC *pdesc = (SYNOCOMM_CHANNEL_DESC *)ch->channel_desc;

	MSG("pch %p, pdesc %p, role %d\n", ch, pdesc, pdesc->role);
	if (SERVER_END == pdesc->role) {
		p = (P_SYNOCOMM_SOCKET)ch->request_channel;
		MSG("ch->request_channel %p\n", ch->request_channel);
	} else if (CLIENT_END == pdesc->role) {
		p = (P_SYNOCOMM_SOCKET)ch->response_channel;
		MSG("ch->response_channel %p\n", ch->response_channel);
	} else {
		goto ERR;
	}

	while(1) {

        struct sockaddr_in host_addr;
#ifndef WIN32
        size_t size = sizeof(struct sockaddr_in);
#else
        int size = sizeof(struct sockaddr_in);
#endif
		MSG("Allow Accept\n");

		p->request_fd = accept(p->listen_fd, (struct sockaddr*)&host_addr, &size);
		MSG("After accept, p->request_fd %d\n", p->request_fd);

#ifndef WIN32
        if (-1 == p->request_fd) {
#else
        if (INVALID_SOCKET == p->request_fd) {
#endif
            MSG("Invalid socket\n");
			break;
        } else {

#ifndef WIN32
			pthread_t thread;
			pthread_attr_t attr;
#else
			uintptr_t thread;
#endif

			P_SYNOCOMM_SOCKET pMedia = (P_SYNOCOMM_SOCKET)malloc(sizeof(SYNOCOMM_SOCKET));
			SYNOCOMM_CHANNEL *pSRqRx = NULL;

			memset(pMedia, 0, sizeof(SYNOCOMM_SOCKET));
			pMedia->request_fd = p->request_fd;
			memcpy(&pMedia->serv_addr, &host_addr, sizeof(struct sockaddr_in));

			if (NULL == (pSRqRx = DupSocketRequestRxChannel(inet_ntoa(host_addr.sin_addr), inet_ntoa(host_addr.sin_addr), pMedia))) {
				if (pMedia->request_fd) {
					close(pMedia->request_fd);
				}
				free(pMedia);
				MSG("pSRqRx %p is NULL ???????\n", pSRqRx);
				continue;
			}

#ifndef WIN32
			if (pthread_attr_init(&attr)) {
				MSG("pthread_attr_init failed\n");
				goto ERR;
			}

			if (pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED)) {
				MSG("set PTHREAD_CREATE_DETACHED failed\n");
				goto ERR;
			}

			if (pthread_create(&thread, &attr, __socket_server_routine, pSRqRx) != 0) {
               fprintf(stderr, "Failed to create thread\n");
				goto ERR;
			}
#else
			if (-1 == (thread = _beginthread(__socket_server_routine, 0, pSRqRx))) {
				MSG("Failed to create thread\n");
				goto ERR;
			}
#endif
		}
	}
	err = 0;
ERR:
	return err;
}

int synocomm_socket_start_service(struct _synocomm_channel_ *ch)
{
	return __start_server_service(ch);
}

int synocomm_socket_stop_service(struct _synocomm_channel_ *ch)
{
	//TODO
	int err = -1;
	SYNOCOMM_CHANNEL_DESC *pDesc = NULL;

	if (NULL == ch ||
		NULL == (pDesc = (SYNOCOMM_CHANNEL_DESC *)ch->channel_desc)) {
		MSG("Invalid parameter\n");
		goto ERR;
	}

	if (SERVER_END == pDesc->role) {
		err = __socket_stop_server(ch);
	} else if (CLIENT_END == pDesc->role) {
		err = __socket_stop_client(ch);
	}

ERR:
	return err;
}

#ifndef WIN32
/* Signal handler to reap zombie processes */
static void wait_for_child(int sig)
{
    while (waitpid(-1, NULL, WNOHANG) > 0);
}
#endif

static P_SYNOCOMM_SOCKET __create_server_socket(char *name, int port)
{
	P_SYNOCOMM_SOCKET p = NULL;

	int reuseaddr = 1;

	p = (P_SYNOCOMM_SOCKET)malloc(sizeof(SYNOCOMM_SOCKET));
	memset(p, 0, sizeof(SYNOCOMM_SOCKET));

	/*
	 * FIXME record the name for this server
	 */
	snprintf(p->name, MAX_PATH_LEN, name);
	MSG("p->name %s\n", p->name);

	p->listen_fd = socket(AF_INET, SOCK_STREAM, 0);
    memset(&(p->serv_addr), '0', sizeof(p->serv_addr));

	p->serv_addr.sin_family = AF_INET;
	p->serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	p->serv_addr.sin_port = htons(port);
    /* Enable the socket to reuse the address */
#ifdef WIN32
    if (setsockopt(p->listen_fd, SOL_SOCKET, SO_REUSEADDR, (const char *)&reuseaddr, sizeof(int)) == SOCKET_ERROR) {
#else
	if (setsockopt(p->listen_fd, SOL_SOCKET, SO_REUSEADDR, &reuseaddr, sizeof(int)) == -1) {
#endif
        MSG("Fail to set reuse\n");
        goto ERR;
    }

	bind(p->listen_fd, (struct sockaddr*)&(p->serv_addr), sizeof(p->serv_addr));

	if (-1 ==listen(p->listen_fd, 10)) {
#ifndef WIN32
		MSG("Error listening\n");
#else
		MSG("Error listening %d\n", WSAGetLastError());
#endif
	}

#ifndef WIN32
	/* Set up the signal handler */
	p->sa.sa_handler = wait_for_child;
	sigemptyset(&p->sa.sa_mask);
	p->sa.sa_flags = SA_RESTART;
	if (sigaction(SIGCHLD, &p->sa, NULL) == -1) {
		MSG("signation error");
		goto ERR;
	}
#endif

	return p;
ERR:
/*
#ifdef WIN32
	if (0 == ret) {
		WSACleanup();
	}
#endif
*/
	if (p) {
		free(p);
	}
	return NULL;
}

static int __nonblocking_client_socket(P_SYNOCOMM_SOCKET p, char *ip, int port)
{
	int err = -1;
	int valopt;
	int status;
	struct timeval  timeout;
	socklen_t lon;
	fd_set set;
#ifdef WIN32
	int iResult;
	unsigned long mode;
#else
	long sockarg;
#endif
	timeout.tv_sec = 10;
	timeout.tv_usec = 0;
	FD_ZERO(&set);
	FD_SET(p->request_fd, &set);
#ifndef WIN32
	fcntl(p->request_fd, F_SETFL, O_NONBLOCK);
#else
	/* mode != 0, turn on non-blocking, mode = 0, turn off non-blocking */
	mode = 1;
	iResult = ioctlsocket(p->request_fd, FIONBIO, &mode);

	if (iResult != NO_ERROR) {
		MSG("Connect Failed with error %ld\n", iResult);
		goto ERR;
	}
#endif

	MSG("Try to connect ip %s\n", ip);
	if((status = connect(p->request_fd, (struct sockaddr *)&(p->serv_addr), sizeof(p->serv_addr))) < 0) {

#ifndef WIN32
		if (errno != EINPROGRESS) {
			MSG("Connect Failed, %d - %s\n", errno, strerror(errno));
			goto ERR;
		} else {
			MSG("Connect InProgress\n");
		}
#endif
	}

	if ((status = select(p->request_fd+1, NULL, &set, NULL, &timeout)) > 0) {
		MSG("Connect to %s Successfully status=%d\n", ip, status);
	} else {
		getsockopt(p->request_fd, SOL_SOCKET, SO_ERROR, (void*)(&valopt), &lon);
		MSG("Connect Failed, Timeout or %d - %s\n", valopt, strerror(valopt));
		goto ERR;
	}

#ifndef WIN32
	sockarg = fcntl(p->request_fd, F_GETFL, NULL);
	sockarg &= (~O_NONBLOCK);
	fcntl(p->request_fd, F_SETFL, sockarg);
#else
	mode = 0;
	iResult = ioctlsocket(p->request_fd, FIONBIO, &mode);
	if (iResult != NO_ERROR) {
		MSG("Connect Failed with error %ld\n", iResult);
		goto ERR;
	}
#endif

	err = 0;
ERR:
	return err;
}

#ifdef WIN32
int InitWinWSA()
{
	int err = -1;
	int ret;

	WORD wVersion = MAKEWORD(2, 2);
	WSADATA wsaData;

    if ((ret = WSAStartup(wVersion, &wsaData)) != 0) {
        EMSG("WSAStartup failed: %d\n", ret);
        goto ERR;
    }

	err = 0;
ERR:
	return err;
}
#endif

static P_SYNOCOMM_SOCKET __open_client_socket(char *name, char *ip, int port)
{
	P_SYNOCOMM_SOCKET p = NULL;

	if (NULL == (p = (P_SYNOCOMM_SOCKET)malloc(sizeof(SYNOCOMM_SOCKET)))) {
		MSG("Fail to malloc SYNOCOMM_SOCKET\n");
		goto ERR;
	}
	memset(p, 0, sizeof(SYNOCOMM_SOCKET));

	snprintf(p->name, MAX_PATH_LEN, name);
	MSG("p->name %s\n", p->name);

    if((p->request_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        MSG("Could not create socket\n");
		goto ERR;
    }

    memset(&(p->serv_addr), '0', sizeof(p->serv_addr));

    p->serv_addr.sin_family = AF_INET;
    p->serv_addr.sin_port = htons(port);
	MSG("Open Socket ip %s\n", ip);

    if(inet_pton(AF_INET, ip, &p->serv_addr.sin_addr)<=0) {
        MSG("inet_pton error occured\n");
		goto ERR;
    }

	if (0 > __nonblocking_client_socket(p, ip, port)) {
		goto ERR;
	}

	return p;
ERR:
	if (p) {
		if (p->request_fd) {
#ifndef WIN32
			shutdown(p->request_fd, SHUT_RDWR);
#else
			shutdown(p->request_fd, SD_BOTH);
#endif
		}
		free(p);
	}
	return NULL;
}

int synocomm_socket_internal_request(struct _synocomm_channel_ *ch, SYNOCOMM_HOST * host)
{
	int err = -1;
	char *ipstr = NULL;
	SYNOCOMM_CHANNEL_DESC *pdesc = (SYNOCOMM_CHANNEL_DESC *)ch->channel_desc;

	if (SERVER_END == pdesc->role) {
		MSG("SERVER_END socket\n")
		ch->request_channel = __create_server_socket(pdesc->serviceid.service_name, SYNOCOMM_CHAN_SOCKET_SVR_PORT);
	} else if (CLIENT_END == pdesc->role) {
		MSG("CLIENT_END socket\n")
		if (0 != Ipv42Str(&host->hip, &ipstr)) {
			goto ERR;
		}
		MSG("ipstr %s\n", ipstr)
		ch->request_channel = __open_client_socket(pdesc->serviceid.service_name, ipstr, SYNOCOMM_CHAN_SOCKET_SVR_PORT);
	} else {
		goto ERR;
	}

	if (NULL != ch->request_channel) {
		err = 0;
	}

ERR:
	if (ipstr) {
		free(ipstr);
	}
	return err;
}

int synocomm_socket_internal_response(struct _synocomm_channel_ *ch, SYNOCOMM_HOST * host)
{
	int err = -1;
	char *ipstr = NULL;
	SYNOCOMM_CHANNEL_DESC *pdesc = (SYNOCOMM_CHANNEL_DESC *)ch->channel_desc;

	if (SERVER_END == pdesc->role) {
		if (0 != Ipv42Str(&host->hip, &ipstr)) {
			goto ERR;
		}
		MSG("ipstr %s\n", ipstr)
		ch->response_channel = __open_client_socket(pdesc->serviceid.service_name, ipstr, SYNOCOMM_CHAN_SOCKET_CLT_PORT);
	} else if (CLIENT_END == pdesc->role) {
		ch->response_channel = __create_server_socket(pdesc->serviceid.service_name, SYNOCOMM_CHAN_SOCKET_CLT_PORT);
	} else {
		goto ERR;
	}

	if (NULL != ch->response_channel) {
		err = 0;
	}

ERR:
	return err;
}

int synocomm_socket_internal_request_media(struct _synocomm_channel_ *ch, void *media)
{
	int err = -1;
	SYNOCOMM_CHANNEL_DESC *pdesc = (SYNOCOMM_CHANNEL_DESC *)ch->channel_desc;

	if (SERVER_END != pdesc->role &&
		CLIENT_END != pdesc->role) {
		goto ERR;
	}
	ch->request_channel = media;

	err = 0;
ERR:
	return err;
}

int synocomm_socket_internal_response_media(struct _synocomm_channel_ *ch, void *media)
{
	return 0;
}

#if 0
static void updatelrecv(SYNOCOMM_CHANNEL *pCH)
{
	P_SYNOCOMM_SOCKET pMedia = NULL;
	SYNOCOMM_CHANNEL_DESC *pDesc = (SYNOCOMM_CHANNEL_DESC *)pCH->channel_desc;
	pMedia = (P_SYNOCOMM_SOCKET)pDesc->request_channel;

	pMedia->lrecv = time(NULL);
}
#endif

static int __synocomm_socket_write_msg(void *pch, char *data, int length)
{
	int ret = -1;
	P_SYNOCOMM_SOCKET p = (P_SYNOCOMM_SOCKET)pch;

	if (NULL == p)
		goto ERR;

	ret = send(p->request_fd, data, length, 0);
	if (ret > 0) {
		p->lsend = time(NULL);
	}

ERR:
	return ret;
}

static int __synocomm_socket_read_msg(void *pch, char *data, int length)
{
	int ret = -1;
	P_SYNOCOMM_SOCKET p = (P_SYNOCOMM_SOCKET)pch;

	if (NULL == p)
		goto ERR;

	ret = recv(p->request_fd, data, length, 0);
	if (ret > 0) {
		p->lrecv = time(NULL);
	}

ERR:
	return ret;
}

BOOL IsSocketAlive(SYNOCOMM_CHANNEL *pCH)
{
	BOOL blAlive = FALSE;
	time_t cTime;
	P_SYNOCOMM_SOCKET pMedia = NULL;
	SYNOCOMM_CHANNEL_DESC *pDesc = (SYNOCOMM_CHANNEL_DESC *)pCH->channel_desc;
	pMedia = (P_SYNOCOMM_SOCKET)pCH->request_channel;

	cTime = time(NULL);
	MSG("**************** cTime %u, lrecv %u, lsend %u\n", (unsigned int)cTime, (unsigned int)pMedia->lrecv, (unsigned int)pMedia->lsend);
	if (cTime - pMedia->lrecv > 10 &&
		cTime - pMedia->lsend > 10) {
		MSG("**************** %s ISN'T UPDATED\n", pDesc->serviceid.serviceuuid);
	} else {
		blAlive = TRUE;
		MSG("**************** %s UPDATED\n", pDesc->serviceid.serviceuuid);
	}
	return blAlive;
}

SYNOCOMM_TRANSPORT socket_channel_transport = {
	SFINIT(.media_type, CH_SOCKET),
	SFINIT(.construct, synocomm_socket_construct),
	SFINIT(.destruct, synocomm_socket_destruct),
	SFINIT(.start_service, synocomm_socket_start_service),
	SFINIT(.stop_service, synocomm_socket_stop_service),
	SFINIT(.create_internal_request, synocomm_socket_internal_request),
	SFINIT(.create_internal_response, synocomm_socket_internal_response),
	SFINIT(.setup_internal_request, synocomm_socket_internal_request_media),
	SFINIT(.setup_internal_response, synocomm_socket_internal_response_media),
	SFINIT(.setup_external_request, synocomm_base_external_request),
	SFINIT(.setup_external_response, synocomm_base_external_response),
	SFINIT(.write_op, __synocomm_socket_write_msg),
	SFINIT(.read_op, __synocomm_socket_read_msg),
	SFINIT(.send_msg, synocomm_base_send_msg),
	SFINIT(.recv_msg, synocomm_base_recv_msg),
};
