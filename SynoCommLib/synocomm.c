#ifndef WIN32
#include <stdlib.h>
#include <search.h>

#include <synocomm/synocomm.h>
#include <synocomm/synocomm_app.h>
#include <synocomm/synocomm_base.h>
#include <synocomm/synocomm_host.h>
#include <synocomm/synocomm_channel.h>
#include <synocomm/synocomm_transport.h>
#include <synocomm/synocomm_channel_manage.h>
#include <synocomm/synocomm_eng_channel_manage.h>
#include <synocomm/synocomm_app_channel_manage.h>
#include <synocomm/synocomm_message.h>
#include <synocomm/synocomm_channel_control.h>
#else
#include "stdafx.h"
#include "process.h"

#include "synocomm.h"
#include "synocomm_app.h"
#include "synocomm_base.h"
#include "synocomm_host.h"
#include "synocomm_macro.h"
#include "synocomm_channel.h"
#include "synocomm_transport.h"
#include "synocomm_channel_manage.h"
#include "synocomm_channel_socket.h"
#include "synocomm_eng_channel_manage.h"
#include "synocomm_app_channel_manage.h"
#include "synocomm_message.h"
#include "synocomm_channel_control.h"
#endif

int CommInit = 0;
extern SYNOCOMM_CHANNEL_SET *pChannelSet;
#ifdef WIN32
FILE *synocomm_log_file = NULL;
INIT_ONCE once_block = INIT_ONCE_STATIC_INIT;
#else
pthread_once_t once_block = PTHREAD_ONCE_INIT;
#endif
SYNOCOMM_MUTEX mCommMutex;

static int __init_global_variables()
{
	if (0 == CommInit) {
#ifdef WIN32
		InitLogFile();
		InitWinWSA();
#endif
		//Init local/remote host set
		InitHostSet();
		//Init channel set
		InitChannelSet();
		//Init comm mutex for thread-safe
		SYNOCOMM_MUTEX_INIT(mCommMutex);

		CommInit = 1;
	}
	return 0;
}

#ifdef WIN32
BOOL CALLBACK once_init(
	PINIT_ONCE InitOnce,
	PVOID Parameter,
	PVOID *lpContext)
{
	__init_global_variables();
	return TRUE;
}

#else
void once_init()
{
	__init_global_variables();
}
#endif

static int InitGlobalVariables()
{
	int err = -1;
#ifdef WIN32
	BOOL status = InitOnceExecuteOnce(&once_block, once_init, NULL, NULL);
	if (FALSE == status) {
		MSG("once_init fail\n");
		goto ERR;
	}
#else
	int status = pthread_once(&once_block, once_init);
	if (0 != status) {
		MSG("once_init fail\n");
		goto ERR;
	}
#endif

	err = 0;
ERR:
	return err;
}

int DestroySynoComm(P_SYNOAPPCOMM pComm)
{
	int err =-1;
	SYNOCOMM_APP *pApp = NULL;
	SYNOCOMM_CHANNEL *pAppCh = NULL;
	SYNOCOMM_HOST *pH = NULL;

	pH = SearchLocalHost(NULL);
	if (NULL == pH) {
		goto ERR;
	}

	pApp = (SYNOCOMM_APP *)pComm->comm_app;
	AppURegister2Engine(pApp);

	//UPDATE_DEL(&pApp->alist, pH->acount);
	HostAbandonApp(pH, pApp);
	//FIXME
	//pAppCh = pApp->channel;
	//RemoveChannel(pChannelSet, pAppCh);
	ReleaseApp(pApp);

	err = 0;
ERR:
	return err;
}

int InitSynoComm(P_SYNOAPPCOMM pComm, const char *local_hostip, const char *hostname, const char *app_name)
{
	int err = -1;
	SYNOCOMM_APP *pApp = NULL;
	SYNOCOMM_CHANNEL *pAppCh = NULL;
	SYNOCOMM_HOST *pH = NULL;
	int commpid;

	if (0 != InitGlobalVariables()) {
		goto ERR;
	}

	if (NULL  == pComm) {
		goto ERR;
	}
	SYNOCOMM_MUTEX_LOCK(mCommMutex);

	memset(pComm, 0,sizeof(SYNOAPPCOMM));

	AddLocalHost(local_hostip, hostname);

	commpid = getcommpid();

	MSG("............................. Generate commpid %d\n", commpid);
	if (NULL == (pApp = CreateLocalAppServer(app_name, commpid))) {
		goto UNLOCK_ERR;
	}

	if (NULL == (pAppCh = AppRequestResponse(SearchLocalHost(local_hostip), app_name, commpid))) {
		goto UNLOCK_ERR;
	}

	BindAppChannel(pApp, pAppCh);

	pH = SearchLocalHost(NULL);
	LocalHostAdoptApp(pH, pApp);

	AppRegister2Engine(pApp);

	pComm->comm_app = (void *)pApp;
	err = 0;

UNLOCK_ERR:
	SYNOCOMM_MUTEX_UNLOCK(mCommMutex);
ERR:
	return err;
}

P_SYNOAPPCOMM CreateSynoComm(const char *local_hostip, const char *hostname, const char *app_name)
{
	P_SYNOAPPCOMM pComm = NULL;

	pComm = (P_SYNOAPPCOMM)malloc(sizeof(SYNOAPPCOMM));

	if (InitSynoComm(pComm, local_hostip, hostname, app_name)) {
		//FIXME
		//Destroy Channel/App
		if (pComm) {
			free(pComm);
			pComm = NULL;
		}
	}
	return pComm;
}

int SynoCommConnect(P_SYNOAPPCOMM pComm, const char *remote_hostip, const char *app_name)
{
	int err = -1;
	if (NULL == pComm || NULL == pComm->comm_app ||
		NULL == remote_hostip ||
		NULL == app_name) {
		goto ERR;
	}

	SYNOCOMM_MUTEX_LOCK(mCommMutex);
	MSG("......... DEBUG 1\n");
	err = AppConnect((P_SYNOCOMM_APP)pComm->comm_app, remote_hostip, app_name);
	MSG("......... DEBUG 2\n");
	SYNOCOMM_MUTEX_UNLOCK(mCommMutex);
ERR:
	return err;
}

int SynoCommDisconnect(P_SYNOAPPCOMM pComm, int fd)
{
	int err = -1;
	if (NULL == pComm ||
		NULL == pComm->comm_app ||
		0 > fd) {
		goto ERR;
	}
	SYNOCOMM_MUTEX_LOCK(mCommMutex);
	err = AppDisconnect((P_SYNOCOMM_APP)pComm->comm_app, fd);
	SYNOCOMM_MUTEX_UNLOCK(mCommMutex);
ERR:
	return err;
}

int SynoCommIsConnect(P_SYNOAPPCOMM pComm, const char *remote_hostip, const char *app_name)
{
	if (NULL == pComm || NULL == pComm->comm_app ||
		NULL == remote_hostip ||
		NULL == app_name) {
		return -1;
	}
	return AppIsConnect((P_SYNOCOMM_APP)pComm->comm_app, remote_hostip, app_name);
}

int SynoCommConnectionStatus(P_SYNOAPPCOMM pComm, const char *remote_hostip, const char *app_name)
{
	if (NULL == pComm || NULL == pComm->comm_app ||
		NULL == remote_hostip ||
		NULL == app_name) {
		return -1;
	}
	return AppConnectionStatus((P_SYNOCOMM_APP)pComm->comm_app, remote_hostip, app_name);
}

int CommSendMessage(P_SYNOAPPCOMM pComm, int fd, const char *data, int len)
{
	return AppSendMessage((P_SYNOCOMM_APP)pComm->comm_app, fd, data, len);
}

int CommUSendMessage(P_SYNOAPPCOMM pComm, void *pToUUID, const char *data, int len)
{
	return AppUSendMessage((P_SYNOCOMM_APP)pComm->comm_app, (FIELD_UUID *)pToUUID, data, len);
}

int CommRecvMessage(P_SYNOAPPCOMM pComm, char *data, int len)
{
	return AppRecvMessage((P_SYNOCOMM_APP)pComm->comm_app, data, len);
}

P_SYNOENGCOMM CreateEngComm(const char *local_hostip, const char *hostname)
{
	int err = -1;
	P_SYNOENGCOMM pEngComm = NULL;
	SYNOCOMM_HOST *pH = NULL;

	if (0 != InitGlobalVariables()) {
		goto ERR;
	}

	AddLocalHost(local_hostip, hostname);

	pH = SearchLocalHost(NULL);
	if (NULL == pH) {
		goto ERR;
	}

	pEngComm = (P_SYNOENGCOMM)malloc(sizeof(SYNOENGCOMM));
	if (NULL == pEngComm) {
		goto ERR;
	}
	memset(pEngComm, 0,sizeof(SYNOENGCOMM));

	//Local Pipe Server Reader
	pEngComm->pPRqRx = EnginePipeRequestReceiver(pH, "PRqRx");
	if (NULL == pEngComm->pPRqRx) {
		goto ERR;
	}
	AddInternalChannel(pChannelSet, pEngComm->pPRqRx);

	//Local Pipe Server Writer
	pEngComm->pPRqSn = EnginePipeRequestSender(pH, "PRqSn");
	if (NULL == pEngComm->pPRqSn) {
		goto ERR;
	}
	AddInternalChannel(pChannelSet, pEngComm->pPRqSn);

	//Local Socket Server Listener, Local Pipe Server Writer as external response
	pEngComm->pSRqLn = EngineSocketRequestListener(pH, "SRqLn");
	AddInternalChannel(pChannelSet, pEngComm->pSRqLn);
	if (NULL == pEngComm->pSRqLn) {
		goto ERR;
	}

	err = 0;
ERR:
	if (err) {
		//FIXME
		pEngComm = NULL;
	}
	return pEngComm;
}

static int PipeServerHandler(SYNOCOMM_CHANNEL *pPRqRx)
{
	int len = 0;
	P_SYNOCOMM_CMD pMsg = (P_SYNOCOMM_CMD)malloc(sizeof(SYNOCOMM_CMD));

	while (1) {
		len = RECVMSG(pPRqRx, (char *)(pMsg), sizeof(SYNOCOMM_CMD));
		if (0 < len) {
			MSG("RECV %d opcode 0x%x\n", len, pMsg->opcode);
			switch(pMsg->opcode) {
				case OPCODE_REGISTRY:
					HandleAppRegister(pMsg);
					break;
				case OPCODE_UREGISTRY:
					HandleAppURegister(pMsg);
					break;
				case OPCODE_RCONNECTV4:
					HandleRConnectV4(pMsg);
					break;
				case OPCODE_CONNECTV4:
					HandleConnectV4(pMsg);
					break;
				case OPCODE_SENDMSG:
					HandleSendMsg(pMsg);
					break;
				case OPCODE_RECVMSG:
					HandleRecvMsg(pMsg);
					break;
				case OPCODE_BINDSERVICE:
					HandleBindMsg(pMsg);
					break;
				case OPCODE_BONDSERVICE:
					HandleBondMsg(pMsg);
					break;
				case OPCODE_BANDSERVICE:
					HandleBandMsg(pMsg);
					break;
				case OPCODE_UBINDSERVICE:
					HandleUBindMsg(pMsg);
					break;
				case OPCODE_UBONDSERVICE:
					HandleUBondMsg(pMsg);
					break;
				case OPCODE_UBANDSERVICE:
					HandleUBandMsg(pMsg);
					break;
				case OPCODE_PINGSERVICE:
					HandlePingMsg(pMsg);
					break;
				case OPCODE_PONGSERVICE:
					HandlePongMsg(pMsg);
					break;
				case OPCODE_PUNGSERVICE:
					HandlePungMsg(pMsg);
					break;
				case OPCODE_SERVICESTOP:
					MSG(">>>>> STOP SERVICE\n");
					goto END;
				default:
					MSG("<<<<< Unreconize opcode 0x%x >>>>>\n", pMsg->opcode);
					break;
			}
			len = 0;
		} else {
			MSG("No Message PRqRx %p\n", pPRqRx);
			break;
		}
	}

END:
	free(pMsg);

	return 0;
}

int StartEngCommSockServer(P_SYNOENGCOMM pEngComm)
{
	int err = -1;
	SYNOCOMM_CHANNEL *pSRqLn = NULL;

	if (NULL == pEngComm) {
		goto ERR;
	}

	pSRqLn = pEngComm->pSRqLn;
	TRANSPORT(pSRqLn)->start_service(pSRqLn);

	err = 0;
ERR:
	return err;
}

int StopEngCommSockServer(P_SYNOENGCOMM pEngComm)
{
	int err = -1;
	SYNOCOMM_CHANNEL *pSRqLn = NULL;

	if (NULL == pEngComm) {
		goto ERR;
	}

	pSRqLn = pEngComm->pSRqLn;
	MSG("Stopping the SRqLn thread .......\n");
	TRANSPORT(pSRqLn)->stop_service(pSRqLn);

	err = 0;
ERR:
	return err;
}

int StartEngCommPipeServer(P_SYNOENGCOMM pEngComm)
{
	int err = -1;
	SYNOCOMM_CHANNEL *pPRqRx = NULL;

	if (NULL == pEngComm) {
		goto ERR;
	}

	pPRqRx = pEngComm->pPRqRx;
	PipeServerHandler(pPRqRx);

	MSG("Stopping the PRqRx thread .......\n");
	TRANSPORT(pPRqRx)->stop_service(pPRqRx);
	MSG("PRqRx thread exit.......\n");
	err = 0;
ERR:
	return err;
}

int StopEngCommPipeServer(P_SYNOENGCOMM pEngComm)
{
	extern SYNOCOMM_CHANNEL_SET *pChannelSet;
	int err = -1;
	SYNOCOMM_CHANNEL *pPRqSn = NULL;
	SYNOCOMM_SERVICE_NOTIFY *pSerNty = NULL;

	if (NULL == pEngComm) {
		goto ERR;
	}

	pPRqSn = GetInternalChannel(pChannelSet, "PRqSn");
	pSerNty = CreateServicePipeServerStop();
	MSG("Send service stop to PRqRx thread .......\n");
	SENDMSG(pPRqSn, pSerNty, sizeof(SYNOCOMM_SERVICE_NOTIFY));
	TRANSPORT(pPRqSn)->stop_service(pPRqSn);

	ChannelBroadCast(pChannelSet, (char *)pSerNty, sizeof(SYNOCOMM_SERVICE_NOTIFY));
	//ChannelConnectionCheck();
	err = 0;
ERR:
	return err;
}

int StartConnectionDetector(P_SYNOENGCOMM pEngComm)
{
	extern SYNOCOMM_CHANNEL_SET *pChannelSet;
	//check each host connection every 10 seconds
	//ChannelConnectionCheck(pChannelSet);
	return 0;
}

int InitSynoCommEvlp(P_SYNOCOMM_ENVELOPE pEvlp)
{
	int err = -1;

	if (NULL == pEvlp) {
		goto ERR;
	}
	memset(pEvlp, 0, sizeof(SYNOCOMM_ENVELOPE));

	pEvlp->pData = malloc(sizeof(SYNOCOMM_DATA));

	if (NULL == pEvlp->pData) {
		goto ERR;
	}

	memset(pEvlp->pData, 0, sizeof(SYNOCOMM_DATA));
	err = 0;
ERR:
	return err;
}

P_SYNOCOMM_ENVELOPE CreateSynoCommEvlp()
{
	P_SYNOCOMM_ENVELOPE pEvlp = (P_SYNOCOMM_ENVELOPE)malloc(sizeof(SYNOCOMM_ENVELOPE));

	if (NULL == pEvlp) {
		goto ERR;
	}

	if (0 != InitSynoCommEvlp(pEvlp)) {
		free(pEvlp);
		pEvlp = NULL;
	}

ERR:
	return pEvlp;
}

int CommRecvEvlp(P_SYNOAPPCOMM pComm, P_SYNOCOMM_ENVELOPE pEvlp)
{
	int ret = -1;
	if (0 < (ret = AppRecvMessage((P_SYNOCOMM_APP)pComm->comm_app, pEvlp->pData, MAX_COMM_DATA_SIZE))) {
		return ((P_SYNOCOMM_DATA)pEvlp->pData)->len;
	}

	return ret;
}

char *CommGetEvlpData(P_SYNOCOMM_ENVELOPE pEvlp)
{
	if (NULL == pEvlp ||
		NULL == pEvlp->pData) {
		return NULL;
	}

	return ((P_SYNOCOMM_DATA)pEvlp->pData)->data;
}

/*
 * Set data into Envelope and swap the from/to address
 * and send the message to engine
 */
int CommRetEvlp(P_SYNOAPPCOMM pComm, P_SYNOCOMM_ENVELOPE pEvlp, char *data, int len)
{
	if (NULL == pEvlp ||
		NULL == pEvlp->pData) {
		return -1;
	}

	return AppRetMessage((P_SYNOCOMM_APP)pComm->comm_app, pEvlp->pData, data, len);
}

int CommGetEvlpDataLen(P_SYNOCOMM_ENVELOPE pEvlp)
{
	if (NULL == pEvlp ||
		NULL == pEvlp->pData) {
		return -1;
	}

	return ((P_SYNOCOMM_DATA)pEvlp->pData)->len;
}

int CommGetEvlpFD(P_SYNOAPPCOMM pComm, P_SYNOCOMM_ENVELOPE pEvlp)
{
	if (NULL == pEvlp ||
		NULL == pEvlp->pData) {
		return -1;
	}

	return AppGetMessageFromFD(pComm->comm_app, (P_SYNOCOMM_DATA)pEvlp->pData);
}

