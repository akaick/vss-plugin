#ifndef WIN32
#include <stdlib.h>
#include <synocomm/synocomm_message.h>
#include <synocomm/synocomm_channel_manage.h>
#include <synocomm/synocomm_channel_control.h>
#else
#include "stdafx.h"
#include "stdlib.h"
#include "synocomm_message.h"
#include "synocomm_channel_manage.h"
#include "synocomm_channel_control.h"
#endif

/*
 * We should have a timeout
 */
int AppRegister2Engine(SYNOCOMM_APP *pApp)
{
	int err = -1;
	int len = 0;

	P_REGISTRY pReg = CreateRegistryMsg(pApp);
	P_REGISACK pRegAck = (P_REGISACK)malloc(sizeof(REGISACK));

	AppSendControl(pApp, (char *)pReg, sizeof(REGISTRY));
	while (1) {
		memset(pRegAck, 0, sizeof(REGISACK));
		len = AppRecvControl(pApp, (char *)pRegAck, sizeof(REGISACK));
		if (len) {
			MSG("opcode 0x%x\n", pRegAck->opcode);
			if ((0 == strcmp(pRegAck->service_name, pApp->serviceid.service_name)) &&
				pRegAck->pid == pApp->serviceid.owner &&
				(0 == strcmp(pRegAck->regsackuuid, pApp->serviceid.serviceuuid))) {

				MSG("REGISACK - service_name:%s, pid:%lu, uuid:%s\n", pRegAck->service_name, pRegAck->pid, pRegAck->regsackuuid);
				err = 0;
				break;
			} else {
				MSG("REGISTER FAIL - service_name:%s, pid:%d\n", pApp->serviceid.service_name, pApp->serviceid.owner);
				break;
			}
		}
		//FIXME timeout
		sleep(1);
	}

	return err;
}

int AppURegister2Engine(SYNOCOMM_APP *pApp)
{
	int err = -1;
	int len = 0;

	P_UREGISTRY pUReg = CreateURegistryMsg(pApp);
	P_UREGISACK pURegAck = (P_UREGISACK)malloc(sizeof(UREGISACK));

	AppSendControl(pApp, (char *)pUReg, sizeof(UREGISTRY));
	while (1) {
		memset(pURegAck, 0, sizeof(UREGISACK));
		len = AppRecvControl(pApp, (char *)pURegAck, sizeof(UREGISACK));
		if (len) {
			MSG("opcode 0x%x\n", pURegAck->opcode);
			if ((0 == strcmp(pURegAck->service_name, pApp->serviceid.service_name)) &&
				pURegAck->pid == pApp->serviceid.owner &&
				(0 == strcmp(pURegAck->regsackuuid, pApp->serviceid.serviceuuid))) {

				MSG("UREGISACK - service_name:%s, pid:%lu, uuid:%s\n", pURegAck->service_name, pURegAck->pid, pURegAck->regsackuuid);
				err = 0;
				break;
			} else {
				MSG("UREGISTER FAIL - service_name:%s, pid:%d\n", pApp->serviceid.service_name, pApp->serviceid.owner);
				break;
			}
		}
		//FIXME timeout
		sleep(1);
	}

	return err;
}

static int AppClientWaitBond(SYNOCOMM_APP *pCliApp, SYNOCOMM_APP *pRApp)
{
	int fd = -1;
	P_SYNOCOMM_BOND pBond = NULL;
	pBond = (P_SYNOCOMM_BOND)malloc(sizeof(SYNOCOMM_BOND));

	MSG("############### %s Wait Bond from %s@%s\n", pCliApp->serviceid.uuid_filed.bits, pRApp->serviceid.service_name, pRApp->host->hostname);
	sleep(1);
	while (1) {
		int len = 0;
		memset(pBond, 0, sizeof(SYNOCOMM_BOND));
		len = AppRecvControl(pCliApp, (char *)pBond, sizeof(SYNOCOMM_BOND));
		if (0 < len) {
			SERVICE_ID *cid = NULL;
			char *fIpStr = NULL;
			SERVICE_ID *sid = NULL;
			char *tIpStr = NULL;

			switch(pBond->opcode) {
				case OPCODE_BONDSERVICE:
					MSG("AppClient BOND\n");
					cid = &pBond->cid;
					Ipv42Str(&pBond->cip, &fIpStr);

					sid = &pBond->sid;
					Ipv42Str(&pBond->sip, &tIpStr);

					MSG("Bond ip %s, service_name %s, owner %d, uuid %s\n", fIpStr, cid->service_name, cid->owner, cid->serviceuuid);
					MSG("To   ip %s, service_name %s, owner %d, uuid %s\n", tIpStr, sid->service_name, sid->owner, sid->serviceuuid);
					MSG("pRApp Original service_name %s, owner %d, uuid %s\n", pRApp->serviceid.service_name, pRApp->serviceid.owner, pRApp->serviceid.serviceuuid);
					UpdateAppID(pRApp, sid);
					MSG("pRApp Update service_name %s, owner %d, uuid %s\n", pRApp->serviceid.service_name, pRApp->serviceid.owner, pRApp->serviceid.serviceuuid);

					//bind success
					fd = AddAppRouteRule(pCliApp, pRApp);
					AppGet(pRApp);
					MSG("BIND fd[%d] pRApp %s\n", fd, pRApp->serviceid.serviceuuid);

					free(fIpStr);
					fIpStr = NULL;
					free(tIpStr);
					tIpStr = NULL;
					goto BONDSUCCESS;
				case OPCODE_BANDSERVICE:
					MSG("AppClient BAND\n");
					goto BONDSUCCESS;
				default:
					MSG("AppClient Recv %d\n", pBond->opcode);
					break;
			}
			len = 0;
		} else {
			//invalid length
			goto BONDSUCCESS;
		}
	}

BONDSUCCESS:
	if (pBond) {
		free(pBond);
	}

	return fd;
}

static int AppLocalConnect(SYNOCOMM_APP *pCliApp, SYNOCOMM_HOST *pRH, const char *service_name)
{
	int fd = -1;
	BOOL blCreateRApp = FALSE;
	P_SYNOCOMM_BIND pBind = NULL;
	SYNOCOMM_APP *pRApp = (SYNOCOMM_APP *)SearchAppInHostByNameExt(pRH, service_name);

	if (NULL == pRApp) {
		MSG("No such App %s\n", service_name);
		if (NULL == (pRApp = CreateLocalAppServer(service_name, getcommpid()))) {
			MSG("Fail to create Local App Server during AppLocalConnect\n");
			goto ERR;
		}

		blCreateRApp = TRUE;

		if (0 != LocalHostAdoptApp(pRH, pRApp)) {
			MSG("Fail to Adopt Local App Server during AppLocalConnect\n");
			goto ERR;
		}
	}

	pBind = CreateBindServiceMsg(pCliApp, pRApp);
	MSG("############### Send Bind Service\n");
	AppSendControl(pCliApp, (char *)pBind, sizeof(SYNOCOMM_BIND));
	MSG("############### Bind Service ?\n");

	fd = AppClientWaitBond(pCliApp, pRApp);
ERR:

	if (0 > fd && blCreateRApp) {
		//ReleaseApp(pRApp);
		AppFree(pRApp);
	}

	if (0 <= fd) {
		AppPut(pRApp);
	}

	if (pBind) {
		free(pBind);
	}
	return fd;
}

int AppConnect(SYNOCOMM_APP *pCliApp, const char *remote_host_ip, const char *service_name)
{
	int fd = -1;
	BOOL blCreateRApp = FALSE;

	SYNOCOMM_HOST *pRH = NULL;
	SYNOCOMM_APP *pRApp = NULL;
	P_RCONNECTV4 pRConV4 = NULL;
	P_SYNOCOMM_BIND pBind = NULL;

	//Local Host App Bind
	MSG("................... DEBUG 4\n");
	if (NULL != (pRH = SearchLocalHost(remote_host_ip))) {
	MSG("................... DEBUG 4.1\n");
		fd = AppLocalConnect(pCliApp, pRH, service_name);
	MSG("................... DEBUG 4.2\n");
		goto ERR;
	}
	MSG("................... DEBUG 5\n");

	//Create remote host and app first
	if (0 != AddRemoteHost(remote_host_ip, remote_host_ip) ||
		NULL == (pRH = SearchRemoteHost(remote_host_ip))) {
		MSG("No such Remote Host Created during AppConnect\n");
		goto ERR;
	}
	MSG("................... DEBUG 6\n");

	if (NULL == (pRApp = (SYNOCOMM_APP *)SearchAppInHostByNameExt(pRH, service_name))) {
		MSG(".................. shit\n");
		if (NULL == (pRApp = CreateRemoteAppServer(service_name, getcommpid()))) {
			MSG("Fail to create Remote App during AppConnect\n");
			goto ERR;
		}
		MSG("<<<<<<<<<< %s Invoke Create %s@%s >>>>>>>>>>\n", pCliApp->serviceid.uuid_filed.bits, pRApp->serviceid.uuid_filed.bits, remote_host_ip);
		blCreateRApp = TRUE;
		RemoteHostAdoptApp(pRH, pRApp);
	} 

	//AppSendMessage RCONNECTV4
	pRConV4 = CreateRConnectV4Msg(pCliApp->host, pRApp->host);
	MSG("############### %s Send RConnect to %s@%s\n", pCliApp->serviceid.uuid_filed.bits, pRApp->serviceid.service_name, pRApp->host->hostname);
	AppSendControl(pCliApp, (char *)pRConV4, sizeof(RCONNECTV4));
	MSG("############### %s Send RConnect to %s@%s Done\n", pCliApp->serviceid.uuid_filed.bits, pRApp->serviceid.service_name, pRApp->host->hostname);

	sleep(2);
	pBind = CreateBindServiceMsg(pCliApp, pRApp);
	MSG("############### %s Send Bind to %s@%s\n", pCliApp->serviceid.uuid_filed.bits, pRApp->serviceid.service_name, pRApp->host->hostname);
	AppSendControl(pCliApp, (char *)pBind, sizeof(SYNOCOMM_BIND));

	fd = AppClientWaitBond(pCliApp, pRApp);

/*
 * If timeout, ERR status and get -1 as fd
 */
ERR:

	if (0 > fd && blCreateRApp) {
		//ReleaseApp(pRApp);
		AppFree(pRApp);
	}

	if (0 <= fd) {
		AppPut(pRApp);
	}

	if (pBind) {
		free(pBind);
	}
	return fd;
}

static int AppClientWaitPong(SYNOCOMM_APP *pCliApp, SYNOCOMM_APP *pRApp)
{
	int status = -1;
	P_SYNOCOMM_PONG pPong = NULL;
	pPong = (P_SYNOCOMM_PONG)malloc(sizeof(SYNOCOMM_PONG));

	MSG("############################ Wait Service Pong\n");
	sleep(1);
	while (1) {
		int len = 0;
		memset(pPong, 0, sizeof(SYNOCOMM_PONG));
		len = AppRecvControl(pCliApp, (char *)pPong, sizeof(SYNOCOMM_PONG));
		if (0 < len) {
			SERVICE_ID *cid = NULL;
			char *fIpStr = NULL;
			SERVICE_ID *sid = NULL;
			char *tIpStr = NULL;

			switch(pPong->opcode) {
				case OPCODE_PONGSERVICE:
					MSG("AppClient PONG\n");
					cid = &pPong->cid;
					Ipv42Str(&pPong->cip, &fIpStr);

					sid = &pPong->sid;
					Ipv42Str(&pPong->sip, &tIpStr);

					MSG("Pong ip %s, service_name %s, owner %d, uuid %s\n", fIpStr, cid->service_name, cid->owner, cid->serviceuuid);
					MSG("To   ip %s, service_name %s, owner %d, uuid %s\n", tIpStr, sid->service_name, sid->owner, sid->serviceuuid);
					MSG("pRApp Original service_name %s, owner %d, uuid %s\n", pRApp->serviceid.service_name, pRApp->serviceid.owner, pRApp->serviceid.serviceuuid);
					UpdateAppID(pRApp, sid);
					MSG("pRApp Update service_name %s, owner %d, uuid %s\n", pRApp->serviceid.service_name, pRApp->serviceid.owner, pRApp->serviceid.serviceuuid);

					MSG("Pong->status 0x%x\n", pPong->status);
					status = pPong->status;
					goto PONGSUCCESS;
				default:
					break;
			}
			len = 0;
			if (fIpStr) {
				free(fIpStr);
				fIpStr = NULL;
			}
			if (tIpStr) {
				free(tIpStr);
				tIpStr = NULL;
			}
		} else {
			//invalid length
			status = 0;
			goto PONGSUCCESS;
		}
	}

PONGSUCCESS:
	if (pPong) {
		free(pPong);
	}

	MSG("****************** status 0x%x\n", status);
	return status;
}

static int AppLocalPing(SYNOCOMM_APP *pCliApp, SYNOCOMM_HOST *pRH, const char *service_name)
{
	int status = -1;
	P_SYNOCOMM_PING pPing = NULL;
	SYNOCOMM_APP *pRApp = (SYNOCOMM_APP *)SearchAppInHostByNameExt(pRH, service_name);

	if (NULL == pRApp) {
		MSG("No such App %s\n", service_name);
		if (NULL == (pRApp = CreateLocalAppServer(service_name, getcommpid()))) {
			MSG("Fail to create Local App Server during AppLocalPing\n");
			goto ERR;
		}

		if (0 != LocalHostAdoptApp(pRH, pRApp)) {
			MSG("Fail to Adopt Local App Server during AppLocalPing\n");
			goto ERR;
		}
	}

	pPing = CreatePingServiceMsg(pCliApp, pRApp);
	AppSendControl(pCliApp, (char *)pPing, sizeof(SYNOCOMM_PING));
	status = AppClientWaitPong(pCliApp, pRApp);

	AppPut(pRApp);
	MSG("****************** status 0x%x\n", status);
ERR:
	if (NULL != pPing) {
		free(pPing);
	}
	return status;
}

int AppIsConnect(SYNOCOMM_APP *pCliApp, const char *remote_host_ip, const char *service_name)
{
	int status = -1;

	SYNOCOMM_HOST *pRH = NULL;
	SYNOCOMM_APP *pRApp = NULL;
	P_SYNOCOMM_PING pPing = NULL;

	//Local Host App Ping
	if (NULL != (pRH = SearchLocalHost(remote_host_ip))) {
		MSG("LocalPing remote_host_ip %s, service_name %s\n", remote_host_ip, service_name);
		status = AppLocalPing(pCliApp, pRH, service_name);
		MSG("****************** status 0x%x\n", status);
		goto ERR;
	}
	MSG("RemotePing remote_host_ip %s, service_name %s\n", remote_host_ip, service_name);

	//Create remote host and app first
	if (0 != AddRemoteHost(remote_host_ip, remote_host_ip) ||
		NULL == (pRH = SearchRemoteHost(remote_host_ip))) {
		MSG("No such Remote Host Created during AppConnect\n");
		goto ERR;
	}

	if (NULL == (pRApp = (SYNOCOMM_APP *)SearchAppInHostByNameExt(pRH, service_name))) {
		if (NULL == (pRApp = CreateRemoteAppServer(service_name, getcommpid()))) {
			MSG("Fail to create Remote App during AppConnect\n");
			goto ERR;
		} else {
			RemoteHostAdoptApp(pRH, pRApp);
		}
	}

	pPing = CreatePingServiceMsg(pCliApp, pRApp);
	AppSendControl(pCliApp, (char *)pPing, sizeof(SYNOCOMM_PING));
	status = AppClientWaitPong(pCliApp, pRApp);
/*
 * If timeout, ERR status and get -1 as fd
 */
ERR:
	if (NULL != pPing) {
		free(pPing);
	}
	AppPut(pRApp);

	MSG("****************** status 0x%x\n", status);
	return status;
}

static int AppClientWaitPung(SYNOCOMM_APP *pCliApp, SYNOCOMM_APP *pSrvApp)
{
	int status = -1;
	P_SYNOCOMM_PUNG pPung = NULL;
	pPung = (P_SYNOCOMM_PUNG)malloc(sizeof(SYNOCOMM_PUNG));

	MSG("############################ Wait Service Pong\n");
	sleep(1);
	while (1) {
		int len = 0;
		memset(pPung, 0, sizeof(SYNOCOMM_PUNG));
		len = AppRecvControl(pCliApp, (char *)pPung, sizeof(SYNOCOMM_PUNG));
		if (0 < len) {
			SERVICE_ID *cid = NULL;
			char *fIpStr = NULL;
			SERVICE_ID *sid = NULL;
			char *tIpStr = NULL;

			switch(pPung->opcode) {
				case OPCODE_PUNGSERVICE:
					MSG("AppClient PUNG\n");
					cid = &pPung->cid;
					Ipv42Str(&pPung->cip, &fIpStr);

					sid = &pPung->sid;
					Ipv42Str(&pPung->sip, &tIpStr);

					MSG("Pong ip %s, service_name %s, owner %d, uuid %s\n", fIpStr, cid->service_name, cid->owner, cid->serviceuuid);
					MSG("To   ip %s, service_name %s, owner %d, uuid %s\n", tIpStr, sid->service_name, sid->owner, sid->serviceuuid);
					MSG("pSrvApp Original service_name %s, owner %d, uuid %s\n", pSrvApp->serviceid.service_name, pSrvApp->serviceid.owner, pSrvApp->serviceid.serviceuuid);
					UpdateAppID(pSrvApp, sid);
					MSG("pSrvApp Update service_name %s, owner %d, uuid %s\n", pSrvApp->serviceid.service_name, pSrvApp->serviceid.owner, pSrvApp->serviceid.serviceuuid);

					MSG("Pung->status 0x%x\n", pPung->status);
					status = pPung->status;
					goto PUNGSUCCESS;
				default:
					break;
			}
			len = 0;
			if (fIpStr) {
				free(fIpStr);
				fIpStr = NULL;
			}
			if (tIpStr) {
				free(tIpStr);
				tIpStr = NULL;
			}
		} else {
			//invalid length
			status = 0;
			goto PUNGSUCCESS;
		}
	}

PUNGSUCCESS:
	if (pPung) {
		free(pPung);
	}

	MSG("****************** status 0x%x\n", status);
	return status;
}

static int AppLocalPung(SYNOCOMM_APP *pCliApp, SYNOCOMM_HOST *pRH, const char *service_name)
{
	int status = -1;
	P_SYNOCOMM_PUNG pPung = NULL;
	SYNOCOMM_APP *pSrvApp = (SYNOCOMM_APP *)SearchAppInHostByNameExt(pRH, service_name);

	if (NULL == pSrvApp) {
		MSG("No such App %s\n", service_name);
		if (NULL == (pSrvApp = CreateLocalAppServer(service_name, getcommpid()))) {
			MSG("Fail to create Local App Server during AppLocalPung\n");
			goto ERR;
		}

		if (0 != LocalHostAdoptApp(pRH, pSrvApp)) {
			MSG("Fail to Adopt Local App Server during AppLocalPung\n");
			goto ERR;
		}
	}

	{
		char *fIpStr = NULL;
		Ipv42Str(&pCliApp->host->hip, &fIpStr);
		MSG("pCliApp hip %s\n", fIpStr);
	}
	pPung = CreatePungServiceMsg(&pCliApp->host->hip, &pCliApp->serviceid, &pSrvApp->host->hip, &pSrvApp->serviceid, 0);
	AppSendControl(pCliApp, (char *)pPung, sizeof(SYNOCOMM_PUNG));
	status = AppClientWaitPung(pCliApp, pSrvApp);

	MSG("****************** status 0x%x\n", status);
ERR:
	if (NULL != pPung) {
		free(pPung);
	}
	AppPut(pSrvApp);

	return status;
}

int AppConnectionStatus(SYNOCOMM_APP *pCliApp, const char *remote_host_ip, const char *service_name)
{
	int status = -1;

	SYNOCOMM_HOST *pRH = NULL;
	SYNOCOMM_APP *pSrvApp = NULL;
	P_SYNOCOMM_PUNG pPung = NULL;

	//Local Host App Ping
	if (NULL != (pRH = SearchLocalHost(remote_host_ip))) {
		MSG("LocalPing remote_host_ip %s, service_name %s\n", remote_host_ip, service_name);
		status = AppLocalPung(pCliApp, pRH, service_name);
		MSG("****************** status 0x%x\n", status);
		goto ERR;
	}
	MSG("RemotePing remote_host_ip %s, service_name %s\n", remote_host_ip, service_name);

	//Create remote host and app first
	if (0 != AddRemoteHost(remote_host_ip, remote_host_ip) ||
		NULL == (pRH = SearchRemoteHost(remote_host_ip))) {
		MSG("No such Remote Host Created during AppConnect\n");
		goto ERR;
	}

	if (NULL == (pSrvApp = (SYNOCOMM_APP *)SearchAppInHostByNameExt(pRH, service_name))) {
		if (NULL == (pSrvApp = CreateRemoteAppServer(service_name, getcommpid()))) {
			MSG("Fail to create Remote App during AppConnect\n");
			goto ERR;
		} else {
			RemoteHostAdoptApp(pRH, pSrvApp);
		}
	}

	pPung = CreatePungServiceMsg(&pCliApp->host->hip, &pCliApp->serviceid, &pSrvApp->host->hip, &pSrvApp->serviceid, 0);
	AppSendControl(pCliApp, (char *)pPung, sizeof(SYNOCOMM_PUNG));
	status = AppClientWaitPung(pCliApp, pSrvApp);
/*
 * If timeout, ERR status and get -1 as fd
 */
ERR:
	if (NULL != pPung) {
		free(pPung);
	}
	AppPut(pSrvApp);

	MSG("****************** status 0x%x\n", status);
	return status;
}

static int AppClientWaitUBond(SYNOCOMM_APP *pCliApp, SYNOCOMM_APP *pRApp)
{
	int fd = -1;
	P_SYNOCOMM_UBOND pUBond = NULL;
	pUBond = (P_SYNOCOMM_UBOND)malloc(sizeof(SYNOCOMM_UBOND));

	MSG("############################ Wait Service UBond\n");
	sleep(1);
	while (1) {
		int len = 0;
		memset(pUBond, 0, sizeof(SYNOCOMM_UBOND));
		len = AppRecvControl(pCliApp, (char *)pUBond, sizeof(SYNOCOMM_UBOND));
		if (0 < len) {
			SERVICE_ID *cid = NULL;
			char *fIpStr = NULL;
			SERVICE_ID *sid = NULL;
			char *tIpStr = NULL;

			switch(pUBond->opcode) {
				case OPCODE_UBONDSERVICE:
					MSG("AppClient UBOND\n");
					cid = &pUBond->cid;
					Ipv42Str(&pUBond->cip, &fIpStr);

					sid = &pUBond->sid;
					Ipv42Str(&pUBond->sip, &tIpStr);

					MSG("UBind ip %s, service_name %s, owner %d, uuid %s\n", fIpStr, cid->service_name, cid->owner, cid->serviceuuid);
					MSG("To    ip %s, service_name %s, owner %d, uuid %s\n", tIpStr, sid->service_name, sid->owner, sid->serviceuuid);
					MSG("Try to remove pRApp route service_name %s, owner %d, uuid %s\n", pRApp->serviceid.service_name, pRApp->serviceid.owner, pRApp->serviceid.serviceuuid);

					if (0 != CompareUUID(&(pRApp->serviceid.uuid_filed), &(sid->uuid_filed))) {
						MSG("************** Different uuid sid %s, pRApp uuid %s **************\n", sid->serviceuuid, pRApp->serviceid.serviceuuid);
					}
					//ubind success
					fd = RemoveAppRouteRule(pCliApp, pRApp);
					//AppFree(pRApp);

					goto UBONDSUCCESS;
				default:
					break;
			}
			len = 0;
		} else {
			goto UBONDSUCCESS;
		}
	}

UBONDSUCCESS:
	if (pUBond) {
		free(pUBond);
	}

	return fd;
}

int AppDisconnect(SYNOCOMM_APP *pCliApp, int fd)
{
	int ret = -1;

	P_SYNOCOMM_BIND pUBind = NULL;
	SYNOCOMM_APP *pRApp = NULL;

	if (NULL == pCliApp) {
		MSG("Invalid parameter\n");
		goto ERR;
	}

	if (NULL == (pRApp = GetToAppByRule(pCliApp, fd))) {
		MSG("Fail to Disconnect fd %d, no such routing found in %s\n",
			fd, pCliApp->serviceid.serviceuuid);
		goto ERR;
	}

	pUBind = CreateUBindServiceMsg(pCliApp, pRApp);
	MSG("<<<<<<<<<<<1 %s Send UBind to %s@%s >>>>>>>>>>\n", pCliApp->serviceid.uuid_filed.bits, pRApp->serviceid.uuid_filed.bits, pRApp->host->hostname);
	AppSendControl(pCliApp, (char *)pUBind, sizeof(SYNOCOMM_UBIND));

	MSG("<<<<<<<<<<<2 %s Wait UBond to %s@%s >>>>>>>>>>\n", pCliApp->serviceid.uuid_filed.bits, pRApp->serviceid.uuid_filed.bits, pRApp->host->hostname);
	ret = AppClientWaitUBond(pCliApp, pRApp);

	if (0 > ret) {
		goto ERR;
	}
	MSG("<<<<<<<<<<<3 %s UBond to %s@%s Successfully >>>>>>>>>>\n", pCliApp->serviceid.uuid_filed.bits, pRApp->serviceid.uuid_filed.bits, pRApp->host->hostname);

	if (ret != fd) {
		MSG("Fail to Disconnect fd %d, ret(%d)\n", fd, ret);
		ret = -1;
	} else {
		MSG("<<<<<<<<<<<4 Release %s@%s >>>>>>>>>>\n", pRApp->serviceid.uuid_filed.bits, pRApp->host->hostname);
		//ReleaseApp(pRApp);
		AppFree(pRApp);
	}
ERR:
	return ret;
}
