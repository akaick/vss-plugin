// SynoCommLib.cpp : Defines the exported functions for the DLL application.
//

#ifndef WIN32
#include <synocomm.h>
#include <synocomm_channel_pipe.h>
#include <synocomm_channel_socket.h>
#include <synocomm_channel_manage.h>
#include <synocomm_channel_control.h>
#include <synocomm_app_channel_manage.h>
#else
#include "stdafx.h"
#include "synocomm.h"
#include "synocomm_channel_pipe.h"
#include "synocomm_channel_socket.h"
#include "synocomm_channel_manage.h"
#include "synocomm_channel_control.h"
#include "synocomm_app_channel_manage.h"
#endif

