#ifndef _SYNO_COMM_MACRO_H_
#define _SYNO_COMM_MACRO_H_

#ifndef WIN32
#define __SYNO_BEGIN_DECLS __BEGIN_DECLS
#define __SYNO_END_DECLS   __END_DECLS

#define HAVE_DESIGNATED_INITIALIZER
#else
#include "time.h"
#include "stdio.h"
#include "stdlib.h"

#define __SYNO_BEGIN_DECLS extern "C" {
#define __SYNO_END_DECLS }
#endif

#ifdef __cplusplus
__SYNO_BEGIN_DECLS
#endif

#ifndef WIN32

#define MSG(format, a...)                                \
{                                                        \
	printf("(%s:%d %s) - "format, __FILE__, __LINE__, __func__, ##a); \
}

#define EMSG(format, a...)                                    \
{                                                             \
	printf("(%s:%d) ERROR - "format, __FILE__, __LINE__, ##a);\
}

#include <stdlib.h>
#include <time.h>

static int getrandpid()
{
	static int callers = 0;
	srand((unsigned int)time(NULL));
	callers++;
	return ((rand() + callers) % 100000);
}

//#define getcommpid getpid
#define getcommpid getrandpid
#else

static int getrandpid()
{
	static int callers = 0;
	srand((unsigned int)time(NULL));
	callers++;
	return rand() + callers;
}

static void InitLogFile()
{
	extern FILE *synocomm_log_file;
	if (NULL == synocomm_log_file) {
		fopen_s(&synocomm_log_file, "C:\\synocomm_log.txt", "w");
	}
}

#define MAX_PATH_LEN _MAX_PATH
#define snprintf(o, l, format, ...)  _snprintf_s(o, l, l, format, __VA_ARGS__)
#define getcommpid getrandpid
#define sscanf    sscanf_s
#define strtok_r  strtok_s
#define strdup    _strdup
#define sleep(t)  Sleep((t) * 1000)
#define close(fd) _close(fd)
#define unlink _unlink

//#define MSG_DEBUG
#ifdef MSG_DEBUG
#define LMSG(format, ...)                                                             \
{                                                                                     \
	extern FILE *synocomm_log_file;                                                   \
	if (NULL == synocomm_log_file) {                                                  \
	    InitLogFile();                                                                \
	}                                                                                 \
	fprintf(synocomm_log_file, "(%s:%d) - "format, __FILE__, __LINE__, __VA_ARGS__);  \
	fflush(synocomm_log_file);                                                        \
}

#define MSG(format, ...)                                         \
{                                                                \
	printf("(%s:%d) - "format, __FILE__, __LINE__, __VA_ARGS__); \
}

#define EMSG(format, ...)                                             \
{                                                                     \
	printf("(%s:%d) ERROR - "format, __FILE__, __LINE__, __VA_ARGS__);\
}

#define TMSG(format, ...)                                                \
{                                                                        \
	printf("(%s:%d) - ", __FILE__, __LINE__);                            \
	_tprintf(TEXT(format) , __VA_ARGS__);                                \
}

#else
#define LMSG(format, ...)
#define MSG(format, ...)
#define EMSG(format, ...)
#define TMSG(format, ...)
#endif

#endif

#ifdef HAVE_DESIGNATED_INITIALIZER
#define SFINIT(f, v) f = v
#else
#define SFINIT(f, v) v
#endif

#ifdef __cplusplus
__SYNO_END_DECLS
#endif

#endif
