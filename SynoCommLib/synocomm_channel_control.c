#ifndef WIN32
#include <stdlib.h>
#include <synocomm/synocomm_base.h>
#include <synocomm/synocomm_channel_pipe.h>
#include <synocomm/synocomm_message.h>
#include <synocomm/synocomm_app.h>
#else
#include "synocomm_base.h"
#include "synocomm_channel_pipe.h"
#include "synocomm_message.h"
#include "synocomm_app.h"
#endif

int control_func()
{
	SYNOCOMM_CMD cmd;
	memset(&cmd, 0, sizeof(cmd));
	return 0;
}

void FreeCmd(void *pcmd)
{
	if (pcmd) {
		free(pcmd);
	}
}

void FreeMsg(SYNOCOMM_CMD *pmsg)
{
	if (pmsg) {
		free(pmsg);
	}
}

int RegistryChannel(P_REGISTRY preg)
{
	int err = -1;

	if (NULL == preg) {
		goto ERR;
	}

	MSG("preg %p\n", preg);
	MSG("pid %lu service_name %s\n", preg->pid, preg->service_name);
	err = 0;
ERR:
	return err;
}

static void *CreateRegisterSerialMsg(P_SYNOCOMM_APP pApp, SYNOCOMM_CMD_OPCODE opcode)
{
	struct _cmd_registry_ *pReg = (struct _cmd_registry_ *)malloc(sizeof(struct _cmd_registry_));

	if (pReg) {
		memset(pReg, 0, sizeof(struct _cmd_registry_));
		pReg->opcode = opcode;
		snprintf(pReg->service_name, sizeof(pReg->service_name), "%s", pApp->serviceid.service_name);
		pReg->pid = pApp->serviceid.owner;
	}

	return pReg;
}

P_REGISTRY CreateRegistryMsg(P_SYNOCOMM_APP pApp)
{
	return (P_REGISTRY)CreateRegisterSerialMsg(pApp, OPCODE_REGISTRY);
}

static void *CreateRegisAckSerialMsg(P_SYNOCOMM_APP pApp, SYNOCOMM_CMD_OPCODE opcode)
{
	struct _cmd_regisack_ *pRegAck = (struct _cmd_regisack_ *)malloc(sizeof(struct _cmd_regisack_));

	if (pRegAck) {
		memset(pRegAck, 0, sizeof(struct _cmd_regisack_));
		pRegAck->opcode = opcode;
		snprintf(pRegAck->service_name, sizeof(pRegAck->service_name), "%s", pApp->serviceid.service_name);
		pRegAck->pid = pApp->serviceid.owner;
		if (0 != ServiceUUIDGenerate(pRegAck->service_name,
					pRegAck->pid, pRegAck->regsackuuid, sizeof(pRegAck->uuid_filed))) {
			free(pRegAck);
			pRegAck = NULL;
		}
	}
	return pRegAck;
}

P_REGISACK CreateRegisAckMsg(P_SYNOCOMM_APP pApp)
{
	return (P_REGISACK)CreateRegisAckSerialMsg(pApp, OPCODE_REGISACK);
}

P_UREGISTRY CreateURegistryMsg(P_SYNOCOMM_APP pApp)
{
	return (P_UREGISTRY)CreateRegisAckSerialMsg(pApp, OPCODE_UREGISTRY);
}

P_UREGISACK CreateURegisAckMsg(P_SYNOCOMM_APP pApp)
{
	return (P_UREGISACK)CreateRegisAckSerialMsg(pApp, OPCODE_UREGISACK);
}

P_CONNECTV4 CreateConnectV4Msg(SYNOCOMM_HOST *pH, SYNOCOMM_HOST *pRH)
{
	P_CONNECTV4 pConV4 = (P_CONNECTV4)malloc(sizeof(CONNECTV4));

	if (NULL == pConV4) {
		goto ERR;
	}

	memset(pConV4, 0, sizeof(CONNECTV4));
	pConV4->opcode = OPCODE_CONNECTV4;
	memcpy(pConV4->client_ip, pH->hostip, MAX_IPV4_LEN);
	memcpy(pConV4->server_ip, pRH->hostip, MAX_IPV4_LEN);

ERR:
	return pConV4;
}

P_RCONNECTV4 CreateRConnectV4Msg(SYNOCOMM_HOST *pH, SYNOCOMM_HOST *pRH)
{
	P_RCONNECTV4 pRConV4 = (P_RCONNECTV4)malloc(sizeof(RCONNECTV4));

	if (NULL == pRConV4) {
		goto ERR;
	}

	memset(pRConV4, 0, sizeof(RCONNECTV4));
	pRConV4->opcode = OPCODE_RCONNECTV4;
	memcpy(pRConV4->client_ip, pH->hostip, MAX_IPV4_LEN);
	memcpy(pRConV4->server_ip, pRH->hostip, MAX_IPV4_LEN);

ERR:
	return pRConV4;
}

static void *CreateBindSerialServiceMsgEx(FIELD_IPV4 *pCIP, SERVICE_ID *pCID, FIELD_IPV4 *pSIP, SERVICE_ID *pSID, SYNOCOMM_CMD_OPCODE opcode)
{
	struct _synocomm_bind_ *pBind = (struct _synocomm_bind_ *)malloc(sizeof(struct _synocomm_bind_));

	if (NULL == pBind) {
		goto ERR;
	}

	memset(pBind, 0, sizeof(struct _synocomm_bind_));
	pBind->opcode = opcode;

	memcpy(&pBind->cip, pCIP, MAX_IPV4_LEN);
	memcpy(&pBind->cid, pCID, sizeof(SERVICE_ID));
	memcpy(&pBind->sip, pSIP, MAX_IPV4_LEN);
	memcpy(&pBind->sid, pSID, sizeof(SERVICE_ID));

ERR:
	return (void *)pBind;
}

static void *CreateBindSerialServiceMsg(SYNOCOMM_APP *pCliApp, SYNOCOMM_APP *pSrvApp, SYNOCOMM_CMD_OPCODE opcode)
{
	return CreateBindSerialServiceMsgEx(&pCliApp->host->hip, &pCliApp->serviceid, &pSrvApp->host->hip, &pSrvApp->serviceid, opcode);
}

P_SYNOCOMM_BIND CreateBindServiceMsg(SYNOCOMM_APP *pCliApp, SYNOCOMM_APP *pSrvApp)
{
	return (P_SYNOCOMM_BIND)CreateBindSerialServiceMsg(pCliApp, pSrvApp, OPCODE_BINDSERVICE);
}

P_SYNOCOMM_BOND CreateBondServiceMsg(SYNOCOMM_APP *pCliApp, SYNOCOMM_APP *pSrvApp)
{
	return (P_SYNOCOMM_BOND)CreateBindSerialServiceMsg(pCliApp, pSrvApp, OPCODE_BONDSERVICE);
}

P_SYNOCOMM_BAND CreateBandServiceMsgEx(FIELD_IPV4 *pCIP, SERVICE_ID *pCID, FIELD_IPV4 *pSIP, SERVICE_ID *pSID)
{
	return (P_SYNOCOMM_BAND)CreateBindSerialServiceMsgEx(pCIP, pCID, pSIP, pSID, OPCODE_BANDSERVICE);
}

P_SYNOCOMM_UBIND CreateUBindServiceMsg(SYNOCOMM_APP *pCliApp, SYNOCOMM_APP *pSrvApp)
{
	return (P_SYNOCOMM_UBIND)CreateBindSerialServiceMsg(pCliApp, pSrvApp, OPCODE_UBINDSERVICE);
}

P_SYNOCOMM_UBOND CreateUBondServiceMsg(SYNOCOMM_APP *pCliApp, SYNOCOMM_APP *pSrvApp)
{
	return (P_SYNOCOMM_UBOND)CreateBindSerialServiceMsg(pCliApp, pSrvApp, OPCODE_UBONDSERVICE);
}

P_SYNOCOMM_UBAND CreateUBandServiceMsgEx(FIELD_IPV4 *pCIP, SERVICE_ID *pCID, FIELD_IPV4 *pSIP, SERVICE_ID *pSID)
{
	return (P_SYNOCOMM_UBAND)CreateBindSerialServiceMsgEx(pCIP, pCID, pSIP, pSID, OPCODE_UBANDSERVICE);
}

static void *CreatePingSerialServiceMsg(FIELD_IPV4 *pCIP, SERVICE_ID *pCID, FIELD_IPV4 *pSIP, SERVICE_ID *pSID, SYNOCOMM_CMD_OPCODE opcode, int status)
{
	struct _synocomm_ping_ *pPing = (struct _synocomm_ping_ *)malloc(sizeof(struct _synocomm_ping_));

	if (NULL == pPing) {
		goto ERR;
	}

	memset(pPing, 0, sizeof(struct _synocomm_ping_));
	pPing->opcode = opcode;

	memcpy(&pPing->cip, pCIP, MAX_IPV4_LEN);
	memcpy(&pPing->cid, pCID, sizeof(SERVICE_ID));
	memcpy(&pPing->sip, pSIP, MAX_IPV4_LEN);
	memcpy(&pPing->sid, pSID, sizeof(SERVICE_ID));

	MSG("********** opcode 0x%x, status = 0x%x\n", opcode, status);
	pPing->status = status;

ERR:
	return (void *)pPing;
}

P_SYNOCOMM_PING CreatePingServiceMsg(SYNOCOMM_APP *pCliApp, SYNOCOMM_APP *pSrvApp)
{
	return (P_SYNOCOMM_PING)CreatePingSerialServiceMsg(&pCliApp->host->hip, &pCliApp->serviceid, &pSrvApp->host->hip, &pSrvApp->serviceid, OPCODE_PINGSERVICE, 0);
}

P_SYNOCOMM_PONG CreatePongServiceMsg(FIELD_IPV4 *pCIP, SERVICE_ID *pCID, FIELD_IPV4 *pSIP, SERVICE_ID *pSID, int status)
{
	return (P_SYNOCOMM_PONG)CreatePingSerialServiceMsg(pCIP, pCID, pSIP, pSID, OPCODE_PONGSERVICE, status);
}

P_SYNOCOMM_PUNG CreatePungServiceMsg(FIELD_IPV4 *pCIP, SERVICE_ID *pCID, FIELD_IPV4 *pSIP, SERVICE_ID *pSID, int status)
{
	return (P_SYNOCOMM_PUNG)CreatePingSerialServiceMsg(pCIP, pCID, pSIP, pSID, OPCODE_PUNGSERVICE, status);
}

static void *CreateAppNotifySerialServiceMsg(FIELD_IPV4 *pSIP, SERVICE_ID *pSID, SYNOCOMM_CMD_OPCODE opcode)
{
	struct _synocomm_appnotify_ *pAppNty = (struct _synocomm_appnotify_ *)malloc(sizeof(struct _synocomm_appnotify_));

	if (NULL == pAppNty) {
		goto ERR;
	}

	memset(pAppNty, 0, sizeof(struct _synocomm_appnotify_));
	pAppNty->opcode = opcode;

	memcpy(&pAppNty->sip, pSIP, MAX_IPV4_LEN);
	memcpy(&pAppNty->sid, pSID, sizeof(SERVICE_ID));

ERR:
	return (void *)pAppNty;
}

P_SYNOCOMM_APPNOTIFY CreateAppBroken(FIELD_IPV4 *pSIP, SERVICE_ID *pSID)
{
	return (P_SYNOCOMM_APPNOTIFY)CreateAppNotifySerialServiceMsg(pSIP, pSID, OPCODE_RAPP_BROKEN);
}

void *CreateServiceNotifySerialServiceMsg(FIELD_IPV4 *pSIP, SERVICE_ID *pSID, SYNOCOMM_CMD_OPCODE opcode)
{
	struct _synocomm_service_notify_ *pSerNty = (struct _synocomm_service_notify_ *)malloc(sizeof(struct _synocomm_service_notify_));

	if (NULL == pSerNty) {
		goto ERR;
	}

	memset(pSerNty, 0, sizeof(struct _synocomm_service_notify_));
	pSerNty->opcode = opcode;

	if (pSIP) {
		memcpy(&pSerNty->sip, pSIP, MAX_IPV4_LEN);
	}

	if (pSID) {
		memcpy(&pSerNty->sid, pSID, sizeof(SERVICE_ID));
	}

ERR:
	return (void *)pSerNty;
}

P_SYNOCOMM_SERVICE_NOTIFY CreateServicePipeServerStop()
{
	return CreateServiceNotifySerialServiceMsg(NULL, NULL, OPCODE_SERVICESTOP);
}
