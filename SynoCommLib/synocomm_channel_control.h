#ifndef _SYNO_COMM_CH_CONTROL_H_
#define _SYNO_COMM_CH_CONTROL_H_

#ifndef WIN32
#include <synocomm/synocomm_base.h>
#include <synocomm/synocomm_message.h>
#include <synocomm/synocomm_app.h>
#include <synocomm/synocomm_transport.h>
#else
#include "synocomm_base.h"
#include "synocomm_message.h"
#include "synocomm_app.h"
#include "synocomm_transport.h"
#endif

#ifdef __cplusplus
__SYNO_BEGIN_DECLS
#endif

extern int control_func(void);
extern void FreeCmd(void *pcmd);
extern void FreeMsg(SYNOCOMM_CMD *);

extern int RegistryChannel(P_REGISTRY preg);
extern P_REGISTRY CreateRegistryMsg(P_SYNOCOMM_APP pApp);
extern P_REGISACK CreateRegisAckMsg(P_SYNOCOMM_APP pApp);
extern P_UREGISTRY CreateURegistryMsg(P_SYNOCOMM_APP pApp);
extern P_UREGISACK CreateURegisAckMsg(P_SYNOCOMM_APP pApp);
extern P_CONNECTV4 CreateConnectV4Msg(SYNOCOMM_HOST *pH, SYNOCOMM_HOST *pRH);
extern P_RCONNECTV4 CreateRConnectV4Msg(SYNOCOMM_HOST *pH, SYNOCOMM_HOST *pRH);
extern P_SYNOCOMM_BIND CreateBindServiceMsg(SYNOCOMM_APP *pCliApp, SYNOCOMM_APP *pSrvApp);
extern P_SYNOCOMM_BOND CreateBondServiceMsg(SYNOCOMM_APP *pCliApp, SYNOCOMM_APP *pSrvApp);
extern P_SYNOCOMM_BAND CreateBandServiceMsgEx(FIELD_IPV4 *pCIP, SERVICE_ID *pCID, FIELD_IPV4 *pSIP, SERVICE_ID *pSID);
extern P_SYNOCOMM_UBIND CreateUBindServiceMsg(SYNOCOMM_APP *pCliApp, SYNOCOMM_APP *pSrvApp);
extern P_SYNOCOMM_UBOND CreateUBondServiceMsg(SYNOCOMM_APP *pCliApp, SYNOCOMM_APP *pSrvApp);
extern P_SYNOCOMM_UBAND CreateUBandServiceMsgEx(FIELD_IPV4 *pCIP, SERVICE_ID *pCID, FIELD_IPV4 *pSIP, SERVICE_ID *pSID);
extern P_SYNOCOMM_PING CreatePingServiceMsg(SYNOCOMM_APP *pCliApp, SYNOCOMM_APP *pSrvApp);
extern P_SYNOCOMM_PONG CreatePongServiceMsg(FIELD_IPV4 *pCIP, SERVICE_ID *pCID, FIELD_IPV4 *pSIP, SERVICE_ID *pSID, int status);
extern P_SYNOCOMM_PUNG CreatePungServiceMsg(FIELD_IPV4 *pCIP, SERVICE_ID *pCID, FIELD_IPV4 *pSIP, SERVICE_ID *pSID, int status);
extern P_SYNOCOMM_APPNOTIFY CreateAppBroken(FIELD_IPV4 *pSIP, SERVICE_ID *pSID);
extern P_SYNOCOMM_SERVICE_NOTIFY CreateServicePipeServerStop();

#ifdef __cplusplus
__SYNO_END_DECLS
#endif

#endif
