#ifndef WIN32
#include <synocomm/synocomm_base.h>
#include <synocomm/synocomm_channel.h>
#include <synocomm/synocomm_host.h>
#include <synocomm/synocomm_app.h>
#include <stdlib.h>
#include <search.h>
#else
#include "synocomm_base.h"
#include "synocomm_channel.h"
#include "synocomm_host.h"
#include "synocomm_app.h"
#include <search.h>
#include "stdlib.h"
#endif

SYNOCOMM_HOST_SET *pRemoteHostSet = NULL;
SYNOCOMM_HOST_SET *pLocalHostSet = NULL;

static SYNOCOMM_HOST_SET *CreateHostSet()
{
	SYNOCOMM_HOST_SET *pHS = (SYNOCOMM_HOST_SET *)malloc(sizeof(SYNOCOMM_HOST_SET));
	memset(pHS, 0, sizeof(SYNOCOMM_HOST_SET));
	COMM_LIST_HEAD_INIT(&pHS->hlist);
	//pthread_mutex_init(&pHS->mutex, NULL);
	SYNOCOMM_MUTEX_INIT(pHS->mutex);
	return pHS;
}

static SYNOCOMM_HOST *CreateHost(int host_type, const char *ip, const char *hostname)
{
	int err = -1;
	SYNOCOMM_HOST *pH = NULL;

	if (HOST_LOCAL != host_type &&
		HOST_REMOTE != host_type) {
		goto ERR;
	}

	pH = (SYNOCOMM_HOST *)malloc(sizeof(SYNOCOMM_HOST));
	memset(pH, 0, sizeof(SYNOCOMM_HOST));

	pH->host_type = (HOST_TYPE)host_type;
	if (0 != Str2Ipv4(ip, &pH->hip)) {
		goto ERR;
	}

	snprintf(pH->hostname, MAX_HOSTNAME_LEN, "%s", hostname);
	COMM_LIST_HEAD_INIT(&pH->alist);
	//pthread_mutex_init(&pH->mutex, NULL);
	SYNOCOMM_MUTEX_INIT(pH->mutex);

	err = 0;
ERR:
	if (err && pH) {
		free(pH);
		pH = NULL;
	}
	return pH;
}

static SYNOCOMM_HOST *SearchHostByIP(SYNOCOMM_HOST_SET *pHS, const char *ip)
{
	BOOL blLocked = FALSE;
	SYNOCOMM_HOST *pH = NULL;
	FIELD_IPV4 fip;

	//if (NULL == pHS || 0 == pHS->hcount) {
	if (NULL == pHS) {
		MSG("No such host %s\n", ip);
		goto ERR;
	}

	SYNOCOMM_MUTEX_LOCK(pHS->mutex);
	blLocked = TRUE;
	if (NULL == ip) {
		if (1 == pHS->hcount) {
			COMM_LIST_FIRST_ENTRY(pH, SYNOCOMM_HOST, &pHS->hlist, hlist);
		} else {
			MSG("Invalid parameter\n");
		}
		goto ERR;
	}

	if (0 != Str2Ipv4(ip, &fip)) {
		goto ERR;
	}

	COMM_LIST_FOR_EACH_ENTRY_BEGIN(pH, SYNOCOMM_HOST, &pHS->hlist, hlist)
	COMM_LIST_FOR_EACH_ENTRY(pH, SYNOCOMM_HOST, &pHS->hlist, hlist) {
		if (0 == memcmp(&pH->hip, &fip, sizeof(FIELD_IPV4))) {
			MSG("Host pH %p, ip %s found\n", pHS, ip);
			goto ERR;
		}
	}
	COMM_LIST_FOR_EACH_ENTRY_END

	pH = NULL;
ERR:
	if (blLocked) {
		SYNOCOMM_MUTEX_UNLOCK(pHS->mutex);
	}
	return pH;
}

static int AddHost(int host_type, const char *ip, const char *hostname)
{
	int err = -1;
	SYNOCOMM_HOST_SET *pHS;
	SYNOCOMM_HOST *pH = NULL;

	switch(host_type) {
		case HOST_LOCAL:
			pHS = pLocalHostSet;
			break;
		case HOST_REMOTE:
			pHS = pRemoteHostSet;
			break;
		default:
			goto ERR;
	}

	if (SearchHostByIP(pHS, ip)) {
		err = 0;
		goto ERR;
	}

	if (NULL == (pH = CreateHost(host_type, ip, hostname))) {
		goto ERR;
	}

	SYNOCOMM_MUTEX_LOCK(pHS->mutex);
	UPDATE_ADD_TAIL(&pH->hlist, &pHS->hlist, pHS->hcount);
	SYNOCOMM_MUTEX_UNLOCK(pHS->mutex);
	err = 0;
ERR:
	return err;
}

int InitHostSet()
{
	int err = -1;

	if (NULL == pRemoteHostSet) {
		pRemoteHostSet = CreateHostSet();
	} else {
		goto ERR;
	}

	if (NULL == pLocalHostSet) {
		pLocalHostSet = CreateHostSet();
	} else {
		goto ERR;
	}

	err = 0;
ERR:
	return err;
}

int AddLocalHost(const char *ip, const char *hostname)
{
	return AddHost(HOST_LOCAL, ip, hostname);
}

int AddRemoteHost(const char *ip, const char *hostname)
{
	return AddHost(HOST_REMOTE, ip, hostname);
}

int RemoveHost(SYNOCOMM_HOST *pH)
{
	int err = -1;
	SYNOCOMM_HOST_SET *pHS = NULL;

	switch(pH->host_type) {
		case HOST_LOCAL:
			pHS = pLocalHostSet;
			break;
		case HOST_REMOTE:
			pHS = pRemoteHostSet;
			break;
		default:
			goto ERR;
	}

	SYNOCOMM_MUTEX_LOCK(pHS->mutex);
	UPDATE_DEL(&pH->hlist, pHS->hcount);
	SYNOCOMM_MUTEX_UNLOCK(pHS->mutex);

	//FIXME check all apps if remove ?
	if (pH->acount) {
		MSG("##################################### still %d apps exist in %s\n", pH->acount, pH->hostname);
		goto ERR;
	}
	free(pH);
	err = 0;
ERR:
	return err;
}

SYNOCOMM_HOST *SearchLocalHost(const char *ip)
{
	return SearchHostByIP(pLocalHostSet, ip);
}

SYNOCOMM_HOST *SearchRemoteHost(const char *ip)
{
	return SearchHostByIP(pRemoteHostSet, ip);
}

enum {
	SEARCH_APP_NAME = 0x01,
	SEARCH_APP_OWNER= 0x02,
	SEARCH_APP_UUID = 0x04,
	SEARCH_APP_ID   = SEARCH_APP_NAME | SEARCH_APP_OWNER | SEARCH_APP_UUID,
};

static void ListAppInHost(SYNOCOMM_HOST *pH);

static SYNOCOMM_APP *GetAppInHost(SYNOCOMM_HOST *pH, SERVICE_ID *pID, int type)
{
	int result = 0;
	SYNOCOMM_APP *pApp = NULL;

	if (ISNULL(pH) ||
		ISNULL(pID)) {
		goto ERR;
	}

	ListAppInHost(pH);

	COMM_LIST_FOR_EACH_ENTRY_BEGIN(pApp, SYNOCOMM_APP, &pH->alist, alist)
	COMM_LIST_FOR_EACH_ENTRY(pApp, SYNOCOMM_APP, &pH->alist, alist) {

		MSG(".......................... Compare App %s\n", pApp->serviceid.serviceuuid);

		if ((SEARCH_APP_NAME & type) &&
			(0 == strcmp(pApp->serviceid.service_name, pID->service_name))) {
			result |= SEARCH_APP_NAME;
		MSG(".......................... Match App %s\n", pApp->serviceid.serviceuuid);
		}

		if ((SEARCH_APP_OWNER & type) &&
			(pApp->serviceid.owner == pID->owner)) {
			result |= SEARCH_APP_OWNER;
		MSG(".......................... Match App %s\n", pApp->serviceid.serviceuuid);
		}

		if ((SEARCH_APP_UUID & type) &&
			(0 == CompareUUID(&pApp->serviceid.uuid_filed, &pID->uuid_filed))) {
			result |= SEARCH_APP_UUID;
		MSG(".......................... Match App %s\n", pApp->serviceid.serviceuuid);
		}

		if (type == result) {
		MSG(".......................... Match App %s\n", pApp->serviceid.serviceuuid);
			pApp = AppGet(pApp);
			goto ERR;
		}
	}

	COMM_LIST_FOR_EACH_ENTRY_END
	pApp = NULL;
	MSG(".......................... Not Match App\n");

ERR:
	return pApp;
}

static SYNOCOMM_APP *SearchAppInHostByUUID(SYNOCOMM_HOST *pH, FIELD_UUID *pUUID)
{
	SYNOCOMM_APP *pApp = NULL;
	P_SERVICE_ID pSID = GenServiceID(NULL, 0, pUUID);

	if (pSID) {
		pApp = GetAppInHost(pH, pSID, SEARCH_APP_UUID);
		free(pSID);
	}
	return pApp;
}

static SYNOCOMM_APP *SearchAppInHostSetByUUID(SYNOCOMM_HOST_SET *pHS, FIELD_UUID *pUUID)
{
	SYNOCOMM_APP *pApp = NULL;
	SYNOCOMM_HOST *pH = NULL;

	SYNOCOMM_MUTEX_LOCK(pHS->mutex);
	COMM_LIST_FOR_EACH_ENTRY_BEGIN(pH, SYNOCOMM_HOST, &pHS->hlist, hlist)
	COMM_LIST_FOR_EACH_ENTRY(pH, SYNOCOMM_HOST, &pHS->hlist, hlist) {
		if ((pApp = SearchAppInHostByUUID(pH, pUUID))) {
			SYNOCOMM_MUTEX_UNLOCK(pHS->mutex);
			goto END;
		}
	}
	COMM_LIST_FOR_EACH_ENTRY_END
	SYNOCOMM_MUTEX_UNLOCK(pHS->mutex);

	pApp = NULL;
END:
	return pApp;
}

static void ListAppInHost(SYNOCOMM_HOST *pH)
{
	int index = 0;
	char *szIpstr = NULL;
	SYNOCOMM_APP *pApp = NULL;

	Ipv42Str(&pH->hip, &szIpstr);
	MSG("<<<<<<<<<< HOST %s APP LIST BEGIN >>>>>>>>>>\n", szIpstr);
	COMM_LIST_FOR_EACH_ENTRY_BEGIN(pApp, SYNOCOMM_APP, &pH->alist, alist)
	COMM_LIST_FOR_EACH_ENTRY(pApp, SYNOCOMM_APP, &pH->alist, alist) {
		MSG("..... APP[%d] = %s\n", index, pApp->serviceid.uuid_filed.bits)
			index++;
	}
	COMM_LIST_FOR_EACH_ENTRY_END
	MSG("<<<<<<<<<< HOST %s APP LIST   EMD >>>>>>>>>>\n", szIpstr);
	if (szIpstr) {
		free(szIpstr);
	}
}

static SYNOCOMM_APP *SearchAppInHostByID(SYNOCOMM_HOST *pH, SERVICE_ID *pID)
{
	return GetAppInHost(pH, pID, SEARCH_APP_ID);
}

static SYNOCOMM_APP *SearchAppInHostByName(SYNOCOMM_HOST *pH, const char *service_name)
{
	SYNOCOMM_APP *pApp = NULL;
	P_SERVICE_ID pSID = GenServiceID(service_name, 0, NULL);

	if (pSID) {
		pApp = GetAppInHost(pH, pSID, SEARCH_APP_NAME);
		free(pSID);
	}

	MSG(" ....... pApp %p\n", pApp);
	return pApp;
}

static SYNOCOMM_APP *SearchAppInHostSetByName(SYNOCOMM_HOST_SET *pHS, const char *service_name)
{
	SYNOCOMM_APP *pApp = NULL;
	SYNOCOMM_HOST *pH = NULL;
	SYNOCOMM_HOST *pHTmp = NULL;

	if ((NULL == service_name) ||
		(strlen(service_name) > MAX_SERVICE_NAME_LEN)) {
		EMSG("Invalid parameter\n");
		goto END;
	}

	SYNOCOMM_MUTEX_LOCK(pHS->mutex);
	COMM_LIST_FOR_EACH_ENTRY_SAFE(pH, pHTmp, SYNOCOMM_HOST, &pHS->hlist, hlist) {

		if (NULL != (pApp = SearchAppInHostByName(pH, service_name))) {
			SYNOCOMM_MUTEX_UNLOCK(pHS->mutex);
			goto END;
		}
	}
	SYNOCOMM_MUTEX_UNLOCK(pHS->mutex);

	pApp = NULL;
END:
	return pApp;
}

void *SearchAppInHostByIDExt(SYNOCOMM_HOST *pH, SERVICE_ID *pID)
{
	return SearchAppInHostByID(pH, pID);
}

void *SearchAppInHostByNameExt(SYNOCOMM_HOST *pH, const char *service_name)
{
	return SearchAppInHostByName(pH, service_name);
}

static SYNOCOMM_APP *SearchAppInHostSet(SYNOCOMM_HOST_SET *pHS, SERVICE_ID *pID)
{
	SYNOCOMM_APP *pApp = NULL;
	SYNOCOMM_HOST *pH = NULL;

	MSG(".......................... Searching %s\n", pID->serviceuuid);
	SYNOCOMM_MUTEX_LOCK(pHS->mutex);
	COMM_LIST_FOR_EACH_ENTRY_BEGIN(pH, SYNOCOMM_HOST, &pHS->hlist, hlist)
	COMM_LIST_FOR_EACH_ENTRY(pH, SYNOCOMM_HOST, &pHS->hlist, hlist) {
		if ((pApp = SearchAppInHostByID(pH, pID))) {
			MSG(".......................... Found App %p\n", pApp);
			SYNOCOMM_MUTEX_UNLOCK(pHS->mutex);
			goto END;
		}
	}
	COMM_LIST_FOR_EACH_ENTRY_END
	SYNOCOMM_MUTEX_UNLOCK(pHS->mutex);
	MSG(".......................... Not Found\n");
	pApp = NULL;
END:
	return pApp;
}

static SYNOCOMM_APP *SearchAppInLocalHost(SYNOCOMM_APP *pAppDst)
{
	if (pAppDst) {
		return SearchAppInHostSet(pLocalHostSet, &pAppDst->serviceid);
	} else {
		return NULL;
	}
}

static SYNOCOMM_APP *SearchAppInRemoteHost(SYNOCOMM_APP *pAppDst)
{
	if (pAppDst) {
		return SearchAppInHostSet(pRemoteHostSet, &pAppDst->serviceid);
	} else {
		return NULL;
	}
}

void *SearchAppInLocalHostSetByName(const char *service_name)
{
	return (void *)SearchAppInHostSetByName(pLocalHostSet, service_name);
}

void *SearchAppInRemoteHostSetByName(const char *service_name)
{
	return (void *)SearchAppInHostSetByName(pRemoteHostSet, service_name);
}

void *SearchAppInLocalHostSetByID(SERVICE_ID *pID)
{
	if (pID) {
		return (void *)SearchAppInHostSet(pLocalHostSet, pID);
	} else {
		return NULL;
	}
}

void *SearchAppInRemoteHostSetByID(SERVICE_ID *pID)
{
	if (pID) {
		return (void *)SearchAppInHostSet(pRemoteHostSet, pID);
	} else {
		return NULL;
	}
}

void *SearchAppInLocalHostSetByUUID(FIELD_UUID *pUUID)
{
	if (pUUID) {
		return (void *)SearchAppInHostSetByUUID(pLocalHostSet, pUUID);
	} else {
		return NULL;
	}
}

void *SearchAppInRemoteHostSetByUUID(FIELD_UUID *pUUID)
{
	if (pUUID) {
		return (void *)SearchAppInHostSetByUUID(pRemoteHostSet, pUUID);
	} else {
		return NULL;
	}
}

int HostAdoptApp(SYNOCOMM_HOST *pH, void *pAppIn)
{
	int err = -1;
	SYNOCOMM_APP *pApp = pAppIn;
	if (ISNULL(pH) || ISNULL(pApp)) {
		goto ERR;
	}

	MSG(">>>>>>>>>>>>>>>>> Adopt %s by %s\n", pApp->serviceid.serviceuuid, pH->hostname);
	SYNOCOMM_MUTEX_LOCK(pH->mutex);
	UPDATE_ADD_TAIL(&pApp->alist, &pH->alist, pH->acount);
	SYNOCOMM_MUTEX_UNLOCK(pH->mutex);

	SYNOCOMM_MUTEX_LOCK(pApp->mutex);
	pApp->host = pH;
	SYNOCOMM_MUTEX_UNLOCK(pApp->mutex);

	err = 0;
ERR:
	return err;
}

int HostAbandonApp(SYNOCOMM_HOST *pH, void *pAppIn)
{
	int err = -1;
	SYNOCOMM_APP *pApp = pAppIn;
	if (ISNULL(pH) || ISNULL(pApp)) {
		goto ERR;
	}

	MSG(">>>>>>>>>>>>>>>>> Abandon %s from %s\n", pApp->serviceid.serviceuuid, pH->hostname);
	SYNOCOMM_MUTEX_LOCK(pH->mutex);
	UPDATE_DEL(&pApp->alist, pH->acount);
	SYNOCOMM_MUTEX_UNLOCK(pH->mutex);

	SYNOCOMM_MUTEX_LOCK(pApp->mutex);
	pApp->host = NULL;
	SYNOCOMM_MUTEX_UNLOCK(pApp->mutex);

	err = 0;
ERR:
	return err;
}

int LocalHostAdoptApp(SYNOCOMM_HOST *pH, void *pApp_)
{
	int err = -1;
	SYNOCOMM_APP *pApp = (SYNOCOMM_APP *)pApp_;

	if (NULL == SearchAppInLocalHost(pApp)) {
		MSG("APP service_name %s, pid %d, uuid %s is adopted by host %s\n",
				pApp->serviceid.service_name, pApp->serviceid.owner, pApp->serviceid.serviceuuid, pH->hostname);
		err = HostAdoptApp(pH, pApp);
	} else {
		MSG("APP service_name %s, pid %d, uuid %s exist\n",
				pApp->serviceid.service_name, pApp->serviceid.owner, pApp->serviceid.serviceuuid);
	}

	return err;
}

int RemoteHostAdoptApp(SYNOCOMM_HOST *pH, void *pApp_)
{
	int err = -1;
	SYNOCOMM_APP *pApp = (SYNOCOMM_APP *)pApp_;

	if (NULL == SearchAppInRemoteHost(pApp)) {
		MSG("APP service_name %s, pid %d, uuid %s exist\n",
				pApp->serviceid.service_name, pApp->serviceid.owner, pApp->serviceid.serviceuuid);
		err = HostAdoptApp(pH, pApp);
	} else {
		MSG("APP service_name %s, pid %d, uuid %s exist\n",
				pApp->serviceid.service_name, pApp->serviceid.owner, pApp->serviceid.serviceuuid);
	}

	return err;
}
