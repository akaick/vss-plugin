#ifndef WIN32
#include <stdio.h>
#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <search.h>

#include <synocomm/synocomm_base.h>
#include <synocomm/synocomm_channel_pipe.h>
#include <synocomm/synocomm_message.h>
#else
#include <stdlib.h>
#include <stdio.h>
#include <tchar.h>
#include <search.h>

#include "stdafx.h"
#include "synocomm_base.h"
#include "synocomm_channel_pipe.h"
#include "synocomm_message.h"
#endif

typedef struct _synocomm_pipe_cmd_packet_ {
	int                 cbCmd;
	char                *szCmd;
	SYNOCOMM_LINKEDLIST list;
} SYNOCOMM_PIPE_CMD_PACKET, *P_SYNOCOMM_PIPE_CMD_PACKET;

typedef struct _synocomm_pipe_cmd_packet_set_ {
	int                 count;
	SYNOCOMM_LINKEDLIST list;
} SYNOCOMM_PIPE_CMD_PACKET_SET, *P_SYNOCOMM_PIPE_CMD_PACKET_SET;

void FreeCmdPacket(SYNOCOMM_PIPE_CMD_PACKET *pPacket)
{
	if (NULL != pPacket) {
		if (NULL != pPacket->szCmd) {
			free(pPacket->szCmd);
			pPacket->szCmd = NULL;
		}
		free(pPacket);
	}
}

SYNOCOMM_PIPE_CMD_PACKET *CreateCmdPacket(const char *s, int l)
{
	SYNOCOMM_PIPE_CMD_PACKET *pPacket = (SYNOCOMM_PIPE_CMD_PACKET *)malloc(sizeof(SYNOCOMM_PIPE_CMD_PACKET));

	if (NULL != pPacket) {
		pPacket->szCmd = (char *)malloc(l);
		pPacket->cbCmd = l;
		if (NULL == pPacket->szCmd) {
			free(pPacket);
			pPacket = NULL;
		} else {
			memcpy(pPacket->szCmd, s, l);
		}
	}
	return pPacket;
}

#ifndef WIN32
typedef struct _synocomm_pipe_media_ {
	/*
	 * pipe fd for request
	 */
	SYNOCOMM_PIPE_FD_TYPE pipe_fd;
	/*
	 * dummy fd to prevent EOF
	 */
	SYNOCOMM_PIPE_FD_TYPE dummy_fd;
	/*
	 * pipe name
	 */
	char name[MAX_PATH_LEN];
	/*
	 * cmd buffers
	 */
	SYNOCOMM_PIPE_CMD_PACKET_SET buffer;
} SYNOCOMM_PIPE, *P_SYNOCOMM_PIPE;

#else
#define CONNECTING_STATE 0
#define READING_STATE 1
#define WRITING_STATE 2
#define PIPE_TIMEOUT 5000
#define BUFSIZE 4096
#define MAX_WIN_PIPE_COUNT 8

typedef struct {
	OVERLAPPED oOverlap;
	HANDLE hPipeInst;
	DWORD cbRead;
	DWORD cbToWrite;
	DWORD dwState;
	BOOL fPendingIO;
	char szRequest[MAX_COMM_DATA_SIZE];
} PIPEINST, *LPPIPEINST;

typedef struct _synocomm_pipe_media_ {
	/*
	 * Server Pipes and Event allow multiple client request
	 * And just for read request
	 */
	PIPEINST *PipeIn;
	HANDLE *hEvents;
	DWORD	instance;
	/*
	 * For client to write request
	 */
	HANDLE PipeOut;
	/*
	 * pipe name
	 */
	char name[MAX_PATH_LEN];
	/*
	 * pipe Wide Char name
	 */
	WCHAR wname[MAX_PATH_LEN];
	/*
	 * cmd buffers
	 */
	SYNOCOMM_PIPE_CMD_PACKET_SET buffer;
} SYNOCOMM_PIPE, *P_SYNOCOMM_PIPE;

int SimulateRead(P_SYNOCOMM_PIPE, char *, DWORD, DWORD *);
VOID DisconnectAndReconnect(P_SYNOCOMM_PIPE, DWORD);
BOOL ConnectToNewClient(HANDLE, LPOVERLAPPED);

LPCWSTR GetPipeName(P_SYNOCOMM_PIPE p)
{
	return p->wname;
}

#endif


struct _synocomm_channel_ * synocomm_pipe_construct(struct _synocomm_channel_ *ch, int ctype, int role, int pid, const char *service_name)
{
	int err = -1;
	SYNOCOMM_CHANNEL_DESC *pdesc = NULL;
	struct _synocomm_channel_ *pch = synocomm_base_construct(ch, ctype, role, pid, service_name);

	if (NULL == pch) {
		goto ERR;
	}

	pch->transport = &pipe_channel_transport;
	pdesc = (SYNOCOMM_CHANNEL_DESC *)pch->channel_desc;
	if (ENG_CH == ctype) {
		pdesc->serviceid.owner = SYNOCOMM_CHAN_PIPE_MSG;//-1
	}

	if (0 != ServiceIDGenerateUUID(&pdesc->serviceid)) {
		goto ERR;
	}

	err = 0;
ERR:
	if (err && pch) {
		if (pch->channel_desc) {
			free(pch->channel_desc);
		}
		free(pch);
		pch = NULL;
	}
	return pch;
}

static void __synocomm_pipe_destruct(P_SYNOCOMM_PIPE p, int rm)
{
#ifdef WIN32
	DWORD i;
#endif

	if (!p)
		return;
#ifndef WIN32
	if (p->pipe_fd) {
		close(p->pipe_fd);
	}
	if (p->dummy_fd) {
		close(p->dummy_fd);
	}
	if (rm) {
		MSG(">>>>>>>>>>>>>>>>>>>>> unlink %s\n", p->name);
		unlink(p->name);
	}
#else
	//MAX_WIN_PIPE_COUNT
	for (i = 0; i < p->instance; i++) {
		if (DisconnectNamedPipe(p->PipeIn[i].hPipeInst)) {
			CloseHandle(p->PipeIn[i].hPipeInst);
		}
	}
	CloseHandle(p->PipeOut);
#endif
	free(p);
}

void synocomm_pipe_destruct(struct _synocomm_channel_ *ch)
{
	SYNOCOMM_CHANNEL_DESC *pDesc = NULL;
	if (!ch)
		return;

	pDesc = ch->channel_desc;
	MSG("Free Channel %p, %s\n", ch, pDesc->serviceid.serviceuuid);

	if (ch->external_request) {
		synocomm_base_destruct(ch->external_request_channel);
		ch->external_request_channel = NULL;
		ch->external_request = 0;
	}
	if (ch->external_response) {
		synocomm_base_destruct(ch->external_response_channel);
		ch->external_response_channel = NULL;
		ch->external_response = 0;
	}

	__synocomm_pipe_destruct((P_SYNOCOMM_PIPE)ch->request_channel, (SERVER_END == pDesc->role));
	__synocomm_pipe_destruct((P_SYNOCOMM_PIPE)ch->response_channel, (CLIENT_END == pDesc->role));

	//free(ch);
	return;
}

#ifndef WIN32
static P_SYNOCOMM_PIPE __create_server_pipe(char *name)
{
	P_SYNOCOMM_PIPE p = NULL;

	p = malloc(sizeof(SYNOCOMM_PIPE));
	memset(p, 0, sizeof(SYNOCOMM_PIPE));

	COMM_LIST_HEAD_INIT(&p->buffer.list);

	snprintf(p->name, MAX_PATH_LEN, name);

	umask(0);
	if (mkfifo(p->name, S_IRUSR | S_IWUSR | S_IWGRP) == -1
			&& errno != EEXIST) {
		goto ERR;
	}

	MSG("mkfifo p->name %s\n", p->name);

	//p->pipe_fd = open(p->name, O_RDONLY);
	p->pipe_fd = open(p->name, O_RDWR);

	if (-1 == p->pipe_fd) {
		MSG("mkfifo %s open fail\n", name);
		goto ERR;
	}

	MSG("Server %s open success\n", name);
	/* Prevent to see EOF */
	p->dummy_fd = open(p->name, O_WRONLY);

	if (-1 == p->dummy_fd) {
		goto ERR;
	}

	if (signal(SIGPIPE, SIG_IGN) == SIG_ERR) {
		goto ERR;
	}

	return p;
ERR:
	if (p) {
		if (p->pipe_fd) {
			close(p->pipe_fd);
		}
		if (p->dummy_fd) {
			close(p->dummy_fd);
		}
		free(p);
	}
	return NULL;
}

static P_SYNOCOMM_PIPE __open_client_pipe(char *name)
{
	P_SYNOCOMM_PIPE p = NULL;

	p = malloc(sizeof(SYNOCOMM_PIPE));
	memset(p, 0, sizeof(SYNOCOMM_PIPE));

	snprintf(p->name, MAX_PATH_LEN, name);

	p->pipe_fd = open(p->name, O_WRONLY);

	if (-1 == p->pipe_fd) {
		MSG("Client %s open fail\n", name);
		goto ERR;
	}

	MSG("Client %s open success\n", name);

	return p;
ERR:
	if (p) {
		if (p->pipe_fd) {
			close(p->pipe_fd);
		}
		free(p);
	}
	return NULL;
}

#else

int __initWinPipeServer(P_SYNOCOMM_PIPE p, DWORD count)
{
	int err = -1;
	DWORD i;

	for (i = 0; i < count; i++) {
		// Create an event object for this instance.
		p->hEvents[i] = CreateEvent(
					NULL,    // default security attribute
					TRUE,    // manual-reset event
					TRUE,    // initial state = signaled
					NULL);   // unnamed event object

		if (p->hEvents[i] == NULL) {
			TMSG("CreateEvent failed with %d.\n", GetLastError());
			goto ERR;
		}

		p->PipeIn[i].oOverlap.hEvent = p->hEvents[i];
		p->PipeIn[i].hPipeInst = CreateNamedPipe(
						GetPipeName(p), // pipe name
						PIPE_ACCESS_DUPLEX |     // read/write access
						FILE_FLAG_OVERLAPPED,    // overlapped mode
						PIPE_TYPE_MESSAGE |      // message-type pipe
						PIPE_READMODE_MESSAGE |  // message-read mode
						PIPE_WAIT,               // blocking mode
						count,               // number of instances
						BUFSIZE*sizeof(TCHAR),   // output buffer size
						BUFSIZE*sizeof(TCHAR),   // input buffer size
						PIPE_TIMEOUT,            // client time-out
						NULL);                   // default security attributes

		if (p->PipeIn[i].hPipeInst == INVALID_HANDLE_VALUE) {
			MSG("CreateNamedPipe %s failed with %d.\n", p->name, GetLastError());
			goto ERR;
		}
		// Call the subroutine to connect to the new client
		p->PipeIn[i].fPendingIO = ConnectToNewClient(
							p->PipeIn[i].hPipeInst,
							&p->PipeIn[i].oOverlap);

		p->PipeIn[i].dwState = p->PipeIn[i].fPendingIO ?
							CONNECTING_STATE : // still connecting
							READING_STATE;     // ready to read
	}

	p->instance = count;
	err = 0;
ERR:
	//TODO clean pipe instance and event if fail
	if (err) {
		MSG("Should Clean trash\n");
	}
	return err;
}

static P_SYNOCOMM_PIPE __create_server_pipes(char *name, DWORD count)
{
	size_t wstrSize = 0;
	P_SYNOCOMM_PIPE p = NULL;

	p = (P_SYNOCOMM_PIPE)malloc(sizeof(SYNOCOMM_PIPE));
	memset(p, 0, sizeof(SYNOCOMM_PIPE));

	snprintf(p->name, MAX_PATH_LEN, name);
	mbstowcs_s(&wstrSize, p->wname, MAX_PATH_LEN, p->name, _TRUNCATE);

	p->PipeIn = (PIPEINST *)malloc(sizeof(PIPEINST) * count);
	p->hEvents = (HANDLE *)malloc(sizeof(HANDLE) * count);

	memset(p->PipeIn, 0, sizeof(PIPEINST) * count);
	memset(p->hEvents, 0, sizeof(HANDLE) * count);

	if (__initWinPipeServer(p, count)) {
		goto ERR;
	}

	MSG("Server %s open success\n", p->name);
	return p;
ERR:
	if (p) {
		free(p);
	}
	return NULL;
}

static P_SYNOCOMM_PIPE __create_server_pipe(char *name)
{
	return __create_server_pipes(name, MAX_WIN_PIPE_COUNT);
}

int __initWinPipeCliet(P_SYNOCOMM_PIPE p)
{
	int err = -1;
	BOOL fSuccess = FALSE;
	DWORD dwMode;

	TMSG("pipe name %s\n", GetPipeName(p));

	while (1) {
		p->PipeOut = CreateFile(
					GetPipeName(p),   // pipe name
					GENERIC_READ |  // read and write access
					GENERIC_WRITE,
					0,              // no sharing
					NULL,           // default security attributes
					OPEN_EXISTING,  // opens existing pipe
					0,              // default attributes
					NULL);          // no template file

		// Break if the pipe handle is valid.
		if (p->PipeOut != INVALID_HANDLE_VALUE) {
			MSG("Break INVALID_HANDLE_VALUE\n");
			break;
		}
		// Exit if an error other than ERROR_PIPE_BUSY occurs.
		if (GetLastError() != ERROR_PIPE_BUSY) {
			TMSG("Could not open pipe %s. GLE=%d\n", GetPipeName(p), GetLastError());
			MSG("pipename %s\n", p->name);
			goto ERR;
		}
		// All pipe instances are busy, so wait for 20 seconds.
		if ( ! WaitNamedPipe(GetPipeName(p), 20000)) {
			MSG("Could not open pipe: 20 second wait timed out.");
			goto ERR;
		}
	}

	// The pipe connected; change to message-read mode.

	dwMode = PIPE_READMODE_MESSAGE;
	fSuccess = SetNamedPipeHandleState(
				p->PipeOut,    // pipe handle
				&dwMode,  // new pipe mode
				NULL,     // don't set maximum bytes
				NULL);    // don't set maximum time
	if (! fSuccess) {
		TMSG("SetNamedPipeHandleState failed. GLE=%d\n", GetLastError() );
		goto ERR;
	}

   err = 0;
ERR:
   if (err) {
	   if (p && p->PipeOut) {
		   CloseHandle(p->PipeOut);
		   p->PipeOut = NULL;
	   }
   }
   return err;
}

static P_SYNOCOMM_PIPE __open_client_pipe(char *name)
{
	size_t wstrSize = 0;
	P_SYNOCOMM_PIPE p = NULL;

	p = (P_SYNOCOMM_PIPE)malloc(sizeof(SYNOCOMM_PIPE));
	memset(p, 0, sizeof(SYNOCOMM_PIPE));

	snprintf(p->name, MAX_PATH_LEN, name);
	mbstowcs_s(&wstrSize, p->wname, MAX_PATH_LEN, p->name, _TRUNCATE);

	MSG("Start init Pipe Client %s\n", name);
	if (__initWinPipeCliet(p)) {
		goto ERR;
	}

	MSG("Client %s open success\n", p->name);

	return p;
ERR:
	if (p) {
		free(p);
	}
	return NULL;
}
#endif

int synocomm_pipe_stop_service(SYNOCOMM_CHANNEL *pPRqRx)
{
	int err = -1;
	SYNOCOMM_CHANNEL_DESC *pDesc = NULL;

	if (NULL == pPRqRx ||
		NULL == (pDesc = (SYNOCOMM_CHANNEL_DESC *)pPRqRx->channel_desc)) {
		MSG("Invalid parameter\n");
		goto ERR;
	}

	TRANSPORT(pPRqRx)->destruct(pPRqRx);

ERR:
	return err;
}

int synocomm_pipe_internal_request(struct _synocomm_channel_ *ch, SYNOCOMM_HOST * host)
{
	int err = -1;
	char name[MAX_PATH_LEN];
	SYNOCOMM_CHANNEL_DESC *pdesc = (SYNOCOMM_CHANNEL_DESC *)ch->channel_desc;

	switch (pdesc->serviceid.owner) {
		case SYNOCOMM_CHAN_PIPE_MSG:
			snprintf(name, MAX_PATH_LEN, SYNOCOMM_CHAN_PIPE_SVR_REQ);
			break;
		case SYNOCOMM_CHAN_PIPE_CTL:
			snprintf(name, MAX_PATH_LEN, SYNOCOMM_CHAN_PIPE_SVR_CTL_REQ);
			break;
		default:
			snprintf(name, MAX_PATH_LEN, SYNOCOMM_CHAN_PIPE_REQ_PATH, pdesc->serviceid.owner);
			break;
	}

	if (SERVER_END == pdesc->role) {
		ch->request_channel = __create_server_pipe(name);
	} else if (CLIENT_END == pdesc->role) {
		ch->request_channel = __open_client_pipe(name);
	} else {
		goto ERR;
	}

	if (NULL != ch->request_channel) {
		err = 0;
	}

	MSG("%s request_channel, name=%s\n", (SERVER_END == pdesc->role ? "SERVER_END":"CLIENT_END"), name);

	err = 0;
ERR:
	return err;
}

int synocomm_pipe_internal_response(struct _synocomm_channel_ *ch, SYNOCOMM_HOST * host)
{
	int err = -1;
	char name[MAX_PATH_LEN];
	SYNOCOMM_CHANNEL_DESC *pdesc = (SYNOCOMM_CHANNEL_DESC *)ch->channel_desc;

	switch (pdesc->serviceid.owner) {
		case SYNOCOMM_CHAN_PIPE_MSG:
			snprintf(name, MAX_PATH_LEN, SYNOCOMM_CHAN_PIPE_SVR_REP);
			break;
		case SYNOCOMM_CHAN_PIPE_CTL:
			snprintf(name, MAX_PATH_LEN, SYNOCOMM_CHAN_PIPE_SVR_CTL_REP);
			break;
		default:
			snprintf(name, MAX_PATH_LEN, SYNOCOMM_CHAN_PIPE_REP_PATH, pdesc->serviceid.owner);
			break;
	}

	if (SERVER_END == pdesc->role) {
		ch->response_channel = __open_client_pipe(name);
	} else if (CLIENT_END == pdesc->role) {
		ch->response_channel = __create_server_pipe(name);
	} else {
		goto ERR;
	}

	if (NULL != ch->response_channel) {
		err = 0;
	}

	MSG("%s request_response, name=%s\n", (SERVER_END == pdesc->role ? "SERVER_END":"CLIENT_END"), name);

	err = 0;
ERR:
	return err;
}

int synocomm_pipe_internal_request_media(struct _synocomm_channel_ *ch, void *media)
{
	return 0;
}

int synocomm_pipe_internal_response_media(struct _synocomm_channel_ *ch, void *media)
{
	return 0;
}

/*
 * SYNOLOGY, each char - 'A'
 */
static char SYNOLOGY_PLUGIN_MSG_HEADER[] = {0x12, 0x18, 0x0D, 0x0E, 0x0B, 0x0E, 0x06, 0x18};
#define SYNOLOGY_PLUGIN_MSG_HEADER_LEN (sizeof(SYNOLOGY_PLUGIN_MSG_HEADER))
#define COMM_PIPE_PACKET_SIZE(l)       (l + SYNOLOGY_PLUGIN_MSG_HEADER_LEN)
#define COMM_PIPE_PACKET_DATA_SIZE(l)  (l - SYNOLOGY_PLUGIN_MSG_HEADER_LEN)

static int __synocomm_pipe_write_msg(void *media, char *data, int length)
{
	P_SYNOCOMM_PIPE p = (P_SYNOCOMM_PIPE)media;
#ifndef WIN32
	int ret = -1;
	int __length = COMM_PIPE_PACKET_SIZE(length);
	char *__data = NULL;
	if ((__data = (char *)malloc(__length))) {
		memcpy(__data, SYNOLOGY_PLUGIN_MSG_HEADER, SYNOLOGY_PLUGIN_MSG_HEADER_LEN);
		memcpy(__data + SYNOLOGY_PLUGIN_MSG_HEADER_LEN, data, length);
		ret = COMM_PIPE_PACKET_DATA_SIZE(write(p->pipe_fd, __data, __length));
		free(__data);
	}
	return ret;
#else
	BOOL  fSuccess = FALSE;
	DWORD  ret;
	fSuccess = WriteFile(p->PipeOut, data, (DWORD)length, &ret, NULL);

	if (! fSuccess) {
		TMSG( "WriteFile to pipe failed. GLE=%d\n", GetLastError() );
		return -1;
	}
	return (int)ret;
#endif
}

static int __synocomm_pipe_parse_packet(SYNOCOMM_PIPE *p, const char *pMsg, int cMsg)
{
	int i = 0;
	int look = 0;
	int prev = -1;
	int count = 0;
	SYNOCOMM_PIPE_CMD_PACKET *pPacket = NULL;
	while (look < cMsg) {
		i = 0;
		//find the first char
		while ((look+i < cMsg) &&
				pMsg[look+i] == SYNOLOGY_PLUGIN_MSG_HEADER[i]) {
			i++;
		}
		if (i == SYNOLOGY_PLUGIN_MSG_HEADER_LEN) {
			if (0 <= prev) {
				pPacket = CreateCmdPacket((pMsg + prev + SYNOLOGY_PLUGIN_MSG_HEADER_LEN),
											(look - prev - SYNOLOGY_PLUGIN_MSG_HEADER_LEN));
				if (NULL != pPacket) {
					UPDATE_ADD_TAIL(&pPacket->list, &p->buffer.list, p->buffer.count);
					count++;
				}
			}
			prev = look;
			look += SYNOLOGY_PLUGIN_MSG_HEADER_LEN;
		} else {
			look++;
		}
	}

	if (0 > prev) {
		MSG("No packet found, pMsg[0] = 0x%2x, total = %d\n", pMsg[0], cMsg);
	} else if (SYNOLOGY_PLUGIN_MSG_HEADER_LEN < look - prev) {
		pPacket = CreateCmdPacket((pMsg + prev + SYNOLOGY_PLUGIN_MSG_HEADER_LEN),
				                 (look - prev - SYNOLOGY_PLUGIN_MSG_HEADER_LEN));
		if (NULL != pPacket) {
			UPDATE_ADD_TAIL(&pPacket->list, &p->buffer.list, p->buffer.count);
			count++;
		}
	}
	return count;
}

static SYNOCOMM_PIPE_CMD_PACKET *__synocomm_pipe_high_priority_packet(SYNOCOMM_PIPE *p)
{
	SYNOCOMM_PIPE_CMD_PACKET *pPacket = NULL;
	COMM_LIST_FOR_EACH_ENTRY_BEGIN(pPacket, SYNOCOMM_PIPE_CMD_PACKET, &p->buffer.list, list)
	COMM_LIST_FOR_EACH_ENTRY(pPacket, SYNOCOMM_PIPE_CMD_PACKET, &p->buffer.list, list)
	{
		if (OPCODE_RAPP_BROKEN == pPacket->szCmd[0]) {
			MSG("^^^^^^^^^^High Priority Command - OPCODE_RAPP_BROKEN^^^^^^^^^^\n");
			return pPacket;
		}
	}
	COMM_LIST_FOR_EACH_ENTRY_END

	return NULL;
}

static int __synocomm_pipe_data_packet_read(SYNOCOMM_PIPE *p, char *data, int length)
{
#ifndef WIN32
	int ret = -1;
#else
	DWORD ret = -1;
#endif
	char *__data = NULL;
	SYNOCOMM_PIPE_CMD_PACKET *pPacket = NULL;

	if (NULL != (pPacket = __synocomm_pipe_high_priority_packet(p))) {
		goto COPY;
	}

	COMM_LIST_FIRST_ENTRY(pPacket, SYNOCOMM_PIPE_CMD_PACKET, &p->buffer.list, list);
	if (NULL != pPacket) {
		goto COPY;
	}

	if (NULL == (__data = (char *)malloc(MAX_COMM_DATA_SIZE))) {
		goto ERR;
	}
	memset(__data, 0, MAX_COMM_DATA_SIZE);
#ifndef WIN32
	ret = read(p->pipe_fd, __data, MAX_COMM_DATA_SIZE);
#else
	SimulateRead(p, __data, MAX_COMM_DATA_SIZE, &ret);
#endif

	if (0 < ret) {
		__synocomm_pipe_parse_packet(p, __data, ret);
	}

	COMM_LIST_FIRST_ENTRY(pPacket, SYNOCOMM_PIPE_CMD_PACKET, &p->buffer.list, list);
	if (NULL == pPacket) {
		ret = -1;
		goto ERR;
	}

COPY:
	UPDATE_DEL(&pPacket->list, p->buffer.count);
	ret = (pPacket->cbCmd <= length) ? pPacket->cbCmd:length;
	memcpy(data, pPacket->szCmd, ret);
	free(pPacket->szCmd);
	free(pPacket);

ERR:
	if (__data) {
		free(__data);
	}
	return ret;
}

static int __synocomm_pipe_read_msg(void *media, char *data, int length)
{
	P_SYNOCOMM_PIPE p = (P_SYNOCOMM_PIPE)media;
#ifndef WIN32
	return __synocomm_pipe_data_packet_read(p, data, length);
#else
	DWORD ret;
	SimulateRead(p, data, length, &ret);
	return ret;
#endif
}

#ifdef WIN32
int SimulateRead(P_SYNOCOMM_PIPE p, char *data, DWORD length, DWORD *_cbRead)
{
	int err = -1;
	DWORD i, dwWait, cbRet, dwErr;
	BOOL fSuccess;

	*_cbRead = 0;
	while (1) {
		// Wait for the event object to be signaled, indicating
		// completion of an overlapped read, write, or
		// connect operation.
		dwWait = WaitForMultipleObjects(
				p->instance,    // number of event objects
				p->hEvents,      // array of event objects
				FALSE,        // does not wait for all
				INFINITE);    // waits indefinitely

		// dwWait shows which pipe completed the operation.

		i = dwWait - WAIT_OBJECT_0;  // determines which pipe
		if (i < 0 || i > (p->instance - 1)) {
			MSG("Index out of range i=%d.\n", i);
			goto END;
		}

		// Get the result if the operation was pending.
		if (p->PipeIn[i].fPendingIO) {
			fSuccess = GetOverlappedResult(
						p->PipeIn[i].hPipeInst, // handle to pipe
						&p->PipeIn[i].oOverlap, // OVERLAPPED structure
						&cbRet,            // bytes transferred
						FALSE);            // do not wait

			switch (p->PipeIn[i].dwState) {
				case CONNECTING_STATE:
					if (! fSuccess) {
						LMSG("Error %d.\n", GetLastError());
						goto END;
					}
					p->PipeIn[i].dwState = READING_STATE;
					break;

				case READING_STATE:
					if (! fSuccess || cbRet == 0) {
						DisconnectAndReconnect(p, i);
						continue;
					}
					p->PipeIn[i].cbRead = cbRet;
					p->PipeIn[i].fPendingIO = FALSE;
					*_cbRead = p->PipeIn[i].cbRead;

					memcpy(data, p->PipeIn[i].szRequest, length);
					if (p->PipeIn[i].cbRead > length) {
						MSG("PIPE [%d] DATA LOST\n", i);
					}
					p->PipeIn[i].cbRead = 0;
					err = 0;
					goto END;

				default:
					LMSG("Invalid pipe state.\n");
					goto END;

			}
		} // pending
		// The pipe state determines which operation to do next.

		switch (p->PipeIn[i].dwState) {
			// READING_STATE:
			// The pipe instance is connected to the client
			// and is ready to read a request from the client.
			case READING_STATE:
				fSuccess = ReadFile(
							p->PipeIn[i].hPipeInst,
							p->PipeIn[i].szRequest,
							length,
							&p->PipeIn[i].cbRead,
							&p->PipeIn[i].oOverlap);

				if (fSuccess && p->PipeIn[i].cbRead != 0) {
					p->PipeIn[i].fPendingIO = FALSE;
					*_cbRead = p->PipeIn[i].cbRead;

					memcpy(data, p->PipeIn[i].szRequest, length);
					if (p->PipeIn[i].cbRead > length) {
						MSG("PIPE [%d] DATA LOST\n", i);
					}
					p->PipeIn[i].cbRead = 0;
					err = 0;
					goto END;
				}
				// The read operation is still pending.
				dwErr = GetLastError();
				if (! fSuccess && (dwErr == ERROR_IO_PENDING)) {
					p->PipeIn[i].fPendingIO = TRUE;
					continue;
				}
				// An error occurred; disconnect from the client.
				DisconnectAndReconnect(p, i);
				break;

			default:
				LMSG("Invalid pipe state.\n");
				goto END;
		}
	}
END:
	return err;
}

VOID DisconnectAndReconnect(P_SYNOCOMM_PIPE p, DWORD i)
{
	// Disconnect the pipe instance.
	if (! DisconnectNamedPipe(p->PipeIn[i].hPipeInst)) {
		LMSG("DisconnectNamedPipe failed with %d.\n", GetLastError());
	}

	// Call a subroutine to connect to the new client.
	p->PipeIn[i].fPendingIO = ConnectToNewClient(
						p->PipeIn[i].hPipeInst,
						&p->PipeIn[i].oOverlap);

	p->PipeIn[i].dwState = p->PipeIn[i].fPendingIO ?
						CONNECTING_STATE : // still connecting
						READING_STATE;     // ready to read
}

BOOL ConnectToNewClient(HANDLE hPipe, LPOVERLAPPED lpo)
{
	BOOL fConnected, fPendingIO = FALSE;
	// Start an overlapped connection for this pipe instance.
	fConnected = ConnectNamedPipe(hPipe, lpo);

	// Overlapped ConnectNamedPipe should return zero.
	if (fConnected) {
		LMSG("ConnectNamedPipe failed with %d.\n", GetLastError());
		return 0;
	}

	switch (GetLastError()) {
		// The overlapped connection in progress.
		case ERROR_IO_PENDING:
			fPendingIO = TRUE;
			break;
		// Client is already connected, so signal an event.
		case ERROR_PIPE_CONNECTED:
			if (SetEvent(lpo->hEvent))
				break;
		// If an error occurs during the connect operation...
		default:
			LMSG("ConnectNamedPipe failed with %d.\n", GetLastError());
			return 0;
	}
	return fPendingIO;
}

#endif

SYNOCOMM_TRANSPORT pipe_channel_transport = {
	SFINIT(.media_type, CH_NAMEPIPE),
	SFINIT(.construct, synocomm_pipe_construct),
	SFINIT(.destruct, synocomm_pipe_destruct),
	SFINIT(.start_service, NULL),
	SFINIT(.stop_service, synocomm_pipe_stop_service),
	SFINIT(.create_internal_request, synocomm_pipe_internal_request),
	SFINIT(.create_internal_response, synocomm_pipe_internal_response),
	SFINIT(.setup_internal_request, synocomm_pipe_internal_request_media),
	SFINIT(.setup_internal_response, synocomm_pipe_internal_response_media),
	SFINIT(.setup_external_request, synocomm_base_external_request),
	SFINIT(.setup_external_response, synocomm_base_external_response),
	SFINIT(.write_op, __synocomm_pipe_write_msg),
	SFINIT(.read_op, __synocomm_pipe_read_msg),
	SFINIT(.send_msg, synocomm_base_send_msg),
	SFINIT(.recv_msg, synocomm_base_recv_msg),
};

