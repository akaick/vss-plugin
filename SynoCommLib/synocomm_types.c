#ifndef WIN32
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
//#include <synosdk/net.h>
#include <synosdk/iscsi.h>
#include <synocomm/synocomm_types.h>

#else
#include "stdlib.h"
#include "string.h"
#include "synocomm_types.h"

#endif

int Str2Ipv4(const char *_str, FIELD_IPV4 *ipv4)
{
	int i = 0;
	int digit = 0;
	int err = -1;

	char *str = strdup(_str);
	char *ntoken = NULL;
	char *token = strtok_r(str, ".", &ntoken);

	while (NULL != token) {
		digit = atoi(token);

		if (digit < 0) {
			goto ERR;
		}
		ipv4->bits[i] = (char)digit;
		i++;

		if (i >= MAX_IPV4_LEN) {
			err = 0;
			break;
		}
		token = strtok_r(NULL, ".", &ntoken);
	}

	err = 0;
ERR:
	if (str) {
		free(str);
	}
	return err;
}

int Ipv42Str(FIELD_IPV4 *ipv4, char **str)
{
	int err = -1;

	if (NULL == str || NULL == ipv4) {
		MSG("Invalid parametr\n");
		goto ERR;
	}

	*str = (char *)malloc(MAX_IPV4_STR_LEN);
	snprintf(*str, MAX_IPV4_STR_LEN, "%d.%d.%d.%d",
			(unsigned char)ipv4->bits[0], (unsigned char)ipv4->bits[1], (unsigned char)ipv4->bits[2], (unsigned char)ipv4->bits[3]);

	err = 0;
ERR:
	return err;
}

int ServiceUUIDGenerate(char *name, int id, char *uuid, int len)
{
	int err = -1;

	if (NULL == name || NULL == uuid) {
		goto ERR;
	}

	snprintf(uuid, len, "%s-%d", name, id);
	err = 0;
ERR:
	return err;
}

int ServiceIDGenerateUUID(SERVICE_ID *psid)
{
	if (NULL == psid) {
		return -1;
	}
	return ServiceUUIDGenerate(psid->service_name,
							psid->owner,
							psid->serviceuuid,
							sizeof(psid->uuid_filed));
}

int CompareUUID(FIELD_UUID *pUUID1, FIELD_UUID *pUUID2)
{
	int err = -1;

	if (NULL == pUUID1 ||
		NULL == pUUID2) {
		EMSG("Invalid parameter\n");
		goto ERR;
	}

#if 0
	if (0 != memcmp(pUUID1, pUUID2, sizeof(FIELD_UUID))) {
#else
	if (0 != strcmp(pUUID1->bits, pUUID2->bits)) {
#endif
		err = 1;
	} else {
		err = 0;
	}
ERR:
	return err;
}

int CompareSID(SERVICE_ID *pID1, SERVICE_ID *pID2)
{
	int err = -1;

	if (NULL == pID1 ||
		NULL == pID2) {
		EMSG("Invalid parameter\n");
		goto ERR;
	}

#if 1	//FIXME Windows Send strange string with non-zero character
		err = (0 == strcmp(pID1->service_name, pID2->service_name) &&
			pID1->owner == pID2->owner &&
			0 == strcmp(pID1->serviceuuid, pID2->serviceuuid)) ? 0:1;
#else
		err = (0 != memcmp(pID1, pID2, sizeof(SERVICE_ID))) ? 1:0;
		if (0 != memcmp(pID1, pID2, sizeof(SERVICE_ID))) {
			err = 1;
		} else {
			err = 0;
		}
#endif

ERR:
	return err;
}

P_SERVICE_ID GenServiceID(const char *service_name, int owner, const FIELD_UUID *pUUID)
{
	P_SERVICE_ID pSID = (P_SERVICE_ID)malloc(sizeof(SERVICE_ID));

	if (pSID) {
		memset(pSID, 0, sizeof(SERVICE_ID));
		if (!ISNULL(service_name)) {
			snprintf(pSID->service_name, sizeof(pSID->service_name), "%s", service_name);
		}
		pSID->owner = owner;
		if (!ISNULL(pUUID)) {
			memcpy(&pSID->uuid_filed, pUUID, sizeof(FIELD_UUID));
		}
	}
	return pSID;
}

#ifdef WIN32
static int WinGetLocalIPV4(FIELD_IPV4 *ipv4)
{
	int err = -1;
	char szBuffer[128];
	struct hostent *host = NULL;

	WSADATA wsaData;
	WORD wVersionRequested = MAKEWORD(2, 0);
	if(WSAStartup(wVersionRequested, &wsaData) != 0) {
		goto END;
	}

	if(gethostname(szBuffer, sizeof(szBuffer)) == SOCKET_ERROR) {
		goto ERR;
	}

	host = gethostbyname(szBuffer);
	if(host == NULL) {
		goto ERR;
	}

    //Obtain the computer's IP
	ipv4->bits[0] = ((struct in_addr *)(host->h_addr))->S_un.S_un_b.s_b1;
	ipv4->bits[1] = ((struct in_addr *)(host->h_addr))->S_un.S_un_b.s_b2;
	ipv4->bits[2] = ((struct in_addr *)(host->h_addr))->S_un.S_un_b.s_b3;
	ipv4->bits[3] = ((struct in_addr *)(host->h_addr))->S_un.S_un_b.s_b4;

	err = 0;
ERR:
	WSACleanup();
END:
	return err;
}
#else

static int SYNOCPBGetLocalIP(char *szIP, int cbIP)
{
	int nMax = NUM_MAXIFACES;
	int numCard = 0;
	int i;
	int iRet = -1;
	PSLIBNETIF rgpsnIf = NULL;

	if ( SLIBCFileLock(LOCK_HOST|LOCK_SH_NB, LOCK_TIMEOUT) == -1 ) {
		MSG("lock fail(%X)", SLIBCErrGet());
		return -1;
	}

	if (NULL == (rgpsnIf = (PSLIBNETIF)malloc(nMax * sizeof(SLIBNETIF)))) {
		MSG("malloc fail");
		goto Err;
	}

	// Enum all interface info
#ifdef SYNO_JUNIOR_INSTALLER
	if (0 >= (numCard = SLIBNetGetInterfaceInfo(rgpsnIf, nMax, NIC_LOGICAL))) {
#else
	if (0 >= (numCard = SLIBNetInterfaceEnum(rgpsnIf, nMax, IF_QUERY_ALL))) {
#endif
		MSG("%s", (0 == numCard) ? "No interface." : "Get interface info fail.");
		goto Err;
	}

	if (2 > numCard) {
		goto JustReturn;
	}

	MSG("numCard = %d\n", numCard);
	for(i=0 ; i<numCard ; i++){
		SYSLOG(LOG_ERR, "szName %s %s is %s\n", rgpsnIf[i].szName, rgpsnIf[i].szDes, (IFM_ACTIVE == rgpsnIf[i].status) ? "Active":"NoneActive");
		if (IFM_ACTIVE == rgpsnIf[i].status) {
			snprintf(szIP, cbIP, "%s", rgpsnIf[i].szDes);
			break;
		}
	}

JustReturn:
	iRet = 0;
Err:
	if (rgpsnIf) free(rgpsnIf);
	SLIBCFileUnlock(LOCK_HOST);
	return iRet;
}
#endif

int GetLocalIPV4Str(char *szIPstr, const int cbIPstr)
{
	int err = -1;

#ifndef WIN32
	if (0 > SYNOCPBGetLocalIP(szIPstr, cbIPstr)) {
		SYSLOG(LOG_ERR, "Fail to get localIP\n");
		goto ERR;
	}
#else
	FIELD_IPV4 ipv4;
	if (NULL == szIPstr || 0 > cbIPstr) {
		MSG("Invalid parametr\n");
		goto ERR;
	}

	if (0 >WinGetLocalIPV4(&ipv4)) {
		goto ERR;
	}

	snprintf(szIPstr, cbIPstr, "%d.%d.%d.%d",
		(unsigned char)ipv4.bits[0], (unsigned char)ipv4.bits[1], (unsigned char)ipv4.bits[2], (unsigned char)ipv4.bits[3]);
#endif

	err = 0;
ERR:
	return err;

}
