#ifndef _SYNO_COMM_CHANNEL_H_
#define _SYNO_COMM_CHANNEL_H_

#ifndef WIN32
#include <synocomm/synocomm_base.h>
#include <synocomm/synocomm_list.h>
#include <synocomm/synocomm_message.h>
#else
#include "synocomm_base.h"
#include "synocomm_list.h"
#include "synocomm_message.h"
#endif

#ifdef __cplusplus
__SYNO_BEGIN_DECLS
#endif

#define	INT_CH		(1)
#define	EXTI_CH		(2)
#define	EXTO_CH		(3)
#define	PROC_CH		(4)
#define	UNKNOW_CH	(5)
typedef struct _channel_set_ {
	int	icount;
	SYNOCOMM_LINKEDLIST internals;
	int eicount;
	SYNOCOMM_LINKEDLIST extins;
	int eocount;
	SYNOCOMM_LINKEDLIST extouts;
	int pcount;
	SYNOCOMM_LINKEDLIST processes;
	/*
	 * mutex for protection
	 */
	SYNOCOMM_MUTEX mutex;
} SYNOCOMM_CHANNEL_SET, *P_SYNOCOMM_CHANNEL_SET;

extern SYNOCOMM_CHANNEL_SET *pChannelSet;

extern int AddInternalChannel(SYNOCOMM_CHANNEL_SET *pCHSet, SYNOCOMM_CHANNEL *pch);
extern int AddExternalInChannel(SYNOCOMM_CHANNEL_SET *pCHSet, SYNOCOMM_CHANNEL *pch);
extern int AddExternalOutChannel(SYNOCOMM_CHANNEL_SET *pCHSet, SYNOCOMM_CHANNEL *pch);
extern int AddProcessChannel(SYNOCOMM_CHANNEL_SET *pCHSet, SYNOCOMM_CHANNEL *pch);
extern int RemoveChannel(SYNOCOMM_CHANNEL_SET *pCHSet, SYNOCOMM_CHANNEL *pch);

extern SYNOCOMM_CHANNEL *GetInternalChannel(SYNOCOMM_CHANNEL_SET *pCHSet, char *service_name);
extern SYNOCOMM_CHANNEL *GetExternalInChannel(SYNOCOMM_CHANNEL_SET *pCHSet, char *service_name);
extern SYNOCOMM_CHANNEL *GetExternalOutChannel(SYNOCOMM_CHANNEL_SET *pCHSet, char *service_name);
extern SYNOCOMM_CHANNEL *GetProcessChannel(SYNOCOMM_CHANNEL_SET *pCHSet, char *service_name);
extern SYNOCOMM_CHANNEL *GetExternalOutChannelByUUID(SYNOCOMM_CHANNEL_SET *pCHSet, FIELD_UUID *ch_uuid);
extern SYNOCOMM_CHANNEL *GetProcessChannelByUUID(SYNOCOMM_CHANNEL_SET *pCHSet, FIELD_UUID *ch_uuid);

extern int InitChannelSet();

extern void ListExternalInChannel(SYNOCOMM_CHANNEL_SET *pCHSet);
extern void ListExternalOutChannel(SYNOCOMM_CHANNEL_SET *pCHSet);
extern void ListInternalChannel(SYNOCOMM_CHANNEL_SET *pCHSet);
extern void ListProcessChannel(SYNOCOMM_CHANNEL_SET *pCHSet);

extern void ChannelConnectionCheck(SYNOCOMM_CHANNEL_SET *pCHSet);
extern void ChannelBroadCast(SYNOCOMM_CHANNEL_SET *pCHSet, const char *pMsg, int length);
extern void ChannelPut(SYNOCOMM_CHANNEL *pCh);
extern void ChannelFree(SYNOCOMM_CHANNEL *pCh);

#ifdef __cplusplus
__SYNO_END_DECLS
#endif

#endif

