; vim:set ts=2 sw=2 sts=2 smarttab expandtab:
; ===========================================================================
!addplugindir "."
!include "MUI2.nsh"
!include "LogicLib.nsh"
!include "x64.nsh"
!include "WinVer.nsh"
!include "Sections.nsh"
!include "WordFunc.nsh"
!include "FileFunc.nsh"
!include "nsProcess.nsh"

; ===========================================================================
; Global Definitions
; ===========================================================================
!define PLUGIN_SERVICE_NAME "SynoVss"
!define PLUGIN_COMMENGINE_NAME "SynoCommEngine"
!define PLUGIN_PROCESS_NAME "SynoVssTray.exe"
!define PLUGIN_ENGINE_NAME "SynoCommEngine.exe"
!define PLUGIN_SYNO_FOLDER "Synology"
!define PLUGIN_START_MENU "Snapshot Manager for Windows"
!define PLUGIN_NETWORK_PROT "3262"

!define PRODUCT_NAME "Synology Snapshot Manager for Windows"
!define PRODUCT_VERSION "1.0.4.0"
!define PRODUCT_REVISION "0"
!define PRODUCT_PROTOCOL_VERSION "1.0"
!define PRODUCT_DISPLAY_VERSION "1.0.4"
!define PRODUCT_PUBLISHER "Synology Inc."
!define PRODUCT_WEB_SITE "http://www.synology.com"
!define PRODUCT_SYNO_KEY   "Software\${PLUGIN_SYNO_FOLDER}"
!define PRODUCT_CONFIG_KEY "${PRODUCT_SYNO_KEY}\${PLUGIN_START_MENU}"
!define PRODUCT_UNINST_KEY "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_NAME}"
!define PRODUCT_VERSION_REG "Version"
!define PRODUCT_ROOT_KEY "HKLM"

var MajorVersion
var MinorVersion
var BuildNumber
var PlatformID
var CSDVersion
var ProductType
var IsUpgradeMode

; ===========================================================================
; Installer Settings
; ===========================================================================
Name "${PRODUCT_NAME} ${PRODUCT_DISPLAY_VERSION}"
OutFile "SynoVssSetup.exe"
SetCompressor /SOLID lzma
SetOverwrite on
ShowInstDetails show
ShowUnInstDetails show
RequestExecutionLevel admin
; need test
XPStyle on
CRCCheck force

; ===========================================================================
; Version Info
; ===========================================================================
VIAddVersionKey "ProductName" "${PRODUCT_NAME}"
VIAddVersionKey "CompanyName" "${PRODUCT_PUBLISHER}"
VIAddVersionKey "LegalCopyright" "(C) ${PRODUCT_PUBLISHER}"
VIAddVersionKey "FileDescription" "Synology VSS plugin"
VIAddVersionKey "FileVersion" "${PRODUCT_VERSION} ${PRODUCT_REVISION}"
VIProductVersion "${PRODUCT_VERSION}"

; ===========================================================================
; Modern UI
; ===========================================================================
!define MUI_ABORTWARNING
!define MUI_FINISHPAGE_NOAUTOCLOSE
!define MUI_UNFINISHPAGE_NOAUTOCLOSE
!define MUI_ICON "images\install.ico"
!define MUI_UNICON "images\uninstall.ico"
; Component pages related
;!define MUI_COMPONENTSPAGE_SMALLDESC

; ===========================================================================
; MUI Language Selection
; ===========================================================================
!define MUI_LANGDLL_ALLLANGUAGES
!define MUI_LANGDLL_REGISTRY_ROOT "HKLM"
!define MUI_LANGDLL_REGISTRY_KEY "${PRODUCT_UNINST_KEY}"
!define MUI_LANGDLL_REGISTRY_VALUENAME "Language"
!define MUI_LANGDLL_WINDOWTITLE "Language Selection"
!define MUI_LANGDLL_INFO "Please select a language"

; ===========================================================================
; MUI Pages
; ===========================================================================
!define MUI_WELCOMEPAGE_TITLE_3LINES
!insertmacro MUI_PAGE_WELCOME
!define MUI_LICENSEPAGE_CHECKBOX
!insertmacro MUI_PAGE_LICENSE $(license)
; Component related pages
;!define MUI_PAGE_CUSTOMFUNCTION_PRE ComponentPre
;!insertmacro MUI_PAGE_COMPONENTS
; Skip installation path selection page
;!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES
!define MUI_FINISHPAGE_TITLE_3LINES
!insertmacro MUI_PAGE_FINISH

; Uninstaller pages
!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES
!define MUI_FINISHPAGE_TITLE_3LINES
!insertmacro MUI_UNPAGE_FINISH

; ===========================================================================
; MUI Languages
; ===========================================================================
!insertmacro MUI_LANGUAGE "English"
!insertmacro MUI_LANGUAGE "TradChinese"
!insertmacro MUI_RESERVEFILE_LANGDLL

LicenseLangString license ${LANG_ENGLISH} "license\License_enu.rtf"
LangString not_supported_os ${LANG_ENGLISH} "Not supported OS version. Synology Snapshot Manager for Windows can only be running on Windows Server 2008 R2, 2012 or 2012 R2."
LangString already_up_to_date ${LANG_ENGLISH} "The installed version is already up to date."
LangString installed_version_is_newer ${LANG_ENGLISH} "The installed version is newer."
LangString older_version_detected ${LANG_ENGLISH} "An older version detected. Do you want to remove it? Click OK to remove older version. Click Cancel to abort this installation."
LangString failed_to_remove_older_version ${LANG_ENGLISH} "Failed to remove previous version. Please remove it manually and then try again."
LangString app_still_running ${LANG_ENGLISH} "The application is still running, please manually stop the application first."
LangString failed_to_stop_service ${LANG_ENGLISH} "Failed to stop service, please manually stop the plugin service (SynoVss) first."
; TODO: add the following keys in string system
LangString desc_app_start ${LANG_ENGLISH} "${PRODUCT_NAME}"
LangString desc_uninstall ${LANG_ENGLISH} "Uninstall ${PRODUCT_NAME}"
LangString firewall_add_warning ${LANG_ENGLISH} "Fail to add Synology Snapshot Manager to firewall exception list. Please check the environment first."
LangString desc_firewall_incoming_rule ${LANG_ENGLISH} "Allow incoming requests for Synology Snapshot Manager."
LangString desc_firewall_outgoing_rule ${LANG_ENGLISH} "Allow Synology Snapshot Manager outgoing requests."

LicenseLangString license ${LANG_TRADCHINESE} "license\License_cht.rtf"
LangString not_supported_os ${LANG_TRADCHINESE} "不支援的作業系統版本。Synology Snapshot Manager for Windows 必須在 Windows Server 2008 R2，2012 以及 2012 R2 上運行。"
LangString already_up_to_date ${LANG_TRADCHINESE} "已安裝的版本已為新版本。"
LangString installed_version_is_newer ${LANG_TRADCHINESE} "已安裝的版本為較新版本。"
LangString older_version_detected ${LANG_TRADCHINESE} "偵測到已安裝的舊版本，是否先刪除後進行升級? 按下確定進行升級，按下取消中止這次安裝。"
LangString failed_to_remove_older_version ${LANG_TRADCHINESE} "移除舊版本失敗，請手動進行移除舊版本後再試一次。"
LangString app_still_running ${LANG_TRADCHINESE} "應用程式尚在執行中，請先關閉後再進行移除。"
LangString failed_to_stop_service ${LANG_TRADCHINESE} "無法停止運行中的 plugin 服務，請先停止 SynoVss 服務。"
LangString desc_app_start ${LANG_TRADCHINESE} "${PRODUCT_NAME}"
LangString desc_uninstall ${LANG_TRADCHINESE} "移除 ${PRODUCT_NAME}"
LangString firewall_add_warning ${LANG_TRADCHINESE} "無法新增 Snasphot Manager 到防火牆例外清單，請先前往檢查防火牆環境設定。"
; FIXME: SimpleFC may have Unicode problems
LangString desc_firewall_incoming_rule ${LANG_TRADCHINESE} "容許外部發給 Synology Snapshot Manager 的要求。"
LangString desc_firewall_outgoing_rule ${LANG_TRADCHINESE} "允許 Synology Snapshot Manager 發給外部的要求。"

; ===========================================================================
; Functions and Macros
; ===========================================================================

Function un.onInit
  SetRebootFlag true
  InitPluginsDir
  ${If} ${RunningX64}
    StrCpy $INSTDIR "$PROGRAMFILES64\${PRODUCT_NAME}"
  ${Else}
    StrCpy $INSTDIR "$PROGRAMFILES\${PRODUCT_NAME}"
  ${EndIf}
FunctionEnd

Function VersionCheck
  ; Check Windows version
  ; Call plugin dll function
  Version::GetWindowsVersion
  Pop $MajorVersion
  Pop $MinorVersion
  Pop $BuildNumber
  Pop $PlatformID
  Pop $CSDVersion
  Pop $ProductType

  ; http://msdn.microsoft.com/en-us/library/windows/desktop/ms724834(v=vs.85).aspx
  ; type is Workstation
  ${if} "1" == $ProductType
    MessageBox MB_OK $(not_supported_os)
    Abort
  ; type is Domain Controller
  ${ElseIf} "2" == $ProductType
    MessageBox MB_OK $(not_supported_os)
    Abort
  ; type is Server
  ${ElseIf} "3" == $ProductType
    ${if} 6 != $MajorVersion
      MessageBox MB_OK $(not_supported_os)
      Abort
    ${EndIf}
    ${if} "1" == $MinorVersion
      ; Windows Server 2008 R2
      goto CheckPluginVersion
    ${EndIf}
    ${if} "2" == $MinorVersion
      ; Windows Server 2012
      goto CheckPluginVersion
    ${EndIf}
    ${if} "3" == $MinorVersion
      ; Windows Server 2012 R2
      goto CheckPluginVersion
    ${EndIf}
    MessageBox MB_OK $(not_supported_os)
    Abort
  ; Unknown type
  ${Else}
    MessageBox MB_OK $(not_supported_os)
    Abort
  ${EndIf}

CheckPluginVersion:
  ; Check old plugin version (if there is one)
  ${If} ${RunningX64}
    SetRegView 64
  ${EndIf}
  ReadRegStr $R0 HKLM "${PRODUCT_UNINST_KEY}" "${PRODUCT_VERSION_REG}"
  ; Skip plugin version check if there is no previously installed version
  StrCmp $R0 "" End

  ${VersionCompare} $R0 ${PRODUCT_VERSION} $R1

  ${If} 0 == $R1
    MessageBox MB_OK $(already_up_to_date)
    Abort
  ${ElseIf} 1 == $R1
    MessageBox MB_OK $(installed_version_is_newer)
    Abort
  ${Else}
    MessageBox MB_OKCANCEL $(older_version_detected) \
    IDOK Upgrade
    Abort
  ${EndIf}

Upgrade:
  ClearErrors
  execWait '"$INSTDIR\uninst.exe" upgrade _?=$INSTDIR' ; Do not copy the uninstaller to a temp file
  IfErrors Uninstall_failed End

Uninstall_failed:
  MessageBox MB_OK $(failed_to_remove_older_version)
  Abort

End:
FunctionEnd

; ===========================================================================
; Installation Sections
; ===========================================================================

Section -
  SetOutPath $INSTDIR
  File "register_app.vbs"
  File "*.cmd"

  ; All the supported OSes are 64bit right now
  File "64bit\SynoVssTray.exe"
  File "64bit\SynoVssPlugin.exe"
  File "64bit\SynoCommEngine.exe"
  File "64bit\SynoVssProvider.dll"
  File "64bit\*.dll"
  SetOutPath "$INSTDIR\platforms"
  File "64bit\platforms\*.*"

SectionEnd

Section -CreateRegs
  ${If} ${RunningX64}
    SetRegView 64
  ${EndIf}
  WriteUninstaller "$INSTDIR\uninst.exe"
  WriteRegStr "${PRODUCT_ROOT_KEY}" "${PRODUCT_UNINST_KEY}" "DisplayName" "${PRODUCT_NAME}"
  WriteRegStr "${PRODUCT_ROOT_KEY}" "${PRODUCT_UNINST_KEY}" "DisplayIcon" "$INSTDIR\SynoVssTray.exe"
  WriteRegStr "${PRODUCT_ROOT_KEY}" "${PRODUCT_UNINST_KEY}" "Publisher" "${PRODUCT_PUBLISHER}"
  WriteRegDWORD "${PRODUCT_ROOT_KEY}" "${PRODUCT_UNINST_KEY}" "VersionMinor" 0
  WriteRegDWORD "${PRODUCT_ROOT_KEY}" "${PRODUCT_UNINST_KEY}" "VersionMajor" 1
  WriteRegStr "${PRODUCT_ROOT_KEY}" "${PRODUCT_UNINST_KEY}" "Version" "${PRODUCT_VERSION}"
  WriteRegStr "${PRODUCT_ROOT_KEY}" "${PRODUCT_UNINST_KEY}" "UninstallString" "$INSTDIR\uninst.exe"
  ; Roughly calculated from included files size (63 MB)
  WriteRegDWORD "${PRODUCT_ROOT_KEY}" "${PRODUCT_UNINST_KEY}" "EstimatedSize" 43000

  ; Plugin GUID is initialized as empty string, application would assign a new value in first execution
  WriteRegStr "${PRODUCT_ROOT_KEY}" "${PRODUCT_CONFIG_KEY}" "PluginId" ""
  ; TODO: assign log file path
  WriteRegStr "${PRODUCT_ROOT_KEY}" "${PRODUCT_CONFIG_KEY}" "LogFilePath" ""
  WriteRegStr "${PRODUCT_ROOT_KEY}" "${PRODUCT_CONFIG_KEY}" "ProtoVersion" "${PRODUCT_PROTOCOL_VERSION}"
  WriteRegStr "${PRODUCT_ROOT_KEY}" "${PRODUCT_CONFIG_KEY}" "PluginVersion" "${PRODUCT_DISPLAY_VERSION}"
SectionEnd

Section -AdditionalIcons
  SetShellVarContext all
  CreateDirectory "$SMPROGRAMS\${PLUGIN_SYNO_FOLDER}"
  CreateDirectory "$SMPROGRAMS\${PLUGIN_SYNO_FOLDER}\${PLUGIN_START_MENU}"
  CreateShortCut "$SMPROGRAMS\${PLUGIN_SYNO_FOLDER}\${PLUGIN_START_MENU}\$(desc_app_start).lnk" "$INSTDIR\${PLUGIN_PROCESS_NAME}"
  CreateShortCut "$SMPROGRAMS\${PLUGIN_SYNO_FOLDER}\${PLUGIN_START_MENU}\$(desc_uninstall).lnk" "$INSTDIR\uninst.exe"
SectionEnd

Section -SetFirewallRules
  SimpleFC::IsFirewallServiceRunning
  Pop $0 ; return error(1) / success(0)
  Pop $1 ; return 1 = IsRunning / 0 = Not Running
  ${If} 0 != $0
    MessageBox MB_OK $(firewall_add_warning)
    goto End
  ${ElseIf} 0 == $1 ; Skip firewall setting when the firewall service is not running
    MessageBox MB_OK "skip firewall setting becuz the service is not running"
    goto End
  ${EndIf}

  ; http://nsis.sourceforge.net/NSIS_Simple_Firewall_Plugin
  ; SimpleFC::AdvAddRule [name] [description] [protocol] [direction]
  ;  [status] [profile] [action] [application] [icmp_types_and_codes]
  ;  [group] [local_ports] [remote_ports] [local_address] [remote_address]

  SimpleFC::AdvExistsRule "${PRODUCT_NAME} (TCP-In)"
  Pop $0 ; return error(1) / success(0)
  Pop $1 ; return 1 = Exists / 0 = Does not exists
  ${If} 0 != $0
    MessageBox MB_OK $(firewall_add_warning)
    goto End
  ${ElseIf} 1 != $1
    SimpleFC::AdvAddRule "${PRODUCT_NAME} (TCP-In)" \
      $(desc_firewall_incoming_rule) "6" "1" "1" "7" "1" "$INSTDIR\${PLUGIN_ENGINE_NAME}" \
      "" "" "${PLUGIN_NETWORK_PROT}" "" "" ""
    Pop $0
    ${If} 0 != $0
      MessageBox MB_OK $(firewall_add_warning)
      goto End
    ${EndIf}
  ${EndIf}

  SimpleFC::AdvExistsRule "${PRODUCT_NAME} (TCP-Out)"
  Pop $0 ; return error(1)/success(0)
  Pop $1 ; return 1=Exists/0=Does not exists
  ${If} 0 != $0
     MessageBox MB_OK $(firewall_add_warning)
     goto End
  ${ElseIf} 1 != $1
    SimpleFC::AdvAddRule "${PRODUCT_NAME} (TCP-Out)" \
      $(desc_firewall_outgoing_rule) "6" "2" "1" "7" "1" "$INSTDIR\${PLUGIN_ENGINE_NAME}" \
      "" "" "" "" "" ""
    Pop $0
    ${If} 0 != $0
      MessageBox MB_OK $(firewall_add_warning)
      goto End
    ${EndIf}
  ${EndIf}

End:
SectionEnd

Section -StartService
  ; Optional: perform slient install?
  ; Install and register provider with VSS service
  execwait '"$INSTDIR\install-synovssprovider.cmd"'

  ; Install requester as a service
  execwait '"$INSTDIR\SynoCommEngine.exe" install'
  execwait '"$INSTDIR\SynoVssPlugin.exe" install'

  ; Start plugin service
  SimpleSC::StartService "${PLUGIN_SERVICE_NAME}" "" 45

  ; Set path to all user version for startup programs
  SetShellVarContext all
  CreateShortCut "$SMSTARTUP\${PRODUCT_NAME}.lnk" "$INSTDIR\SynoVssTray.exe"
  ; Start icon tray
  Exec "$INSTDIR\SynoVssTray.exe"
SectionEnd

Function .onInit
  InitPluginsDir
  !insertmacro MUI_LANGDLL_DISPLAY

  ${If} ${RunningX64}
    StrCpy $INSTDIR "$PROGRAMFILES64\${PRODUCT_NAME}"
  ${Else}
    StrCpy $INSTDIR "$PROGRAMFILES\${PRODUCT_NAME}"
  ${EndIf}

  Call VersionCheck
FunctionEnd

; ===========================================================================
; Section descriptions
; ===========================================================================
; Add this part back if we want to include multiple components in the installer
;!insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
;!insertmacro MUI_FUNCTION_DESCRIPTION_END

; ===========================================================================
; Uninstallation Section
; ===========================================================================
Section "Uninstall"
  ${If} ${RunningX64}
    SetRegView 64
  ${EndIf}

  ; Check if this is upgrade mode (called by another installer)
  ${GetParameters} $R0
  ${If} "upgrade" == $R0
    StrCpy $IsUpgradeMode "true"
  ${EndIf}

  ; Check if icon tray still running
  ${nsProcess::FindProcess} "${PLUGIN_PROCESS_NAME}" $R0
  ${If} 0 == $R0
    ${nsProcess::CloseProcess} "${PLUGIN_PROCESS_NAME}" $R0
    ${If} 0 != $R0
      MessageBox MB_OK $(app_still_running)
      Abort
    ${EndIf}
  ${EndIf}

  ; Check if service exists
  SimpleSC::ServiceIsRunning "${PLUGIN_SERVICE_NAME}"
  Pop $0
  ${If} 0 != $0
    goto PluginServiceNotExisting
  ${EndIf}
  Pop $1
  ${If} 1 != $1
    goto PluginServiceNotRunning
  ${EndIf}

  ; Stop plugin service
  SimpleSC::StopService "${PLUGIN_SERVICE_NAME}" 1 45
  Pop $0
  ${If} 0 != $0
    MessageBox MB_OK $(failed_to_stop_service)
    Abort
  ${EndIf}

PluginServiceNotRunning:
  ; Delete plugin service
  SimpleSC::RemoveService "${PLUGIN_SERVICE_NAME}"

PluginServiceNotExisting:
  ; TODO: should remove redundant codes, but there is an issue with function naming rules in uninstall section

  ; Check if communication engine service exists
  SimpleSC::ServiceIsRunning "${PLUGIN_COMMENGINE_NAME}"
  Pop $0
  ${If} 0 != $0
    goto CommEngineNotExisting
  ${EndIf}
  Pop $1
  ${If} 1 != $1
    goto CommEngineNotRunning
  ${EndIf}

  ; Stop communication engine
  SimpleSC::StopService "${PLUGIN_COMMENGINE_NAME}" 1 45
  Pop $0
  ${If} 0 != $0
    MessageBox MB_OK $(failed_to_stop_service)
    Abort
  ${EndIf}

CommEngineNotRunning:
  ; Delete plugin service
  SimpleSC::RemoveService "${PLUGIN_COMMENGINE_NAME}"

CommEngineNotExisting:

  ; Unregister provider
  execwait '"$INSTDIR\uninstall-synovssprovider.cmd"'

  ; Remove everything in $INSTDIR
  RMDir /r "$INSTDIR"
  SetShellVarContext all
  RMDir /r "$SMPROGRAMS\${PLUGIN_SYNO_FOLDER}\${PLUGIN_START_MENU}"
  ; Only remove Synology folder when it is empty
  RMDir "$SMPROGRAMS\${PLUGIN_SYNO_FOLDER}"
  Delete "$SMSTARTUP\${PRODUCT_NAME}.lnk"

  ${If} "true" != $IsUpgradeMode
    SimpleFC::AdvRemoveRule "${PRODUCT_NAME} (TCP-In)"
    SimpleFC::AdvRemoveRule "${PRODUCT_NAME} (TCP-Out)"
	DeleteRegKey "${PRODUCT_ROOT_KEY}" "${PRODUCT_CONFIG_KEY}"

	; Remove Synology config registry key if there is no subkey left in it
	StrCpy $0 0
	EnumRegKey $1 HKLM Software $0
	StrCmp $1 "" Done
    DeleteRegKey "${PRODUCT_ROOT_KEY}" "${PRODUCT_SYNO_KEY}"
Done:
  ${EndIf}
  DeleteRegKey "${PRODUCT_ROOT_KEY}" "${PRODUCT_UNINST_KEY}"
SectionEnd
