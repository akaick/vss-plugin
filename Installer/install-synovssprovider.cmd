@rem @echo off
@setlocal


@rem the DLLs to be installed must be either in the current directory as this script or
@rem in the SDK BIN directory.
@set FOUND_FILES=0
@set CMD_DIR=%~dp0


:installfromCurrentDir
@set DLL_DIR=%CMD_DIR%
@set VBS_DIR=%CMD_DIR%
@call :checkfiles
@if %FOUND_FILES% EQU 0 (goto :installfromSDKDirs) else (goto :goodproc)


:installfromSDKDirs
@set DLL_DIR=%CMD_DIR%\..\..\..\..\BIN
@set VBS_DIR=%CMD_DIR%
@call :checkfiles
@if %FOUND_FILES% EQU 0 (goto :missingfiles) else (goto :goodproc)



:goodproc

rem Remove existing installation
call "%CMD_DIR%\uninstall-synovssprovider.cmd"

@rem Get the complete %DLL_DIR% and %VBS_DIR%
@pushd %DLL_DIR%
@set DLL_DIR=%CD%
@popd
@pushd %VBS_DIR%
@set VBS_DIR=%CD%
@popd

rem Register VSS hardware provider
cscript "%VBS_DIR%\register_app.vbs" -register "SynoVssProvider" "%DLL_DIR%\SynoVssProvider.dll" "Syno VSS HW Provider"

set EVENT_LOG=HKLM\SYSTEM\CurrentControlSet\Services\Eventlog\Application\SynoVssProvider
reg.exe add %EVENT_LOG% /f
reg.exe add %EVENT_LOG% /f /v CustomSource /t REG_DWORD /d 1
reg.exe add %EVENT_LOG% /f /v EventMessageFile /t REG_EXPAND_SZ /d "%DLL_DIR%\SynoVssProvider.dll"
reg.exe add %EVENT_LOG% /f /v TypesSupported /t REG_DWORD /d 7

echo.
goto :EOF



:checkfiles

@if not exist "%DLL_DIR%\SynoVssProvider.dll" goto :EOF
@if not exist "%VBS_DIR%\register_app.vbs"      goto :EOF
@set FOUND_FILES=1
@goto :EOF


:missingfiles

@echo.
@echo One or more important files are missing. All the files listed below either need
@echo to be copied to a single directory (e.g. C:\SynoVssProvider), or stays in
@echo their original directories in the SDK BIN and SAMPLES.
@echo.
@echo   SynoVssProvider.dll
@echo   register_app.vbs
@echo   install-synovssprovider.cmd
@echo   uninstall-synovssprovider.cmd
@echo.
@goto :EOF
