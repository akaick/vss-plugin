
#pragma once

#include <synocomm_base.h>
#include <synocomm.h>
#include <SynoUtils.h>
#include <ServiceBase.h>


namespace Syno {

class SynoCommEngineService : public CServiceBase
{
public:
    SynoCommEngineService(PWSTR pszServiceName, 
        BOOL fCanStop = TRUE, 
        BOOL fCanShutdown = TRUE, 
        BOOL fCanPauseContinue = FALSE);
    virtual ~SynoCommEngineService(void);

protected:
    virtual void OnStart(DWORD dwArgc, PWSTR *pszArgv);
    virtual void OnStop();

private:
    BOOL            m_fStopping;
    HANDLE          m_hStoppedEvent;

    P_SYNOENGCOMM   m_pEngComm;

private:
    void ServiceWorkerThread();
    void PipeThread();
    void SocketThread();

};

}