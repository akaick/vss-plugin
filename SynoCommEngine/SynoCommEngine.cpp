
#pragma region Includes
#include "stdafx.h"
#include <stdio.h>
#include <ServiceInstaller.h>
#include "SynoCommEngineService.h"
#pragma endregion


// Internal name of the service
#define SERVICE_NAME             L"SynoCommEngine"

// Displayed name of the service
#define SERVICE_DISPLAY_NAME     L"Synology Comm Engine Service"

// Service start options.
#define SERVICE_START_TYPE       SERVICE_AUTO_START

// List of service dependencies - "dep1\0dep2\0\0"
#define SERVICE_DEPENDENCIES     L""

// The name of the account under which the service should run
#define SERVICE_ACCOUNT          NULL

// The password to the service account name
#define SERVICE_PASSWORD         NULL


int wmain(int argc, wchar_t *argv[])
{
    if (argc > 1) {
        if (_wcsicmp(L"install", argv[1]) == 0) {
            // Install the service when the command is 
            // "-install" or "/install".
            InstallService(
                SERVICE_NAME,               // Name of service
                SERVICE_DISPLAY_NAME,       // Name to display
                SERVICE_START_TYPE,         // Service start type
                SERVICE_DEPENDENCIES,       // Dependencies
                SERVICE_ACCOUNT,            // Service running account
                SERVICE_PASSWORD            // Password of the account
                );
        } else if (_wcsicmp(L"uninstall", argv[1]) == 0) {
            // Uninstall the service when the command is 
            // "-remove" or "/remove".
            UninstallService(SERVICE_NAME);
        }
    } else {
        wprintf(L"Copyright (c) 2003-2014 Synology Inc. All rights reserved.\n");
        wprintf(L"Parameters:\n");
        wprintf(L" install     to install the service.\n");
        wprintf(L" uninstall   to uninstall the service.\n");

        Syno::SynoCommEngineService service(SERVICE_NAME);
        if (!CServiceBase::Run(service)) {
            fwprintf(stderr, L"Service failed to run w/err 0x%08lx\n", GetLastError());
        }
    }

    return 0;
}
