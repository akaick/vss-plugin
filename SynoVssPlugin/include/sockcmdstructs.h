#pragma once

#define DEFAULT_BUFLEN          512
#define DEFAULT_PORT            "27030"
#define CONNECT_TIME_OUT        5
#define RECEIVE_TIME_OUT        200
#define MAX_SN_LEN              16      // In case DSM SN may be extended in the future
#define MAX_IP_LEN              255
#define MAX_USERNAME_LEN        64
#define MAX_PWD_LEN             128
#define OS_TYPE                 "win_server"

// TODO: remove when not used
#define RESP_CONNECTED          "connected"
#define RESP_SUCCEEDED          "succeeded"

// TODO: codes refine
enum SOCKET_CMD_ERR {
    SOCKET_CMD_ERR_UNKNOWN = -1,
    SOCKET_CMD_SUCCESS = 0,
    SOCKET_CMD_INVALID_USER,
    SOCKET_CMD_WRONG_PWD,
    SOCKET_CMD_DSM_NO_RESPONSE,
    SOCKET_CMD_DSM_OUTDATED,        // DSM version outdated
    SOCKET_CMD_PLUGIN_OUTDATED,     // Plugin version outdated
    SOCKET_CMD_INVALID_CMD
};

enum SOCKET_RESP_RESULT {
    SOCKET_RESULT_UNKNOWN,
    SOCKET_RESULT_DSM_IS_CONNECTED,
    SOCKET_RESULT_DSM_IS_DISCONNECTED,
    SOCKET_RESULT_DSM_NOT_IN_LIST
};

enum SOCKET_CMD_TYPE {
    CMD_NULL = 0,
    CMD_DSM_IS_CONNECTED,
    CMD_DSM_REGISTER,
    CMD_DSM_REMOVE
};

// Can take either SN or IP address as input
typedef struct _DATA_IS_DSM_CONNECTED {
    char dsmSn[MAX_SN_LEN];
    char dsmIp[MAX_IP_LEN];
} DATA_IS_DSM_CONNECTED, *PDATA_IS_DSM_CONNECTED;

typedef struct _DATA_REGISTER_DSM {
    char ip[MAX_IP_LEN];
    char user[MAX_USERNAME_LEN];
    char password[MAX_PWD_LEN];
} DATA_REGISTER_DSM, *PDATA_REGISTER_DSM;

typedef struct _DATA_REMOVE_DSM {
    char dsmSn[MAX_SN_LEN];
} DATA_REMOVE_DSM, *PDATA_REMOVE_DSM;

typedef struct _SOCKET_CMD {
    // Header
    DWORD opCode;
    DWORD dataSize;

    // Data
    char data[1];
} SOCKET_CMD, *PSOCKET_CMD;

// Only 1 type of response at this moment, could be extended in the future
typedef struct _DATA_SOCKET_RESP {
    DWORD errCode;
    DWORD resultCode;
} DATA_SOCKET_RESP, *PDATA_SOCKET_RESP;

typedef struct _SOCKET_RESP {
    // Header
    DWORD opCode;
    DWORD dataSize;

    // Data
    char data[1];
} SOCKET_RESP, *PSOCKET_RESP;