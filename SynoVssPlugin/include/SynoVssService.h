#pragma once

#include "stdafx.h"
#include <atomic>
#include <concurrent_queue.h>

#include <synocomm_base.h>
#include <synocomm.h>
#include <SynoUtils.h>
#include <SynoVssInfo.h>
#include <ServiceBase.h>
#include "taskhandler.h"

namespace Syno {
#define NETWORKER_FREQUENCY 2
#define CHECK_STATUS_FREQUENCY 60
#define DL_MAX_ACCESSING_THREADS 4      //maximum threads allowed to access dsinfo lists
#define EVENT_WAITTIME  2000            //in milisec 

class SynoVssService : public CServiceBase
{
public:
    SynoVssService(PWSTR pszServiceName,
        BOOL fCanStop = TRUE,
        BOOL fCanShutdown = TRUE,
        BOOL fCanPauseContinue = FALSE);
    virtual ~SynoVssService(void);

protected:
    virtual void OnStart(DWORD dwArgc, PWSTR *pszArgv);
    virtual void OnStop();

private:
    enum {
        STOPPED_EVENT_MAIN = 0,
        STOPPED_EVENT_TRAY,
        STOPPED_EVENT_REQUESTER,
        STOPPED_EVENT_NETWORK_SEND,
        STOPPED_EVENT_NETWORK_RECEIVE,
        STOPPED_EVENT_MAX,
    };

    enum {
        CHANNEL_REMOTE_RDY = 0,
        CHANNEL_LOCAL_RDY,
        CONNECTION_REMOTE_RDY,
        CONNECTION_LOCAL_RDY,
        CONNECTION_EVENT_MAX
    };

    P_SYNOAPPCOMM   m_netWorkerChannel;
    int             m_netWorkerHandle;

    BOOL            m_fStopping;
    BOOL            m_isChannelRdy;
    HANDLE          m_hStoppedEvents[STOPPED_EVENT_MAX];
    HANDLE          m_hConnectionEvents[CONNECTION_EVENT_MAX];

    HANDLE          m_TaskEvent;
    VssTaskHandler  m_TaskHandler;
    P_SYNOENGCOMM   m_pEngComm;
    std::mutex      m_mReader;
    std::mutex      m_mWriter;
    uint32_t        m_nReader;

    uint32_t        m_commandSn;
    std::wstring    m_pluginId;
    std::wstring    m_pluginVer;
    std::wstring    m_protoVer;
    std::wstring    m_osVer;
    std::string     m_hostname;

    std::vector<std::string>    m_addrList;
    Syno::StringDsInfoList      m_stringDsInfoList;
    Syno::CommandSnList         m_commandSnList;
    Syno::SnDsInfoList          m_SnDsInfoList;

    // TrayEventWork -> MainWorker
    concurrency::concurrent_queue<Json::Value> m_TrayEventQueue;
    // MainWorker -> NetworSendkWorker
    concurrency::concurrent_queue<PVssCommand> m_NetworkSendQueue;
    // NetworkReceiveWorker -> MainWorker
    concurrency::concurrent_queue<PVssCommand> m_NetworkReceiveQueue;
    // MainWorker -> RequesterWorker
    concurrency::concurrent_queue<PVssCommand> m_RequestEventQueue;

private:
    void MainWorker();
    void TrayEventWorker();
    void RequesterWorker();
    void NetworkSendWorker();
    void NetworkReceiveWorker();

    //DsInfoLists access controls
    void AcquireReadListLock();
    void ReleaseReadListLock();
    void AcquireWriteListLock();
    void ReleaseWriteListLock();

    void LoadInfo();
    void SetupComm();
    void HandleReconnect(PDsInfo &dsInfo);
    void HandleTrayEvents(const Json::Value& task);
    void HandleRecvNetworkEvents(const PVssCommand& pCmd);
    void SenderRedirectCommands();
    void SenderCheckStatus();
    void SenderHandleReconnect(PDsInfo &dsInfo);
    void SenderHandleTimeout(PDsInfo &dsInfo);
    void ReceiverHandleRequest(Json::Value &json);
    void ReceiverHandleResponse(Json::Value &json);
    void ReceiverCheckStatus();
    void ReceiverHandleReconnect(PDsInfo &dsInfo);
    void KillDsConnection(std::wstring dsmsn);
    void ReconnectDs(std::wstring dsmsn);
};
}
