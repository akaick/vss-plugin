
#pragma once

// Winsock headers
#include <winsock2.h>
#include <ws2tcpip.h>

// Define socket status for IPC
enum SOCKET_STATUS {
    NOT_INITED = 0,
    WSADATA_INITED,
    SOCKET_INITED,
    RECEIVING_DATA,
    DATA_RECEIVED
};

extern bool SetupSocket(SOCKET& listenSocket, SOCKET_STATUS& status);
extern int ReceiveData(SOCKET& listenSocket, SOCKET& clientSocket, SOCKET_STATUS& status, char* cmdBuf, const size_t bufSize, const timeval& timeout);
extern bool SendData(SOCKET& clientSocket, const char* data, const size_t dataSize);
