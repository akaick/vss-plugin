// prevent conflicts between winsock.h and winsock2.h
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#pragma once

#include <windows.h>
#include <tchar.h>
#include <strsafe.h>
#include <atlbase.h>

 // A message file must end with a period on its own line
 // followed by a blank line.