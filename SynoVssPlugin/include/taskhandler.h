
#pragma once

#include <SynoVssCommand.h>
#include "sockcmdstructs.h"

namespace Syno {

typedef
enum _SYNO_SS_PLUGIN_STATE {
    SYNO_SS_UNKNOWN = 0,
    SYNO_SS_READY,
    SYNO_SS_INITIAL,
    SYNO_SS_PREPARING,
    SYNO_SS_PREPARED,
    SYNO_SS_TAKE_SNAPSHOT,
    SYNO_SS_POST_SNAPSHOT,
    SYNO_SS_ABORTED,
    SYNO_SS_STATE_COUNT
} SYNO_SS_PLUGIN_STATE;

class VssTaskHandler
{
public:
    // Constructor
    VssTaskHandler();

    // Destructor
    ~VssTaskHandler();

    void Initialize();
    void UpdateState(SYNO_SS_PLUGIN_STATE state);
    void AbortTask();

    // TODO: Change return type
    HRESULT ParseCmds(const Syno::PVssCommand& cmd);

private:
    SYNO_SS_PLUGIN_STATE m_state;
};

}