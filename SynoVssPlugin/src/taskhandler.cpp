
#include "stdafx.h"
#include "taskhandler.h"
#include <SynoRequester.h>

namespace Syno {

// Constructor
VssTaskHandler::VssTaskHandler()
{
    m_state = SYNO_SS_UNKNOWN;
}

// Destructor
VssTaskHandler::~VssTaskHandler()
{

}

void VssTaskHandler::Initialize()
{
    m_state = SYNO_SS_READY;
}

void VssTaskHandler::AbortTask()
{

}

HRESULT VssTaskHandler::ParseCmds(const Syno::PVssCommand& cmd)
{
    FunctionTracer ft(DBG_INFO);
    HRESULT hr = -1;
    Syno::VssRequester vssRequester;

    string cmdName = cmd->GetCommandName();

    if (SZ_CMD_INIT_SNAPSHOT == cmdName) {
        wstring lunSerial;

        if (SYNO_SS_READY != m_state) {
            ft.WriteLine(SYNOVSS_LOG_ERR, L"Operation denied, wrong states sequence");
            goto END;
        }

        // Update plugin state
        UpdateState(SYNO_SS_INITIAL);

        lunSerial = String2WString(cmd->GetData(SZ_SOURCE_LUN_P80).asString());
        vssRequester.CreateLunSnapshot(lunSerial);

        // Reset plugin state
        UpdateState(SYNO_SS_READY);

        hr = S_OK;
    } else if ("import_ss" == cmdName) {
        // TODO: not yet implemented
        // Reading the backup components document
        /*wstring xmlDoc = ReadFileContents(L"C:\\rec");
        wstring strSnapshotId = argList[1];
        VSS_ID snapshotId = WString2Guid(strSnapshotId);

        // Initialize the VSS client
        vssRequester.Initialize(VSS_CTX_ALL, xmlDoc);

        vssRequester.ImportSnapshotSet(snapshotId);*/

        hr = S_OK;
    } else if ("break_ss" == cmdName) {

        hr = S_OK;
    } else if ("resync_lun" == cmdName) {
        /*wstring xmlDoc = ReadFileContents(L"C:\\rec");
        wstring snapshotId = argList[1];

        vssRequester.Initialize(VSS_CTX_ALL, xmlDoc, true);
        // TODO: integrate resync with import and restore flows
        vssRequester.DoResync(WString2Guid(snapshotId));
        */
        hr = S_OK;
    } else if (SZ_CMD_ABORT_TASK == cmdName) {
        if (SYNO_SS_TAKE_SNAPSHOT <= m_state) {
            ft.WriteLine(SYNOVSS_LOG_ERR, L"Can not abort the task right now");
            goto END;
        }

        AbortTask();
        UpdateState(SYNO_SS_READY);

        hr = S_OK;
    } else if ("query_state" == cmdName) {
        ft.WriteLine(SYNOVSS_LOG_NORMAL, L"Current plugin state: %d", m_state);

        hr = S_OK;
    } else if (1) {
        hr = S_OK;
    }

END:

    return hr;
}

void VssTaskHandler::UpdateState(SYNO_SS_PLUGIN_STATE state)
{
    m_state = state;
}

}