
#include "stdafx.h"
#include <util.h>
#include <tracing.h>
#include "trayhandler.h"
#include "sockcmdstructs.h"


//// Socket thread for SynoVssTray communication

bool SetupSocket(SOCKET& listenSocket, SOCKET_STATUS& status)
{
    FunctionTracer ft(DBG_INFO);
    bool ret = false;
    int iResult;
    int on = 1;
    struct addrinfo *result = NULL, hints;

    ZeroMemory(&hints, sizeof (hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;
    hints.ai_flags = AI_PASSIVE;

    // Resolve the local address and port to be used by the server
    // TODO: check port (see if any other applications using it)
    // TODO: check firewall
    if (0 != (iResult = getaddrinfo(NULL, DEFAULT_PORT, &hints, &result))) {
        ft.WriteLine(SYNOVSS_LOG_ERR, L"getaddrinfo failed: %d", iResult);
        goto END;
    }

    // Create a SOCKET for the server to listen for client connections
    if (INVALID_SOCKET == (listenSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol))) {
        ft.WriteLine(SYNOVSS_LOG_ERR, L"Error at socket(): %d", WSAGetLastError());
        goto END;
    }

    // Update status, closesocket required after this stage
    status = SOCKET_INITED;

    // Setup the TCP listening socket
    if (SOCKET_ERROR == (iResult = bind(listenSocket, result->ai_addr, (int)result->ai_addrlen))) {
        ft.WriteLine(SYNOVSS_LOG_ERR, L"bind failed with error: %d", WSAGetLastError());
        goto END;
    }

    if (SOCKET_ERROR == listen(listenSocket, SOMAXCONN)) {
        ft.WriteLine(SYNOVSS_LOG_ERR, L"Listen failed with error: %ld", WSAGetLastError());
        goto END;
    }

    // Set the socket operation to be non-blocking
    if (SOCKET_ERROR == ioctlsocket(listenSocket, FIONBIO, (u_long*)&on)) {
        ft.WriteLine(SYNOVSS_LOG_ERR, L"ioctlsocket() failed");
        goto END;
    }

    ret = true;

END:
    if (result) {
        freeaddrinfo(result);
    }
    return ret;
}

int ReceiveData(SOCKET& listenSocket, SOCKET& clientSocket, SOCKET_STATUS& status, char* cmdBuf, const size_t bufSize, const timeval& timeout)
{
    FunctionTracer ft(DBG_INFO);
    int ret = -1;
    int iResult;
    char recvbuf[DEFAULT_BUFLEN];
    FD_SET ReadSet;

    FD_ZERO(&ReadSet);
    FD_SET(listenSocket, &ReadSet);

    if (SOCKET_ERROR == (iResult = select(1, &ReadSet, NULL, NULL, &timeout))) {
        ft.WriteLine(SYNOVSS_LOG_ERR, L"Select() failed: %d", WSAGetLastError());
        goto END;
    }

    if (0 == iResult) {
        // No packet arrives
        ret = 0;
        goto END;
    }

    // Accept a client socket
    if (INVALID_SOCKET == (clientSocket = accept(listenSocket, NULL, NULL))) {
        ft.WriteLine(SYNOVSS_LOG_ERR, L"Accept() failed: %d", WSAGetLastError());
        goto END;
    }
    status = RECEIVING_DATA;

    // Receive data until the peer shuts down the connection
    do {
        iResult = recv(clientSocket, recvbuf, DEFAULT_BUFLEN, 0);
        if (iResult > 0) {
            // Appending received data into command buffer
            // TODO: handle bufSize v.s. DEFAULT_BUFLEN
            memcpy(cmdBuf, &recvbuf, bufSize);
        } else if (iResult == 0) {
                ft.WriteLine(SYNOVSS_LOG_NORMAL, L"Connection closing...");
        } else {
            ft.WriteLine(SYNOVSS_LOG_WARN, L"Recv failed but continue(no data): %d", WSAGetLastError());
        }
    } while (iResult > 0);

    // There is received data
    ret = 1;

END:
    return ret;
}

bool SendData(SOCKET& clientSocket, const char* data, const size_t dataSize)
{
    FunctionTracer ft(DBG_INFO);
    bool ret = false;

    int iSendResult = send(clientSocket, data, (int)dataSize, 0);
    if (iSendResult == SOCKET_ERROR) {
        ft.WriteLine(SYNOVSS_LOG_ERR, L"Send data failed: %d", WSAGetLastError());
        goto END;
    }

    ret = true;

END:
    return ret;
}