#pragma region Includes
#include <winsock2.h>

#include <ThreadPool.h>
#include <time.h>
#include "SynoVssService.h"
#include "taskhandler.h"
#include "trayhandler.h"
#pragma endregion

namespace Syno {
SynoVssService::SynoVssService(
    PWSTR pszServiceName,
    BOOL fCanStop,
    BOOL fCanShutdown,
    BOOL fCanPauseContinue)
    : CServiceBase(pszServiceName, fCanStop, fCanShutdown, fCanPauseContinue)
{
    m_fStopping = FALSE;

    // Create a manual-reset event that is not signaled at first to indicate
    // the stopped signal of the service.
    for (size_t i = 0; i < STOPPED_EVENT_MAX; ++i) {
        m_hStoppedEvents[i] = CreateEvent(
            NULL,   // default security attributes
            TRUE,   // manual reset event
            FALSE,  // not signaled
            NULL);  // no name
        if (m_hStoppedEvents[i] == NULL) {
            throw GetLastError();
        }
    }

    for (size_t i = 0; i < CONNECTION_EVENT_MAX; ++i) {
        m_hConnectionEvents[i] = CreateEvent(
            NULL,   // default security attributes
            TRUE,   // manual reset event
            FALSE,  // not signaled
            NULL);  // no name
        if (m_hConnectionEvents[i] == NULL) {
            throw GetLastError();
        }
    }

    m_TaskEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
    if (m_TaskEvent == NULL) {
        throw GetLastError();
    }

    m_nReader = 0;
}

SynoVssService::~SynoVssService(void)
{
    for (size_t i = 0; i < STOPPED_EVENT_MAX; ++i) {
        if (m_hStoppedEvents[i]) {
            CloseHandle(m_hStoppedEvents[i]);
            m_hStoppedEvents[i] = NULL;
        }
    }

    for (size_t i = 0; i < CONNECTION_EVENT_MAX; ++i) {
        if (m_hConnectionEvents[i]) {
            CloseHandle(m_hConnectionEvents[i]);
            m_hConnectionEvents[i] = NULL;
        }
    }

    if (m_TaskEvent) {
        CloseHandle(m_TaskEvent);
        m_TaskEvent = NULL;
    }

    if (m_pEngComm) {
        // TODO: destory m_pEngComm
    }

    VssComm::DestroyChannel();
}

//
//   FUNCTION: SynoVssService::OnStart(DWORD, LPWSTR *)
//
//   PURPOSE: The function is executed when a Start command is sent to the
//   service by the SCM or when the operating system starts (for a service
//   that starts automatically). It specifies actions to take when the
//   service starts. In this code sample, OnStart logs a service-start
//   message to the Application log, and queues the main service function for
//   execution in a thread pool worker thread.
//
//   PARAMETERS:
//   * dwArgc   - number of command line arguments
//   * lpszArgv - array of command line arguments
//
//   NOTE: A service application is designed to be long running. Therefore,
//   it usually polls or monitors something in the system. The monitoring is
//   set up in the OnStart method. However, OnStart does not actually do the
//   monitoring. The OnStart method must return to the operating system after
//   the service's operation has begun. It must not loop forever or block. To
//   set up a simple monitoring mechanism, one general solution is to create
//   a timer in OnStart. The timer would then raise events in your code
//   periodically, at which time your service could do its monitoring. The
//   other solution is to spawn a new thread to perform the main service
//   functions, which is demonstrated in this code sample.
//
void SynoVssService::OnStart(DWORD dwArgc, LPWSTR *lpszArgv)
{
    // Log a service start message to the Application log.
    WriteEventLogEntry(Syno::FormatWString(L"%s service in OnStart", GetName()).c_str(), EVENTLOG_INFORMATION_TYPE);

    try {
        // Assign an initial value of command SN
        m_isChannelRdy = FALSE;
        m_commandSn = 1;
        // Check: removed setup comm
        LoadInfo();
    }
    catch(std::system_error error) {
        WriteErrorLogEntry(StringToWString(error.what()).c_str(), error.code().value());
    }

    // Queue the main service functions for execution in worker threads.
    CThreadPool::QueueUserWorkItem(&SynoVssService::MainWorker, this);
    CThreadPool::QueueUserWorkItem(&SynoVssService::TrayEventWorker, this);
    CThreadPool::QueueUserWorkItem(&SynoVssService::RequesterWorker, this);
    CThreadPool::QueueUserWorkItem(&SynoVssService::NetworkSendWorker, this);
    CThreadPool::QueueUserWorkItem(&SynoVssService::NetworkReceiveWorker, this);
}

//
//   FUNCTION: SynoVssService::OnStop(void)
//
//   PURPOSE: The function is executed when a Stop command is sent to the
//   service by SCM. It specifies actions to take when a service stops
//   running. In this code sample, OnStop logs a service-stop message to the
//   Application log, and waits for the finish of the main service function.
//
//   COMMENTS:
//   Be sure to periodically call ReportServiceStatus() with
//   SERVICE_STOP_PENDING if the procedure is going to take long time.
//
void SynoVssService::OnStop()
{
    // Log a service stop message to the Application log.
    WriteEventLogEntry(Syno::FormatWString(L"%s service in OnStop", GetName()).c_str(), EVENTLOG_INFORMATION_TYPE);

    // Indicate that the service is stopping and wait for the finish of the
    // main service function (ServiceWorkerThread).
    m_fStopping = TRUE;
    if (WaitForMultipleObjects(STOPPED_EVENT_MAX, m_hStoppedEvents, TRUE, INFINITE) != WAIT_OBJECT_0) {
        throw GetLastError();
    }
}

void SynoVssService::AcquireReadListLock()
{
    m_mReader.lock();
    if (1 == ++m_nReader) {
        m_mWriter.lock();
    }
    m_mReader.unlock();
}

void SynoVssService::ReleaseReadListLock()
{
    m_mReader.lock();
    if (0 == --m_nReader) {
        m_mWriter.unlock();
    }
    m_mReader.unlock();
}

void SynoVssService::AcquireWriteListLock()
{
    m_mWriter.lock();
}

void SynoVssService::ReleaseWriteListLock()
{
    m_mWriter.unlock();
}

#pragma region Work threads
void SynoVssService::MainWorker()
{
    int sleepInMiliSec;
    time_t  start,
            sleepClocks;

    while (WAIT_OBJECT_0 != WaitForSingleObject(m_hConnectionEvents[CONNECTION_REMOTE_RDY], EVENT_WAITTIME)) {
        if (m_fStopping) {
            goto WORKERSTOP;
        }
    }

    // TODO: workaround
    //::Sleep(2000);

    // Connect all DSMs stored in DS info list
    for (StringDsInfoList::iterator it = m_stringDsInfoList.begin(); it != m_stringDsInfoList.end(); ++it) {
        PDsInfo pDsInfo = it->second;

        // TODO: create a function for these codes
        std::wstring dsmSn = pDsInfo->GetSN();

        std::wstring dsmIp = pDsInfo->GetAddress();
        std::wstring account = pDsInfo->GetAccount();
        std::wstring key = pDsInfo->GetKey();

        Syno::ConnectReq* pCommand = new ConnectReq(m_commandSn, m_pluginId, key);

        m_commandSnList[m_commandSn] = SZ_CMD_CONNECT;
        m_commandSn++;

        pCommand->SetAccount(WStringToString(account));
        pCommand->SetOsType(OS_TYPE);
        // TODO: replace hard codes
        pCommand->SetOsVersion(WStringToString(m_osVer));
        pCommand->SetProtocolVersion(WStringToString(m_protoVer));
        pCommand->SetDsmSn(WStringToString(dsmSn));

        if (m_addrList.empty()) {
            throw make_system_error("Failed to get network IP address");
        }
        pCommand->AddAddress(m_addrList[0]);

        m_NetworkSendQueue.push(PVssCommand(pCommand));
    }

    while (!m_fStopping) {
        Json::Value sendTask;
        start = clock();

        if (m_TrayEventQueue.try_pop(sendTask)) {
            HandleTrayEvents(sendTask);
        }

        Syno::PVssCommand receivedTask;

        if (m_NetworkReceiveQueue.try_pop(receivedTask)) {
            HandleRecvNetworkEvents(receivedTask);
        }

        AcquireReadListLock();
        for (auto it : m_stringDsInfoList ) {
            HandleReconnect(it.second);
        }
        ReleaseReadListLock();

        // sleep till next tick
        sleepClocks = (int)(NETWORKER_FREQUENCY * CLOCKS_PER_SEC) - (clock()-start);
        sleepInMiliSec = (int)(((float)sleepClocks/CLOCKS_PER_SEC) * 1000);
        Sleep(sleepInMiliSec < 0 ? 0 : (sleepInMiliSec > NETWORKER_FREQUENCY*1000 ? NETWORKER_FREQUENCY*1000 : sleepInMiliSec));
    }

WORKERSTOP:
    SetEvent(m_hStoppedEvents[STOPPED_EVENT_MAIN]);
}

//
// Handle event from SynoVssTray
// Copied from synosnap.cpp::SocketThread()
//
void SynoVssService::TrayEventWorker()
{
    WSADATA wsaData;
    SOCKET listenSocket = INVALID_SOCKET;
    SOCKET clientSocket = INVALID_SOCKET;
    struct timeval timeout;
    // TODO: check default error
    int iResult;
    SOCKET_STATUS status = NOT_INITED;
    char cmdBuf[DEFAULT_BUFLEN] = {0};

    // Initialize Winsock
    if (0 != (iResult = WSAStartup(MAKEWORD(2, 2), &wsaData))) {
        WriteErrorLogEntry(L"WSAtartup in TrayEventWorker", iResult);
        goto END;
    }

    status = WSADATA_INITED;

    // Stage: setup socket
    if (!SetupSocket(listenSocket, status)) {
        goto END;
    }

    timeout.tv_sec = 2;
    timeout.tv_usec = 0;

    while (!m_fStopping) {
        memset(cmdBuf, 0, DEFAULT_BUFLEN);
        iResult = ReceiveData(listenSocket, clientSocket, status, cmdBuf, DEFAULT_BUFLEN, timeout);
        if (0 == iResult) {
            continue;
        } else if (0 > iResult) {
            // Error codition
            goto END;
        }

        PSOCKET_CMD cmd = (PSOCKET_CMD)cmdBuf;

        DATA_SOCKET_RESP respData;
        char respBuf[DEFAULT_BUFLEN] = {0};
        size_t respSize = sizeof(DWORD) * 2 + sizeof(DATA_SOCKET_RESP);
        PSOCKET_RESP resp = (PSOCKET_RESP)malloc(respSize);

        respData.errCode = SOCKET_CMD_ERR_UNKNOWN;
        respData.resultCode = SOCKET_RESULT_UNKNOWN;

        // All commands will be put into task queue except CMD_DSM_IS_CONNECTED
        switch (cmd->opCode) {
        case CMD_DSM_IS_CONNECTED:
        {
            PDsInfo pDsInfo;
            PDATA_IS_DSM_CONNECTED cmdData = (PDATA_IS_DSM_CONNECTED)cmd->data;
            std::string dsmSn = cmdData->dsmSn;
            std::string dsmIp = cmdData->dsmIp;
            std::wstring wDsmSn(dsmSn.begin(), dsmSn.end());
            std::wstring wDsmIp(dsmIp.begin(), dsmIp.end());

            // Input search key could be either DSM Sn or IP
            if (!wDsmSn.empty()) {
                Syno::StringDsInfoList::iterator it;
                if (m_stringDsInfoList.end() != (it = m_stringDsInfoList.find(wDsmSn))) {
                    respData.errCode = SOCKET_CMD_SUCCESS;
                    pDsInfo = it->second;
                    respData.resultCode = pDsInfo->IsConnected() ? SOCKET_RESULT_DSM_IS_CONNECTED : SOCKET_RESULT_DSM_IS_DISCONNECTED;
                }
            } else {
                for (Syno::StringDsInfoList::iterator it = m_stringDsInfoList.begin(); it != m_stringDsInfoList.end(); it++) {
                    pDsInfo = it->second;
                    if (wDsmIp != pDsInfo->GetAddress()) {
                        continue;
                    }
                    respData.errCode = SOCKET_CMD_SUCCESS;
                    respData.resultCode = pDsInfo->IsConnected() ? SOCKET_RESULT_DSM_IS_CONNECTED : SOCKET_RESULT_DSM_IS_DISCONNECTED;
                    break;
                }
            }

            // The DSM is no longer in the registered list
            if (SOCKET_CMD_SUCCESS != respData.errCode) {
                respData.errCode = SOCKET_CMD_SUCCESS;
                respData.resultCode = SOCKET_RESULT_DSM_NOT_IN_LIST;
            }
            resp->opCode = CMD_DSM_IS_CONNECTED;

            break;
        }
        case CMD_DSM_REGISTER:
        {
            PDATA_REGISTER_DSM cmdData = (PDATA_REGISTER_DSM)cmd->data;
            std::string ip = cmdData->ip;
            std::string user = cmdData->user;
            std::string pwd = cmdData->password;
            Json::Value task;

            task["cmd"] = CMD_DSM_REGISTER;
            task["ip"] = ip;
            task["user"] = user;
            task["pwd"] = pwd;

            m_TrayEventQueue.push(task);
            respData.errCode = SOCKET_CMD_SUCCESS;
            resp->opCode = CMD_DSM_REGISTER;
            break;
        }
        case CMD_DSM_REMOVE:
        {
            PDATA_REMOVE_DSM cmdData = (PDATA_REMOVE_DSM)cmd->data;
            std::string dsmSn = cmdData->dsmSn;
            Json::Value task;

            task["cmd"] = CMD_DSM_REMOVE;
            task[SZ_DSM_SN] = dsmSn;
            m_TrayEventQueue.push(task);
            respData.errCode = SOCKET_CMD_SUCCESS;
            resp->opCode = CMD_DSM_REMOVE;
            break;
        }

        default:
            // Invalid commands, this case shouldn't happen
            respData.errCode = SOCKET_CMD_INVALID_CMD;
            resp->opCode = CMD_NULL;
            break;
        }

        memcpy(resp->data, &respData, sizeof(DATA_SOCKET_RESP));
        memcpy(respBuf, resp, respSize);

        // Send response
        // Echo the buffer back to the sender (TODO: send back real response)
        if (!SendData(clientSocket, respBuf, DEFAULT_BUFLEN)) {
            if (resp) {
                free(resp);
            }
            goto END;
        }
        // Shutdown the connection since we're done
        if (SOCKET_ERROR == (iResult = shutdown(clientSocket, SD_SEND))) {
            WriteErrorLogEntry(L"shutdown in TrayEventWorker", WSAGetLastError());
            if (resp) {
                free(resp);
            }
            goto END;
        }

        closesocket(clientSocket);
        // Update status, the clientSocket is closed after this stage
        status = DATA_RECEIVED;

        if (resp) {
            free(resp);
        }
    }

END:
    if (WSADATA_INITED <= status) {
        WSACleanup();
    }

    if (SOCKET_INITED <= status) {
        closesocket(listenSocket);
    }

    if (RECEIVING_DATA == status) {
        closesocket(clientSocket);
    }

    SetEvent(m_hStoppedEvents[STOPPED_EVENT_TRAY]);
}

void SynoVssService::RequesterWorker()
{
    m_TaskHandler.Initialize();

    while (!m_fStopping) {
        if (WAIT_TIMEOUT != WaitForSingleObject(m_TaskEvent, 2000)) {
            Syno::PVssCommand cmd;
            if (m_RequestEventQueue.try_pop(cmd)) {
                try {
                    m_TaskHandler.ParseCmds(cmd);
                }
                catch (HRESULT hr) {
                    // TODO: handle VSS exceptions
                }
                catch (std::system_error error) {
                    // TODO: handle VSS exceptions
                }
                catch (...) {
                    // TODO: handle VSS exceptions
                }
            }
            ResetEvent(m_TaskEvent);
        }
    }

    SetEvent(m_hStoppedEvents[STOPPED_EVENT_REQUESTER]);
}

void SynoVssService::NetworkSendWorker()
{
    int checkStatusCd,
        sleepInMiliSec;
    time_t start,
           sleepClocks;

    // Wait for main channel
    while (WAIT_OBJECT_0 != WaitForSingleObject(m_hConnectionEvents[CHANNEL_REMOTE_RDY], EVENT_WAITTIME)) {
        if (m_fStopping) {
            goto WORKERSTOP;
        }
    }

    //start sender to receiver comm
    m_netWorkerChannel = NULL;
    m_netWorkerHandle = -1;

    m_netWorkerChannel = CreateSynoComm(m_addrList[0].c_str(), m_hostname.c_str(), "APP_SENDER");

    if (NULL == m_netWorkerChannel) {
        // TODO: critical error here
        //WriteErrorLogEntry(StringToWString(error.what()).c_str(), error.code().value());
        MyWriteErrorLog("worker channel failed");
    }

    SetEvent(m_hConnectionEvents[CHANNEL_LOCAL_RDY]);

    m_netWorkerHandle = SynoCommConnect(m_netWorkerChannel, m_addrList[0].c_str(), "SynoVssService");

    if (-1 == m_netWorkerHandle) {
        // TODO: critical error here
        MyWriteErrorLog("worker handle failed");
    }

    SetEvent(m_hConnectionEvents[CONNECTION_LOCAL_RDY]);

    checkStatusCd = CHECK_STATUS_FREQUENCY;

    // Start handling sender jobs
    while (!m_fStopping) {
        start = clock();
        SenderRedirectCommands();

        AcquireReadListLock();
        for (auto it : m_stringDsInfoList ) {
            SenderHandleTimeout(it.second);
        }
        ReleaseReadListLock();

        //check connection status every CHECK_STATUS_FREQUENCY secs
        if (0 >= checkStatusCd) {
            SenderCheckStatus();
            checkStatusCd = CHECK_STATUS_FREQUENCY;
        }

        checkStatusCd -= NETWORKER_FREQUENCY;

        // sleep till next tick
        sleepClocks = (int)(NETWORKER_FREQUENCY * CLOCKS_PER_SEC) - (clock()-start);
        sleepInMiliSec = (int)(((float)sleepClocks/CLOCKS_PER_SEC) * 1000);
        Sleep(sleepInMiliSec < 0 ? 0 : (sleepInMiliSec > NETWORKER_FREQUENCY*1000 ? NETWORKER_FREQUENCY*1000 : sleepInMiliSec));
    }

WORKERSTOP:
        SetEvent(m_hStoppedEvents[STOPPED_EVENT_NETWORK_SEND]);
}

void SynoVssService::NetworkReceiveWorker()
{
    Syno::PVssCommand pRes;
    Json::Value nextJson(Json::objectValue);

    SetEvent(m_hStoppedEvents[STOPPED_EVENT_NETWORK_RECEIVE]);

    //workaround, give synocomm sometime
    sleep(2);

    try {
        SetupComm();
        SetEvent(m_hConnectionEvents[CHANNEL_REMOTE_RDY]);
    }
    catch (std::system_error error) {
        // TODO: critical error here, should just stop the plugin
    }

    while (WAIT_OBJECT_0 != WaitForSingleObject(m_hConnectionEvents[CONNECTION_LOCAL_RDY], EVENT_WAITTIME)) {
        if (m_fStopping) {
            goto WORKERSTOP;
        }
    }

    // TODO: workaround
    //sleep(4);

    try {
        InitiateConnection(m_stringDsInfoList);
        // Set flag of channel ready
        // TODO: what if only some DS are connected?
        m_isChannelRdy = TRUE;
    }
    catch (std::system_error error) {
        MyWriteErrorLog(error.what());  // TODO: temp
        //WriteErrorLogEntry(StringToWString(error.what()).c_str(), error.code().value());
    }

    // TODO: workaround
    //sleep(2);

    SetEvent(m_hConnectionEvents[CONNECTION_REMOTE_RDY]);

    while (!m_fStopping) {
        try {
            MyWriteErrorLog("Start receiving");
            Syno::VssComm::ReceiveJson(nextJson);
        }
        catch (std::system_error error) {
            // TODO: handle receive error
            MyWriteErrorLog(error.what());
            sleep(3);
            continue;
        }

        //FOR TESTING
        MyWriteErrorLog(nextJson.toStyledString().c_str());

        AcquireWriteListLock();
        try {
            if (nextJson.isMember(SZ_COMMAND)) {// a request
                ReceiverHandleRequest(nextJson);
            } else if (nextJson.isMember(SZ_SUCCESS)){// a response
                ReceiverHandleResponse(nextJson);
            }
        }
        catch (std::system_error error) {
            ReleaseWriteListLock();
            // TODO: handle error here
        }
        ReleaseWriteListLock();
    }

WORKERSTOP:
    return;
}
#pragma endregion

#pragma region Helper functions
void SynoVssService::LoadInfo()
{
    Syno::LoadPluginId(m_pluginId);
    WriteEventLogEntry(FormatWString(L"Load plugin ID: %s", m_pluginId.c_str()).c_str(), EVENTLOG_INFORMATION_TYPE);

    Syno::LoadPluginVersion(m_pluginVer);
    WriteEventLogEntry(FormatWString(L"Load plugin version: %s", m_pluginVer.c_str()).c_str(), EVENTLOG_INFORMATION_TYPE);

    Syno::LoadProtoVersion(m_protoVer);
    WriteEventLogEntry(FormatWString(L"Load protocol version: %s", m_protoVer.c_str()).c_str(), EVENTLOG_INFORMATION_TYPE);

    Syno::LoadOsVersion(m_osVer);
    WriteEventLogEntry(FormatWString(L"Load Windows OS version: %s", m_osVer.c_str()).c_str(), EVENTLOG_INFORMATION_TYPE);

    Syno::LoadDsInfoList(m_stringDsInfoList);
    WriteEventLogEntry(FormatWString(L"Load %d registered DSM info", m_stringDsInfoList.size()).c_str(), EVENTLOG_INFORMATION_TYPE);
}

void SynoVssService::SetupComm()
{
    Syno::GetHostname(m_hostname);
    if (m_hostname.empty()) {
        throw make_system_error("Failed to get hostname");  // treat as error??
    }

    Syno::GetAddrList(m_hostname, m_addrList);
    if (m_addrList.empty()) {
        throw make_system_error("Failed to get network IP address");
    }

    Syno::VssComm::CreateChannel("SynoVssService", m_addrList[0].c_str(), m_hostname.c_str());
}

void SynoVssService::HandleTrayEvents(const Json::Value& task)
{
    switch(task["cmd"].asInt()) {
        case CMD_DSM_REGISTER:
        {
            // Replace m_commandSn with static member
            Syno::RegisterReq* pCommand = new RegisterReq(m_commandSn, WStringToString(m_pluginId));

            m_commandSnList[m_commandSn] = SZ_CMD_REGISTER;
            m_commandSn++;

            pCommand->SetAccount(task["user"].asString());
            pCommand->SetPassword(task["pwd"].asString());
            pCommand->SetRemoteAddress(task["ip"].asString());
            m_NetworkSendQueue.push(PVssCommand(pCommand));

            break;
        }
        case CMD_DSM_REMOVE:
        {
            // TODO: replace test-key with registered key from dsInfo
            // TODO: replace m_commandSn++ with the static one
            std::string key;
            std::string dsmSn;
            Syno::DisconnectReq* pDisconnectCmd;
            Syno::UnregisterReq* pUnregisterCmd;
            Syno::StringDsInfoList::iterator it;

            dsmSn = task[SZ_DSM_SN].asString();

            // The selected DSM is not in the registered list
            if (m_stringDsInfoList.end() == (it = m_stringDsInfoList.find(StringToWString(dsmSn)))) {
                return;
            }

            key = WStringToString(it->second->GetKey());

            pDisconnectCmd = new Syno::DisconnectReq(m_commandSn, WStringToString(m_pluginId), key);
            pDisconnectCmd->SetDsmSn(task[SZ_DSM_SN].asString());
            m_commandSnList[m_commandSn] = SZ_CMD_DISCONNECT;
            m_commandSn++;

            m_NetworkSendQueue.push(PVssCommand(pDisconnectCmd));

            // Sending unregister command
            pUnregisterCmd = new Syno::UnregisterReq(m_commandSn, WStringToString(m_pluginId), key);
            pUnregisterCmd->SetDsmSn(task[SZ_DSM_SN].asString());
            m_commandSnList[m_commandSn] = SZ_CMD_UNREGISTER;
            m_commandSn++;

            m_NetworkSendQueue.push(PVssCommand(pUnregisterCmd));

            break;
        }
        default:
            break;
    }
}

void SynoVssService::HandleRecvNetworkEvents(const PVssCommand& pCmd)
{
    std::string cmdName;
    std::wstring dsmSn = StringToWString(pCmd->GetDsmSn());
    Syno::CommandSnList::iterator itCmd;

    // Handle initial snapshot command
    if (SZ_CMD_INIT_SNAPSHOT == pCmd->GetCommandName()) {
        m_RequestEventQueue.push(pCmd);
        // TODO: check event status
        SetEvent(m_TaskEvent);
        return;
    }

    if (m_commandSnList.end() == (itCmd = m_commandSnList.find(pCmd->GetCommandSN()))) {
        // Timeout or error condition
        return;
    }
    cmdName = itCmd->second;
    // Remove the responsed command from the command list
    m_commandSnList.erase(itCmd);

    if (SZ_CMD_REGISTER == cmdName) {
        std::wstring dsmIp;
        std::wstring account;
        std::wstring key;
        Json::Value resp;
        Syno::StringDsInfoList::iterator itDsInfo;
        PDsInfo pDsInfo;
        VssResponse* pResp = (VssResponse*)pCmd.get();

        resp = pResp->ToJson();

        if (!resp[SZ_SUCCESS].asBool()) {
            // TODO: error conditions
            return;
        }

        AcquireReadListLock(); //reading list
        if (m_stringDsInfoList.end() == (itDsInfo = m_stringDsInfoList.find(dsmSn))) {
            // The DS info no longer exists (timed out or deleted)
            ReleaseReadListLock();
            return;
        }
        // Update registry
        pDsInfo = itDsInfo->second;

        // TODO: create a function for these codes
        dsmIp = pDsInfo->GetAddress();
        account = pDsInfo->GetAccount();
        key = pDsInfo->GetKey();
        ReleaseReadListLock(); // Releasing list lock

        RegValueSet(HKEY_LOCAL_MACHINE, REG_SYNO_VSS_GROUPS_KEY L"\\" + dsmSn, L"Address", dsmIp);
        RegValueSet(HKEY_LOCAL_MACHINE, REG_SYNO_VSS_GROUPS_KEY L"\\" + dsmSn, L"Account", account);
        RegValueSet(HKEY_LOCAL_MACHINE, REG_SYNO_VSS_GROUPS_KEY L"\\" + dsmSn, L"Key", key);

        // Add connect command into task queue
        Syno::ConnectReq* pCommand = new ConnectReq(m_commandSn, m_pluginId, key);

        m_commandSnList[m_commandSn] = SZ_CMD_CONNECT;
        m_commandSn++;

        pCommand->SetAccount(WStringToString(account));
        pCommand->SetOsType(OS_TYPE);
        // TODO: replace hard codes
        pCommand->SetOsVersion(WStringToString(m_osVer));
        pCommand->SetProtocolVersion(WStringToString(m_protoVer));
        pCommand->SetDsmSn(WStringToString(dsmSn));

        if (m_addrList.empty()) {
            throw make_system_error("Failed to get network IP address");
        }
        pCommand->AddAddress(m_addrList[0]);

        m_NetworkSendQueue.push(PVssCommand(pCommand));
    } else if (SZ_CMD_CONNECT == cmdName) {
        Syno::StringDsInfoList::iterator itDsInfo;
        PDsInfo pDsInfo;
        VssResponse* pResp = (VssResponse*)pCmd.get();
        Json::Value resp = pResp->ToJson();

        if (!resp[SZ_SUCCESS].asBool()) {
            // TODO: add log for error conditions
            return;
        }

        AcquireReadListLock();
        if (m_stringDsInfoList.end() == (itDsInfo = m_stringDsInfoList.find(dsmSn))) {
            // The DS info no longer exists (timed out or deleted)
            ReleaseReadListLock();
            return;
        }
        // Change DS info connection status to true
        pDsInfo = itDsInfo->second;
        pDsInfo->SetConnectionStatus(true);
        ReleaseReadListLock();
    } else if (SZ_CMD_DISCONNECT == cmdName) {
        Syno::StringDsInfoList::iterator itDsInfo;
        PDsInfo pDsInfo;
        VssResponse* pResp = (VssResponse*)pCmd.get();
        Json::Value resp = pResp->ToJson();

        if (!resp[SZ_SUCCESS].asBool()) {
            // TODO: add log for error conditions
            return;
        }

        AcquireReadListLock();
        if (m_stringDsInfoList.end() == (itDsInfo = m_stringDsInfoList.find(dsmSn))) {
            // The DS info no longer exists (timed out or deleted)
            ReleaseReadListLock();
            return;
        }

        // Change DS info connection status to false
        pDsInfo = itDsInfo->second;
        pDsInfo->SetConnectionStatus(false);
        ReleaseReadListLock();
    } else if (SZ_CMD_UNREGISTER == cmdName) {
        Syno::StringDsInfoList::iterator itDsInfo;
        PDsInfo pDsInfo;
        VssResponse* pResp = (VssResponse*)pCmd.get();
        Json::Value resp = pResp->ToJson();

        if (!resp[SZ_SUCCESS].asBool()) {
            // Add log for error conditions
            return;
        }

        // Unbind the channel connection with this DSM
        KillDsConnection(dsmSn);

        RegKeyDelete(HKEY_LOCAL_MACHINE, REG_SYNO_VSS_GROUPS_KEY L"\\" + dsmSn);
    } else {
        // Error condition, unknown command names
    }
}

void SynoVssService::HandleReconnect(PDsInfo &dsInfo)
{
    if (dsInfo->IsReconnecting() && dsInfo->IsTimeToReconnect(NETWORKER_FREQUENCY)) {
        // Add connect command into task queue
        Syno::ConnectReq* pCommand = new ConnectReq(m_commandSn, m_pluginId, dsInfo->GetKey());

        m_commandSnList[m_commandSn] = SZ_CMD_CONNECT;
        m_commandSn++;

        pCommand->SetAccount(WStringToString(dsInfo->GetAccount()));
        pCommand->SetOsType(OS_TYPE);
        // TODO: replace hard codes
        pCommand->SetOsVersion(WStringToString(m_osVer));
        pCommand->SetProtocolVersion(WStringToString(m_protoVer));
        pCommand->SetDsmSn(WStringToString(dsInfo->GetSN()));

        if (m_addrList.empty()) {
            throw make_system_error("Failed to get network IP address");
        }

        pCommand->AddAddress(m_addrList[0]);

        m_NetworkSendQueue.push(PVssCommand(pCommand));

        dsInfo->InitiateReconnectCd();
    }
}

void SynoVssService::ReceiverHandleRequest(Json::Value &json)
{
    Syno::PDsInfo dsInfo;
    std::string command = json[SZ_COMMAND].asString();
    uint32_t errorCode = 0;

    // TODO: verify the json

    if (SZ_CMD_INIT_SNAPSHOT == command) {
        PVssCommand pCommand(new InitSnapReq(json));
        m_NetworkReceiveQueue.push(pCommand);
        return;
    }

    if (SZ_CMD_UPDATE_STATUS == command) {
        ReceiverCheckStatus();
        return;
    }

    if (SZ_CMD_RECONNECT == command) {
        ReceiverHandleReconnect(m_stringDsInfoList[StringToWString(json[SZ_DSM_SN].asString())]);
        return;
    }

    if (SZ_CMD_KILL_CONNECTION == command) {
        Syno::StringDsInfoList::iterator it = m_stringDsInfoList.find(StringToWString(json[SZ_DSM_SN].asString()));
        if (m_stringDsInfoList.end() == it) {
            MyWriteErrorLog("cannot find ds to delete"); // TODO: temp
        }

        it->second->Disconnect();
        m_stringDsInfoList.erase(it);
        return;
    }

    if (SZ_CMD_REGISTER != command) {
        StringDsInfoList::iterator it = m_stringDsInfoList.find(StringToWString(json[SZ_DSM_SN].asString()));

        if (m_stringDsInfoList.end() == it) {
            errorCode = ERR_DSM_NOTFOUND;
            goto RESPFAIL;
        }

        dsInfo = it->second;
    } else {
        std::string remoteIp = json[SZ_IP].asString();
        std::string ipAddr;

        if (0 > VssComm::DomainToIp(remoteIp, ipAddr)) {
            errorCode = ERR_DSM_UNAVAILABLE;
            goto RESPFAIL;
        }

        // Handle duplicate registration
        for (auto ds:m_stringDsInfoList) {
            std::string dsIp;
            VssComm::DomainToIp(WStringToString(ds.second->GetAddress()), dsIp);

            if (ipAddr == dsIp) {
                errorCode = ERR_DSM_REGISTERED;
                goto RESPFAIL;
            }
        }

        // New registration
        std::wstring tempSn = L"temp" + json[SZ_COMMAND_SN].asUInt();
        dsInfo = Syno::PDsInfo(new DsInfo(tempSn));

        dsInfo->SetAccount(StringToWString(json[SZ_DATA][SZ_ACCOUNT].asString()));
        dsInfo->SetAddress(StringToWString(remoteIp));
        dsInfo->SetKey(L"");

        try {
            dsInfo->SetupCommConnection();
        }
        catch (std::system_error error) {
            MyWriteErrorLog("Register request:DsInfo with ip:[%s] SetupCommConnection failed", WStringToString(dsInfo->GetAddress()).c_str()); // TODO: temp

            // TODO: set error code
            errorCode = ERR_DSM_UNAVAILABLE;
            goto RESPFAIL;
        }

        dsInfo->ToConnecting();
        m_stringDsInfoList.insert(std::pair<std::wstring, PDsInfo>(tempSn, dsInfo));
    }

    ReceiverHandleReconnect(dsInfo);    //try reconnect once

    if (dsInfo->IsReconnecting()) {
        // TODO: error code
        errorCode = ERR_DSM_UNAVAILABLE;
        goto RESPFAIL;
    }

    m_SnDsInfoList.insert(std::pair<uint32_t, PDsInfo>(json[SZ_COMMAND_SN].asUInt(), dsInfo));
    MyWriteErrorLog("Sending to DSM:%s", json.toStyledString().c_str());
    dsInfo->SendCommand(json);
    return;

RESPFAIL:
    m_NetworkReceiveQueue.push(PVssCommand(new VssResponse(json[SZ_COMMAND_SN].asUInt(), false, errorCode, json[SZ_DSM_SN].asString())));
}

void SynoVssService::ReceiverHandleResponse(Json::Value &json)
{
    std::uint32_t commandSn;
    Syno::PDsInfo dsInfo;
    Syno::PVssCommand pRes;

    // TODO: verify the json

    commandSn = json[SZ_COMMAND_SN].asUInt();

    Syno::SnDsInfoList::iterator it = m_SnDsInfoList.find(commandSn);
    if (m_SnDsInfoList.end() == it) {
        // already timeout
        MyWriteErrorLog("Command with SN %ld timeout", commandSn);
        ToErrorResponse(json, ERR_CMD_TIMEOUT);
        goto SENDRES;
    }

    dsInfo = it->second;
    dsInfo->DeleteCommandFromTimeout(commandSn);
    m_SnDsInfoList.erase(it);

    if (SZ_CMD_REGISTER == m_commandSnList[json[SZ_COMMAND_SN].asUInt()]) {
        m_stringDsInfoList.erase(dsInfo->GetSN());

        if (!json[SZ_SUCCESS].asBool()) {
           MyWriteErrorLog("Register failed");  // TODO: temp
           dsInfo->Disconnect();
           goto SENDRES;
        }

        dsInfo->SetKey(StringToWString(json[SZ_DATA][SZ_KEY].asString()));
        dsInfo->SetSN(StringToWString(json[SZ_DSM_SN].asString()));

        m_stringDsInfoList.insert(std::pair<std::wstring, Syno::PDsInfo>(dsInfo->GetSN(), dsInfo));
    }

SENDRES:
    //generate response
    pRes = Syno::PVssCommand(new Syno::VssResponse(json));
    // TEMP
    MyWriteErrorLog("Pushing:%s", json.toStyledString().c_str());
    m_NetworkReceiveQueue.push(pRes);
}

void SynoVssService::SenderRedirectCommands()
{
    Syno::PVssCommand nextCommand;

    while (m_NetworkSendQueue.try_pop(nextCommand)) {
        std::string stringCommand;

        stringCommand = nextCommand->ToJson().toStyledString();

        if (0 > CommSendMessage(m_netWorkerChannel, m_netWorkerHandle, stringCommand.c_str(), (int)strlen(stringCommand.c_str()))) {
            // TODO: handle inner msg sending error
        }
    }
}

void SynoVssService::SenderCheckStatus()
{
    Syno::PVssRequest pReq(new VssRequest(SZ_CMD_UPDATE_STATUS));

    pReq->ToInnerCommand();

    std::string stringCommand = pReq->ToJson().toStyledString();

    if (0 > CommSendMessage(m_netWorkerChannel, m_netWorkerHandle, stringCommand.c_str(), (int)strlen(stringCommand.c_str()))) {
        throw make_system_error("Cannot send check status msg from sender to receiver");
    }
}

void SynoVssService::ReceiverCheckStatus()
{
    for (auto p : m_stringDsInfoList) {
        PDsInfo dsInfo = p.second;

        // WARNING: sending and receiving msg here
        if (dsInfo->IsReconnecting() || dsInfo->IsCommConnected()) {
            continue;
        }

        try {
            dsInfo->Disconnect();
        }
        catch (std::system_error error) {
            // TODO: expected an error here,
        }

        dsInfo->ToReconnecting();
    }
}

void SynoVssService::SenderHandleTimeout(PDsInfo &dsInfo)
{
    std::vector<uint32_t> timeouts;
    dsInfo->HandleCommandTimeout(NETWORKER_FREQUENCY, timeouts);

    for (auto cn : timeouts) {
        std::string cmdName = m_commandSnList[cn];
        // TODO: set error code
        Syno::PVssResponse pRes(new VssResponse(cn, false, ERR_CMD_TIMEOUT, WStringToString(dsInfo->GetSN())));
        std::string stringRes = pRes->ToJson().toStyledString();

        if (0 > CommSendMessage(m_netWorkerChannel, m_netWorkerHandle, stringRes.c_str(), (int)strlen(stringRes.c_str()))) {
            throw make_system_error("Cannot foward timeout response from sender to receiver");
        }
    }
}

void SynoVssService::SenderHandleReconnect(PDsInfo &dsInfo)
{
    if (dsInfo->IsReconnecting() && dsInfo->IsTimeToReconnect(NETWORKER_FREQUENCY)) {
        Syno::PVssRequest pReq(new VssRequest(SZ_CMD_RECONNECT));
        pReq->ToInnerCommand();
        pReq->SetDsmSn(WStringToString(dsInfo->GetSN()));

        std::string stringCommand = pReq->ToJson().toStyledString();

        if (0 > CommSendMessage(m_netWorkerChannel, m_netWorkerHandle, stringCommand.c_str(), (int)strlen(stringCommand.c_str()))) {
            throw make_system_error("Cannot foward reconnect request from sender to receiver");
        }

        dsInfo->InitiateReconnectCd();
    }
}

void SynoVssService::ReceiverHandleReconnect(PDsInfo &dsInfo)
{
    if (!dsInfo->IsReconnecting()) {
        return;
    }

    try {
        dsInfo->Reconnect();
    }
    catch (std::system_error error) {
        // TODO: handle error
        MyWriteErrorLog(error.what());
        return;
    }

    dsInfo->ToConnecting();
}

// WARNING: call this only before deleting DsInfo
void SynoVssService::KillDsConnection(std::wstring dsmsn)
{
    Syno::PVssRequest pReq(new VssRequest(SZ_CMD_KILL_CONNECTION));
    pReq->ToInnerCommand();
    pReq->SetDsmSn(WStringToString(dsmsn));
    m_NetworkSendQueue.push(pReq);
}

void SynoVssService::ReconnectDs(std::wstring dsmsn)
{
    Syno::PVssRequest pReq(new VssRequest(SZ_CMD_RECONNECT));
    pReq->ToInnerCommand();
    pReq->SetDsmSn(WStringToString(dsmsn));
    m_NetworkSendQueue.push(pReq);
}

#pragma endregion
}