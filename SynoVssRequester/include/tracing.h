/////////////////////////////////////////////////////////////////////////
// Copyright ?Microsoft Corporation. All rights reserved.
// 
//  This file may contain preliminary information or inaccuracies, 
//  and may not correctly represent any associated Microsoft 
//  Product as commercially released. All Materials are provided entirely 
//  �AS IS.?To the extent permitted by law, MICROSOFT MAKES NO 
//  WARRANTY OF ANY KIND, DISCLAIMS ALL EXPRESS, IMPLIED AND STATUTORY 
//  WARRANTIES, AND ASSUMES NO LIABILITY TO YOU FOR ANY DAMAGES OF 
//  ANY TYPE IN CONNECTION WITH THESE MATERIALS OR ANY INTELLECTUAL PROPERTY IN THEM. 
// 


#pragma once


/////////////////////////////////////////////////////////////////////////
//  Generic tracing/logger class
//

enum SYNOVSS_LOG_LEVEL {
    SYNOVSS_LOG_NORMAL = 0,
    SYNOVSS_LOG_WARN,
    SYNOVSS_LOG_ERR
};

// Very simple tracing/logging class 
class FunctionTracer
{
public:
    FunctionTracer(std::wstring fileName, INT lineNumber, std::wstring functionName);
    ~FunctionTracer();
    
    // tracing routine
    void Trace(std::wstring file, int line, std::wstring functionName, std::wstring format, ...);
    
    void WriteLine(const SYNOVSS_LOG_LEVEL level, const std::wstring format, ...);
    // console logging routine
    void WriteLine(std::wstring format, ...);
    
    // Converts a HRESULT into a printable message
    static std::wstring HResult2String(HRESULT hrError);

    // Enables tracing
    static void EnableTracingMode();
    static void SetLogLevel(const SYNOVSS_LOG_LEVEL level);

private:

    //
    //  Data members
    //

    static bool m_traceEnabled;
    static SYNOVSS_LOG_LEVEL m_logLevel;

    std::wstring     m_fileName;
    int         m_lineNumber;
    std::wstring     m_functionName;

};

