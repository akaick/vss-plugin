
#pragma once

#include "stdafx.h"
#include "vssclient.h"

#define NELEMENTS(x) (sizeof (x) / sizeof (*(x)))

namespace Syno {

enum VssError {
    ERR_DEFAULT = 0,
    ERR_VOLUME_NOT_FOUND
};

class VssRequester : private VssClient
{
public:
    VssRequester();
    ~VssRequester();

    // Create: Create a shadow copy set with corresonding LUN serial
    void CreateLunSnapshot(const wstring lunSerial);

    // Resync: Import the snapshot set
    void ImportSnapshotSet(const wstring xmlDoc);

    // Resync: Must be performed after the snapshot is imported
    void ResyncLun(const wstring xmlDoc, const vector<VSS_ID> snapshotIdList);

    void RequesterWriteEventLog(
        WORD type,
        LPCWSTR pFormat,
        ...
        );

private:
    bool IsDiskAvailable(const CComPtr<IVdsDisk>& pDisk);
    bool IsDiskMatched(const CComPtr<IVdsDisk>& pDisk, const wstring& lunSerial);
    void LoadVdsService(CComPtr<IVdsService>& pService);
    void GetVolumePropForVdsId(const CComPtr<IVdsService>& pService, const VDS_OBJECT_ID volumeId, VDS_VOLUME_PROP& volumeProp);
    void GetVolumesByVdsDisk(const CComPtr<IVdsService>& pService, const CComPtr<IVdsDisk>& pDisk, vector<wstring>& volumeList);
    void GetVolumesByLunSerial(const wstring& lunSeriel, vector<wstring>& volumeList);

    int GetUniqueVolumeNameByLunSerial(wstring serialNum, vector<wstring>& volumeList);

};

}

