
#include <algorithm>
#include <SynoUtils.h>
#include "SynoRequester.h"

namespace Syno {

VssRequester::VssRequester()
{
}

VssRequester::~VssRequester()
{
}

void VssRequester::CreateLunSnapshot(const wstring lunSerial)
{
    // TODO: check if auto-recovery needed
    DWORD dwContext = VSS_CTX_BACKUP | VSS_CTX_APP_ROLLBACK | VSS_VOLSNAP_ATTR_TRANSPORTABLE;
    vector<wstring> excludeList;
    vector<wstring> includeList;
    vector<wstring> volumeList;

    try {
        Initialize(dwContext);
    }
    catch (HRESULT hr) {
        RequesterWriteEventLog(EVENTLOG_ERROR_TYPE, L"Initializing internal pointer failed:%s", FunctionTracer::HResult2String(hr).c_str());
        throw hr;
    }

    try {
        GetVolumesByLunSerial(lunSerial, volumeList);
    }
    catch (HRESULT hr) {
        RequesterWriteEventLog(EVENTLOG_ERROR_TYPE, L"Get corresponding volume from LUN failed:%s", FunctionTracer::HResult2String(hr).c_str());
        throw hr;
    }

    try {
        CreateSnapshotSet(volumeList, L"", excludeList, includeList);
    }
    catch (HRESULT hr) {
        RequesterWriteEventLog(EVENTLOG_ERROR_TYPE, L"Create snapshot failed:%s", FunctionTracer::HResult2String(hr).c_str());
        throw hr;
    }

    try {
        // TODO: temp
        MyWriteErrorLog("Before backupcomplete");
        BackupComplete(true);
        MyWriteErrorLog("After backupcomplete");
    }
    catch (HRESULT hr) {
        RequesterWriteEventLog(EVENTLOG_ERROR_TYPE, L"Complete Backup failed:%s", FunctionTracer::HResult2String(hr).c_str());
        throw hr;
    }
}

void VssRequester::ImportSnapshotSet(wstring xmlDoc)
{
    // TODO: get snapshot set ID from xml content
    // TODO: check is the snapshot set imported already
    Initialize(VSS_CTX_ALL, xmlDoc);

    VssClient::ImportSnapshotSet();
}

void VssRequester::ResyncLun(const wstring xmlDoc, const vector<VSS_ID> snapshotIdList)
{
    for (size_t i = 0; i < snapshotIdList.size(); i++) {
        AddResyncSet(snapshotIdList[i], L"");
    }

    Initialize(VSS_CTX_ALL, xmlDoc, true);
    DoResync(0);
}

void VssRequester::LoadVdsService(CComPtr<IVdsService>& pService)
{
    FunctionTracer ft(DBG_INFO);
    CComPtr<IVdsServiceLoader> pLoader;

    CHECK_COM(CoCreateInstance(CLSID_VdsLoader,
        NULL,
        CLSCTX_LOCAL_SERVER,
        __uuidof(IVdsServiceLoader),
        (void **)&pLoader));

    CHECK_COM(pLoader->LoadService(NULL, &pService));
    CHECK_COM(pService->WaitForServiceReady());
}

bool VssRequester::IsDiskAvailable(const CComPtr<IVdsDisk>& pDisk)
{
    FunctionTracer ft(DBG_INFO);

    bool ret = true;
    HRESULT hr;

    VDS_DISK_PROP diskProp;
    hr = pDisk->GetProperties(&diskProp);
    if (VDS_E_OBJECT_DELETED == hr) {
        goto END;
    }

    // TODO: how to judge whether disks should be ignored ???
    // Disk properties: http://msdn.microsoft.com/en-us/library/windows/desktop/aa383340(v=vs.85).aspx
    // Disk status: http://msdn.microsoft.com/en-us/library/windows/desktop/aa383343(v=vs.85).aspx
    // Disk health: http://msdn.microsoft.com/en-us/library/windows/desktop/aa383364(v=vs.85).aspx
    if ((VDS_VS_FAILED == diskProp.status) && (VDS_H_FAILED == diskProp.health) || !diskProp.pwszName) {
        goto END;
    }

    ret = false;
END:
    return ret;
}

bool VssRequester::IsDiskMatched(const CComPtr<IVdsDisk>& pDisk, const wstring& lunSerial)
{
    FunctionTracer ft(DBG_INFO);
    bool ret = false;

    VDS_LUN_INFORMATION lunInfo;
    CHECK_COM(pDisk->GetIdentificationData(&lunInfo));

    if (NULL == lunInfo.m_szSerialNumber) {
        goto END;
    }
     
    if (lunSerial != String2WString(lunInfo.m_szSerialNumber)) {
        goto END;
    }

    ret = true;
END:
    return ret;
}

/*
 * @pService [IN]
 * @volumeID [IN]
 * @volumeName [OUT] A volume GUID path, for example, "\\?\Volume{26a21bda-a627-11d7-9931-806e6f6e6963}\".
 */
void VssRequester::GetVolumePropForVdsId(const CComPtr<IVdsService>& pService, const VDS_OBJECT_ID volumeId, VDS_VOLUME_PROP& volumeProp)
{
    FunctionTracer ft(DBG_INFO);

    CComPtr<IUnknown> pUnknown;
    CHECK_COM(pService->GetObject(volumeId, VDS_OT_VOLUME, &pUnknown));

    CComQIPtr<IVdsVolume> pVolume = pUnknown;
    CHECK_COM(pVolume->GetProperties(&volumeProp));
}

/*
 * Determine how many volumes occupy the disk.
 *
 * @pService [IN] VDS service
 * @pDisk [IN]
 * @volumeList [OUT] will push back new elements
 */
void VssRequester::GetVolumesByVdsDisk(const CComPtr<IVdsService>& pService, const CComPtr<IVdsDisk>& pDisk, vector<wstring>& volumeList)
{
    FunctionTracer ft(DBG_INFO);

    LONG cDiskExtents;
    VDS_DISK_EXTENT* pDiskExtents;
    CHECK_COM(pDisk->QueryExtents(&pDiskExtents, &cDiskExtents));
    for ( LONG iExtent = 0; iExtent < cDiskExtents; ++iExtent) {
        VDS_DISK_EXTENT extent = pDiskExtents[iExtent];
                    
        // http://msdn.microsoft.com/en-us/library/cc249482.aspx
        if (VDS_DET_DATA != extent.type) {
            continue;
        }

                    
        VDS_VOLUME_PROP volumeProp;
        GetVolumePropForVdsId(pService, extent.volumeId, volumeProp);

        // http://msdn.microsoft.com/en-us/library/aa383942(v=vs.85).aspx
        if (VDS_VS_ONLINE != volumeProp.status) {
            continue;
        }

        // There are three types of volume mount points:
        //  - A drive letter, for example, "C:\".
        //  - A volume GUID path, for example, "\\?\Volume{26a21bda-a627-11d7-9931-806e6f6e6963}\".
        //  - A mounted folder, for example, "C:\MountD\".
        volumeList.push_back(GetUniqueVolumeNameForMountPoint(volumeProp.pwszName));
    }

    CoTaskMemFree(pDiskExtents);
}

/*
 * Return unique volume name of each volume corresponding to LUN
 * 
 * @lunSerial [IN]
 * @volumeList [OUT] will push back new elements
 */
void VssRequester::GetVolumesByLunSerial(const wstring& lunSeriel, vector<wstring>& volumeList)
{
    FunctionTracer ft(DBG_INFO);

    // Load VDS service
    CComPtr<IVdsService> pService;
    LoadVdsService(pService);

    // Enumerate the software providers
    CComPtr<IEnumVdsObject> pEnumProvider;
    CHECK_COM(pService->QueryProviders(VDS_QUERY_SOFTWARE_PROVIDERS, &pEnumProvider));
    for (const CComQIPtr<IVdsSwProvider> pSwProvider : EnumerateVdsObjects(pEnumProvider)) {

        // Enumerate packs for this provider
        CComPtr<IEnumVdsObject> pEnumPack;
        CHECK_COM(pSwProvider->QueryPacks(&pEnumPack));
        for (const CComQIPtr<IVdsPack> pPack : EnumerateVdsObjects(pEnumPack)) {

            // Enumerate disks. TODO: skip failed VDS pack?
            CComPtr<IEnumVdsObject> pEnumDisks;
            CHECK_COM(pPack->QueryDisks(&pEnumDisks));
            for (const CComQIPtr<IVdsDisk> pDisk : EnumerateVdsObjects(pEnumDisks)) {
                if (IsDiskAvailable(pDisk)) {
                    continue;
                }
                if (!IsDiskMatched(pDisk, lunSeriel)) {
                    continue;
                }

                GetVolumesByVdsDisk(pService, pDisk, volumeList);
            }
        }
    }

    // Remove duplicated elements
    volumeList.erase(std::unique(volumeList.begin(), volumeList.end()), volumeList.end());
}

void VssRequester::RequesterWriteEventLog(
        WORD type,
        LPCWSTR pFormat,
        ...
        )
{
    HANDLE hEventSource = NULL;
    LPCWSTR lpszStrings[2] = { NULL, NULL };
    WCHAR buf[4096];

    va_list args;
    va_start(args, pFormat);
    HRESULT hr = StringCchVPrintf(buf, NELEMENTS(buf), pFormat, args);
    va_end(args);

    hEventSource = RegisterEventSource(NULL, L"SynoVssRequester");
    if (hEventSource) {
        lpszStrings[0] = L"SynoVssRequester";
        lpszStrings[1] = buf;

        ReportEvent(hEventSource,   // Event log handle
            type,                   // Event type
            0,                      // Event category
            0,                      // Event identifier
            NULL,                   // No security identifier
            2,                      // Size of lpszStrings array
            0,                      // No binary data
            lpszStrings,            // Array of strings
            NULL                    // No binary data
            );

        DeregisterEventSource(hEventSource);
    }
}
}

