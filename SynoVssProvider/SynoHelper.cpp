
#include "stdafx.h"

#include <SynoUtils.h>
#include <SynoVssCommand.h>
#include "SynoProvider.h"


static std::uint32_t uCommandSN = 0;

/*
 * Set up SynoComm channel
 */
void CSynoProvider::SynoSetupComm()
{
    Syno::GetHostname(m_hostname);
    Syno::GetAddrList(m_hostname, m_addrList);

    if (m_addrList.empty()) {
        /* TODO: error handling */
        throw Syno::make_system_error("Provider cannot read system local address");
    }

    Syno::VssComm::CreateChannel("SynoVssProvider", m_addrList[0].c_str(), m_hostname.c_str());

}

/*
 * Load the following data from registry.
 *  - pluginid
 */
void CSynoProvider::SynoLoadInfo()
{
    Syno::LoadPluginId(m_pluginId);
    Syno::LoadPluginVersion(m_pluginVer);
    Syno::LoadProtoVersion(m_protoVer);
    Syno::LoadDsInfoList(m_stringDsInfoList);
}

void CSynoProvider::SynoSetupConnection()
{
    try {
        Syno::InitiateConnection(m_stringDsInfoList);
    }
    catch (std::system_error error) {
        //handle error
    }
}

/*
 * The VDS_LUN_INFO of the supportd LUNs must have
 *   VendorId  = "SYNOLOGY"
 *   ProductId = "iSCSI Storage"
 */
bool CSynoProvider::SynoIsLunSupported(
    const VDS_LUN_INFORMATION& lun
    )
{
    bool ret = false;

    if (0 != strcmp(lun.m_szVendorId, SYNO_VENDOR_ID) ||
        0 != strcmp(lun.m_szProductId, SYNO_PRODUCT_ID)) {
        TraceMsg(L"Vendor or product ID does not match");
        goto END;
    }


    // TODO: issue request 'is_lun_supported' to all connected DS.
    // TODO: save mapping of source_lun to DSs.
    // TODO: If Synology LUN is found but no corresponding DS connection -> LogEvent()
    for (auto p : m_stringDsInfoList) {
        Syno::PDsInfo dsInfo = p.second;
        Json::Value jsonResponse;
        Syno::IsLunSupportedReq *pReq = new Syno::IsLunSupportedReq(Syno::VssCommand::s_SN++, m_pluginId, dsInfo->GetKey(), lun.m_szSerialNumber);

        try {
            dsInfo->SendThenReceive(Syno::PVssCommand(pReq), jsonResponse);
        }
        catch (std::system_error error) {
            //handle receive error
        }

        if (jsonResponse[SZ_SUCCESS].asBool()) {
            m_LunSnDsInfoList.insert(std::pair<std::string, Syno::PDsInfo>(lun.m_szSerialNumber, dsInfo));
            goto LUNFOUND;
        }
        // TODO: handle error code
    }

    LogEvent(L"Cannot find corresponding DS for lun %s", lun.m_szSerialNumber);
    goto END;

LUNFOUND:
    ret = true;
END:
    return ret;
}


void CSynoProvider::SynoStartMirrorLun(
    SnapshotInfo& snap
    )
{
    Json::Value jsonResponse;

    // TODO: issue 'start_mirror' for all LUNs in m_vSnapshotInfo to all coresponding DS's
    Syno::StartMirrorReq *pReq = new Syno::StartMirrorReq(Syno::VssCommand::s_SN++, Syno::WStringToString(m_pluginId), Syno::WStringToString(snap.dsInfo->GetKey()));
    pReq->SetTaskId(Syno::GUIDToString(m_setId));
    pReq->SetSourceLun(Syno::WStringToString(*(snap.srcLunId.get())));

    try {
        (snap.dsInfo)->SendThenReceive(Syno::PVssCommand(pReq), jsonResponse);
    }
    catch (std::system_error error) {
        // TODO: handle receive error
        throw (HRESULT) VSS_E_PROVIDER_VETO;
    }

    if (!(jsonResponse[SZ_SUCCESS].asBool())) {
        // TODO: handle error code
        LogEvent(L"Failed to begin prepare snapshot for LUN %s", "<GUID>");
        throw (HRESULT) VSS_E_PROVIDER_VETO;
    }
}


void CSynoProvider::SynoWaitForMirrorLuns()
{
    // TODO: issue 'is_mirror_done' to all DS's.
    // TODO: only return when all LUNs are finished.
    // TODO: It's a bloking function, and needs to handle timeout issue. (throw exception)

    bool *mirrorDone;
    int doneLun;
    std::string response;
    Syno::PDsInfo dsInfo;
    Json::Value jsonResponse;

    mirrorDone = new bool[m_vSnapshotInfo.size()]();
    doneLun = 0;

    while (m_vSnapshotInfo.size() != doneLun) {
        for (int i = 0; i < m_vSnapshotInfo.size(); ++i) {
            if (mirrorDone[i]) {
                continue;
            }

            dsInfo = m_vSnapshotInfo[i].dsInfo;

            Syno::IsMirrorDoneReq *pReq = new Syno::IsMirrorDoneReq(
                Syno::VssCommand::s_SN++, Syno::WStringToString(m_pluginId), Syno::WStringToString(m_vSnapshotInfo[i].dsInfo->GetKey()));
            pReq->SetTaskId(Syno::GUIDToString(m_setId));
            pReq->SetSourceLun(Syno::WStringToString(*(m_vSnapshotInfo[i].srcLunId.get())));

            try {
                dsInfo->SendThenReceive(Syno::PVssCommand(pReq), jsonResponse);
            }
            catch (std::system_error error) {
                //handle receive error
            }

            if (!(jsonResponse[SZ_SUCCESS].asBool())) {
                // TODO: handle fail execution
                continue;
            }

            mirrorDone[i] = true;
            ++doneLun;
        }
    }

    delete[] mirrorDone;
}


void CSynoProvider::SynoCommitSnapshot(
    SnapshotInfo& snap
    )
{
    // TODO: issue 'depart_relation' to all DS's
    // TODO: should be finished in few seconds

    Json::Value jsonResponse;

    Syno::DepartRelationReq *pReq = new Syno::DepartRelationReq(Syno::VssCommand::s_SN++, Syno::WStringToString(m_pluginId), Syno::WStringToString(snap.dsInfo->GetKey()));
    pReq->SetTaskId(Syno::GUIDToString(m_setId));
    pReq->SetSourceLun(Syno::WStringToString(*(snap.srcLunId)));

    try {
        (snap.dsInfo)->SendThenReceive(Syno::PVssCommand(pReq), jsonResponse);
    }
    catch (std::system_error error) {
        //handle receive error
    }

    if (!(jsonResponse[SZ_SUCCESS].asBool())) {
        // TODO: handle execution error
    }
}

void CSynoProvider::SynoGetDestLunId(
    SnapshotInfo& snap
    )
{
    // Fill up mirroredLuns by issuing 'get_mirrored_lun'
    Json::Value jsonResponse;
    Syno::GetMirroredLunReq *pReq = new Syno::GetMirroredLunReq(Syno::VssCommand::s_SN++, Syno::WStringToString(m_pluginId), Syno::WStringToString(snap.dsInfo->GetKey()));
    pReq->SetSourceLun(Syno::WStringToString(*(snap.srcLunId.get())));

    try {
        (snap.dsInfo)->SendThenReceive(Syno::PVssCommand(pReq), jsonResponse);
    }
    catch (std::system_error error) {
        //handle receive error
    }

    if (!(jsonResponse[SZ_SUCCESS].asBool())) {
        // TODO: handle execution error
        return;
    }

    snap.dstLunId = std::make_shared<std::wstring>(Syno::StringToWString(jsonResponse[SZ_DATA][SZ_MIRRORED_LUN].asString()));
}


/*
 * http://msdn.microsoft.com/en-us/library/windows/desktop/aa384634.aspx
 *
 * Portion of VDS_LUN_INFOMARTIONN consiss of
 *   spc4r36m: 7.8.6 Device Identification VPD page (0x83)
 *   spc4r36m: 7.8.18 Unit Serial Number VPD page (0x80)
 */
void
CSynoProvider::SynoCopyLunInfo(
    VDS_LUN_INFORMATION& srcLun, /* const */
    VDS_LUN_INFORMATION& dstLun
    )
{
    ZeroMemory(&dstLun, sizeof(VDS_LUN_INFORMATION));

    dstLun.m_version            = srcLun.m_version;
    dstLun.m_DeviceType         = srcLun.m_DeviceType;
    dstLun.m_DeviceTypeModifier = srcLun.m_DeviceTypeModifier;
    dstLun.m_bCommandQueueing   = srcLun.m_bCommandQueueing;
    dstLun.m_BusType            = srcLun.m_BusType;

    // These NewString() calls may throw HRESULT exceptions, the
    // caller should be prepared to deal with them
    dstLun.m_szVendorId         = NewString(srcLun.m_szVendorId);
    dstLun.m_szProductId        = NewString(srcLun.m_szProductId);
    dstLun.m_szProductRevision  = NewString(srcLun.m_szProductRevision);
    dstLun.m_szSerialNumber     = NewString(srcLun.m_szSerialNumber);

    dstLun.m_diskSignature      = srcLun.m_diskSignature;

    // Copy all identifiers
    // TODO: ignore NAA identifier?
    VDS_STORAGE_DEVICE_ID_DESCRIPTOR& srcDesc = srcLun.m_deviceIdDescriptor;
    VDS_STORAGE_DEVICE_ID_DESCRIPTOR& dstDesc = dstLun.m_deviceIdDescriptor;

    dstDesc.m_version           = srcDesc.m_version;
    dstDesc.m_cIdentifiers      = srcDesc.m_cIdentifiers;
    dstDesc.m_rgIdentifiers = reinterpret_cast<VDS_STORAGE_IDENTIFIER*>(CoTaskMemAlloc(sizeof(VDS_STORAGE_IDENTIFIER) * dstDesc.m_cIdentifiers));
    for (ULONG i = 0; i < dstDesc.m_cIdentifiers; ++i) {
        VDS_STORAGE_IDENTIFIER& srcIdentifier = srcDesc.m_rgIdentifiers[i];
        VDS_STORAGE_IDENTIFIER& dstIdentifier = dstDesc.m_rgIdentifiers[i];

        dstIdentifier.m_CodeSet         = srcIdentifier.m_CodeSet;
        dstIdentifier.m_Type            = srcIdentifier.m_Type;
        dstIdentifier.m_cbIdentifier    = srcIdentifier.m_cbIdentifier;
        dstIdentifier.m_rgbIdentifier   = reinterpret_cast<BYTE*>(CoTaskMemAlloc(dstIdentifier.m_cbIdentifier));
        memcpy(dstIdentifier.m_rgbIdentifier, srcIdentifier.m_rgbIdentifier, dstIdentifier.m_cbIdentifier);
    }

    // skip copy m_rgInterconnects ?
}


void CSynoProvider::SynoLocateLuns(LONG lLunCount, VDS_LUN_INFORMATION* rgSourceLuns)
{
    std::wstring addr;
    std::wstring targetName;
    USHORT targetPort;

    // TODO: maybe need to check duplicated LUNs?
    for (long i = 0; i < lLunCount; i++) {
	    VDS_LUN_INFORMATION& lunInfo = rgSourceLuns[i];
	    std::string lunSerial = lunInfo.m_szSerialNumber;

	    if (!SynoCreateMappedTarget(Syno::StringToWString(lunSerial), addr, targetName, targetPort)) {
		    TraceMsg(L"Failed to get target LUN params id = %s", lunSerial.c_str());
		    return;
	    }
	    if (!SynoConnectTarget(addr, targetName, targetPort)) {
		    TraceMsg(L"Failed to connect target LUN");
		    return;
	    }
    }
}


// This function asks DSM to create a LUN target and return corresponding information
// The LUN serial ID is used for determining which DSM should we send this request command
BOOL CSynoProvider::SynoCreateMappedTarget(
    const std::wstring& lunSerial,
    std::wstring& addr,
    std::wstring& targetName,
    USHORT& targetPort
    )
{
    BOOL ret = FALSE;
    Json::Value jsonResponse,
                empty,
                firstAddr;
    Syno::PDsInfo dsInfo = m_LunSnDsInfoList[Syno::WStringToString(lunSerial)];

    Syno::CreateMappaedTargetReq *pReq = new Syno::CreateMappaedTargetReq(
        Syno::VssCommand::s_SN++, Syno::WStringToString(m_pluginId), Syno::WStringToString(dsInfo->GetKey()));

    pReq->SetInitiatorIQN("");
    pReq->SetLun(Syno::WStringToString(lunSerial));

    try {
        dsInfo->SendThenReceive(Syno::PVssCommand(pReq), jsonResponse);
    }
    catch (std::system_error error) {
        //handle receive error
        goto END;
    }

    if (!(jsonResponse[SZ_SUCCESS].asBool())) {
        // TODO: handle execution error
        goto END;
    }

    if (!jsonResponse.isMember(SZ_DATA) || !jsonResponse[SZ_DATA].isMember(SZ_TARGET_PORT) ||
            !jsonResponse[SZ_DATA].isMember(SZ_TARGET_ADDRESS_LIST) || !jsonResponse[SZ_DATA].isMember(SZ_TARGET_IQN)) {
        goto END;
    }

    firstAddr = jsonResponse[SZ_DATA][SZ_TARGET_ADDRESS_LIST].get((unsigned)0, empty);

    addr = Syno::StringToWString(firstAddr.asString());
    targetName = Syno::StringToWString(jsonResponse[SZ_DATA][SZ_TARGET_IQN].asString());
    targetPort = jsonResponse[SZ_DATA][SZ_TARGET_PORT].asUInt();

    ret = TRUE;
END:
    return ret;
}


/*
 * TODO: review required
 */
BOOL CSynoProvider::SynoConnectTarget(
    const std::wstring& addr,
    const std::wstring& targetName,
    const USHORT targetPort
    )
{
    HRESULT hr = S_OK;

    // http://msdn.microsoft.com/en-us/library/windows/desktop/bb870801.aspx
    // Initializing the login options required
    ISCSI_LOGIN_OPTIONS loginOptions; 
    loginOptions.Version = ISCSI_LOGIN_OPTIONS_VERSION;
    loginOptions.AuthType = ISCSI_NO_AUTH_TYPE;  // No CHAP enabled
    loginOptions.DataDigest = ISCSI_DIGEST_TYPE_NONE; // Need to be checked
    loginOptions.DefaultTime2Retain = 20;
    loginOptions.DefaultTime2Wait = 2;
    loginOptions.HeaderDigest =ISCSI_DIGEST_TYPE_NONE; // None should be there
    loginOptions.InformationSpecified =
        ISCSI_LOGIN_OPTIONS_DEFAULT_TIME_2_RETAIN |
        ISCSI_LOGIN_OPTIONS_AUTH_TYPE |
        ISCSI_LOGIN_OPTIONS_DATA_DIGEST |
        ISCSI_LOGIN_OPTIONS_DEFAULT_TIME_2_WAIT |
        ISCSI_LOGIN_OPTIONS_HEADER_DIGEST;

    loginOptions.LoginFlags = ISCSI_LOGIN_FLAG_MULTIPATH_ENABLED; // No effect if the target server side does not enable it
    loginOptions.MaximumConnections = 2;
    loginOptions.UsernameLength = NULL;
    loginOptions.Username = NULL;
    loginOptions.Password = NULL;
    loginOptions.PasswordLength = NULL ;

    ISCSI_TARGET_PORTAL targetPortal;
    memset(targetPortal.Address, 0, 256);
    targetPortal.Socket = NULL;
    memset(targetPortal.SymbolicName, 0, 256);
    targetPortal.Socket = targetPort;

    swprintf(targetPortal.Address, sizeof(targetPortal.Address), addr.c_str());

    // http://msdn.microsoft.com/en-us/library/windows/desktop/bb870776.aspx
    hr = AddIScsiSendTargetPortal(NULL, ISCSI_ALL_INITIATOR_PORTS, &loginOptions, NULL, &targetPortal);	
    if (S_OK == hr) {
	    TraceMsg(L"AddIScsiSendTargetPortal() succeeded");
    } else {
	    TraceMsg(L"AddIScsiSendTargetPortal() failed");
	    goto DONE;
    }

    // login specific target
    ISCSI_UNIQUE_SESSION_ID uniqueSessionId;
    uniqueSessionId.AdapterSpecific = NULL;
    uniqueSessionId.AdapterUnique = NULL;
    ISCSI_UNIQUE_CONNECTION_ID uniqueConnectionId;
    uniqueConnectionId.AdapterSpecific = NULL;
    uniqueConnectionId.AdapterUnique = NULL;

    // http://msdn.microsoft.com/en-us/library/windows/desktop/bb870820.aspx
    // TODO: check, should use targetPort instead of ISCSI_ANY_INITIATOR_PORT?
    hr = LoginIScsiTarget((PWSTR)targetName.c_str(), false, NULL, ISCSI_ANY_INITIATOR_PORT, 
	    NULL, NULL, NULL, NULL, NULL, NULL, false, &uniqueSessionId, &uniqueConnectionId);

    if (S_OK == hr) {
	    TraceMsg(L"Target login successfully");
    } else {
	    TraceMsg(L"Target login failed");
    }

DONE:
    return (S_OK == hr) ? TRUE : FALSE;
}


void CSynoProvider::SynoDeleteLun(
    const std::wstring& lunSerial
    )
{
    // issue request 'delete_lun'
    // TODO: update m_vSnapshotInfo ? need to check is this command acceptable during snapshot flows
    Json::Value jsonResponse;
    Syno::PDsInfo dsInfo = m_LunSnDsInfoList[Syno::WStringToString(lunSerial)];

    Syno::DeleteLunReq *pReq = new Syno::DeleteLunReq(
        Syno::VssCommand::s_SN++, Syno::WStringToString(m_pluginId), Syno::WStringToString(dsInfo->GetKey()));

    pReq->SetLun(Syno::WStringToString(lunSerial));

    try {
        dsInfo->SendThenReceive(Syno::PVssCommand(pReq), jsonResponse);
    }
    catch (std::system_error error) {
        //handle receive error
    }

    if (!(jsonResponse[SZ_SUCCESS].asBool())) {
        // TODO: handle execution error
    }
}


void CSynoProvider::SynoRestoreLun(
    const std::wstring& srcLunSerial,
    const std::wstring& dstLunSerial
    )
{
    // issue request 'restore_lun'
    // No snapshot set info involved during LUN resync
}