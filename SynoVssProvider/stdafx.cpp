/*--

Module Name:

    stdafx.cpp

Abstract:

    Source file that includes just the standard includes
    SynoVssProvider.pch will be the pre-compiled header
    stdafx.obj will contain the pre-compiled type information

Notes:

Revision History:

--*/

#include "stdafx.h"
