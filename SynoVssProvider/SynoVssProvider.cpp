/*--

Module Name:

    SynoVssProvider.cpp

Abstract:

    Implementation of COM DLL exports, global GUIDs and strings

Notes:

Revision History:

    10/11/2007  Update to the version string

--*/

#include "stdafx.h"
#include "SynoVssProvider.h"

// {1A805726-BF91-47B2-B0D8-8A4317CD7642}
static const GUID g_gProviderId = 
    { 0x1a805726, 0xbf91, 0x47b2, { 0xb0, 0xd8, 0x8a, 0x43, 0x17, 0xcd, 0x76, 0x42 } };
// {06f98abd-c58e-4a4d-a847-5dcbde4f05d0}
static const GUID g_gProviderVersion = 
    { 0x06f98abd, 0xc58e, 0x4a4d, { 0xa8, 0x47, 0x5d, 0xcb, 0xde, 0x4f, 0x05, 0xd0 } };

WCHAR* g_wszProviderName = L"Synology VSS HW Provider";

#ifdef _PRELONGHORN_HW_PROVIDER
static WCHAR* g_wszProviderVersion = LVER_PRODUCTVERSION_STR L" without extended hardware interface";
#else
static WCHAR* g_wszProviderVersion = LVER_PRODUCTVERSION_STR;
#endif

class CSynoVssProviderModule : public CAtlDllModuleT< CSynoVssProviderModule >
{
public :
    DECLARE_LIBID(LIBID_SynoVssProviderLib)
    DECLARE_REGISTRY_APPID_RESOURCEID(IDR_SYNOVSSPROVIDER, "{BAFB1857-FB9A-48C2-A5DB-D76F934D4E3F}")
};

CSynoVssProviderModule _AtlModule;


// DLL Entry Point
extern "C" BOOL WINAPI DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
    hInstance;
    return _AtlModule.DllMain(dwReason, lpReserved); 
}


// Used to determine whether the DLL can be unloaded by OLE
STDAPI DllCanUnloadNow(void)
{
    return _AtlModule.DllCanUnloadNow();
}


// Returns a class factory to create an object of the requested type
STDAPI DllGetClassObject(_In_ REFCLSID rclsid, _In_ REFIID riid, _Outptr_ LPVOID* ppv)
{
    return _AtlModule.DllGetClassObject(rclsid, riid, ppv);
}


// DllRegisterServer - Adds entries to the system registry
STDAPI DllRegisterServer(void)
{
    // registers object, typelib and all interfaces in typelib
    HRESULT hr = _AtlModule.DllRegisterServer();
    
    CComPtr<IVssAdmin> pVssAdmin;

    if (SUCCEEDED( hr )) {
        hr = CoCreateInstance( CLSID_VSSCoordinator,
            NULL,
            CLSCTX_ALL,
            IID_IVssAdmin,
            (void **) &pVssAdmin);
    }

    if (SUCCEEDED( hr )) {
        hr = pVssAdmin->RegisterProvider(g_gProviderId,
            CLSID_SynoProvider,
            g_wszProviderName,
            VSS_PROV_HARDWARE,
            g_wszProviderVersion,
            g_gProviderVersion );
    }
    
    pVssAdmin.Release();

    return hr;
}


// DllUnregisterServer - Removes entries from the system registry
STDAPI DllUnregisterServer(void)
{
    CComPtr<IVssAdmin> pVssAdmin;
    
    HRESULT hr = CoCreateInstance( CLSID_VSSCoordinator,
                           NULL,
                           CLSCTX_ALL,
                           IID_IVssAdmin,
                           (void **) &pVssAdmin);

    if (SUCCEEDED( hr )) {
        hr = pVssAdmin->UnregisterProvider( g_gProviderId );

        if(FAILED(hr))
        {
            TraceMsg(L"Error was returned calling UnregisterProvider, hr: %x \n", hr);
        }

    }
    
    hr = _AtlModule.DllUnregisterServer();

    pVssAdmin.Release();

    return hr;
}
