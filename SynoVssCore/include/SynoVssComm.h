
#pragma once

#ifdef _WIN32
#undef WIN32
#define WIN32
#endif

#include <map>
#include <string>
#include <synocomm.h>
#include <synocomm_base.h>

#include "SynoVssCommand.h"


namespace Syno {


class VssComm {
public:
    VssComm(
        const std::string& remoteAppName,
        const std::string& remoteAddress
        );
    ~VssComm();

    void SetRemote(const std::string& appName, const std::string& address);

    // Connect with SynoComm engine not DS yet
    void Connect();
    void Disconnect();

    void Send(const std::string& message);
    void Send(const VssRequest& command);
    bool IsAlive() const;
    int GetHandle()
    {
        return m_handle;
    }

private:
    int         m_handle;
    std::string m_remoteAppName;
    std::string m_remoteAddress;

public:     // static member functions
    static void CreateChannel(const std::string& appName, const std::string& address, const std::string& hostname);
    static void DestroyChannel();
    static bool IsChannelAvailable();
    static int Receive(std::string& message);
    static int ReceiveJson(Json::Value &json);
    //distinguish between ip and domain name, IP do nothing, transform to IP is it's a domain name
    static int DomainToIp(const std::string &dn, std::string &ip);

private:    // static member fields
    static std::string      s_appName;
    static std::string      s_localAddress;
    static std::string      s_localHostname;

    static SYNOAPPCOMM*     s_channel;
};

typedef std::shared_ptr<VssComm> PVssComm;

}
