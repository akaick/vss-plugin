#pragma once

#include <string>
#include <atomic>
#include <memory>
#include <cstdint>
#include <json/value.h>

#include "SynoUtils.h"

namespace Syno {
// command fileds
#define SZ_COMMAND                  "command"
#define SZ_COMMAND_SN               "command_sn"
#define SZ_DATA                     "data"
#define SZ_DSM_SN                   "dsm_sn"
#define SZ_ACCOUNT                  "account"
#define SZ_DESTINATION_LUN          "destination_lun"
#define SZ_ERROR                    "error"
#define SZ_KEY                      "key"
#define SZ_LUN_ID_TYPE              "type"
#define SZ_LUN_ID_TYPE_80           "80" // VPD Page 80
#define SZ_SOURCE_LUN               "source_lun"
#define SZ_SUCCESS                  "success"
#define SZ_TASK_ID                  "task_id"
#define SZ_PLUGIN_ID                "plugin_id"
#define SZ_IP                       "ip"
#define SZ_RESULT                   "result"
#define SZ_MIRRORED_LUN             "mirrored_lun"
#define SZ_TARGET_PORT              "target_port"
#define SZ_TARGET_ADDRESS_LIST      "target_addr_list"
#define SZ_TARGET_IQN               "target_iqn"
#define SZ_SOURCE_LUN_P80           "source_lun_p80"

// request commands
#define SZ_CMD_INIT_SNAPSHOT        "init_snapshot"
#define SZ_CMD_REGISTER             "register"
#define SZ_CMD_CONNECT              "connect"
#define SZ_CMD_DISCONNECT           "disconnect"
#define SZ_CMD_UNREGISTER           "unregister"
#define SZ_CMD_IS_LUN_SUPPORTED     "is_lun_supported"
#define SZ_CMD_START_MIRROR         "start_mirror"
#define SZ_CMD_IS_MIRROR_DONE       "is_mirror_done"
#define SZ_CMD_DEPART_RELATION      "depart_relation"
#define SZ_CMD_ABORT_TASK           "abort_task"
#define SZ_CMD_GET_MIRRORED_LUN     "get_mirrored_lun"
#define SZ_CMD_CREATE_MAPPED_TARGET "create_mapped_target"
#define SZ_CMD_DELETE_LUN           "delete_lun"
#define SZ_CMD_RESTORE_LUN          "restore_lun"
#define SZ_CMD_UPDATE_STATUS        "update_connect_status"
#define SZ_CMD_RECONNECT            "reconnect"
#define SZ_CMD_KILL_CONNECTION      "kill_connection"
#define SZ_CMD_ASK_STARTER          "ask_starter"

// command attributes
#define COMMAND_TIMEOUT             15

// error codes
    enum {
        ERR_CMD_SUCCESS,
        ERR_CMD_TIMEOUT,
        ERR_DSM_UNAVAILABLE,
        ERR_DSM_NOTFOUND,
        ERR_DSM_REGISTERED
    };

/*
 * VssCommand
 */
class VssCommand {
public:
    VssCommand()
        : m_data(Json::objectValue)
    {
    }

    virtual ~VssCommand() {}

    void SetCommandSn(const std::uint32_t command_sn)
    {
        m_command_sn = command_sn;
    }

    std::uint32_t GetCommandSN() const
    {
        return m_command_sn;
    }

    std::string GetCommandName()
    {
        return m_command;
    }

    void SetDsmSn(const std::string& sn)
    {
        m_dsm_sn = sn;
    }

    std::string GetDsmSn()
    {
        return m_dsm_sn;
    }

    template<typename value_t>
    void SetData(const std::string& key, const value_t& value)
    {
        m_data[key] = value;
    }

    void SetData(const Json::Value& json)
    {
        m_data = json;
    }

    Json::Value GetData(const std::string& key) const
    {
        return m_data[key];
    }

    virtual Json::Value ToJson() const = 0;

public:
    static std::atomic_uint s_SN;

protected:
    // leave empty command name in response
    std::string     m_command;

    // serial number of each request/response pair
    std::uint32_t   m_command_sn;

    // Serial number of DSM
    std::string     m_dsm_sn;

    // command specified data
    Json::Value     m_data;
};

typedef std::shared_ptr<VssCommand> PVssCommand;

Json::Value ParseJson(const std::string& document);
PVssCommand CreateCommand(const std::string& document);
PVssCommand CreateCommand(const Json::Value& json);
void ToErrorResponse(Json::Value &jsonResponse, std::uint32_t errorCode);

/*
 * VssRequest
 */
class VssRequest : public VssCommand {
public:
    VssRequest(const std::string& command)
    {
        m_command       = command;
    }

    virtual ~VssRequest() {}

    void SetPluginId(const std::string& plugin_id)
    {
        m_plugin_id = plugin_id;
    }

    std::string GetPluginId()
    {
        return m_plugin_id;
    }

    void SetKey(const std::string& key)
    {
        m_key = key;
    }

    std::string GetKey()
    {
        return m_key;
    }

    void ToInnerCommand() {
        SetCommandSn(0);
        SetKey("");
        SetPluginId("");
    }

    virtual Json::Value ToJson() const {
        Json::Value json(Json::objectValue);

        // header
        json[SZ_COMMAND]    = m_command;
        json[SZ_COMMAND_SN] = m_command_sn;
        json[SZ_PLUGIN_ID]  = m_plugin_id;
        json[SZ_KEY]        = m_key;
        json[SZ_DSM_SN]     = m_dsm_sn;

        // data
        json[SZ_DATA]       = m_data;

        return json;
    }

    std::string ToString() const {
        return ToJson().toStyledString();
    }

protected:
    // TODO: validator for each type

private:
    // unique identified to present vss/vmware plugin
    std::string     m_plugin_id;

    // all requests from plugin except ��register�� should carry it.
    // DSM generates it using hash function with logged-in data (account + password + plugin_id)
    std::string     m_key;
};

/*
 * VssResponse
 */
class VssResponse : public VssCommand  {
public:
    VssResponse(
        const std::uint32_t command_sn,
        bool success,
        std::uint32_t error,
        std::string dsmSn
        )
    {
        m_command       = "";
        m_command_sn    = command_sn;
        m_success       = success;
        m_error         = error;
        m_dsm_sn        = dsmSn;
    }

    VssResponse(const Json::Value& json)
    {
        m_command       = "";
        m_command_sn    = json[SZ_COMMAND_SN].asUInt();
        m_success       = json[SZ_SUCCESS].asBool();
        m_error         = json[SZ_ERROR].asUInt();
        m_dsm_sn        = json[SZ_DSM_SN].asString();

        if (json.isMember(SZ_DATA)) {
            m_data = json[SZ_DATA];
        }
    }

    virtual ~VssResponse() {}

    bool GetSuccess()
    {
        return m_success;
    }

    std::uint32_t GetError()
    {
        return m_error;
    }

    virtual Json::Value ToJson() const {
        Json::Value json(Json::objectValue);

        // header
        json[SZ_COMMAND_SN] = m_command_sn;
        json[SZ_SUCCESS]    = m_success;
        json[SZ_ERROR]      = m_error;
        json[SZ_DSM_SN]     = m_dsm_sn;

        // data
        json[SZ_DATA]       = m_data;

        return json;
    }

    std::string ToString() const {
        return ToJson().toStyledString();
    }

private:
    bool            m_success;

    std::uint32_t   m_error;
};

typedef std::shared_ptr<VssRequest> PVssRequest;
typedef std::shared_ptr<VssResponse> PVssResponse;
// ----------------------------

class InitSnapReq : public VssRequest {
public:
    InitSnapReq(const Json::Value& json)
        : VssRequest(SZ_CMD_INIT_SNAPSHOT)
    {
        SetCommandSn(json[SZ_COMMAND_SN].asUInt());
        SetData(json[SZ_DATA]);

        SetDsmSn(json[SZ_DSM_SN].asString());
        SetSourceLun(json[SZ_SOURCE_LUN_P80].asString());
    }

    ~InitSnapReq() {}

    void SetSourceLun(const std::string& lunSerial)
    {
        SetData(SZ_LUN_ID_TYPE, SZ_LUN_ID_TYPE_80);
        SetData(SZ_SOURCE_LUN, lunSerial);
    }
};

class RegisterReq : public VssRequest {
public:
    RegisterReq(
        const std::uint32_t command_sn,
        const std::string& plugin_id
        ) : VssRequest(SZ_CMD_REGISTER)
    {
        SetCommandSn(command_sn);
        SetPluginId(plugin_id);
        // ignore KEY in register request
    };

    ~RegisterReq() {}

    void SetAccount(const std::string& account)
    {
        SetData(SZ_ACCOUNT, account);
    }

    void SetPassword(const std::string& password)
    {
        SetData("password", password);
    }

    void SetRemoteAddress(const std::string& remoteAddress)
    {
        m_remote_address = remoteAddress;
    }

    std::string GetRemoteAddress()
    {
        return m_remote_address;
    }

    std::string GetAccount()
    {
        return GetData(SZ_ACCOUNT).asCString();
    }

    virtual Json::Value ToJson() const {
        Json::Value json(Json::objectValue);

        json = VssRequest::ToJson();
        json[SZ_IP] = m_remote_address;

        return json;
    }

private:
    std::string m_remote_address;
};

class UnregisterReq : public VssRequest {
public:
    UnregisterReq(
        const std::uint32_t command_sn,
        const std::string& plugin_id,
        const std::string& key
        ) : VssRequest(SZ_CMD_UNREGISTER)
    {
        SetCommandSn(command_sn);
        SetPluginId(plugin_id);
        SetKey(key);
    }

    ~UnregisterReq() {}
};

class ConnectReq : public VssRequest {
public:
    ConnectReq(
        const std::uint32_t command_sn,
        const std::wstring& plugin_id,
        const std::wstring& key
        ) : VssRequest(SZ_CMD_CONNECT)
    {
        SetCommandSn(command_sn);
        SetPluginId(WStringToString(plugin_id));
        SetKey(WStringToString(key));
    }

    ~ConnectReq() {}

    void SetAccount(const std::string& account)
    {
        SetData(SZ_ACCOUNT, account);
    }

    void SetOsType(const std::string& osType) {
        SetData("os_type", osType);
    }

    void SetOsVersion(const std::string& osVersion) {
        SetData("os_version", osVersion);
    }

    void SetProtocolVersion(const std::string& protoVersion) {
        SetData("protocol_version", "1.0"); // TODO
    }

    void AddAddress(const std::string& address) {
        GetData("addr_list").append(address);   // ??
    }
};

class DisconnectReq : public VssRequest {
public:
    // FIXME: parameter types?
    DisconnectReq(
        const std::uint32_t command_sn,
        const std::string& plugin_id,
        const std::string& key
        ) : VssRequest(SZ_CMD_DISCONNECT)
    {
        SetCommandSn(command_sn);
        SetPluginId(plugin_id);
        SetKey(key);
    }

    ~DisconnectReq() {}
};

class IsLunSupportedReq : public VssRequest {
public:
    IsLunSupportedReq(
        const std::uint32_t command_sn,
        const std::wstring& plugin_id,
        const std::wstring& key,
        const std::string& lunSerial
        ) : VssRequest(SZ_CMD_IS_LUN_SUPPORTED)
    {
        SetCommandSn(command_sn);
        SetPluginId(WStringToString(plugin_id));
        SetKey(WStringToString(key));
        SetSourceLun(lunSerial);
    }

    ~IsLunSupportedReq() {}

    void SetSourceLun(const std::string& lunSerial)
    {
        SetData(SZ_LUN_ID_TYPE, SZ_LUN_ID_TYPE_80);
        SetData(SZ_SOURCE_LUN, lunSerial);
    }
};

class StartMirrorReq : public VssRequest {
public:
    StartMirrorReq(
        const std::uint32_t command_sn,
        const std::string& plugin_id,
        const std::string& key
        ) : VssRequest(SZ_CMD_START_MIRROR)
    {
        SetCommandSn(command_sn);
        SetPluginId(plugin_id);
        SetKey(key);
    }

    ~StartMirrorReq() {}

    void SetTaskId(const std::string& taskId)
    {
        SetData(SZ_TASK_ID, taskId);
    }

    void SetSourceLun(const std::string& lunSerial)
    {
        SetData(SZ_LUN_ID_TYPE, SZ_LUN_ID_TYPE_80);
        SetData(SZ_SOURCE_LUN, lunSerial);
    }
};

class IsMirrorDoneReq : public VssRequest {
public:
    IsMirrorDoneReq(
        const std::uint32_t command_sn,
        const std::string& plugin_id,
        const std::string& key
        ) : VssRequest(SZ_CMD_IS_MIRROR_DONE)
    {
        SetCommandSn(command_sn);
        SetPluginId(plugin_id);
        SetKey(key);
    }

    ~IsMirrorDoneReq() {}

    void SetTaskId(const std::string& taskId)
    {
        SetData(SZ_TASK_ID, taskId);
    }

    void SetSourceLun(const std::string& lunSerial)
    {
        SetData(SZ_LUN_ID_TYPE, SZ_LUN_ID_TYPE_80);
        SetData(SZ_SOURCE_LUN, lunSerial);
    }
};

class DepartRelationReq : public VssRequest {
public:
    DepartRelationReq(
        const std::uint32_t command_sn,
        const std::string& plugin_id,
        const std::string& key
        ) : VssRequest(SZ_CMD_DEPART_RELATION)
    {
        SetCommandSn(command_sn);
        SetPluginId(plugin_id);
        SetKey(key);
    }

    ~DepartRelationReq() {}

    void SetTaskId(const std::string& taskId)
    {
        SetData(SZ_TASK_ID, taskId);
    }

    void SetSourceLun(const std::string& lunSerial)
    {
        SetData(SZ_LUN_ID_TYPE, SZ_LUN_ID_TYPE_80);
        SetData(SZ_SOURCE_LUN, lunSerial);
    }
};

class AbortTaskReq : VssRequest {
public:
    AbortTaskReq(
        const std::uint32_t command_sn,
        const std::string& plugin_id,
        const std::string& key
        ) : VssRequest(SZ_CMD_ABORT_TASK)
    {
        SetCommandSn(command_sn);
        SetPluginId(plugin_id);
        SetKey(key);
    }

    ~AbortTaskReq() {}

    void SetTaskId(const std::string& taskId)
    {
        SetData(SZ_TASK_ID, taskId);
    }
};

class GetMirroredLunReq : public VssRequest {
public:
    GetMirroredLunReq(
        const std::uint32_t command_sn,
        const std::string& plugin_id,
        const std::string& key
        ) : VssRequest(SZ_CMD_GET_MIRRORED_LUN)
    {
        SetCommandSn(command_sn);
        SetPluginId(plugin_id);
        SetKey(key);
    }

    ~GetMirroredLunReq() {}

    void SetSourceLun(const std::string& lunSerial)
    {
        SetData(SZ_LUN_ID_TYPE, SZ_LUN_ID_TYPE_80);
        SetData(SZ_SOURCE_LUN, lunSerial);
    }
};

class CreateMappaedTargetReq : public VssRequest {
public:
    CreateMappaedTargetReq(
        const std::uint32_t command_sn,
        const std::string& plugin_id,
        const std::string& key
        ) : VssRequest(SZ_CMD_CREATE_MAPPED_TARGET)
    {
        SetCommandSn(command_sn);
        SetPluginId(plugin_id);
        SetKey(key);
    }

    ~CreateMappaedTargetReq() {}

    void SetLun(const std::string& lunSerial)
    {
        SetData(SZ_LUN_ID_TYPE, SZ_LUN_ID_TYPE_80);
        SetData("lun", lunSerial);
    }

    void SetInitiatorIQN(const std::string& iqn)
    {
        SetData("initiator_iqn", iqn);
    }
};

class DeleteLunReq : public VssRequest {
public:
    DeleteLunReq(
        const std::uint32_t command_sn,
        const std::string& plugin_id,
        const std::string& key
        ) : VssRequest(SZ_CMD_DELETE_LUN)
    {
        SetCommandSn(command_sn);
        SetPluginId(plugin_id);
        SetKey(key);
    }

    ~DeleteLunReq() {}

    void SetLun(const std::string& lunSerial)
    {
        SetData(SZ_LUN_ID_TYPE, SZ_LUN_ID_TYPE_80);
        SetData("lun", lunSerial);
    }
};

class RestoreLunReq : public VssRequest {
public:
    RestoreLunReq(
        const std::uint32_t command_sn,
        const std::string& plugin_id,
        const std::string& key
        ) : VssRequest(SZ_CMD_RESTORE_LUN)
    {
        SetCommandSn(command_sn);
        SetPluginId(plugin_id);
        SetKey(key);
    }

    ~RestoreLunReq() {}

    void SetSourceLun(const std::string& lunSerial)
    {
        SetData(SZ_LUN_ID_TYPE, SZ_LUN_ID_TYPE_80);
        SetData(SZ_SOURCE_LUN, lunSerial);
    }

    void SetDistinationLun(const std::string& lunSerial)
    {
        SetData(SZ_LUN_ID_TYPE, SZ_LUN_ID_TYPE_80);
        SetData(SZ_DESTINATION_LUN, lunSerial);
    }
};
}
