
#pragma once

#include <array>
#include <vector>
#include <string>
#include <system_error>
#include <random>
#include <windows.h>
#include <guiddef.h>


namespace Syno {
inline std::wstring StringToWString(const std::string& src)
{
    return std::wstring(src.begin(), src.end());
}

inline std::string WStringToString(std::wstring src)
{
    std::vector<CHAR> chBuffer;
    int iChars = WideCharToMultiByte(CP_ACP, 0, src.c_str(), -1, NULL, 0, NULL, NULL);
    if (iChars > 0) {
        chBuffer.resize(iChars);
        WideCharToMultiByte(CP_ACP, 0, src.c_str(), -1, &chBuffer.front(), (int)chBuffer.size(), NULL, NULL);
    }
    
    return std::string(&chBuffer.front());
}

// Helper macros to print a GUID using printf-style formatting
#define STR_GUID_FMT  "%.8x-%.4x-%.4x-%.2x%.2x-%.2x%.2x%.2x%.2x%.2x%.2x"
#define LSTR_GUID_FMT L"%.8x-%.4x-%.4x-%.2x%.2x-%.2x%.2x%.2x%.2x%.2x%.2x"

#define GUID_PRINTF_ARG(X)                                  \
    (X).Data1,                                              \
    (X).Data2,                                              \
    (X).Data3,                                              \
    (X).Data4[0], (X).Data4[1], (X).Data4[2], (X).Data4[3], \
    (X).Data4[4], (X).Data4[5], (X).Data4[6], (X).Data4[7]

inline std::string GUIDToString(const GUID guid)
{
    std::array<char, 40> output;
    _snprintf_s(output.data(), output.size(), _TRUNCATE, STR_GUID_FMT, GUID_PRINTF_ARG(guid));
	return std::string(output.data());
}

inline GUID StringToGUID(const std::string& guid)
{
    // TODO
    return GUID();
}

inline std::wstring GUIDToWString(const GUID guid)
{
    std::array<wchar_t, 40> output;
    _snwprintf_s(output.data(), output.size(), _TRUNCATE, LSTR_GUID_FMT, GUID_PRINTF_ARG(guid));
	return std::wstring(output.data());
}

inline GUID WStringToGUID(const std::wstring& guid)
{
    // TODO
    return GUID();
}

inline std::string FormatString(const char* fmt, ...)
{
    char buf[BUFSIZ] = {0};

    va_list args;
    va_start(args, fmt);
    _vsnprintf_s(buf, _TRUNCATE, fmt, args);
    va_end(args);

    return buf;
}

inline std::wstring FormatWString(const wchar_t* fmt, ...)
{
    wchar_t buf[BUFSIZ] = {0};

    va_list args;
    va_start(args, fmt);
    _vsnwprintf_s(buf, _TRUNCATE, fmt, args);
    va_end(args);

    return buf;
}


/*
 * system_error::what()
 * system_error::code()
 * system_error::code()::name()
 * system_error::code()::value()
 * system_error::code()::message()
 *
 * Ref:
 * http://en.cppreference.com/w/cpp/header/system_error
 * http://stackoverflow.com/questions/15854930/how-do-you-use-stdsystem-error-with-getlasterror
 */
inline std::system_error make_system_error(
    const std::string& desc, 
    const int err = -1,
    const std::error_category& cat = std::system_category()
    )
{
    return std::system_error(err, cat, desc);
}

void GetHostname(std::string& hostname);
void GetAddrList(const std::string& hostname, std::vector<std::string>& addrList);

#define REG_MAX_KEY_LENGTH      255
#define REG_MAX_VALUE_NAME      16383

void RegValueGet(const HKEY root, const std::wstring& key, const std::wstring& name, std::wstring& value);
void RegValueSet(const HKEY root, const std::wstring& key, const std::wstring& name, const std::wstring& value);
void RegValueDelete(const HKEY root, const std::wstring& key, const std::wstring& name);
void RegKeyDelete(const HKEY root, const std::wstring& key);
void RegKeyEnum(const HKEY root, const std::wstring& key, std::vector<std::wstring>& keys);

// WARNING: only for testing (by charlie)
void MyWriteErrorLog(const char* format, ...);
void providerErrorLog(const char* format, ...);

int UniformRandomInt(const int min, const int max);
}