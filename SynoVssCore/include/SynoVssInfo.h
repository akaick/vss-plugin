
#pragma once

#include <string>
#include <vector>
#include <memory>
#include <mutex>
#include "SynoVssComm.h"
#include "SynoUtils.h"


namespace Syno {

#define VSS_PLUGIN_VERSION          "0.1"
#define VSS_PROTO_VERSION           "0.1"
#define VSS_OS_VERSION              "0.1"

#define REG_SYNO_KEY                L"SOFTWARE\\Synology"
#define REG_SYNO_VSS_KEY            REG_SYNO_KEY L"\\Snapshot Manager for Windows"
#define REG_SYNO_VSS_GROUPS_KEY     REG_SYNO_VSS_KEY L"\\Groups"   

#define DSM_APP_NAME                "ISS-SERVER"

typedef std::map<std::uint32_t, int> CommandTimeoutList;

class DsInfo {
public:
    DsInfo(const std::wstring& sn) 
        : m_comm()
    {
        DsInfo();
        SetSN(sn);
    }
    DsInfo()
        : m_comm()
    {
        m_isConnected = false;
        ToReconnecting();
    }
    ~DsInfo()
    {
    }

public:
    void SetConnectionStatus(const bool status) {
        m_isConnected = status;
    }

    bool IsConnected() const {
        return m_isConnected;
    }

    bool IsCommConnected() const {
        return m_comm->IsAlive();
    }

    void SetupCommConnection() {
        if (m_address.empty()) {
            throw make_system_error("No remote address for connection");
        }

        m_comm = PVssComm(new VssComm(DSM_APP_NAME, WStringToString(m_address)));

        m_comm->Connect();
    }

    void Reconnect() {
        if (!m_comm->IsAlive()) {
            m_comm->Connect();
        }
    }

    void Disconnect() {
        if (m_comm->IsAlive()) {
            MyWriteErrorLog("before");
            m_comm->Disconnect();
            MyWriteErrorLog("after");
        }
    }

    // basic setters/getters
public:
#pragma region Basic setters/getters
    void SetSN(const std::wstring& sn) {
        m_sn = sn;
    }

    std::wstring GetSN() {
        return m_sn;
    }

    void SetAccount(const std::wstring& account) {
        m_account = account;
    }

    std::wstring GetAccount() {
        return m_account;
    }

    void SetKey(const std::wstring& key) {
        m_key = key;
    }

    std::wstring GetKey() {
        return m_key;
    }

    void SetAddress(const std::wstring& address) {
        m_address = address;
    }

    void SetReconnectPeriod(const int period) {
        m_reconnectPeriod = period;
    }

    std::wstring GetAddress() {
        return m_address;
    }

    int GetCommHandle() {
        return m_comm->GetHandle();
    }
#pragma endregion
    void SendCommand(Syno::PVssCommand pcommand);
    void SendCommand(const Json::Value &command);
    void SendThenReceive(Syno::PVssCommand command, Json::Value &json);
    bool IsCommandTimeout(std::uint32_t commandSN);
    void DeleteCommandFromTimeout(std::uint32_t commandSN);
    void HandleCommandTimeout(int inc, std::vector<uint32_t> &timeouts);
    void InsertCommandTimeout(std::uint32_t commandSN);
    bool IsTimeToReconnect(const int dec);
    bool IsReconnecting();
    void InitiateReconnectCd();
    void ToReconnecting();
    void ToConnecting();


private:
    // This status indicates whether the DS is connected, it is differ from the result of m_comm->conenct() and m_comm->IsAlive()
    bool                m_isConnected;
    int                 m_reconnectPeriod;
    int                 m_reconnectCd;
    std::wstring        m_sn;
    std::wstring        m_account;
    std::wstring        m_key;
    std::wstring        m_address;
    std::mutex          m_mutex;
    std::mutex          m_rcMutex;
    PVssComm            m_comm;
    CommandTimeoutList  m_timeout_list;
};

typedef std::shared_ptr<DsInfo> PDsInfo;
typedef std::map<int, PDsInfo> DsInfoList;
typedef std::map<std::wstring, PDsInfo> StringDsInfoList;
typedef std::map<std::uint32_t, PDsInfo> SnDsInfoList;
typedef std::map<std::uint32_t, std::string> CommandSnList;
typedef std::map<std::string, PDsInfo> LunSnDsInfoList;

void LoadPluginId(std::wstring& pluginId);
void LoadPluginVersion(std::wstring& pluginVer);
void LoadProtoVersion(std::wstring& protoVer);
void LoadOsVersion(std::wstring& osVer);
void LoadDsInfoList(StringDsInfoList& sDsList);
void InitiateConnection(StringDsInfoList& sDsList);

}
