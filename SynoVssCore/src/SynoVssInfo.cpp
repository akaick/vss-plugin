
#include "SynoVssInfo.h"
#include "SynoUtils.h"


namespace Syno {
#pragma region Class SynoDsInfo

void DsInfo::SendCommand(Syno::PVssCommand pcommand)
{
    SendCommand(pcommand->ToJson());
}


void DsInfo::SendCommand(const Json::Value& command)
{
    InsertCommandTimeout(command[SZ_COMMAND_SN].asUInt());
    m_comm->Send(command.toStyledString());
}

void DsInfo::SendThenReceive(Syno::PVssCommand command, Json::Value &json)
{
    SendCommand(command);
    VssComm::ReceiveJson(json);
}

bool DsInfo::IsCommandTimeout(std::uint32_t commandSN)
{
    bool ret = false;

    m_mutex.lock();

    if (m_timeout_list.end() == m_timeout_list.find(commandSN)) {
        ret = true;
    }

    m_mutex.unlock();

    return ret;
}

void DsInfo::InsertCommandTimeout(std::uint32_t commandSN)
{
    m_mutex.lock();
    m_timeout_list.insert(std::pair<std::uint32_t, int>(commandSN, 0));
    m_mutex.unlock();
}

void DsInfo::DeleteCommandFromTimeout(std::uint32_t commandSN)
{
    m_mutex.lock();
    m_timeout_list.erase(commandSN);
    m_mutex.unlock();
}

void DsInfo::HandleCommandTimeout(int inc, std::vector<uint32_t> &timeouts)
{
    m_mutex.lock();

    for (CommandTimeoutList::iterator it = m_timeout_list.begin(); it != m_timeout_list.end(); ++it) {
        if (COMMAND_TIMEOUT <= (it->second += inc)) {
            timeouts.push_back(it->first);
            m_timeout_list.erase(it->first);
        }
    }

    m_mutex.unlock();
}

bool DsInfo::IsTimeToReconnect(const int dec)
{
    m_rcMutex.lock();
    bool ret = (0 >= m_reconnectCd);
    m_reconnectCd -= dec;
    m_rcMutex.unlock();

    return ret;
}

bool DsInfo::IsReconnecting()
{
    m_rcMutex.lock();
    bool ret = 0 != m_reconnectPeriod;
    m_rcMutex.unlock();
    return ret;
}

void DsInfo::InitiateReconnectCd()
{
    m_rcMutex.lock();
    m_reconnectCd = m_reconnectPeriod;
    // TODO: hard coded first
    m_reconnectPeriod = UniformRandomInt(10, 30);
    //TEMP
    MyWriteErrorLog("reconnect in %d sec(s)", m_reconnectCd);
    m_rcMutex.unlock();
}

void DsInfo::ToReconnecting()
{
    m_rcMutex.lock();
    // TODO: magic number first
    m_reconnectPeriod = 10;
    m_rcMutex.unlock();
    InitiateReconnectCd();
    SetConnectionStatus(false);
}

void DsInfo::ToConnecting()
{
    m_rcMutex.lock();
    m_reconnectPeriod = m_reconnectCd = 0;
    m_rcMutex.unlock();
}

#pragma endregion


#pragma region VSS info helper functions
void LoadPluginId(std::wstring& pluginId )
{
    try {
        RegValueGet(HKEY_LOCAL_MACHINE, REG_SYNO_VSS_KEY, L"PluginId", pluginId);
    }
    catch(std::system_error err) {
        // Generate a new plugin ID if nothing found
        if (ERROR_FILE_NOT_FOUND == err.code().value()) {
            GUID guid;
            CoCreateGuid(&guid);
            pluginId = GUIDToWString(guid);
            RegValueSet(HKEY_LOCAL_MACHINE, REG_SYNO_VSS_KEY, L"PluginId", pluginId);
        }
    }
}

void LoadPluginVersion(std::wstring& pluginVer)
{
    try {
        RegValueGet(HKEY_LOCAL_MACHINE, REG_SYNO_VSS_KEY, L"PluginVersion", pluginVer);
    }
    catch(std::system_error err) {
        // use default plugin version
        pluginVer = TEXT(VSS_PLUGIN_VERSION);
    }
}

void LoadProtoVersion(std::wstring& protoVer)
{
    try {
        RegValueGet(HKEY_LOCAL_MACHINE, REG_SYNO_VSS_KEY, L"ProtoVersion", protoVer);
    }
    catch(std::system_error err) {
        // user default protocol version
        protoVer = TEXT(VSS_PROTO_VERSION);
    }
}

void LoadOsVersion(std::wstring& osVer)
{
    wchar_t szOsVer[32] = {0};
    OSVERSIONINFO osvi;

    try {
        ZeroMemory(&osvi, sizeof(OSVERSIONINFO));
        osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
        osvi.dwMajorVersion;
        swprintf_s(szOsVer, L"%d.%d", (int)osvi.dwMajorVersion, (int)osvi.dwMinorVersion);
    }
    catch(std::system_error err) {
        // user default protocol version
        osVer = TEXT(VSS_OS_VERSION);
    }
}

void LoadDsInfoList(StringDsInfoList& sDsList)
{
    std::wstring value;
    std::vector<std::wstring> dsSerials;

    try {
        RegKeyEnum(HKEY_LOCAL_MACHINE, REG_SYNO_VSS_GROUPS_KEY, dsSerials);
    }
    catch (std::system_error error) {
        MyWriteErrorLog(error.what());
    }

    for (const auto i : dsSerials) {
            PDsInfo dsInfo(new DsInfo(i));

            RegValueGet(HKEY_LOCAL_MACHINE, REG_SYNO_VSS_GROUPS_KEY L"\\" + i, L"Account", value);
            dsInfo->SetAccount(value);
            RegValueGet(HKEY_LOCAL_MACHINE, REG_SYNO_VSS_GROUPS_KEY L"\\" + i, L"Key", value);
            dsInfo->SetKey(value);
            RegValueGet(HKEY_LOCAL_MACHINE, REG_SYNO_VSS_GROUPS_KEY L"\\" + i, L"Address", value);
            dsInfo->SetAddress(value);

            sDsList.insert(std::pair<std::wstring, PDsInfo>(dsInfo->GetSN(), dsInfo));
    }
}

void InitiateConnection(StringDsInfoList& sDsList)
{
    bool failure = false;
    std::string errorDesciption;

    for (const auto ds : sDsList) {
        try {
            ds.second->SetupCommConnection();
        }
        catch (std::system_error error) {
            ds.second->ToReconnecting();
            errorDesciption = errorDesciption + "DS(" + WStringToString(ds.second->GetAddress()) 
                + ") init connection failed with error:" + error.what() + " "; 
            failure = true;
            continue;
        }

        ds.second->ToConnecting();
    }

    if (failure) {
        throw make_system_error(errorDesciption);
    }
}

#pragma endregion VSS info helper functions

}
