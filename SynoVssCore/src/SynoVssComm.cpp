#include <algorithm>
#include "SynoVssComm.h"
#include "SynoUtils.h"


namespace Syno {

std::string  VssComm::s_appName;
std::string  VssComm::s_localAddress;
std::string  VssComm::s_localHostname;
SYNOAPPCOMM* VssComm::s_channel = NULL;

void VssComm::CreateChannel(const std::string& appName, const std::string& address, const std::string& hostname)
{
    if (s_channel) {
        goto END;
    }

    s_appName       = appName;
    s_localAddress  = address;
    s_localHostname = hostname;

    if (s_appName.empty() || s_localAddress.empty() || s_localHostname.empty()) {
        throw make_system_error("Arguments for communicatio channel are not available.");
    }

    s_channel = ::CreateSynoComm(s_localAddress.c_str(), s_localHostname.c_str(), s_appName.c_str());

    if (NULL == s_channel) {
        throw make_system_error("Failed to create communication channel.");
    }

END:
    return;
}

void VssComm::DestroyChannel()
{
    if (NULL == s_channel) {
        goto END;
    }

    // TODO: release m_synocomm
    int ret = ::DestroySynoComm(s_channel);

    if (0 > s_channel) {
        throw make_system_error("Failed to destroy communication channel.");
    }

    s_channel = NULL;
END:
    return;
}

bool VssComm::IsChannelAvailable()
{
    return NULL != s_channel;
}

int VssComm::Receive(std::string& message)
{
    int ret = -1;

    if (NULL == s_channel) {
        throw make_system_error("Communication channel is not available.");
    }

    // TODO: timeout mechanism
    P_SYNOCOMM_ENVELOPE pEvlp = CreateSynoCommEvlp();

    int len = CommRecvEvlp(s_channel, pEvlp);

    if (0 > len) {
        throw make_system_error("Failed to receive message");
    } else if (0 <= len) {
        message = CommGetEvlpData(pEvlp);
        ret = CommGetEvlpFD(s_channel, pEvlp);
    }

    return ret;
}

int VssComm::ReceiveJson(Json::Value &json)
{
    int ret = -1;
    std::string receive;

    ret = Receive(receive);

    try {
        json = ParseJson(receive);
    }
    catch (std::system_error error) {
        throw make_system_error("Unable to parse received msg");
    }

    return ret;
}


//////////////////////////


VssComm::VssComm(
    const std::string& remoteAppName,
    const std::string& remoteAddress
    )
{
    m_handle = -1;
    SetRemote(remoteAppName, remoteAddress);
}

VssComm::~VssComm()
{
}

void VssComm::SetRemote(const std::string& appName, const std::string& address)
{
    m_remoteAppName = appName;
    m_remoteAddress = address;
}

void VssComm::Connect()
{
    if (NULL == s_channel) {
        throw make_system_error("Communication channel is not available.");
    }

    if (m_remoteAddress.empty() || m_remoteAppName.empty()) {
        throw make_system_error("Remote information isn't available");
    }

    // transform to ipv4 address
    std::string ip;

    if (0 > DomainToIp(m_remoteAddress, ip)) {
        throw make_system_error(FormatString("Cannot get ip addr from domain name:%s", m_remoteAddress.c_str()));
    }

    m_remoteAddress = ip;

    int handle = SynoCommConnect(s_channel, m_remoteAddress.c_str(), m_remoteAppName.c_str());

    if (-1 == handle) {
        throw make_system_error(FormatString("Failed to connect remote host %s (%s)", m_remoteAddress, m_remoteAppName));
    }

    m_handle = handle;

    return;
}

void VssComm::Disconnect()
{
    if (0 > SynoCommDisconnect(s_channel, m_handle)) {
        throw make_system_error(FormatString("Failed to disconnect from remote host"));
    }
}

void VssComm::Send(const std::string& message)
{
    if (NULL == s_channel) {
        throw make_system_error("Communication channel is not available.");
    }

    if (-1 == m_handle) {
        Connect();
    }

    int ret = CommSendMessage(s_channel, m_handle, message.c_str(), (int)message.size());

    // The returned ret is qeual to bytes sent, and it should be larger than 0
    if (0 >= ret) {
        throw make_system_error("Failed to send message", ret);
    }
}

void VssComm::Send(const VssRequest& command)
{
    Send(command.ToString());
}

bool VssComm::IsAlive() const
{
    // TODO: return code defined in synocomm_message.h change to MACRO later
    return 2 == (SynoCommIsConnect(s_channel, m_remoteAddress.c_str(), m_remoteAppName.c_str()) & (0x00000002));
}

int VssComm::DomainToIp(const std::string &dn, std::string &ip)
{
    int ret = -1;
    WSADATA wsaData;
    struct addrinfo *result = NULL;
    struct addrinfo hints;
    IN_ADDR buf;

    if (1 == InetPton(AF_INET, StringToWString(dn).c_str(), &buf)) {
        ip = dn;
        ret = 0;
        goto END;
    }

    if (0 != WSAStartup(MAKEWORD(2, 2), &wsaData)) {
        ip = dn;
        goto END;
    }

    ZeroMemory( &hints, sizeof(hints) );
    hints.ai_family = AF_UNSPEC;
    hints.ai_protocol = IPPROTO_TCP;

    if (0 != getaddrinfo(dn.c_str(), NULL, &hints, &result)) {
        goto CLEANUP;
    }

    if (AF_INET != result->ai_family) {
        goto CLEANUP;
    }

    struct sockaddr_in  *sockaddr_ipv4 = (struct sockaddr_in *) result->ai_addr;

    ip = inet_ntoa(sockaddr_ipv4->sin_addr);

    MyWriteErrorLog("ip:%s", ip.c_str());

    ret = 0;

CLEANUP:
    WSACleanup();
END:
    return ret;
}
}