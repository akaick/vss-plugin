#include "SynoUtils.h"
#include <mutex>

//#define MYLOG

#ifdef MYLOG
std::mutex testMutex;
#endif

namespace Syno {


void GetHostname(std::string& hostname)
{
    WSAData wsaData;
    if (0 != WSAStartup(MAKEWORD(2, 2), &wsaData)) {
        // TODO: error handling
        goto END;
    }

    char szHostname[BUFSIZ] = {0};
    if (SOCKET_ERROR == ::gethostname(szHostname, sizeof(szHostname))) {
        // TODO: error handling with WSAGetLastError()
        goto END;
    }

    hostname = szHostname;

END:
    WSACleanup();
}

void GetAddrList(const std::string& hostname, std::vector<std::string>& addrList)
{
    WSAData wsaData;
    if (0 != WSAStartup(MAKEWORD(2, 2), &wsaData)) {
        // TODO: error handling
        goto END;
    }

    struct hostent* pHost = ::gethostbyname(hostname.c_str());
    if (NULL == pHost) {
        DWORD dwError = WSAGetLastError();
        if (WSAHOST_NOT_FOUND == dwError) {
            // "Host not found"
        } else if (WSANO_DATA == dwError) {
            // "No data record found"
        } else {
            // dwError
        }
        goto END;
    }

    for (size_t i = 0; pHost->h_addr_list[i] != 0; ++i) {
        struct sockaddr_in socketAddress;
        memcpy(&socketAddress.sin_addr, pHost->h_addr_list[i], pHost->h_length);
        addrList.push_back(inet_ntoa(socketAddress.sin_addr));
    }

END:
    WSACleanup();
}

void RegValueGet(
    const HKEY root,
    const std::wstring& key,
    const std::wstring& name,
    std::wstring& value
    )
{
    LONG ret;
    HKEY hKey;

    ret = RegOpenKeyEx(root, key.c_str(), 0, KEY_READ, &hKey);
    if (ERROR_SUCCESS != ret) {
        throw make_system_error("Failed to open registry key", ret);
    }

    DWORD type, cbData;
    ret = RegQueryValueEx(hKey, name.c_str(), NULL, &type, NULL, &cbData);
    if (ERROR_SUCCESS != ret) {
        RegCloseKey(hKey);
        throw make_system_error("Could not read registry value", ret);
    }

    if (REG_SZ != type) {
        RegCloseKey(hKey);
        throw make_system_error("Incorrect registry value type", ret);
    }

    value = std::wstring(cbData/sizeof(wchar_t), L'\0');
    ret = RegQueryValueEx(hKey, name.c_str(), NULL, NULL, reinterpret_cast<LPBYTE>(&value[0]), &cbData);
    if (ERROR_SUCCESS != ret) {
        RegCloseKey(hKey);
        throw make_system_error("Failed to read registry value", ret);
    }

    RegCloseKey(hKey);

    size_t firstNull = value.find_first_of(L'\0');
    if (firstNull != std::wstring::npos) {
        value.resize(firstNull);
    }
}

void RegValueSet(
    const HKEY root,
    const std::wstring& key,
    const std::wstring& name,
    const std::wstring& value
    )
{
    LONG ret;
    HKEY hKey;

    // if the key already exists, the function opens it
    ret = RegCreateKeyEx(root, key.c_str(), 0, NULL, 0, KEY_ALL_ACCESS, NULL, &hKey, NULL);
    if (ERROR_SUCCESS != ret) {
        throw make_system_error("Failed to open registry key", ret);
    }

    ret = RegSetValueEx(hKey, name.c_str(), 0, REG_SZ, reinterpret_cast<const BYTE*>(&value[0]), (value.size() + 1) * sizeof(WCHAR));
    if (ERROR_SUCCESS != ret) {
        RegCloseKey(hKey);
        throw make_system_error("Failed to set registry value", ret);
    }

    RegCloseKey(hKey);
}

void RegValueDelete(
    const HKEY root,
    const std::wstring& key,
    const std::wstring& name
    )
{
    LONG ret;
    HKEY hKey;

    ret = RegOpenKeyEx(root, key.c_str(), 0, KEY_WRITE, &hKey);
    if (ERROR_SUCCESS != ret) {
        throw make_system_error("Failed to open registry key", ret);
    }

    ret = RegDeleteValue(hKey, name.c_str());
    if (ERROR_SUCCESS != ret) {
        RegCloseKey(hKey);
        throw make_system_error("Failed to delete value", ret);
    }

    RegCloseKey(hKey);
}

void RegKeyDelete(
    const HKEY root,
    const std::wstring& key
    )
{
    LONG ret;

    ret = RegDeleteKeyEx(root, key.c_str(), KEY_WRITE, 0);
    if (ERROR_SUCCESS != ret) {
        throw make_system_error("Failed to delete key", ret);
    }
}

void RegKeyEnum(const HKEY root, const std::wstring& key, std::vector<std::wstring>& keys)
{
    LONG ret;
    HKEY hKey;

    ret = RegOpenKeyEx(root, key.c_str(), 0, KEY_READ, &hKey);
    if (ERROR_SUCCESS != ret) {
        throw make_system_error("Failed to open registry key", ret);
    }

    for (DWORD i = 0; ERROR_SUCCESS == ret; ++i ) {
        TCHAR achKey[REG_MAX_KEY_LENGTH];
        DWORD cbName = REG_MAX_KEY_LENGTH;
        ret = RegEnumKeyEx(hKey, i, achKey, &cbName, NULL, NULL, NULL, NULL);
        if (ERROR_SUCCESS == ret) {
            keys.push_back(achKey);
        }
    }

    RegCloseKey(hKey);
}

// WARNING: only for testing (by charlie)
void MyWriteErrorLog(const char* format, ...)
{
#ifdef MYLOG
    testMutex.lock();
    FILE *fd;
    char buffer[1024*16];
    fopen_s(&fd, "c:\\error.log", "a");

    va_list args;
    va_start(args, format);
    vsprintf_s(buffer, format, args);
    fprintf(fd, "%s\n", buffer);
    va_end(args);
    fclose(fd);
    testMutex.unlock();
#endif
}

void providerErrorLog(const char* format, ...)
{
#ifdef MYLOG
    FILE *fd;
    char buffer[1024*16];
    fopen_s(&fd, "c:\\provider.log", "a");

    va_list args;
    va_start(args, format);
    vsprintf_s(buffer, format, args);
    fprintf(fd, "%s\n", buffer);
    va_end(args);
    fclose(fd);
#endif
}

int UniformRandomInt(const int min, const int max)
{
    static std::random_device rd;
    static std::default_random_engine e1(rd());
    static std::uniform_int_distribution<int> uniform_dist(min, max);

    return uniform_dist(e1);
}
}