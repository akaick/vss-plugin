
#include <json/reader.h>
#include "SynoVssCommand.h"


namespace Syno {


Json::Value
ParseJson(const std::string& document)
{
    Json::Value json;

    if (!Json::Reader().parse(document, json, false)) {
        throw make_system_error("Failed to parse response to json format");
    }

    return json;
}


PVssCommand
CreateCommand(const std::string& document)
{
    return CreateCommand(ParseJson(document));
}


// simple factory of VssCommand
PVssCommand
CreateCommand(const Json::Value& json)
{
    VssCommand* command = nullptr;

    // Response has no field "command"
    if (!json.isMember(SZ_COMMAND) || json[SZ_COMMAND].asString().empty()) {
        //command = new VssResponse(json);
    } 
    // Should be request. Now only init_snapshot is received.
    else {
        std::string command_name = json[SZ_COMMAND].asString();

        if (SZ_CMD_INIT_SNAPSHOT == command_name) {
            command = new InitSnapReq(json);
        } else {
            throw make_system_error(FormatString("Failed to create command %s from json", command_name.c_str()));
        }
    }

    return PVssCommand(command);
}

void ToErrorResponse(Json::Value &jsonResponse, std::uint32_t errorCode)
{
    jsonResponse[SZ_SUCCESS] = false;
    jsonResponse[SZ_ERROR] = errorCode;
}

std::atomic_uint VssCommand::s_SN;

}